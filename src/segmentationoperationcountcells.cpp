/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationcountcells.h"

// OpenCV
#include "opencv2/core/core.hpp"

// Project
#include "exceptionbase.h"
#include "imageprocessingtools.h"
#include "jobsegmentation.h"
#include "logging.h"
#include "tools.h"

// Static definitions
QSet<int> SegmentationOperationCountCells::m_timePointsWithCellCounts;
QMutex SegmentationOperationCountCells::m_timePointsWithCellCountsMutex;
QHash<int, QHash<int, int>> SegmentationOperationCountCells::m_cellCounts; 
QMutex SegmentationOperationCountCells::m_cellCountsMutex;


void SegmentationOperationCountCells::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	// Check type 
	if(*imageDataType != IDT_UInt8Binary && *imageDataType != IDT_UInt32LabelMap)
		throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));
	
	// Convert image (makes no deep copy)
	cv::Mat cvImage(sizeY, sizeX, CV_8UC1, *imageData);

	// Count blobs
	std::vector<Blob> blobs;
	if(*imageDataType == IDT_UInt8Binary)
		blobs = jobSegmentation->getImageProcessingTools()->convertBinaryMatToBlobs(cvImage, 255, jobSegmentation->getPixelConnectivity());
	else
		blobs = jobSegmentation->getImageProcessingTools()->convertUInt32LabelMapToBlobs(reinterpret_cast<unsigned int*>(*imageData), sizeX, sizeY);

	// Get time point and position
	//QString imgName = jobSegmentation->getImageName();
	//int tp = Tools::getTpNumberFromFileName(imgName);
	//int pos = Tools::getPosNumberFromDirName(imgName);
	PictureIndex pi = jobSegmentation->getPictureIndex();
	int tp = pi.timePoint;
	int pos = pi.positionNumber;

	// Synchronize and save results
	QMutexLocker lock1(&m_timePointsWithCellCountsMutex);
	QMutexLocker lock2(&m_cellCountsMutex);
	m_timePointsWithCellCounts.insert(tp);
	m_cellCounts[pos][tp] = blobs.size();
}

bool SegmentationOperationCountCells::exportCellCounts(const QString& fileName)
{
	// Synchronize
	QMutexLocker lock1(&m_timePointsWithCellCountsMutex);
	QMutexLocker lock2(&m_cellCountsMutex);

	// Sort time points and positions
	QList<int> timePoints = m_timePointsWithCellCounts.toList();
	qSort(timePoints);
	QList<int> positions = m_cellCounts.keys();
	qSort(positions);

	// Create file
	QFile file(fileName);
	if(!file.open(QIODevice::WriteOnly)) 
		return false;
	QTextStream s(&file);

	// Write header
	s << "TimePoint";
	for(auto itPos = positions.begin(); itPos != positions.end(); ++itPos) {
		s << "; Position" << *itPos;
	}

	// Export data
	for(auto itTP = timePoints.begin(); itTP != timePoints.end(); ++itTP) {
		int tp = *itTP;
		s << "\r\n" << tp;
		for(auto itPos = positions.begin(); itPos != positions.end(); ++itPos) {
			int pos = *itPos;
			int count = m_cellCounts[pos].value(tp, 0);
			s << "; " << count;
		}
	}
	return true;
}
