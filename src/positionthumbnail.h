/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONTHUMBNAIL_H
#define POSITIONTHUMBNAIL_H

// Qt
#include <QGraphicsPixmapItem>
#include <QImage>

// Forward declarations
class PositionDisplay;
class Experiment;


/**
	@author Bernhard
	
	This class represents a thumbnail of an experiment position and is used in PositionDisplay to create a layout of all positions.
	
	Note: it is assumed that all resizing action is done from outside
*/
class PositionThumbnail : public QGraphicsPixmapItem
{
	
public:
	
	// Constructor
	PositionThumbnail (Experiment *_experiment, int positionNumber, bool _drawFrame, PositionDisplay *_positionDisplay, float _zValue);
		
	/**
	 * Update display according to current tv-factor, ocular factor and other display relevant settings
	 * @param _width thumbnail width in pixels (according to scene coordinate system)
	 * @param _height thumbnail height in pixels
	 */
	void updateDisplay(int _width, int _height);

	/**
	 * Overloaded function. Works only if thumbnail size has been set correctly (which is the case
	 * after the actual updateDisplay function has been called)
	 */
	void updateDisplay();

	/**
	 * displays this position as not available (draw a red cross on the picture)
	 * useful, if for example the log file is not available. Does NOT update display automatically
	 */
	void lock();

	/**
	 * Sets this position to be the (in)active one (displayed with a green frame). Does NOT update display automatically!
	 * @param _active whether this position is the active one (default)
	 */
	void setSelected (bool _active = true);

	/**
	 * @return if this position thumbnail is in selected state
	 */
	bool isSelected() const {
		return selected;
	}

	/**
	 * @return the index of the position associated with this thumbnail
	 */
	int getPositionNumber() const {
		return m_positionNumber;
	}

	/**
	 * @return whether the first image was found
	 */
	bool getFoundFirstImage() const {
		return foundFirstImage;
	}

	/**
	 * Set the first image. Does NOT update display automatically
	 * @param _image the new first image to be used
	 */
	void setFirstImage(const QPixmap& _image);

	// Z-values are increased for every image (so that positions overlap each other in a defined way) by Z_IMAGE_DELTA
	static const float Z_IMAGE_DELTA;

	// Frame will have corresponding image z-value + Z_FRAME_DELTA_TO_IMAGE
	static const float Z_FRAME_DELTA_TO_IMAGE;

	// Text will have corresponding image z-value + Z_TEXT_DELTA_TO_IMAGE
	static const float Z_TEXT_DELTA_TO_IMAGE;

	// Frame width in pixels (at 100% zoom)
	static const int FRAME_WIDTH = 25;

	// Color for active and inactive thumbnails (i.e. for text and frame)
	static const QColor colorSelected;
	static const QColor colorNotSelected;

	// Font point size
	static const int FONT_POINT_SIZE = 120;

protected:

	// Mouse pressed (needs to be reimplemented in order to be able to receive mouse release and mouse double click events!)
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);

	// Mouse button released event
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);

	// Double click event
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _event);

private:

	// Experiment descriptor
	Experiment *experiment;

	// Corresponding position
	int m_positionNumber;

	// First picture (white picture if first picture is not available)
	QPixmap firstImage;
	bool foundFirstImage;

	// Desired picture or placeholder size in pixels (=scene coordinates) or 0 if not set
	int thumbNailSizeX, thumbNailSizeY;

	// The PositionDisplay instance this thumbnail belongs to
	PositionDisplay *positionDisplay;

	///whether this position is selected
	bool selected;

	// Thumbnail frame (objects are owned by scene)
	QList<QGraphicsRectItem*> frame;

	// Text in the middle (object is owned by scene)
	QGraphicsTextItem *midText;

	// Lines for cross of locked positions
	QGraphicsLineItem *line1;
	QGraphicsLineItem *line2;

	///whether this position is locked
	bool locked;
};

#endif
