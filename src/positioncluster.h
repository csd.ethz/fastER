/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef positioncluster_h__
#define positioncluster_h__

// STL
#include <vector>

// QT
#include <QRectF>

class Experiment;


/**
 * A set of one or more adjacent (overlapping) positions.
 */
class PositionCluster {

public:

	/**
	 * Constructor.
	 */
	PositionCluster(Experiment* experiment, int position);

	/**
	 * Add position.
	 */
	void addPosition(int position);

	/**
	 * Check if position intersects this cluster.
	 */
	bool intersects(int position) const;

	/**
	 * Merge with other position cluster.
	 */
	void merge(const PositionCluster* other);

	/**
	 * Get positions.
	 */
	const std::vector<int>& getPositions() const {
		return m_positions;
	}

private:

	// Corresponding experiment
	Experiment* m_experiment;
	
	// Positions in this cluster.
	std::vector<int> m_positions;

	// Size (in micrometers) and position of this cluster (bounding rect)
	QRectF m_dimensions;
};


#endif // positioncluster_h__
