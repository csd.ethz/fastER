/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobsegmentation_h__
#define jobsegmentation_h__


// Project
#include "job.h"
#include "segmentationoperationlist.h"
#include "segmentationoperation.h"
#include "pictureindex.h"

// STL
#include <vector>
#include <memory>

// OpenCV
#include "opencv2/core/core.hpp"

// QT
#include <QString>

class ImageProcessingTools;
namespace faster {
	class FastERDetector;
}

/**
 * Segmentation job, segments one image.
 */
class JobSegmentation : public Job {

public:

	/**
	 * Constructor.
	 */
	JobSegmentation(int id, std::shared_ptr<SegmentationOperationList> operationList, const unsigned char* imageData, int imageSizeX, int imageSizeY, faster::FastERDetector* fasterDetector, ImageProcessingTools* imageProcessingTools, const PictureIndex& imagePictureIndex, bool previewMode = false)
		: Job(id), 
		m_operationList(operationList), 
		m_fasterDetector(fasterDetector), 
		m_imageProcessingTools(imageProcessingTools), 
		m_imagePictureIndex(imagePictureIndex),
		m_imageSizeX(imageSizeX),
		m_imageSizeY(imageSizeY),
		m_previewMode(previewMode)
	{
		// Copy data into own buffer
		m_imageDataType = IDT_UInt8Gray;
		m_imageData = new unsigned char[m_imageSizeY * imageSizeX];
		memcpy(m_imageData, imageData, m_imageSizeY * imageSizeX);

		// Create another buffer with original image data
		m_imageDataOriginal = new unsigned char[m_imageSizeY * imageSizeX];
		memcpy(m_imageDataOriginal, imageData, m_imageSizeY * imageSizeX);
	}

	/**
	 * Destructor.
	 */
	~JobSegmentation()
	{
		// Cleanup
		delete[] m_imageData;
		delete[] m_imageDataOriginal;
	}

	/**
	 * Reimplemented. 
	 * @param callingJobRunner is always an instance of JobRunnerSegmentation.
	 */
	void run(JobRunner* callingJobRunner);

	/**
	 * Get fastER detector.
	 */
	faster::FastERDetector* getFastERDetector() const 
	{
		return m_fasterDetector;
	}

	/**
	 * Get image processing tools.
	 */
	ImageProcessingTools* getImageProcessingTools() const
	{
		return m_imageProcessingTools;
	}

	/**
	 * Access image data.
	 */
	unsigned char* getImageData() const 
	{
		return m_imageData;
	}
	int getImageSizeX() const
	{
		return m_imageSizeX;
	}
	int getImageSizeY() const
	{
		return m_imageSizeY;
	}

	/**
	 * Get a copy of the image data (Caller must delete[] returned pointer).
	 */
	unsigned char* getImageDataCopy(ImageDataType desiredFormat) const 
	{
		return m_imageProcessingTools->convertImageData(m_imageData, m_imageSizeX, m_imageSizeY, m_imageDataType, desiredFormat, m_operationList->getPixelConnectivity());
	}

	///**
	// * Access image data in requested format (image data will be converted if necessary).
	// */
	//const unsigned char* getImageData(ImageDataType format) const 
	//{
	//	// If current format does not match format, convert data
	//	if(format != m_imageDataType) {
	//		if(m_imageDataType == IDT_UInt32LabelMap && format == IDT_UInt8Gray) {
	//			// 32 bit label map to 8 bit binary image

	//		}
	//		else if(m_imageDataType == IDT_UInt8Gray && format == IDT_UInt32LabelMap) {
	//			// 8 bit binary image to 32 bit label mask

	//		}
	//	}

	//	return m_imageData;
	//}

	/**
	 * Get bytes per pixel of current image data.
	 */
	int getImageDataBytesPerPixel() const
	{
		return bytesPerPixelForImageDataType(m_imageDataType);
	}

	/**
	 * Get type of image data (not thread-safe: do not use while job is running).
	 */
	ImageDataType getImageDataType() const
	{
		return m_imageDataType;
	}

	const PictureIndex& getPictureIndex() const 
	{
		return m_imagePictureIndex;
	}

	/**
	 * Get segmentation mask index.
	 */
	int getSegmentationMaskIndex() const 
	{
		return m_operationList->maskIndex;
	}

	/**
	 * Get pixel connectivity.
	 */
	int getPixelConnectivity() const 
	{
		return m_operationList->getPixelConnectivity();
	}


private:

	// Image data to work on (pointer and data type can change when operations are executed, but not the number of pixels)
	unsigned char* m_imageData;
	ImageDataType m_imageDataType;

	// Original unprocessed image data
	unsigned char* m_imageDataOriginal;
	int m_imageSizeX;
	int m_imageSizeY;

	// Image index
	PictureIndex m_imagePictureIndex;

	// Segmentation operations to perform
	std::shared_ptr<SegmentationOperationList> m_operationList;

	// fastER
	faster::FastERDetector* m_fasterDetector;

	// Image processing tools
	ImageProcessingTools* m_imageProcessingTools;

	// Segmentation preview mode
	bool m_previewMode;
};


#endif // jobsegmentation_h__
