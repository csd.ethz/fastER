/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmmainwindow_h__
#define frmmainwindow_h__

#include "ui_frmMainWindow.h"

// Qt
#include <QHash>
#include <QProgressDialog>
#include <QTimer>
#include <QSharedPointer>

// STL
#include <vector>

// Project
#include "blob.h"
#include "imageprocessingtools.h"
#include "pictureindex.h"

// fastER
#include "faster.h"

// OpenCV
#include "opencv2/core/core.hpp"


class FrmMainWindow;
class FrmSelectExperiment;
class FrmSelectPositions;
class FrmSegmentationPreview;
class FrmOriginalImageView;
class FrmConsole;
class FrmImageViewSettings;
class FrmStartTasks;
class FrmColorTransformation;
class FrmSegmentationPreview;
class FrmBasicSegmentationSettings;
class JobRunnerMaster;
class FrmProgress;
class JobSet;
class MainWindowMenuBar;
class FrmVersionInfo;
class Experiment;
class FrmReQuantify;
class FrmTreeVisualization3D;
class FrmHelp;

/**
 * Global pointer to main window.
 */
extern FrmMainWindow* g_FrmMainWindow;


class FrmMainWindow : public QMainWindow {

	Q_OBJECT

public:

	/**
	 * History:
	 * 1.0.1	04.03.2014
	 * 1.0.2	27.03.2014
	 * 1.0.3	31.03.2014
	 * 1.0.4	13.05.2014
	 * 1.0.5	19.05.2014
	 * 1.0.6	27.05.2014
	 * 1.0.7	27.05.2014
	 * 1.1.0	17.06.2014
	 * 1.1.1	20.06.2014
	 * 1.1.2	30.06.2014
	 * 1.1.3	23.07.2014
	 * 1.1.4	04.08.2014
	 * 1.1.5	11.08.2014
	 * 1.1.6	27.08.2014
	 * 1.1.7	29.08.2014
	 * 1.1.8	14.09.2014
	 * 1.1.9	14.10.2014
	 * 1.2.0	28.10.2014
	 * 1.2.1	31.10.2014
	 * 1.2.2	31.10.2014
	 * 1.2.3	21.12.2014
	 * 1.2.4	09.04.2015
	 * 1.2.5	01.06.2015
	 * 1.2.6	09.06.2015
	 * 1.2.7	30.06.2015
	 * 1.2.8	09.07.2015
	 * 1.2.9	28.09.2015
	 * 1.3.0	28.10.2015
	 * 1.3.1	29.10.2015
	 * 1.3.2	01.11.2015
	 * 1.3.3	20.11.2015	// TJC
	 * 1.3.4	04.12.2015	// Release version (new features not included)
	 * 1.3.5	04.12.2015	// Release version 2, renamed to fastER
	 */
	static const int VERSION_MAJOR = 1;
	static const int VERSION_MINOR_1 = 3;
	static const int VERSION_MINOR_2 = 5;
	static QString getVersionString() 
	{
		return QString("%1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR_1).arg(VERSION_MINOR_2);
	}

	/**
	 * Constructor. Shows and initializes main window.
	 */
	FrmMainWindow(QWidget* parent = 0);

	/**
	 * Destructor.
	 */
	~FrmMainWindow();

	/**
	 * Get currently loaded experiment.
	 */
	Experiment* experiment()
	{
		return m_experiment.data();
	}

	/**
	 * Update statistics from extracted samples (including mean and std. deviation which are needed by the SVM). 
	 */
	void updateSampleFeatureStatistics();

	/**
	 * Get PictureIndex of currently displayed preview image.
	 */
	PictureIndex getCurrentPreviewImage() const 
	{
		return m_currentPictureIndex;
	}

	/**
	 * @return if logging of faster summary message is enabled.
	 */
	bool getFastERLogging() const 
	{
		return m_ui.actionLog_FastER_Training_Results->isChecked();
	}

	/**
	 * Update basic label statistics (min/max label size etc.) taking labels from all images.
	 */
	void updateLabelStatistics();

	/**
	 * Find candidate regions fitting to current labels and store their features.
	 * If automatic dob/bod detection is on and the current image contains most 
	 * labels or onlyInCurrentImage is false, the dob/bod setting will also be determined.
	 * If the determined dob/bod setting has changed or if onlyInCurrentImage is false, 
	 * candidate regions will be extracted from current labels in all images and not just
	 * the current. Updates sample statistics.
	 */
	void extractSamplesForLabels(bool useAllLabeledImages);

	/**
	 * Learn parameters for fastER.
	 */
	void learnParametersSvm();

	// Dock widgets
	FrmSegmentationPreview* m_frmSegPreview;
	QDockWidget* m_dockSegPreview;
	FrmOriginalImageView* m_frmOriginalImageView;
	FrmBasicSegmentationSettings* m_frmBasicSegmentationSettings;
	QDockWidget* m_dockOriginalImage;
	FrmConsole* m_frmConsole;
	FrmImageViewSettings* m_frmImageViewSettings;
	FrmStartTasks* m_frmStartTasks;

public slots:

	/**
	 * Update display of selected positions
	 */
	void updateDisplayOfSelectedPositions();

	/**
	 * Randomly select preview image.
	 */
	void randomPreviewPic();

	/**
	 * Run segmentation pipeline on current image and show results.
	 */
	void applySegmentationSettingsAndUpdatePreview();

	/**
	 * Get number of found candidate regions fitting to cell and background labels by the user.
	 */
	int getNumLabeledCellExtremalRegions() const 
	{
		// Count
		int count = 0;
		for(auto it = m_posSampleFeatures.cbegin(); it != m_posSampleFeatures.cend(); ++it)
			count += it->size();
		return count / faster::NUM_FEATURES;
	}
	int getNumLabeledBackgroundExtremalRegions() const 
	{
		// Count
		int count = 0;
		for(auto it = m_negSampleFeatures.cbegin(); it != m_negSampleFeatures.cend(); ++it)
			count += it->size();
		return count / faster::NUM_FEATURES;
	}

	/**
	 * Get number of foreground and background labels by the user.
	 */
	int getNumForegroundLabels() const;
	int getNumBackgroundLabels() const;

signals:

	/**
	 * Current experiment has changed.
	 */
	void currentExperimentChanged();

protected:

	/**
	 * Reimplemented.
	 */
	void closeEvent(QCloseEvent *event);
	void showEvent( QShowEvent *event );
	void keyReleaseEvent(QKeyEvent* event);

private slots:

	/**
	 * Experiment selection.
	 */
	void showExperimentSelectionForm();
	void experimentSelected(QSharedPointer<Experiment> experiment, bool selectPositions);
	void experimentSelectionClosed();

	/**
	 * Position selection.
	 */
	void showPositionSelectionForm();

	/**
	 * User finished changing labels.
	 */
	void markingFinished();

	/**
	 * Start tracking.
	 */
	void startTracking();

	/**
	 * Zoom in image view changed.
	 */
	void zoomChanged(float scaling);

	/**
	 * Display settings dialog requested or settings changed by user.
	 */
	void displaySettingsChanged();

	/**
	 * Mouse move events.
	 */
	void mouseMoved(int posX, int posY);

	/**
	 * Image selection.
	 */
	void previewNextTimePoint();
	void previewPrevTimePoint();
	//void selectPreviewPic();
	void imageTimepointChanged(int value);
	void imagePositionChanged(int value);
	void previewChangeTimePoint(int delta);

	///**
	// * Extract ERs fitting to current labels in all images and determine if dark-on-bright or bright-on-dark should be
	// * used (performs full optimization).
	// */
	//void findExtremalRegionsForCurrentLabelsAndUpdateObservationsInAllImages();

	/**
	 * Export/import labels.
	 */
	void exportLabels();
	void importLabels();

	/**
	 * Start tracking.
	 */
	void startTrackingClicked();

	///**
	// * Matlab: test fastER
	// */
	//void matlabTestFastER();

	/**
	 * Display of found extremal regions.
	 */
	void updateDisplayOfFoundExtremalRegions();

	/**
	 * Hide user markings.
	 */
	void hideMarkings();

	/**
	 * Wavelength (=channel) to use for segmentation changed.
	 */
	void channelSegmentationChanged(int newChannel);

	/**
	 * Wavelength (=channel) to use for preview changed.
	 */
	void channelPreviewChanged(int newChannel);

	/**
	 * First/last time point selected.
	 */
	void setStartTpToFirst();
	void setStopTpToLast();

	/**
	 * Start selected tasks.
	 */
	void startTasks();

	/**
	 * Count cells.
	 */
	void countCells();

	/**
	 * Export foreground labels of current image into bw image.
	 */
	void exportFGLabelsOfCurrentImage();

	///**
	// * Display help.
	// */
	//void showHelp();

	/**
	 *	Show quick instructions.
	 */
	void showQuickInstructions();

	/**
	 * Dock widgets top level changed (docked or undocked).
	 */
	void dockSegPreviewTopLevelChanged(bool floating);

	/**
	 * Import labels from a binary image and add to current image.
	 */
	void addLabelsFromBinaryImage();
	
	/**
	 * Show about window.
	 */
	void showAboutWindow();

private:

	
	/**
	 * Find candidate regions fitting to current labels in the indicated image, extract corresponding
	 * features (stored in m_posSampleFeatures/m_negSampleFeatures), and create overlay images
	 * to visualize the found samples (stored in m_foundExtremalRegionsPos/m_foundExtremalRegionsNeg).
	 * @param pi image to use
	 * @param DOB 0: try both dob&bod, return better score and update m_automaticallyDeterminedDob accordingly; 1: use dob; 2: use bod
	 */
	void extractSamplesForLabelsSingleImage(const PictureIndex& pi, int& DOB);
	
	/**
	 * Find PictureIndex of image with most foreground labels.
	 */
	PictureIndex findIndexOfImageWithMostLabels() const;

	/**
	 * Create overlay image to visualize samples.
	 */
	QImage createOverlayImage(faster::FastERDetector& d, bool dob, bool positive, int imageSizeX, int imageSizeY) const;


	/**
	 * Update window title.
	 */
	void updateWindowTitle();

	/**
	 * Set current image.
	 */
	void setImage(const PictureIndex& pi);

	/**
	 * Show progress form.
	 */
	void showProgressForm();

	/**
	 * Get positive and negative sample features from all images in one vector.
	 */
	void convertSampleFeaturesToVectors(std::vector<double>& obsPos, std::vector<double>& obsNeg) const;

	// Convert full path (e.g. C:\130423DL1\130423DL1_p0001\img.png) to experiment relative path (e.g. 130423DL1_p0001\img.png)
	QString fullPathToExpRelPath(const QString& imgPath);
	QString expRelPathToFullPath(const QString& imgPath);

	// Import old labels (i.e. stored in deprecated formats)
	void importLabelsOld(QDataStream& in, int version);	// For labels file version 1 and 2

	// Get foreground and background labels for indicated image as linear pixel indexes
	void convertLabelsToLinearPixelIndexes(const PictureIndex& pi, int imageSizeX, std::vector<std::vector<int>>& foregroundLabels, std::vector<int>& backgroundPixels) const;

	// Remove labels (m_labelsForeground, m_labelsBackground), found samples (m_foundExtremalRegionsPos, m_foundExtremalRegionsNeg)
	// and sample features (m_posSampleFeatures, m_negSampleFeatures) for current image or all images
	// Note: does not update any statistics (label or sample statistics) and does not update display (of found samples, of segmentation, or window titles, etc.)
	void clearLabels(bool fromAllLabeledImages);

	// Export quantification in single file: helper functions
	void exportQuantificationInSingleFileHorizontalInternal(QTextStream& s, int maxNumRows, const QVector<PictureIndex>& pis, const QHash<PictureIndex, QVector<QString>>& columns, const QHash<PictureIndex, QVector<QVector<QVariant>>>& data, QProgressDialog* progressBar);
	void exportQuantificationInSingleFileVerticalInternal(QTextStream& s, const QSet<QString>& allColumns, const QVector<PictureIndex>& pis, const QHash<PictureIndex, QVector<QString>>& columnsByFrame, const QHash<PictureIndex, QVector<QVector<QVariant>>>& dataByFrame, QProgressDialog* progressBar);

	// Load pipeline
	void loadFastERPipeline();

	// GUI
	Ui::MainWindow m_ui;

	// Current experiment, 0 at start
	QSharedPointer<Experiment> m_experiment;

	// Select experiment form
	FrmSelectExperiment* m_frmSelectExperiment;

	// Select positions form
	FrmSelectPositions* m_frmSelectPositions;

	// Progress widget
	FrmProgress* m_frmProgress;

	// Current image (Format_Indexed8) and path + filename
	PictureIndex m_currentPictureIndex;
	QImage m_curImage;

	// Channel used to display segmentation
	int m_segmentationPreviewChannel;

	// Change image settings
	FrmColorTransformation* m_frmColorTransformation;

	// Markings (key: image, value: markings) of foreground and background
	QHash<PictureIndex, std::vector<Blob> > m_labelsForeground;
	QHash<PictureIndex, std::vector<cv::Point2i> > m_labelsBackground;

	// Overlay images visualizing found extremal regions corresponding to markings
	QHash<PictureIndex, QImage> m_foundExtremalRegionsPos;
	QHash<PictureIndex, QImage> m_foundExtremalRegionsNeg;

	// If samples have to be updated from all labeled images (and not only for the current image) next time
	// samples are extracted (e.g. after denoising parameters were changed)
	bool m_enforceUpdateOfSamplesInAllImages;

	// Statistics derived from markings
	int m_minForegroundLabelSize,
		m_maxForegroundLabelSize,
		m_numForegroundLabels;

	// Detectors used to get samples from user labels and for segmentation preview
	faster::FastERDetector m_fasterDetectorDob;
	faster::FastERDetector m_fasterDetectorBod;

	// Features derived from ERs corresponding to markings for each image
	QHash<PictureIndex, faster::sample_feature_list_type> m_posSampleFeatures;
	QHash<PictureIndex, faster::sample_feature_list_type> m_negSampleFeatures;

	// Automatically determined dob/bod setting: 0 = not determined, 1 = dob, 2 = bod
	int m_automaticallyDeterminedDob;

	// Observation statistics
	double m_sampleFeatureStatsMean[faster::NUM_FEATURES];					
	double m_sampleFeatureStatsStdDev[faster::NUM_FEATURES];		

	// Image processing tools
	ImageProcessingTools m_imageProcessingTools;

	// If calls to imageTimepointChanged() should be ignored
	bool m_ignoreImageTimepointChanged;
	bool m_ignorePositionChanged;

	// Version info form
	FrmVersionInfo* m_frmVersionInfo;

	// 3D Tree visualization window
	FrmTreeVisualization3D* m_frmTreeVisualization3D;

	// Quick help window
	FrmHelp* m_frmQuickHelp;

	friend FrmReQuantify;
};


#endif // frmmainwindow_h__
