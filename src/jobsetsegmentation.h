/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobsetsegmentation_h__
#define jobsetsegmentation_h__

// STL
#include <vector>

// Project
#include "jobset.h"
#include "pictureindex.h"

// QT
#include <QMutex>



class JobSetSegmentation : public JobSet {

public:

	/**
	 * Constructor. Can throw Exception.
	 */
	JobSetSegmentation();

	/**
	 * Destructor.
	 */
	~JobSetSegmentation();

	/**
	 * Reimplemented.
	 */
	Job* popJob(JobRunner* jobRunner);

	/**
	 * Reimplemented.
	 */
	void jobFinished(JobRunner* jobRunner, Job* job);


	/**
	 * Reimplemented.
	 */
	int getNumJobsTotal() const
	{
		return (m_stopTp - m_startTp + 1) * m_positions.size();
	}

	/**
	 * Reimplemented.
	 */
	int getNumJobsProcessed() const
	{
		QMutexLocker lock(&m_currentImageMutex);
		return m_numJobsCompleted - 1;
	}

	/**
	 * Reimplemented.
	 */
	bool requestCancel()
	{
		m_canceled = true;
		return true;
	}

	/**
	 * Reimplemented.
	 */
	QString getDescription() const 
	{
		return m_descr;
	}

	/**
	 * Change the description
	 */
	void setDescription(const QString& newDescr)
	{
		m_descr = newDescr;
	}

private:

	/**
	 * Finds the next image starting with the current image that exists and sets current image to the next image. 
	 * Returns 1 if no image is left. Otherwise returns 0 or 2 in case of error.
	 */
	int openCurrentImageAndGoToNextImage(PictureIndex& outPI, QImage& outImage, int& outNextJobId);

	// Positions to segment, start and stop timepoint (-1 if all time points are used)
	std::vector<int> m_positions;
	int m_startTp, m_stopTp;

	// Tracking wavelength and zindex
	int m_wl, m_zIndex;

	// Index of current position and current time point and number of jobs completed
	int m_currentPositionIndex;
	int m_currentTimePoint;
	int m_numJobsCompleted;
	mutable QMutex m_currentImageMutex;

	// Output folder mutex (so that no two threads try to create same output folder)
	mutable QMutex m_outFolderMutex;

	// If cancel was requested
	bool m_canceled;

	// Description
	QString m_descr;
};


#endif // jobsetsegmentation_h__
