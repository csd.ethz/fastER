/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationfaster_h__
#define segmentationoperationfaster_h__

// Project
#include "segmentationoperation.h"
#include "logging.h"

/**
 * fastER.
 */
class SegmentationOperationFastER : public SegmentationOperation {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	SegmentationOperationFastER(int _minSize = 50, int _maxSize = 1000, double _minProbability = 0.5, bool _darkOnBright = false, bool _disableErSplitting = false)
		: minSize(_minSize), maxSize(_maxSize), minProbability(_minProbability), darkOnBright(_darkOnBright), disableErSplitting(_disableErSplitting)
	{}

	/**
	 * Reimplemented.
	 */
	void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner);

	/**
	 * Reimplemented.
	 */
	QString getName() const
	{
		return "fastER";
	}

	/**
	 * Reimplemented.
	 */
	QHash<QString, QVariant> getParameters() const
	{
		QHash<QString, QVariant> params;
		params.insert("minSize", minSize);
		params.insert("maxSize", maxSize);
		params.insert("minProbability", minProbability);
		params.insert("darkOnBright", darkOnBright);
		params.insert("disableErSplitting", disableErSplitting);
		params.insert("constrainPosSamples", constrainPosSamples);

		return params;
	}

	/**
	 * Reimplemented.
	 */
	void setParameters(const QHash<QString, QVariant>& parameters)
	{
		// Change parameters
		for(auto it = parameters.begin(); it != parameters.end(); ++it) {
			if(it.key() == "minSize" && it.value().type() == QVariant::Int) 
				minSize = it.value().toInt();
			else if(it.key() == "maxSize" && it.value().type() == QVariant::Int)
				maxSize = it.value().toInt();
			else if(it.key() == "minProbability" && it.value().type() == QVariant::Double)
				minProbability = it.value().toDouble();
			else if(it.key() == "darkOnBright" && it.value().type() == QVariant::Bool)
				darkOnBright = it.value().toBool();
			else if(it.key() == "disableErSplitting" && it.value().type() == QVariant::Bool)
				disableErSplitting = it.value().toBool();
			else if(it.key() == "constrainPosSamples" && it.value().type() == QVariant::Bool)
				constrainPosSamples = it.value().toBool();
			else
				gDbg << __FUNCTION__ << " - invalid call to setParameter() - key: " << it.key() << " - value: " << it.value().toString() << "\n";
		}
	}

	// Parameters
	int minSize;
	int maxSize;
	double minProbability;
	bool darkOnBright;
	bool disableErSplitting;
	bool constrainPosSamples;
};

#endif // segmentationoperationfaster_h__
