/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "experimenttat.h"

// Qt
#include <QFile>
#include <QDir>
#include <QXmlStreamReader>
#include <QRect>
#include <QRectF>
#include <QImage>
#include <QImageReader>

// Project
#include "pngio.h"
#include "settings.h"
#include "fastdirectorylisting.h"
#include "exceptionbase.h"
#include "positioncluster.h"
#include "logging.h"


ExperimentTAT::ExperimentTAT(const QString& path)
	: Experiment(path)
{
	// Init variables
	m_picSizeX = 0;
	m_picSizeY = 0;
	m_numDigitsForTimePoint = 0;
	m_numDigitsForPosition = 0;
	m_numDigitsForWl = 0;
	m_numDigitsForZIndex = 0;
	m_firstImageAcquisitionTime = 0;
	m_invertedCoordinates = false;
	m_microMeterPerPixelChannel0 = -1.0;
	m_minLeft = std::numeric_limits<float>::max();
	m_minTop = std::numeric_limits<float>::max();
	m_maxLeft = std::numeric_limits<float>::lowest();
	m_maxTop = std::numeric_limits<float>::lowest();

	// Initialize experiment
	if(!initializeExperiment())
		throw ExceptionBase(__FUNCTION__, "Experiment initialization failed.");
}

bool ExperimentTAT::initializeExperiment()
{
	// Find positions
	if(!initializePositions())
		return false;

	// Get image sizes
	determineImageInformation();

	// Read tat info
	m_microMeterPerPixelChannel0 = -1.0;
	readExperimentInfo(Settings::isSet("ocularFactor"), Settings::isSet("tvFactor"));
	if(m_microMeterPerPixelChannel0 <= 0.0)
		updateMicroMeterPerPixel();

	// Determine position layout
	loadPositionLayout();

	// Find background images
	initializeBackgroundImages();

	return true;
}

bool ExperimentTAT::initializePositions()
{
	// Open experiment
	QDir expDir(m_path);
	if(!expDir.exists())
		throw ExceptionBase(__FUNCTION__, "Cannot open experiment folder.");

	// Iterate over positions folders
	const QStringList folders = expDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);
	QStringList::const_iterator itCurFolder = folders.constBegin();

	QSet<int> positionsSet;
	while(itCurFolder != folders.constEnd()) {

		// Extract position number 
		int curFolderPos = Tools::getPosNumberFromDirName(*itCurFolder);

		// Skip invalid folders
		if(curFolderPos == -1) {
			gLog << QString("Skipping folder: '%1'.").arg(*itCurFolder) << "\n";
			++itCurFolder;
			continue;
		}

		// Set m_numDigitsForPosition
		if(m_numDigitsForPosition <= 0)
			m_numDigitsForPosition = Tools::getTagLength(*itCurFolder, "_p");

		// Add position
		positionsSet.insert(curFolderPos);

		// Go to next folder 
		++itCurFolder;
	}
	m_positionNumbers = positionsSet.toList();
	qSort(m_positionNumbers);

	return true;
}

void ExperimentTAT::loadPositionLayout()
{

	for(auto it = m_positionNumbers.constBegin(); it != m_positionNumbers.constEnd(); ++it) {
		int curPos = *it;

		// Check if position intersects with any existing clusters
		std::vector<int> intersections;
		for(int i = 0; i < m_positionClusters.size(); ++i) {
			if(m_positionClusters[i]->intersects(curPos))
				intersections.push_back(i);
		}
		if(intersections.size() > 0) {
			// Merge all found clusters and add position
			int firstIntersectionIndex = intersections[0];
			for(int i = 1; i < intersections.size(); ++i) {
				int otherIndex = intersections[i];
				m_positionClusters[firstIntersectionIndex]->merge(m_positionClusters[otherIndex]);
				delete m_positionClusters[otherIndex];
				m_positionClusters[otherIndex] = 0;
			}
			m_positionClusters[firstIntersectionIndex]->addPosition(curPos);

			// Delete NULL entries
			int newIndex = 0;
			for(int i = 0; i < m_positionClusters.size(); ++i) {
				if(m_positionClusters[i]) {
					m_positionClusters[newIndex++] = m_positionClusters[i];
				}
			}
			m_positionClusters.resize(newIndex);
		}
		else {
			// Create new cluster
			m_positionClusters.push_back(new PositionCluster(this, curPos));
		}
	}

	// Debug output
	gLog << "Position clusters: ";
	for(auto it = m_positionClusters.begin(); it != m_positionClusters.end(); ++it) {
		gLog << "[";
		auto positions = (*it)->getPositions();
		int posCount = 0;
		for(auto it2 = positions.begin(); it2 != positions.end(); ++it2) {
			if(posCount++)
				gLog << ", ";
			gLog << (*it2);
		}
		gLog << "] ";
	}
	gLog << "\n";



	// Image size in micrometers
	float imSizeXmm = m_picSizeX * m_microMeterPerPixelChannel0;
	float imSizeYmm = m_picSizeY * m_microMeterPerPixelChannel0;

	// Get if verbose mode is on
	bool verbose = Settings::get<bool>(Settings::KEY_VERBOSE);

	// Compare each initialized position to each initialized position
	for(auto it1 = m_positionNumbers.constBegin(); it1 != m_positionNumbers.constEnd(); ++it1) {
		int pos1 = *it1;
		// Get position rectangle including adjacency distance in global coordinates
		/*
		float width = (m_picSizeX + PositionDescriptor::ADJACENCY_DISTANCE) * mmpp; 
		float height = (m_picSizeY + PositionDescriptor::ADJACENCY_DISTANCE) * mmpp; 
		float left = (*it1)->getLeft() - (PositionDescriptor::ADJACENCY_DISTANCE * mmpp);
		float top = (*it1)->getTop() - (PositionDescriptor::ADJACENCY_DISTANCE * mmpp);
		*/
		float width = m_picSizeX * m_microMeterPerPixelChannel0; 
		float height = m_picSizeY * m_microMeterPerPixelChannel0;
		QPointF offset = getImageOffset(PictureIndex(pos1, 1, 0, 1));
		float left = offset.x();
		float top = offset.y();
		const QRectF pos1Rect(left, top, width, height);

		// Output for verbose mode
		QString output;
		int found = 0;
		if(verbose) 
			output = QString("Detected neighbors %1 - ").arg(pos1);

		for(auto it2 = it1 + 1; it2 != m_positionNumbers.constEnd(); ++it2) {
			int pos2 = *it2;

			// Get position rectangle of other position without adjacency distance in global coordinates
			QPointF offset = getImageOffset(PictureIndex(pos1, 1, 0, 1));
			const QRectF pos2Rect(offset.x(), offset.y(), imSizeXmm, imSizeYmm);

			// Check if they overlap
			if(pos1Rect.intersects(pos2Rect)) {
				if(pos1 >= m_overlappingPositions.size())
					m_overlappingPositions.resize(pos1 + 1);
				if(pos2 >= m_overlappingPositions.size())
					m_overlappingPositions.resize(pos2 + 1);

				m_overlappingPositions[pos1].push_back(pos2);
				m_overlappingPositions[pos2].push_back(pos1);

				// Output
				if(verbose) {
					output += QString("%1 ").arg(pos2);
					++found;
				}
			}
		}

		// Output position layout if verbose mode
		if(verbose && found) {
			gLog << output << "\n";
		}
	}
}


std::vector<PictureIndex> ExperimentTAT::getAllPictureIndexesInPosition(int position)
{
	// Enumerate image files
	std::vector<PictureIndex> foundImages;
	QString path = m_path + m_name + QString("_p%1/").arg(position, m_numDigitsForPosition, 10, QChar('0'));
	if(!QDir(path).exists(path))
		return foundImages;
	const QStringList images = FastDirectoryListing::listFiles(QDir(path), QStringList() << "png" << "jpg" << "tif");
	for(auto it = images.begin(); it != images.end(); ++it) {
		const QString& fileName = *it;
		if(Tools::getPosNumberFromDirName(fileName) != position)
			continue;
		PictureIndex pi;
		pi.positionNumber = position;
		pi.channel = Tools::getWlNumberFromFileName(fileName);
		pi.timePoint = Tools::getTpNumberFromFileName(fileName);
		pi.zIndex = Tools::getZIndexFromFileName(fileName);
		foundImages.push_back(pi);
	}

	return foundImages;
}

int ExperimentTAT::getImage(const PictureIndex& pi, QImage& outImage)
{
	// Create file name
	QString imageExpRelFileName =  getImageExperimentRelativeFileName(pi);
	if(imageExpRelFileName.isEmpty())
		return 1;
	QString imageFileName = m_path + imageExpRelFileName;

	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Try to open
	outImage = QImage(imageFileName);
	if(outImage.isNull())
		return 2;

	// Success
	return 0;
}

int ExperimentTAT::getImage(const PictureIndex& pi, cv::Mat& outImage)
{
	// Create file name
	QString imageExpRelFileName =  getImageExperimentRelativeFileName(pi);
	if(imageExpRelFileName.isEmpty())
		return 1;
	QString imageFileName = m_path + imageExpRelFileName;

	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Try to open
	outImage = cv::imread(imageFileName.toStdString(), CV_LOAD_IMAGE_UNCHANGED);
	if(!outImage.data)
		return 2;

	// Success
	return 0;
}

void ExperimentTAT::readExperimentInfo(bool _ignoreOcFact, bool _ignoreTvFact)
{
	// Generate file path
	QString xmlPath = m_path + m_name + "_TATexp.xml";

	// Open file
	QFile f(xmlPath);
	if(!f.open(QIODevice::ReadOnly))
		throw ExceptionBase(__FUNCTION__, QString("Cannot open TAT-XML (%1)").arg(xmlPath));

	// Reset old
	m_wlInformation.clear();

	// Read into QString (this is done because the tatxml files often cause problems because of invalid encoded characters..)
	QString data = f.readAll();

	// Replace ',' with '.'
	data.replace(QChar(','), QChar('.'));

	// Ignore ocular and tv factor if tag exists that specifies micrometer per pixel directly (in that case, other tags contain garbage in some YouScope experimnents)
	if(data.contains("<MicrometerPerPixel")) {
		_ignoreOcFact = true;
		_ignoreTvFact = true;
	}

	// Open stream
	double ocularFactor;
	double tvFactor;
	QXmlStreamReader xml(data);
	while(!xml.atEnd()) {
		// Read next
		QXmlStreamReader::TokenType type = xml.readNext();

		// Check for position data section
		if(type == QXmlStreamReader::StartElement) {
			QStringRef name = xml.name();
			bool ok = true;

			if(name == "PositionData") {
				// Parse <PositionData>
				parsePositionData(xml);
			}
			else if(!_ignoreOcFact && name == "CurrentObjectiveMagnification") {
				// Get value of <CurrentObjectiveMagnification>
				const QXmlStreamAttributes& attr = xml.attributes();
				QString val = attr.value("value").toString();
				ocularFactor = val.toFloat(&ok);
			}
			else if(!_ignoreTvFact && name == "CurrentTVAdapterMagnification") {
				// Get value of <CurrentTVAdapterMagnification>
				const QXmlStreamAttributes& attr = xml.attributes();
				QString val = attr.value("value").toString();
				tvFactor = val.toFloat(&ok);
			}
			else if(name == "MicrometerPerPixel") {
				// Get value of <MicrometerPerPixel>
				const QXmlStreamAttributes& attr = xml.attributes();
				QString val = attr.value("value").toString();
				double valDouble = val.toDouble(&ok);
				if(ok)
					updateMicroMeterPerPixel(valDouble);
			}
			else if(name == "WavelengthData") {
				// Parse <WavelengthData>
				parseWavelengthData(xml);
			}

			// Check for error
			if(!ok)
				throw ExceptionBase(__FUNCTION__, QString("Cannot parse TAT-Xml element: %1").arg(name.toString()));
		}
	}

	// check for errors
	if(xml.hasError()) 
		throw ExceptionBase(__FUNCTION__, QString("Cannot process TAT-XML (%1): %2").arg(xmlPath).arg(xml.errorString()));

	// Set ocular and tv factor
	if(!_ignoreOcFact)
		Settings::set("ocularFactor", ocularFactor);
	if(!_ignoreTvFact)
		Settings::set("tvFactor", tvFactor);

	// Update maximal wavelength
	m_maxPictureIndex.channel = qMax(m_maxPictureIndex.channel, m_wlInformation.size()-1);
}

void ExperimentTAT::determineImageInformation()
{
	int validFilesFound = 0,
		invalidFilesFound = 0;

	// We must find at least one image with wavelength 0 as this is the standard image size
	QImage rndW0Image;

	// List images
	assert(m_numDigitsForPosition > 0);
	for(auto it = m_positionNumbers.constBegin(); it != m_positionNumbers.constEnd(); ++it) {
		int curPos = *it;

		// Get folder
		QString posFolderString = m_path + m_name + QString("_p%1/").arg(curPos, m_numDigitsForPosition, 10, QChar('0'));
		QDir posFolder(posFolderString);

		// List images
		QStringList imageExtensions;
		imageExtensions << ".jpg" << ".tif" << ".png";
		const QStringList imageList = FastDirectoryListing::listFiles(posFolder, imageExtensions);

		// Iterate over images and remember image sizes for all wavelengths
		for(QStringList::const_iterator curFile = imageList.constBegin(); curFile != imageList.constEnd(); ++curFile) {
			// Set number of digits
			if(m_numDigitsForTimePoint <= 0) 
				m_numDigitsForTimePoint = Tools::getTagLength(*curFile, "_t");
			if(m_numDigitsForWl <= 0) 
				m_numDigitsForWl = Tools::getTagLength(*curFile, "_w");
			if(m_numDigitsForZIndex <= 0)
				m_numDigitsForZIndex = Tools::getTagLength(*curFile, "_z");

			// Set maximum PictureIndex
			PictureIndex pi = converFileNameToPictureIndex(*curFile);
			m_maxPictureIndex = m_maxPictureIndex.isValid() ? qMax(m_maxPictureIndex, pi) : pi;
			m_minPictureIndex = m_minPictureIndex.isValid() ? qMin(m_minPictureIndex, pi) : pi;

			// Skip file if invalid or size for wl already known
			int wl = Tools::getWlNumberFromFileName(*curFile);
			if(wl < 0 || m_imageSizes.contains(wl))
				continue;

			// Remember image extension
			if(wl >= m_imageExtensions.size())
				m_imageExtensions.resize(wl + 1);
			if(m_imageExtensions[wl].isEmpty())
				m_imageExtensions[wl] = curFile->mid(curFile->lastIndexOf('.')+1);

			// Try to open image
			QImageReader reader(posFolderString + *curFile);
			QImage img = reader.read();

			// Count invalid files and report error for the first one only (to avoid 5000 error messages if something is wrong)
			if(img.isNull()) {
				if(invalidFilesFound++ == 0) {
					auto supported = reader.supportedImageFormats();
					QString supportedString;
					for(auto it = supported.begin(); it != supported.end(); ++it)
						supportedString += *it + " ";
					gLog << QString("Warning: Could not open file '%1': %2 (supported: %3)").arg(posFolderString + *curFile).arg(reader.errorString()).arg(supportedString) << "\n";
				}

				continue;
			}

			// Image is valid
			validFilesFound++;

			// Remember pic if wl is 0
			if(wl == 0) 
				rndW0Image = img;

			// Remember size
			m_imageSizes.insert(wl, img.size());
		}

		// Exit loop if images were found in last position
		if(validFilesFound > 0)
			break;
	}

	// Check if we have found an w0 image
	if(rndW0Image.isNull())
		throw ExceptionBase(__FUNCTION__, QString("Could not find and open any wl0 picture in the selected experiment ('%1' - %2 - %3).").arg(m_path).arg(m_positionNumbers.size()).arg(invalidFilesFound));

	// Check if there were invalid images
	if(invalidFilesFound > 0)
		gLog << QString("Warning: Could not open %1 images.").arg(invalidFilesFound) << "\n";

	// Save image size
	m_picSizeX = rndW0Image.width();
	m_picSizeY = rndW0Image.height();

	// Set scanBinFactor
	int scanBinFactor;
	switch(m_picSizeX) {
	case 2048:
		scanBinFactor = 1;
		break;
	case 1388:		//1388x1040
		//base size: scan factor 1.0
		scanBinFactor = 1;
		break;
	case 694:		//694x520
		scanBinFactor = 4;
		break;
	case 462:		//462x346
		scanBinFactor = 9;
		break;
	case 346:		//346x260
		scanBinFactor = 16;
		break;
	case 276:		//276x208
		scanBinFactor = 25;
		break;
	case 2776:		//2776x2080
		scanBinFactor = 2;
		break;
	case 4164:		//4164x3120
		scanBinFactor = 3;
		break;
	default:
		scanBinFactor = 1;
		gLog << QString("Warning: Could not determine scanBinFactor (Image width: %1).").arg(m_picSizeX) << "\n";
	}
	Settings::set("scanBinFactor", scanBinFactor);

	if(m_numDigitsForWl <= 0 || m_numDigitsForPosition <= 0 || m_numDigitsForTimePoint <= 0)
		gLog << QString("Warning: Could not determine digits for tags (pos: %1, wl: %2).").arg(m_numDigitsForPosition).arg(m_numDigitsForWl) << "\n";
}

void ExperimentTAT::parsePositionData( QXmlStreamReader& _xml )
{
	while(!_xml.atEnd()) {
		// Read next
		QXmlStreamReader::TokenType type = _xml.readNext();

		// Check for end
		if(type == QXmlStreamReader::EndElement) {
			if(_xml.name() == "PositionData")
				return;
		}
		// Check for PosInfoDimension
		else if(type == QXmlStreamReader::StartElement) {
			if(_xml.name() == "PosInfoDimension") 
				parsePosInfoDimension(_xml.attributes());
		}
	}
}

void ExperimentTAT::parsePosInfoDimension( const QXmlStreamAttributes& _attributes )
{
	// Information to be extracted
	int index;
	float posX, posY;

	// Iterate over attributes
	for(QXmlStreamAttributes::const_iterator it = _attributes.constBegin(); it != _attributes.constEnd(); ++it) {
		// Parse attribute
		bool ok = true;
		const QStringRef name = it->name();
		if(name == "index") {
			index = it->value().toString().toInt(&ok);
		}
		else if(name == "posX") {
			posX = it->value().toString().toFloat(&ok);
		}
		else if(name == "posY") {
			posY = it->value().toString().toFloat(&ok);
		}

		// Check for error
		if(!ok)
			throw ExceptionBase(__FUNCTION__, QString("Cannot parse TAT-Xml element: %1").arg(name.toString()));
	}

	// Update boundaries
	if(posX < m_minLeft)
		m_minLeft = posX;
	if(posY < m_minTop)
		m_minTop = posY;
	if(posX > m_maxLeft)
		m_maxLeft = posX;
	if(posY > m_maxTop)
		m_maxTop = posY;

	// Save information
	if(index >= 0) {
		if(index >= m_positionOffsets.size())
			m_positionOffsets.resize(index + 1);
		m_positionOffsets[index] = QPointF(posX, posY);
	}
}

bool ExperimentTAT::hasImage(const PictureIndex& pi) const
{
	Settings::get<int>(Settings::KEY_STARTFRAME);

	// Create file name
	QString imageFileName = m_path + getImageExperimentRelativeFileName(pi);

	// Check if exists
	return QFile::exists(imageFileName);
}

PictureIndex ExperimentTAT::getRandomImage(bool allowDifferentChannel)
{
	// Randomly select position
	if(m_positionNumbers.size() == 0)
		return PictureIndex();
	QList<QVariant> selectedPositionKeys = Settings::get<QList<QVariant> >(Settings::KEY_SELECTEDPOSITIONS);
	int randomPosition;
	if(selectedPositionKeys.size())
		randomPosition = selectedPositionKeys[qrand()%selectedPositionKeys.size()].toInt();
	else
		randomPosition = m_positionNumbers[qrand()%m_positionNumbers.size()];

	// Determine required wl
	int desiredWl = Settings::get<int>(Settings::KEY_TRACKINGWL);
	int desiredZ = Settings::get<int>(Settings::KEY_TRACKINGZINDEX);

	// Randomly select image in position
	PictureIndex pi;
	QString posFolderString = m_path + m_name + QString("_p%1/").arg(randomPosition, m_numDigitsForPosition, 10, QChar('0'));
	QStringList files = FastDirectoryListing::listFiles(posFolderString, QStringList() << ".png" << ".jpg" << ".tif");
	if(files.size() == 0)
		return PictureIndex();
	int startIndex = qrand() % files.size();
	int i = startIndex + 1;
	while(i != startIndex) {
		if(i == files.size()) {
			i = 0;
			continue;
		}
		if(Tools::getWlNumberFromFileName(files[i]) == desiredWl && Tools::getZIndexFromFileName(files[i]) == desiredZ) {
			pi = getPictureIndexFromFileName(files[i]);
			break;
		}
		++i;
	}
	if(!pi.isValid() && allowDifferentChannel) {
		// No image with desired channel found -> return image with different channel if allowed
		return getPictureIndexFromFileName(files[0]);
	}

	return pi;
}

int ExperimentTAT::getBackgroundImage(const PictureIndex& pi, std::shared_ptr<unsigned char>& outImage, int& outBitDepth, int& outWidth, int& outHeight)
{
	// Create file name
	QString relFileName = getBackgroundImageExperimentRelativeFileName(pi);
	if(relFileName.isEmpty())
		return 1;
	QString imageFileName = m_path + relFileName; 

	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Try to open
	int bitDepth, width, height;
	unsigned char* backgroundImageData = 0;
	int errCode = openPngGrayScaleImage(QDir::toNativeSeparators(imageFileName).toLatin1(), &backgroundImageData, bitDepth, width, height, true);	// Open background image, too

	// Check for error
	if(errCode || !backgroundImageData) {
		if(backgroundImageData)
			delete[] backgroundImageData;
		return 2;
	}

	// Success
	outImage = std::shared_ptr<unsigned char>(backgroundImageData, std::default_delete<unsigned char[]>());
	outWidth = width;
	outHeight = height;
	outBitDepth = bitDepth;
	return 0;
}

int ExperimentTAT::getGainImage(const PictureIndex& pi, bool loadIfNecessary, std::shared_ptr<unsigned char>& outImag, int& outBitDepth, int& outWidth, int& outHeight)
{
	QMutexLocker lock(&m_gainImagesMutex);

	// Use only position and channel as key, all other values must be default-initialized
	PictureIndex key;
	key.positionNumber = pi.positionNumber;
	key.channel = pi.channel;

	// Check if image exists
	outImag = m_gainImages.value(key);
	if(outImag)
		// Image already loaded
		return 0;

	if(!loadIfNecessary)
		// Image should not be loaded
		return 3;

	// Check if image exists
	QString path = m_path + getGainImageExperimentRelativeFileName(pi);
	if(!QFile::exists(path))
		return 1;

	// Try to load image
	int bitDepth;
	int width, height;
	unsigned char* gainImageData = 0;
	if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&gainImageData, bitDepth, width, height, true) != 0) {
		delete[] gainImageData;
		return 2;
	}

	// Success
	outImag = std::shared_ptr<unsigned char>(gainImageData, std::default_delete<unsigned char[]>());
	m_gainImages[key] = outImag;
	return 0;
}

int ExperimentTAT::getSegmentationData(const PictureIndex& pi, std::vector<Blob>& outSegmentationData, int segmentationMethod, int sizeX, int sizeY)
{
	// Get segmentation file name
	QString imageFileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);

	// Try to open
	return Tools::loadSegmentationFromPNG(imageFileName, outSegmentationData, sizeX, sizeY);
}

int ExperimentTAT::writeSegmentationData(const PictureIndex& pi, const QImage& maskImage, int segmentationMethod /*= 0*/)
{
	// Create output folder
	QString filePath = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);
	if(!Tools::createDirectoryThreadSafe(filePath, true))
		return 2;

	// Save image data
	if(!maskImage.save(filePath))
		return 1;

	return 0;
}

int ExperimentTAT::writeSegmentationData(const PictureIndex& pi, unsigned short* labelMap, int sizeX, int sizeY, int segmentationMethod /*= 0*/)
{
	// Create output folder
	QString filePath = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);
	if(!Tools::createDirectoryThreadSafe(filePath, true))
		return 2;

	// Save image
	if(save16BitImageAsPng(reinterpret_cast<unsigned char*>(labelMap), sizeX, sizeY, filePath.toLatin1(), true) != 0)
		return 1;

	return 0;
}

int ExperimentTAT::writeSegmentationQuantification(const PictureIndex& pi, const QString& quantificationData, int segmentationMethod /*= 0*/)
{
	// Get file name
	QString fileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod, false);
	if(fileName.isEmpty())
		return 1;
	fileName += ".csv";

	// Create output folder
	if(!Tools::createDirectoryThreadSafe(fileName, true))
		return 2;

	// Write data
	QFile csvFile(fileName); 
	if(!csvFile.open(QIODevice::WriteOnly))
		return 3;
	QTextStream ss(&csvFile);
	ss.setLocale(QLocale("C"));		// Make sure formatting is right
	ss << quantificationData;

	return (ss.status() != QTextStream::WriteFailed) ? 0 : 4;
}

int ExperimentTAT::readSegmentationQuantification(const PictureIndex& pi, QString& outData, int segmentationMethod /*= 0*/)
{
	// Get file name
	QString fileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod, false);
	if(fileName.isEmpty())
		return 2;
	fileName += ".csv";

	// Check if file exists
	if(!QFile::exists(fileName))
		return 1;

	// Read data
	QFile csvFile(fileName); 
	if(!csvFile.open(QIODevice::ReadOnly))
		return 3;
	QTextStream ss(&csvFile);
	ss.setLocale(QLocale("C"));		// Make sure formatting is right
	outData = ss.readAll();
	
	return (ss.status() == QTextStream::Ok) ? 0 : 4;
}


bool ExperimentTAT::supportsMicroMeterCoordinates() const
{
	return true;
}

float ExperimentTAT::getMicroMetersPerPixel(const PictureIndex& pi) const
{
	// Value for channel 0 should be available
	assert(m_microMeterPerPixelChannel0 > 0.0);
	if(pi.channel == 0)
		return m_microMeterPerPixelChannel0;

	// Other channel, calculate based on value for channel 0
	if(!m_imageSizes.contains(pi.channel))
		throw ExceptionBase(__FUNCTION__, QString("Micrometer per pixel requested for unknown wavelength (%1).").arg(pi.channel));

	// Calculate scaling factor
	double factor = static_cast<double>(m_imageSizes.value(pi.channel).width()) / m_picSizeX;

	// If e.g. w1 images have 0.5 times the size of w0 images, they have 2 times more micrometers per pixel
	return m_microMeterPerPixelChannel0 / factor;
}

bool ExperimentTAT::supportsGlobalCoordinates() const
{
	return true;
}

QRectF ExperimentTAT::getCoordinateBoundaries() const
{
	return QRectF(QPointF(m_minLeft, m_minTop), QPointF(m_maxLeft, m_maxTop));
}

PictureIndex ExperimentTAT::getMinPictureIndex() const
{
	return m_minPictureIndex;
}

PictureIndex ExperimentTAT::getMaxPictureIndex() const
{
	return m_maxPictureIndex;
}

int ExperimentTAT::getImageSizeX(const PictureIndex& pi) const
{
	assert(pi.channel == 0 || !pi.isValid());
	return m_picSizeX;
}

int ExperimentTAT::getImageSizeY(const PictureIndex& pi) const
{
	assert(pi.channel == 0 || !pi.isValid());
	return m_picSizeY;
}

bool ExperimentTAT::getCoordinatesInverted() const
{
	return m_invertedCoordinates;
}

void ExperimentTAT::setCoordinatesInverted(bool inverted)
{
	m_invertedCoordinates = inverted;
}

QPointF ExperimentTAT::getImageOffset(const PictureIndex& pi) const
{
	if(pi.positionNumber >= 0 && pi.positionNumber < m_positionOffsets.size())
		return m_positionOffsets[pi.positionNumber];
	return QPointF();
}

bool ExperimentTAT::updateMicroMeterPerPixel(double microMeterPerPixel /*= 0.0*/)
{
	if(microMeterPerPixel > 0.0) {
		// Use parameter
		m_microMeterPerPixelChannel0 = microMeterPerPixel;
	}
	else {
		// Calculate value
		microMeterPerPixel = BASE_ANGSTROOM_PER_PIXEL;
		microMeterPerPixel *= 20.0 / Settings::get<double>("ocularFactor");
		microMeterPerPixel /= Settings::get<double>("tvFactor");
		switch (Settings::get<int>("scanBinFactor")) {
		case 1:
			microMeterPerPixel *= 3.0;
			break;
		case 2:
			microMeterPerPixel *= 1.5;
			break;
		case 3:
			//already ok in base value
			break;
		case 4:
			microMeterPerPixel *= 6.0;
			break;
		case 9:
			microMeterPerPixel *= 9.0;
			break;
		case 16:
			microMeterPerPixel *= 12.0;
			break;
		case 25:
			microMeterPerPixel *= 15.0;
			break;
		default:
			//scanning 1 assumed
			microMeterPerPixel *= 3.0;
		}
		microMeterPerPixel /= 10000.0;

		m_microMeterPerPixelChannel0 = microMeterPerPixel;
	}

	gLog << QString("MicrometerPerPixel set to: %1.").arg(m_microMeterPerPixelChannel0) << "\n";

	return true;
}

const std::vector<PositionCluster*> ExperimentTAT::getPositionClusters() const
{
	return m_positionClusters;
}

QVector<int> ExperimentTAT::getOverlappingPositions(const PictureIndex& pi) const
{
	if(pi.positionNumber >= 0 && pi.positionNumber < m_overlappingPositions.size())
		return m_overlappingPositions[pi.positionNumber];

	return QVector<int>();
}

int ExperimentTAT::getImageAcquisitionTime(const PictureIndex& pi, QDateTime& outAcquisitionTime)
{
	// Check if entry exists
	outAcquisitionTime = m_acquisitionTimes.value(pi);
	if(outAcquisitionTime.isValid())
		return 0;

	// From now on, threads must be synchronized (other thread could be loading log file currently)
	QMutexLocker lock(&m_acquisitionTimesMutex);

	// Check if acquisition times were loaded successfully for position
	if(m_positionsWithAcquisitionTimes.contains(pi.positionNumber))
		return 2;

	// Check if there were error at position
	if(m_positionsWithAcquisitionTimeErrors.contains(pi.positionNumber))
		return 3;

	// Try to load
	int errCode = readLogFile(pi.positionNumber);
	if(!errCode) {
		outAcquisitionTime = m_acquisitionTimes.value(pi);
		if(outAcquisitionTime.isNull())
			return 4;
		else
			return 0;
	}
	else 
		return errCode;
}

int ExperimentTAT::readLogFile(int positionNumber)
{
	m_positionsWithAcquisitionTimeErrors.insert(positionNumber);

	// Open file
	QString posName = m_name + QString("_p%1").arg(positionNumber, m_numDigitsForPosition, 10, QChar('0'));
	QString logFileName = m_path + posName + '/' + posName + ".log"; 
	if(!QFile::exists(logFileName))
		return 2;
	QFile file(logFileName);
	if(!file.open(QIODevice::ReadOnly))
		return 3;

	// Read first 4 bytes to distinguish old log files from new log files
	QDataStream s(&file);
	s.setByteOrder(QDataStream::LittleEndian);
	quint32 first4Bytes;
	s >> first4Bytes;
	if(first4Bytes != LOG_SIGNATURE) 
		return 3;

	/**
	 * New log file format
	 */

	// Fileversion
	quint32 fileVersion;
	s >> fileVersion;
	if(fileVersion != 1)
		return 3;

	// Number of FSIArrayLength
	quint32 FSIArrayLength;
	s >> FSIArrayLength;

	// Number of wavelengths
	quint32 NumberOfWaveLengths;
	s >> NumberOfWaveLengths;

	// Number of entries
	quint32 numEntries;
	s >> numEntries;

	// Check for errors
	if(FSIArrayLength <= 0 || NumberOfWaveLengths <= 0 || numEntries == 0)
		return 3;

	// Read entries
	//int deltaSeconds = 0;	// If daylight saving time change occurred during experiment, dates can be wrong
	QDateTime previousTimeStamp; // Last dateTime, used to check for log file errors in which date and month were mixed up for some time points
	int previousTimePoint = 0;
	for(int i = 0; i < numEntries; ++i) {
		// Timepoint
		quint32 timePoint;
		s >> timePoint;

		// Wavelength
		quint16 wavelength;
		s >> wavelength;

		// Z-index
		qint16 zIndex;
		s >> zIndex;
		if(zIndex < 0)
			zIndex = 1;

		// Timestamp
		quint64 timeStamp;
		s >> timeStamp;

		// Calc date
		PictureIndex curPi(positionNumber, timePoint, wavelength, zIndex);
		QDateTime& dateTime = m_acquisitionTimes[curPi];
		dateTime.setTimeSpec(Qt::UTC);
		dateTime.setMSecsSinceEpoch(timeStamp);

		//// Debug
		//qDebug() << curPi << " - time: " << dateTime.toString();

		// Check time stamps 
		if(previousTimeStamp.isValid()) {
			QDate prevDate = previousTimeStamp.date();
			QDate curDate = dateTime.date();
			if(/*(dateTime.secsTo(previousTimeStamp) > 60*60 && timePoint > previousTimePoint)	// error 1: this time point is more than one hour before the last time point
				||*/ (prevDate.month() != curDate.month() && prevDate.day() == curDate.day())		// error 2: month changed, but day stayed the same
				|| (dateTime.secsTo(previousTimeStamp) > 0 && timePoint > previousTimePoint)) {		// new: do not allow any negative delta times
				return 3;
			}
		}
		previousTimeStamp = dateTime;
		previousTimePoint = timePoint;
	}

	// Success
	m_positionsWithAcquisitionTimeErrors.remove(positionNumber);
	m_positionsWithAcquisitionTimes.insert(positionNumber);
	return 0;
}

qint64 ExperimentTAT::getImageAcquisitionTimeRelative(const PictureIndex& pi)
{
	// Check for error state
	if(m_firstImageAcquisitionTime < 0)
		return -1;

	// Determine acquisition time of first image if not yet available
	if(m_firstImageAcquisitionTime == 0) {
		QMutexLocker lock(&m_firstImageAcquisitionTimeMutex);
		// Must check again (other thread may have determined m_firstImageAcquisitionTime in the mean time)
		if(m_firstImageAcquisitionTime < 0)
			return -1;
		if(m_firstImageAcquisitionTime == 0) {
			// Find the PictureIndex of the image that was acquired first
			PictureIndex piFirstImage = findFirstAcquiredImage();
			if(!piFirstImage.isValid()) {
				// Fail
				m_firstImageAcquisitionTime = -1;
				gErr << "Error: cannot find image acquired first\n";
				return -1;
			}

			// Get acquisition time of that image
			QDateTime firstImageTime;
			int errCode = getImageAcquisitionTime(piFirstImage, firstImageTime);
			if(errCode != 0 || !firstImageTime.isValid()) {
				// Fail
				gErr << "Error: cannot determine acquisition time of image acquired first (error code: " << errCode << ")\n";
				m_firstImageAcquisitionTime = -1;
				return -1;
			}

			// Success
			m_firstImageAcquisitionTime = firstImageTime.toMSecsSinceEpoch();
		}
	}

	// Get acquisition time of image for which relative time was requested
	QDateTime acqTime;
	int errCode = getImageAcquisitionTime(pi, acqTime);
	if(errCode != 0 || !acqTime.isValid()) {
		// Fail
		return -1;
	}

	// Success
	assert(acqTime.toMSecsSinceEpoch() >= m_firstImageAcquisitionTime);
	return acqTime.toMSecsSinceEpoch() - m_firstImageAcquisitionTime;
}

PictureIndex ExperimentTAT::findFirstAcquiredImage() const
{
	// Find image with lowest time point in lowest position with wl 0 and z index 1
	for(int tp = m_minPictureIndex.timePoint; tp <= m_maxPictureIndex.timePoint; ++tp) {
		for(auto itPos = m_positionNumbers.constBegin(); itPos != m_positionNumbers.constEnd(); ++itPos) {
			PictureIndex curPi(*itPos, tp, 0, 1);
			if(hasImage(curPi))
				return curPi;
		}
	}

	// Nothing found
	return PictureIndex();
}

void ExperimentTAT::parseWavelengthData(QXmlStreamReader& _xml)
{
	while(!_xml.atEnd()) {
		// Read next
		QXmlStreamReader::TokenType type = _xml.readNext();

		// Check for end
		if(type == QXmlStreamReader::EndElement) {
			if(_xml.name() == "WavelengthData")
				return;
		}
		// Check for WLInfo
		else if(type == QXmlStreamReader::StartElement) {
			if(_xml.name() == "WLInfo") 
				parseWLInfo(_xml.attributes());
		}
	}		
}

void ExperimentTAT::parseWLInfo(const QXmlStreamAttributes& _attributes)
{
	// Iterate over attributes
	QHash<QString, QString> attributesHash;
	int wlIndex = -1;
	for(QXmlStreamAttributes::const_iterator it = _attributes.constBegin(); it != _attributes.constEnd(); ++it) {
		// Put everything in attributesHash
		const QStringRef name = it->name();
		attributesHash.insert(name.toString(), it->value().toString());

		// Look for "Name" tag to get wlIndex
		bool ok = true;
		if(name == "Name") {
			wlIndex = it->value().toString().toInt(&ok);
		}

		// Check for error
		if(!ok)
			throw ExceptionBase(__FUNCTION__, QString("Cannot parse TAT-Xml element: %1").arg(name.toString()));
	}

	// Extract image width/height
	int width = attributesHash.value("width", 0).toInt();
	int height = attributesHash.value("height", 0).toInt();
	if(width > 0 && height > 0 && !m_imageSizes.contains(wlIndex))
		m_imageSizes[wlIndex] = QSize(width, height);

	// Save
	if(wlIndex >= 0) {
		if(wlIndex >= m_wlInformation.size())
			m_wlInformation.resize(wlIndex+1);
		m_wlInformation[wlIndex] = attributesHash;
	}
}

void ExperimentTAT::initializeBackgroundImages()
{
	const QList<int>& positions = getPositionNumbers();
	m_backgroundImagePaths.resize(positions.size());

	// Use old folder structure, if "background" folder exists in experiment folder, and openBis structure otherwise
	if(QDir(m_path + "background").exists()) {
		gLog << "Using QTFy background images (remove or rename \"background\" folder from experiment to use openBis background images).\n";
		for(auto itPos = positions.begin(); itPos != positions.end(); ++itPos) {
			int posNr = *itPos;
			if(posNr >= m_backgroundImagePaths.size())
				m_backgroundImagePaths.resize(posNr + 1);

			// Simply use background/positionFolder
			m_backgroundImagePaths[posNr] = QString("background/%1_p%2/").arg(m_name).arg(posNr, m_numDigitsForPosition, 10, QChar('0'));
		}
	}
	else {
		gLog << "Using OpenBis background images.\n";
		QString positionsWithoutBackground;
		for(auto itPos = positions.begin(); itPos != positions.end(); ++itPos) {
			int posNr = *itPos;
			if(posNr >= m_backgroundImagePaths.size())
				m_backgroundImagePaths.resize(posNr + 1);

			// Use newest folder in positionFolder/background
			QString posBackgroundFolderRel = QString("%1_p%2/background/").arg(m_name).arg(posNr, m_numDigitsForPosition, 10, QChar('0'));
			QString posBackgroundFolderFull = m_path + posBackgroundFolderRel;
			QStringList backgroundFolders = QDir(posBackgroundFolderFull).entryList(QDir::Dirs | QDir::NoDotAndDotDot);
			if(backgroundFolders.size() == 0) {
				if(positionsWithoutBackground.size())
					positionsWithoutBackground += ", ";
				positionsWithoutBackground += QString::number(posNr);
				continue;
			}
			qSort(backgroundFolders);	// After sorting, the newest background correction images will be in last position
			m_backgroundImagePaths[posNr] = posBackgroundFolderRel + backgroundFolders.back() + '/';
		}
		if(positionsWithoutBackground.size())
			gLog << "Warning: no background images found for positions: " << positionsWithoutBackground << " - background correction will not be available in these positions.\n";
	}
}