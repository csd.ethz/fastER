/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmselectpositions_h__
#define frmselectpositions_h__

#include "ui_frmSelectPositions.h"

// QT
#include <QShowEvent>

/**
 * Window to select positions.
 */
class FrmSelectPositions : public QWidget {

	Q_OBJECT

public:

	FrmSelectPositions(QWidget* parent = 0);

public slots:

	/**
	 * Experiment changed.
	 */
	void experimentChanged()
	{
		// Need to update display
		m_needInitialization = true;
	}

private slots:

	// User clicked in position in position layout
	void positionLeftClicked (int _index);

	// Updated zoom in positiondisplay
	void positionDisplayZoomChanged(float _scaling);

	// Update thumbnail time points / wavelengths
	void updateThumbnails();

	// Ok clicked
	void okClicked();

	// Inverted system changed
	void setInvertedSystem(bool val);

	// Tv factor changed
	void setTVAdapterFactor(const QString &);

	// Ocular factor
	void setOcularFactor (const QString &_text);

	// Update tv and ocular factor in gui
	void setTvAdapterFactorAndOcularFactorInGui();

protected:

	/**
	 * Show event.
	 */
	void showEvent( QShowEvent* ev );


private:

	// If display is outdated and needs to be reinitialized completely
	bool m_needInitialization;

	// GUI
	Ui::SelectPositions m_ui;

};


#endif // frmselectpositions_h__
