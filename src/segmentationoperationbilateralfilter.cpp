/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationbilateralfilter.h"

#include <QDebug>

const char* SegmentationOperationBilateralFilter::SEGOP_NAME = "Bilateral filter";


void SegmentationOperationBilateralFilter::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	// Currently only one data type is supported
	if(*imageDataType != IDT_UInt8Gray)
		throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));

	try {
		//qDebug() << "Filter bil. - sigmaColor: " << sigmaColor << " sigmaSpace: " << sigmaSpace << " k: " << pixelNeighborhoodDiameter;
		//qDebug() << "\tImage data: " << ((void*)*imageData) << " - first before: " << (*imageData)[0];

		// Apply filter
		cv::Mat cvImageDst(sizeY, sizeX, CV_8UC1, *imageData);	// no deep copy
		cv::Mat cvImageSrc = cvImageDst.clone();	// deep copy
		bilateralFilter(cvImageSrc, cvImageDst, pixelNeighborhoodDiameter, sigmaColor, sigmaSpace);

		//qDebug() << "\tImage data: " << ((void*)*imageData) << " - first after: " << (*imageData)[0];
	}
	catch(cv::Exception& e) {
		throw ExceptionBase(__FUNCTION__, QString("OpenCV Exception: %1").arg(e.what()));
	}
}
