/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef experimenttat_h__
#define experimenttat_h__

// Project
#include "experiment.h"
#include "tools.h"

// STL
#include <vector>
#include <cassert>

// Qt
#include <QHash>
#include <QPointF>
#include <QRectF>
#include <QString>
#include <QPair>

/**
 * Experiment created with 'Timm's acquiring tool' (TAT).
 */
class ExperimentTAT : public Experiment {

	Q_OBJECT

public:

	/**
	 * Constructor. Initializes experiment, can throw ExceptionBase if experiment is invalid.
	 */
	ExperimentTAT(const QString& path);

	/**
	 * @reimplemented: access image data.
	 */
	std::vector<PictureIndex> getAllPictureIndexesInPosition(int position);
	PictureIndex getMinPictureIndex() const;
	PictureIndex getMaxPictureIndex() const;
	int getImage(const PictureIndex& pi, QImage& outImage);
	int getImage(const PictureIndex& pi, cv::Mat& outImage);
	bool hasImage(const PictureIndex& pi) const;
	PictureIndex getRandomImage(bool allowDifferentChannel);
	int getBackgroundImage(const PictureIndex& pi, std::shared_ptr<unsigned char>& outImage, int& outBitDepth, int& outWidth, int& outHeight);
	int getGainImage(const PictureIndex& pi, bool loadIfNecessary, std::shared_ptr<unsigned char>& outImag, int& outBitDepth, int& outWidth, int& outHeight);
	int getSegmentationData(const PictureIndex& pi, std::vector<Blob>& outSegmentationData, int segmentationMethod, int sizeX, int sizeY);
	int writeSegmentationData(const PictureIndex& pi, const QImage& maskImage, int segmentationMethod = 0);
	int writeSegmentationData(const PictureIndex& pi, unsigned short* labelMap, int sizeX, int sizeY, int segmentationMethod = 0);
	int writeSegmentationQuantification(const PictureIndex& pi, const QString& quantificationData, int segmentationMethod = 0);
	int readSegmentationQuantification(const PictureIndex& pi, QString& outData, int segmentationMethod = 0);
	PictureIndex converFileNameToPictureIndex(const QString& fileName) const
	{
		return getPictureIndexFromFileName(fileName);
	}
	int getImageSizeX(const PictureIndex& pi) const;
	int getImageSizeY(const PictureIndex& pi) const;
	QString getImageFileNameRelative(const PictureIndex& pi, bool includeFileExtension = true) const
	{
		return getImageExperimentRelativeFileName(pi, includeFileExtension);
	}
	
	/**
	 * @reimplemented: global image coordinates.
	 */	
	bool supportsMicroMeterCoordinates() const;
	QPointF getImageOffset(const PictureIndex& pi) const;
	float getMicroMetersPerPixel(const PictureIndex& pi) const;
	bool supportsGlobalCoordinates() const;
	QRectF getCoordinateBoundaries() const;
	bool getCoordinatesInverted() const;
	void setCoordinatesInverted(bool inverted);

	/**
	 * @reimplemented position layout.
	 */
	const std::vector<PositionCluster*> getPositionClusters() const;
	QVector<int> getOverlappingPositions(const PictureIndex& pi) const;
	

	/**
	 * @reimplemented real time.
	 */
	bool supportsRealTime() const
	{
		return true;
	}
	int getImageAcquisitionTime(const PictureIndex& pi, QDateTime& outAcquisitionTime);
	qint64 getImageAcquisitionTimeRelative(const PictureIndex& pi);


private:

	/**
	 * Mapping of TAT file names to PictureIndex and vice versa.
	 */
	QString getImageExperimentRelativeFileName(const PictureIndex& pi, bool addExtension = true, bool addPositionFolder = true) const
	{
		// This must be set
		assert(m_numDigitsForPosition > 0 && m_numDigitsForTimePoint > 0);
		if(pi.positionNumber < 0 || pi.timePoint < 0 || pi.channel < 0)
			return QString();

		// Add position folder
		QString imageFileName;
		if(addPositionFolder) {
			imageFileName = m_name;
			imageFileName += QString("_p%1/").arg(pi.positionNumber, m_numDigitsForPosition, 10, QChar('0'));
		}

		// Add file name
		imageFileName += m_name;
		imageFileName += QString("_p%1").arg(pi.positionNumber, m_numDigitsForPosition, 10, QChar('0'));
		imageFileName += QString("_t%1").arg(pi.timePoint, m_numDigitsForTimePoint, 10, QChar('0'));
		if(m_numDigitsForZIndex > 0)
			imageFileName += QString("_z%1").arg(pi.zIndex, m_numDigitsForZIndex, 10, QChar('0'));
		if(m_numDigitsForWl > 0)
			imageFileName += QString("_w%1").arg(pi.channel, m_numDigitsForWl, 10, QChar('0'));
		
		// Add extension
		if(addExtension) {
			imageFileName += '.';
			imageFileName += getFileExtensionForChannel(pi.channel);
		}

		return imageFileName;
	}
	QString getBackgroundImageExperimentRelativeFileName(const PictureIndex& pi) const
	{
		if(pi.positionNumber >= m_backgroundImagePaths.size() || m_backgroundImagePaths[pi.positionNumber].isEmpty())
			return "";

		// Background images always have extension png
		QString imageFileName = m_backgroundImagePaths[pi.positionNumber];
		imageFileName += getImageExperimentRelativeFileName(pi, false, false);
		imageFileName += ".png";

		return imageFileName;
	}
	QString getGainImageExperimentRelativeFileName(const PictureIndex& pi) const
	{
		if(pi.positionNumber >= m_backgroundImagePaths.size() || m_backgroundImagePaths[pi.positionNumber].isEmpty())
			return "";

		// Background images always have extension png
		QString imageFileName = m_backgroundImagePaths[pi.positionNumber];
		imageFileName += QString("gain_w%1.png").arg(pi.channel, 2, 10, QChar('0'));

		return imageFileName;
	}
	QString getSegmentationImageExperimentRelativeFileName(const PictureIndex& pi, int segmentationMethodIndex, bool addExtension = true) const
	{
		// This must be set
		assert(m_numDigitsForPosition > 0 && m_numDigitsForTimePoint > 0);
		assert(pi.positionNumber >= 0 && pi.timePoint >= 0 && pi.channel >= 0);

		// Add position and segmentation folder
		QString imageFileName = m_name;
		imageFileName += QString("_p%1/segmentation/").arg(pi.positionNumber, m_numDigitsForPosition, 10, QChar('0'));

		// Add file name
		imageFileName += m_name;
		imageFileName += QString("_p%1").arg(pi.positionNumber, m_numDigitsForPosition, 10, QChar('0'));
		imageFileName += QString("_t%1").arg(pi.timePoint, m_numDigitsForTimePoint, 10, QChar('0'));
		if(m_numDigitsForZIndex > 0)
			imageFileName += QString("_z%1").arg(pi.zIndex, m_numDigitsForZIndex, 10, QChar('0'));
		if(m_numDigitsForWl > 0)
			imageFileName += QString("_w%1").arg(pi.channel, m_numDigitsForWl, 10, QChar('0'));
		imageFileName += QString("_m%1_mask").arg(segmentationMethodIndex, 2, 10, QChar('0'));
		if(addExtension)
			imageFileName += ".png";

		return imageFileName;
	}
	PictureIndex getPictureIndexFromFileName(const QString& fileName) const
	{
		PictureIndex pi;
		pi.positionNumber = Tools::getNumberFromString(fileName, "_p");
		pi.timePoint = Tools::getNumberFromString(fileName, "_t");
		pi.channel = Tools::getNumberFromString(fileName, "_w");
		pi.zIndex = Tools::getNumberFromString(fileName, "_z");

		return pi;
	}

	// Initialize, check which positions are available, read log files
	// Returns true on success (can also throw exceptions)
	bool initializeExperiment();

	// List all existing positions
	bool initializePositions();

	// Load layout (determine adjacent positions)
	void loadPositionLayout();

	// List images and set images size and set scanBinFactor
	void determineImageInformation();

	// Read experiment info
	void readExperimentInfo(bool _ignoreOcFact, bool _ignoreTvFact);
	void parsePositionData(QXmlStreamReader& _xml);
	void parsePosInfoDimension(const QXmlStreamAttributes& _attributes);
	void parseWavelengthData(QXmlStreamReader& _xml);
	void parseWLInfo(const QXmlStreamAttributes& _attributes);

	// Calculate micrometer per pixel. Calculated from ocularFactor, tvFactor and scanBinFactor if parameter microMeterPerPixel is 0.
	bool updateMicroMeterPerPixel(double microMeterPerPixel = 0.0);

	// Read log file
	int readLogFile(int positionNumber);

	// Find image in the experiment that was acquired first and actually exists
	PictureIndex findFirstAcquiredImage() const;

	// Initialize background image folders and store in m_backgroundImagePaths
	void initializeBackgroundImages();

	// Get file extension for wavelength
	QString getFileExtensionForChannel(int ch) const
	{
		// At least for one channel extension must be known
		if(m_imageExtensions.size() == 0)
			return QString();
		if(ch < m_imageExtensions.size() && !m_imageExtensions[ch].isEmpty())
			// Extension is known
			return m_imageExtensions[ch];
		else 
			// Extension is not known, so ch is probably > 0, all channels > 0 can be assumed to have same extension and the last entry of m_imageExtensions is always set
			return m_imageExtensions.back();

	}

	// Number of digits for wavelength and timepoint tags, valid after initializeExperiment() has been called, can be 0 if tag does not exist
	int m_numDigitsForPosition;
	int m_numDigitsForTimePoint;
	int m_numDigitsForWl;
	int m_numDigitsForZIndex;

	// WL0 image size
	int m_picSizeX;
	int m_picSizeY;

	// Image sizes of all wavelengths (in pixels, as in the actual files)
	QHash<int, QSize> m_imageSizes;

	// Image extensions for each wavelength (note: if positions contain different wavelengths, this array may not be complete
	// or could contain empty values for some wavelengths - however, it can be assumed that all wavelengths > 0 have the same
	// extension)
	std::vector<QString> m_imageExtensions;

	// Gain images for different positions and wavelengths, only loaded on demand
	QHash<PictureIndex, std::shared_ptr<unsigned char>> m_gainImages;
	QMutex m_gainImagesMutex;

	// Min/max PictureIndex
	PictureIndex m_minPictureIndex;
	PictureIndex m_maxPictureIndex;

	// Global coordinates: coordinate boundaries
	float m_minLeft, m_minTop, m_maxLeft, m_maxTop;

	// Global coordinates: position offsets (left/top coordinates of all positions), index is position number, contains null-values for non-existing positions
	QVector<QPointF> m_positionOffsets;

	// Global coordinates: overlapping positions for each position, index is position number
	QVector<QVector<int>> m_overlappingPositions;

	// Global coordinates: inverted coordinate system
	bool m_invertedCoordinates;

	// Global coordinates: micrometer per pixel for channel 0
	double m_microMeterPerPixelChannel0;

	// Position clusters
	std::vector<PositionCluster*> m_positionClusters;

	// Image acquisition times of all images in UTC (of positions for which this was loaded)
	QHash<PictureIndex, QDateTime> m_acquisitionTimes;
	QSet<int> m_positionsWithAcquisitionTimes;
	QSet<int> m_positionsWithAcquisitionTimeErrors;
	QMutex m_acquisitionTimesMutex;

	// Image acquisition time (UTC) of first acquired image
	qint64 m_firstImageAcquisitionTime;
	QMutex m_firstImageAcquisitionTimeMutex;

	// Wavelength information from tatxml (one entry for each wavelength with attribute->value pairs)
	QVector<QHash<QString, QString>> m_wlInformation;

	// Experiment relative paths for background images for each position
	QVector<QString> m_backgroundImagePaths;

	// Parameter needed for micrometer per pixel calculation
	static const int BASE_ANGSTROOM_PER_PIXEL = 1075;

	// Binary log files magic number (inverted order due to little endian)
	static const quint32 LOG_SIGNATURE = 0x66676F6C;
};



#endif // experimenttat_h__
