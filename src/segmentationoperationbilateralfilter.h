/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationbilateralfilter_h__
#define segmentationoperationbilateralfilter_h__

#include "segmentationoperation.h"
#include "logging.h"


/**
 * Bilateral filter.
 */
class SegmentationOperationBilateralFilter : public SegmentationOperation
{

	Q_OBJECT

public:

	static const char* SEGOP_NAME;

	/**
	 * Constructor.
	 */
	SegmentationOperationBilateralFilter(int _pixelNeighborhoodDiameter = 7, double _sigmaColor = 10, double _sigmaSpace = 10) 
		: pixelNeighborhoodDiameter(_pixelNeighborhoodDiameter), sigmaColor(_sigmaColor), sigmaSpace(_sigmaSpace)
	{}

	/**
	 * Reimplemented.
	 */
	void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner);

	/**
	 * Reimplemented.
	 */
	QString getName() const
	{
		return SEGOP_NAME;
	}

	/**
	 * Reimplemented.
	 */
	QHash<QString, QVariant> getParameters() const
	{
		// Return parameters
		QHash<QString, QVariant> params;
		params.insert("pixelNeighborhoodDiameter", pixelNeighborhoodDiameter);
		params.insert("sigmaColor", sigmaColor);
		params.insert("sigmaSpace", sigmaSpace);

		return params;
	}

	/**
	 * Reimplemented.
	 */
	void setParameters(const QHash<QString, QVariant>& parameters)
	{
		// Change parameters
		for(auto it = parameters.begin(); it != parameters.end(); ++it) {
			if(it.key() == "pixelNeighborhoodDiameter" && it.value().type() == QVariant::Int)
				pixelNeighborhoodDiameter = it.value().toInt();
			else if(it.key() == "sigmaColor" && it.value().type() == QVariant::Double)
				sigmaColor = it.value().toDouble();
			else if(it.key() == "sigmaSpace" && it.value().type() == QVariant::Double)
				sigmaSpace = it.value().toDouble();
			else
				gDbg << __FUNCTION__ << " - invalid call to setParameter() - key: " << it.key() << " - value: " << it.value().toString() << "\n";
		}
	}

	// Parameters (also changed in Settings::applyLearnedDenoisingParameters())
	int pixelNeighborhoodDiameter;
	double sigmaColor;
	double sigmaSpace;
};

#endif // segmentationoperationbilateralfilter_h__