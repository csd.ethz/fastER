/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobrunnermaster_h__
#define jobrunnermaster_h__

// Qt
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <QHash>

// STL
#include <vector>
#include <memory>

// Project
#include "jobrunner.h"
#include "exceptionbase.h"


class JobRunnerMaster : public QThread {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	JobRunnerMaster(QObject* parent = 0)
		: QThread(parent), m_error(false)
	{}

	/**
	 * Destructor.
	 */
	~JobRunnerMaster()
	{
		for(auto it = m_jobRunners.begin(); it != m_jobRunners.end(); ++it)
			delete *it;
	}

	/**
	 * Add JobRunner to execute. Transfers ownership.
	 */
	void addJobRunner(JobRunner* jobRunner)
	{
		m_jobRunners.push_back(jobRunner);
	}

	/**
	 * QMutex management: provide different JobRunners (i.e. different threads) with arbitrary QMutex objects 
	 * identified by a name.
	 * @param mutexName the name of the mutex (new mutex will be inserted if it does not exist yet).
	 * @return reference to the mutex.
	 */
	QMutex& getMutexByName(const QString& mutexName)
	{
		QMutexLocker lock(&m_mutexStorageMutex);

		// Check if mutex exists
		QMutex* mutex = m_mutexStorage.value(mutexName, std::shared_ptr<QMutex>()).get();
		if(mutex)
			return *mutex;

		// Create mutex
		mutex = new QMutex();
		m_mutexStorage.insert(mutexName, std::shared_ptr<QMutex>(mutex));
		return *mutex;
	}

	/**
	 * Same as getMutexByName, but returns a pointer (never returns 0).
	 */
	QMutex* getMutexPtrByName(const QString& mutexName) {
		QMutexLocker lock(&m_mutexStorageMutex);

		// Check if mutex exists
		QMutex* mutex = m_mutexStorage.value(mutexName, std::shared_ptr<QMutex>()).get();
		if(mutex)
			return mutex;

		// Create mutex
		mutex = new QMutex();
		m_mutexStorage.insert(mutexName, std::shared_ptr<QMutex>(mutex));
		return mutex;
	}

	/**
	 * Get optimal number of threads.
	 */
	static int optimalThreadCount();

	/**
	 * Expose sleep function.
	 */
	static void sleep(unsigned long ms)
	{
		QThread::msleep(ms);
	}

protected:

	/**
	 * Thread entry point. Executes and waits for all JobRunners.
	 */
	void run()
	{
		m_error = false;
		try {
			// Start all JobRunners
			for(auto it = m_jobRunners.begin(); it != m_jobRunners.end(); ++it)
				(*it)->start();

			// Wait for JobRunners
			for(auto it = m_jobRunners.begin(); it != m_jobRunners.end(); ++it)
				(*it)->wait();
		}
		catch(ExceptionBase& e) {
			m_error = true;
			gErr << "Exception in JobRunner::run(): " << e.what() << " - source: " << e.where() << "\n";
		}
	}

private:

	/**
	 * JobRunners to execute.
	 */
	std::vector<JobRunner*> m_jobRunners;

	// If an error occurred
	bool m_error;

	// Mutex manager
	QHash<QString, std::shared_ptr<QMutex>> m_mutexStorage;
	QMutex m_mutexStorageMutex;
};


#endif // jobrunnermaster_h__
