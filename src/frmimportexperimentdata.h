/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmimportexperimentdata_h__
#define frmimportexperimentdata_h__

#include "ui_frmImportExperimentData.h"

// Project
#include "pictureindex.h"
#include "experiment.h"

// Qt
#include<QHash>
#include<QStringList>
#include<QAbstractItemModel>
#include<QSharedPointer>

class FrmImportExperimentDataModel;

/**
 * This window helps the user to import experiments that are not in the TAT format (i.e. have no TATExp.xml)
 *
 * The procedure is as follows:
 *	- if there at least 1 subfolders containing images:
 *		-> treat as experiment with subsets (e.g. positions or fields of view or conditions)
 *		-> images in root folder (if exist) are regarded as additional position
 *	- if there are no subfolders with images and at least 1 images in the root folder
 *		-> treat as experiment with only one 'root' position
 *	- else if there are 0 images in root and no folders with image data
 *		-> error
 */
class FrmImportExperimentData : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmImportExperimentData(QWidget* parent = 0);

	/**
	 * Set current folder.
	 * Enumerates sub folders and image files (ignores folders starting with "_" and
	 * image files containing "segmentation")
	 */
	void setCurrentFolder(QString folder);

	QString getCurrentFolder() const 
	{
		return m_currentFolder;
	}

signals:

	/**
	 * User selected experiment.
	 * @param experiment instance of a derived class of Experiment
	 * @param selectPositions if user wants to restrict the positions to be used.
	 */
	void experimentImported(QSharedPointer<Experiment> experiment, bool selectPositions);


private slots:

	/**
	 * Import data.
	 */
	void importSelectedImages();

	/**
	 * Apply filter on data.
	 */
	void applyFilter();

private:

	// Map image name to PictureIndex based on advanced import options specified by user
	PictureIndex mapImageNameToPictureIndex(const QString& imgName) const
	{
		return PictureIndex(mapImageNameToPositionNumber(imgName), 
			mapImageNameToTimePoint(imgName),
			mapImageNameToChannel(imgName),
			mapImageNameToZIndex(imgName));
	}
	int mapImageNameToPositionNumber(const QString& imgName) const;
	int mapImageNameToTimePoint(const QString& imgName) const;
	int mapImageNameToChannel(const QString& imgName) const;
	int mapImageNameToZIndex(const QString& imgName) const;

	// Current folder
	QString m_currentFolder;

	// Found images
	FrmImportExperimentDataModel* m_foundImages;

	// Currently applied filter and to what
	QString m_currentFilter;
	bool m_currentFilterAppliedToFolderNames;
	bool m_currentFilterAppliedToImageNames;

	// Ui
	Ui::FrmImportExperimentData m_ui;

	friend FrmImportExperimentDataModel;
};

/**
 * Model for display of found image data.
 */
class FrmImportExperimentDataModel : public QAbstractItemModel {
	
	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmImportExperimentDataModel(FrmImportExperimentData* importWindow, QObject* parent = 0) 
		: m_importWindow(importWindow), QAbstractItemModel(parent)
	{}

	/**
	 * Set displayed data. Note: Uses move semantics, so referenced containers will be empty after calling this function.
	 */
	void setData(QHash<QString, QStringList>& imagesInSubfolders, QList<QString>& imagesInRoot)
	{
		// Notify views
		beginResetModel();

		// Change data
		m_foundImagesSubFolders = std::move(imagesInSubfolders);
		m_foundImagesRoot = std::move(imagesInRoot);

		// Extract and sort directories
		m_foundImagesSubFoldersKeysSorted= m_foundImagesSubFolders.keys();
		qSort(m_foundImagesSubFoldersKeysSorted);

		// Done
		endResetModel();
	}

	/**
	 * Get number of displayed subfolders.
	 */
	int getSubFoldersCount() const 
	{
		return m_foundImagesSubFoldersKeysSorted.size();
	}

	// Reimplemented
	QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const
	{
		if(parent.isValid()) {
			// Set internal value to parent row, as we have only one level of depth
			return createIndex(row, column, parent.row());
		}
		else
			// No parent
			return createIndex(row, column, -1);
	}
	
	// Reimplemented
	QModelIndex parent(const QModelIndex& index) const
	{
		if(!index.isValid())
			return QModelIndex();

		// If index has parent, the row of it should be contained in its internal value
		if(index.internalId() >= 0 && index.internalId() < m_foundImagesSubFoldersKeysSorted.size())
			return createIndex(index.internalId(), 0, -1);

		
		return QModelIndex();
	}
	
	// Reimplemented
	int	rowCount(const QModelIndex& parent = QModelIndex()) const
	{
		if(!parent.isValid())
			// Return top level row count
			return m_foundImagesSubFoldersKeysSorted.size() + m_foundImagesRoot.size();
		else {
			if(!parent.parent().isValid() && parent.row() < m_foundImagesSubFolders.size()) {
				// Return row count for specific subfolder
				return m_foundImagesSubFolders[m_foundImagesSubFoldersKeysSorted[parent.row()]].size();
			}
		}
		return 0;
	}
	
	// Reimplemented
	int	columnCount(const QModelIndex& parent = QModelIndex()) const
	{
		return 5;	// Image or folder name, position, timepoint, channel, zindex
	} 
	
	// Reimplemented
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const 
	{
		if(!index.isValid())
			return QVariant();

		// Handle display data
		if(role == Qt::DisplayRole) {
			if(!index.parent().isValid()) {
				// Top level data
				if(index.row() < m_foundImagesSubFoldersKeysSorted.size()) {
					// Folder
					QString folderName = m_foundImagesSubFoldersKeysSorted[index.row()];
					switch(index.column()) {
						case 0:
							// Name
							return m_foundImagesSubFoldersKeysSorted[index.row()];
						case 1:
							{
								int pos = m_importWindow->mapImageNameToPositionNumber(folderName);
								if(pos >= 0)
									// Mapping worked
									return pos;
								else
									// Folders by default correspond to positions with number folderIndex + 1
									return index.row() + 1;
							}
						default:
							;
					};
				}
				else if(index.row() < m_foundImagesSubFoldersKeysSorted.size() + m_foundImagesRoot.size()) {
					// Image in root folder
					QString imgName = m_foundImagesRoot[index.row() - m_foundImagesSubFoldersKeysSorted.size()];
					switch(index.column()) {
					case 0:
						return imgName;
					case 1:		// Position
						{ 
							int pos = m_importWindow->mapImageNameToPositionNumber(imgName);
							if(pos >= 0)
								// Mapping worked
								return pos;
							else
								// Root level images are by default in an artificial position with number subfolderCount + 1
								return m_foundImagesSubFoldersKeysSorted.size() + 1;
						}
					case 2:		// TimePoint
						{ 
							int tp = m_importWindow->mapImageNameToTimePoint(imgName);
							if(tp >= 0)
								// Mapping worked
								return tp;
							else
								// Root level images by default have a time point corresponding to their row index (excluding sub-folders) + 1
								return index.row() - m_foundImagesSubFoldersKeysSorted.size() + 1;
						}
					case 3:		// Channel
						{ 
							int ch = m_importWindow->mapImageNameToChannel(imgName);
							if(ch >= 0)
								// Mapping worked
								return ch;
							else
								// All images are by default in channel 0
								return 0;
						}
					case 4:		// Z-Index
						{ 
							int z = m_importWindow->mapImageNameToZIndex(imgName);
							if(z >= 0)
								// Mapping worked
								return z;
							else
								// All images are by default in z-index 1
								return 1;
						}
					default:
						;
					};
				}
			}
			else { 
				int parentRow = index.internalId();
				if(parentRow >= 0 && parentRow < m_foundImagesSubFoldersKeysSorted.size()) {
					// Image in a sub folder, get image name without causing a deep copy of the QStringList in m_foundImagesSubFolders[m_foundImagesSubFoldersKeysSorted[parentRow]]
					QHash<QString, QStringList>* foundImagesSubFoldersNoConstPtr = const_cast<QHash<QString, QStringList>*>(&m_foundImagesSubFolders);
					QStringList& imagesInSubfolder = (*foundImagesSubFoldersNoConstPtr)[m_foundImagesSubFoldersKeysSorted[parentRow]];
					if(index.row() < imagesInSubfolder.size()) {
						QString imgName = imagesInSubfolder[index.row()];
						switch(index.column()) {
						case 0:
							// Name
							return imgName;
							break;
						case  1:
							// Position, same as for parent folder
							return data(createIndex(parentRow, 1, -1), role);

						case 2:		// TimePoint
							{ 
								int tp = m_importWindow->mapImageNameToTimePoint(imgName);
								if(tp >= 0)
									// Mapping worked
									return tp;
								else
									// images by default have a time point corresponding to their row index + 1
									return index.row() + 1;
							}
						case 3:		// Channel
							{ 
								int ch = m_importWindow->mapImageNameToChannel(imgName);
								if(ch >= 0)
									// Mapping worked
									return ch;
								else
									// All images are by default in channel 0
									return 0;
							}
						case 4:		// Z-Index
							{ 
								int z = m_importWindow->mapImageNameToZIndex(imgName);
								if(z >= 0)
									// Mapping worked
									return z;
								else
									// All images are by default in z-index 1
									return 1;
							}
						};
					}
				}
			}
		}

		return QVariant();
	}

	// Reimplemented
	Qt::ItemFlags flags(const QModelIndex &index) const
	{
		if(!index.isValid())
			return 0;		

		return Qt::ItemIsEnabled;
	}

	// Reimplemented
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const
	{
		// Only one horizontal column
		if(orientation == Qt::Horizontal && role == Qt::DisplayRole) {
			switch(section) {
			case 0:
				return QString("Name");
			case 1:
				return QString("Position");
			case 2:
				return QString("Time Point");
			case 3:
				return QString("Channel");
			case 4:
				return QString("Z-Index");
			default:
				;
			}
		}
		return QVariant();
	}

private slots:

	// Emit data changed
	void emitDataChanged()
	{
		QVector<int> roles;
		roles.push_back(Qt::DisplayRole);
		emit dataChanged(createIndex(0, 0, -1), createIndex(rowCount()-1, columnCount()-1, -1), roles);
	}

private:

	// Import window
	FrmImportExperimentData* m_importWindow;

	// Found images, by sub folder (images in root folder of experiment are not included)
	QHash<QString, QStringList> m_foundImagesSubFolders;
	QList<QString> m_foundImagesSubFoldersKeysSorted;

	// Found images in root folder of experiment
	QList<QString> m_foundImagesRoot;

	friend FrmImportExperimentData;
};


#endif
