/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmconsole_h__
#define frmconsole_h__

#include "ui_frmConsole.h"

// Qt
#include <QObject>
#include <QMutex>
#include <QMutexLocker>

class FrmMainWindow;
class FrmConsole;

/**
 * Helper class for thread safe logging. Do not use directly - use FrmConsole::out() for status window output.
 */
class FrmConsoleHelper : public QObject {

	Q_OBJECT

private:

	FrmConsoleHelper() {}

	void out(const QString& what)
	{
		emit outSignal(what);
	}

signals:
	void outSignal(const QString&);

	friend FrmConsole;
};

/**
 * Console window to show status messages.
 */
class FrmConsole : public QWidget {

	Q_OBJECT

public:

	/**
	 * Get singleton instance.
	 */
	static FrmConsole* inst()
	{
		return m_inst;
	}

	/**
	 * Create new singleton instance.
	 */
	static FrmConsole* createInst(QWidget* parent=0) 
	{
		if(m_inst) {
			m_inst->close();
			delete m_inst;
		}
		m_inst = new FrmConsole(parent);
		return m_inst;
	}

	/**
	 * Append text to singleton instance [thread safe]. Can be called directly from non-GUI threads.
	 */
	static void out(const QString& s)
	{
		if(m_inst) {
			QMutexLocker lock(&m_inst->m_outMutex);

			// Use thread safe connection for logging
			m_inst->m_loggingHelper.out(s);
		}
	}

	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(200, 500);
	}

private slots:

	/**
	 * Append text to singleton instance [thread safe].
	 */
	void outSlot(const QString& s)
	{
		// Append text
		m_text.append(s);
		m_ui.txtOut->setPlainText(m_text);

		// Move cursor to end for auto-scroll
		m_ui.txtOut->moveCursor(QTextCursor::End);
	}

private:

	/**
	 * Constructor.
	 */
	FrmConsole(QWidget* parent = 0)
		: QWidget(parent)
	{
		m_ui.setupUi(this);
		connect(&m_loggingHelper, SIGNAL(outSignal(const QString&)), this, SLOT(outSlot(const QString&)), Qt::QueuedConnection);
	}

	// Helper
	FrmConsoleHelper m_loggingHelper;

	// Text
	QString m_text;

	// Instance
	static FrmConsole* m_inst;

	// Out function mutex
	QMutex m_outMutex;

	// GUI
	Ui::Console m_ui;

};


#endif // frmconsole_h__
