/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positionthumbnail.h"

// Project
#include "positiondisplay.h"
#include "experiment.h"
#include "fastdirectorylisting.h"
#include "settings.h"
#include "frmmainwindow.h"
#include "experiment.h"

// QT
#include <QGraphicsSceneMouseEvent>
#include <QFile>
#include <QMessageBox>


// Z-Values: Make text appear in front of frame lines and frame lines in front of image
const float PositionThumbnail::Z_IMAGE_DELTA = 0.001f;
const float PositionThumbnail::Z_FRAME_DELTA_TO_IMAGE = 0.0001f;
const float PositionThumbnail::Z_TEXT_DELTA_TO_IMAGE = 0.0002f;

// Colors
const QColor PositionThumbnail::colorSelected = QColor(255, 0, 0);
const QColor PositionThumbnail::colorNotSelected = QColor(0, 255, 0); 


PositionThumbnail::PositionThumbnail(Experiment *_experiment, int posNumber, bool _drawFrame, PositionDisplay *_positionDisplay, float _zValue)
	: QGraphicsPixmapItem()
{
	// Init variables
	m_positionNumber = posNumber;
	positionDisplay = _positionDisplay;
	selected = false;
	midText = 0;
	thumbNailSizeX = 0;
	thumbNailSizeY = 0;
	line1 = 0;
	line2 = 0;
	locked = false;
	experiment = _experiment;

	// Load thumbnail image
	QImage tmpImage;
	int errCode = g_FrmMainWindow->experiment()->getImage(PictureIndex(m_positionNumber, positionDisplay->thumbnailsTimePoint, positionDisplay->thumbnailsWaveLength, 1), tmpImage);
	
	if(!errCode && !tmpImage.isNull()) {
		foundFirstImage = true;
		firstImage = QPixmap::fromImage(tmpImage);
	}
	else {
		foundFirstImage = false;

		// If no first image available create white image (use arbitrary size, will be scaled later)
		firstImage = QPixmap(10, 10);

		// Fill with white color
		firstImage.fill(Qt::white);
	}

	// Set first image
	setPixmap(firstImage);

	// Set z-value
	setZValue(_zValue);
}

void PositionThumbnail::updateDisplay(int _width, int _height)
{
	// Scale firstImage if necessary
	if(firstImage.width() != _width || firstImage.height() != _height) {
		firstImage = firstImage.scaled(_width, _height);
		setPixmap(firstImage);
	}

	// Remember them
	thumbNailSizeX = _width;
	thumbNailSizeY = _height;

	// Get boundaries of coordinate system (in micrometers)
	const QRectF& coordinateBounds = experiment->getCoordinateBoundaries();

	// Determine thumbnail position
	int left = 0;
	int top = 0;
	if(g_FrmMainWindow->experiment()->supportsGlobalCoordinates()) {

		// Get left and top of this position
		QPointF offset = g_FrmMainWindow->experiment()->getImageOffset(PictureIndex(m_positionNumber, positionDisplay->thumbnailsTimePoint, positionDisplay->thumbnailsWaveLength, 1));
		left = offset.x();
		top = offset.y();

		//regard coordinate system bounds and whether the coordinate system is inverted
		if (g_FrmMainWindow->experiment()->getCoordinatesInverted()) {
			//subtract coordinates from the bottom right corner (which in fact is the top left)
			left = coordinateBounds.right() - left;
			top = coordinateBounds.bottom() - top;
		}
		else {
			//subtract top left corner to account for a shifted coordinate origin
			left = left - coordinateBounds.left();
			top = top - coordinateBounds.top();
		}

		// Transform to pixels
		if (g_FrmMainWindow->experiment()->supportsMicroMeterCoordinates()) {
			float mmpp = g_FrmMainWindow->experiment()->getMicroMetersPerPixel(PictureIndex(m_positionNumber, positionDisplay->thumbnailsTimePoint, positionDisplay->thumbnailsWaveLength, 1));
			left /= mmpp;
			top /= mmpp;
		}
	}
	else {
		// Layout position in one row from left to right
		int imageSizeX = g_FrmMainWindow->experiment()->getImageSizeX(PictureIndex(m_positionNumber, positionDisplay->thumbnailsTimePoint, positionDisplay->thumbnailsWaveLength, 1));
		left = m_positionNumber * (imageSizeX + 100);
	}

	// Set position
	setPos(left, top);

	// Delete old frame
	for(int i = 0; i < frame.size(); ++i) {
		scene()->removeItem(frame[i]);
		delete frame[i];
	}
	frame.clear();

	// Create frame
	if(positionDisplay->getDrawThumbNailFrames()) {
		//frame.append(new QGraphicsRectItem(left, top, _width, FRAME_WIDTH));						// Top
		//frame.append(new QGraphicsRectItem(left, top+_height-FRAME_WIDTH, _width, FRAME_WIDTH));	// Bottom
		//frame.append(new QGraphicsRectItem(left, top, FRAME_WIDTH, _height));						// Left
		//frame.append(new QGraphicsRectItem(left+_width-FRAME_WIDTH, top, FRAME_WIDTH, _height));	// Right
		frame.append(new QGraphicsRectItem(0, 0, _width, FRAME_WIDTH, this));						// Top
		frame.append(new QGraphicsRectItem(0, _height-FRAME_WIDTH, _width, FRAME_WIDTH, this));	// Bottom
		frame.append(new QGraphicsRectItem(0, 0, FRAME_WIDTH, _height, this));						// Left
		frame.append(new QGraphicsRectItem(_width-FRAME_WIDTH, 0, FRAME_WIDTH, _height, this));	// Right

		// Add frame to scene, set Z-value and color
		for(int i = 0; i < 4; ++i) {
			frame[i]->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);
			//scene()->addItem(frame[i]);

			// Color
			if(isSelected())
				frame[i]->setBrush(QBrush(colorSelected, Qt::SolidPattern));
			else
				frame[i]->setBrush(QBrush(colorNotSelected, Qt::SolidPattern));

			// Disable drawing of frame outline
			frame[i]->setPen(Qt::NoPen);
		}
	}

	// Delete old text
	if(midText) {
		scene()->removeItem(midText);
		delete midText;
		midText = 0;
	}

	// Create text
	if(positionDisplay->getDrawText()) {
		// Generate text
		QString text = QString("%1").arg(m_positionNumber);

		// Create QGraphicsItem
		midText = new QGraphicsTextItem(text, this);

		// Set font
		midText->setFont(QFont("Arial", FONT_POINT_SIZE));

		// Calc position (middle of thumbnail)
		const QRectF sceneRect = midText->sceneBoundingRect();
		int posX = _width / 2 - sceneRect.width() / 2;
		int posY = _height / 2 - sceneRect.height() / 2;;
		midText->setPos(posX, posY);
	
		// Set color
		if(isSelected())
			midText->setDefaultTextColor(colorSelected);
		else
			midText->setDefaultTextColor(colorNotSelected);

		// Set z-value
		midText->setZValue(zValue() + Z_TEXT_DELTA_TO_IMAGE);

		//// Add to scene
		//scene()->addItem(midText);
	}

	// Delete old lock lines
	if(line1) {
		delete line1;
		line1 = 0;
	}
	if(line2) {
		delete line2;
		line2 = 0;
	}

	// Indicate if position is locked
	if(locked) {
		// Create lines for cross
		line1 = new QGraphicsLineItem(0, 0, _width, _height, this);
		line2 = new QGraphicsLineItem(0, _height, _width, 0, this);

		// Set z-value
		line1->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);
		line2->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);

		// Set pen
		QPen pen(Qt::red);
		pen.setWidth(FRAME_WIDTH);
		line1->setPen(pen);
		line2->setPen(pen);

		//// Add to scene
		//scene()->addItem(line1);
		//scene()->addItem(line2);
	}

}

void PositionThumbnail::updateDisplay()
{
	// Check image sizes and call actual function
	if(thumbNailSizeX && thumbNailSizeY)
		updateDisplay(thumbNailSizeX, thumbNailSizeY);
}


void PositionThumbnail::lock()
{
	locked = true;
}

void PositionThumbnail::setSelected( bool _active /*= true*/ )
{
	selected = _active;
}

void PositionThumbnail::mouseReleaseEvent( QGraphicsSceneMouseEvent* _event )
{
	if(_event->button() == Qt::LeftButton) {
		// Delegate
		positionDisplay->reportPositionLeftClicked(m_positionNumber);
	}
	else if(_event->button() == Qt::RightButton) {
		// Delegate
		positionDisplay->reportPositionRightClicked(m_positionNumber);
	}
}

void PositionThumbnail::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* _event )
{
	if(_event->button() == Qt::LeftButton) {
		// Delegate
		positionDisplay->reportPositionDoubleClicked(m_positionNumber);
	}
}

void PositionThumbnail::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{
	// Nothing (needs to be reimplemented in order to be able to receive mouse release and mouse double click events!)
}

void PositionThumbnail::setFirstImage( const QPixmap& _image )
{
	// Check image
	if(_image.isNull())
		return;

	// Set image
	firstImage = _image;
	setPixmap(firstImage);
}