/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tools.h"

// PNG
#include "pngio.h"

// Qt
#include <QPoint>
#include <QLineF>
#include <QStringList>
#include <QPixmap>
#include <QBitmap>
#include <QPainter>
#include <QThread>
#include <QMessageBox>
#include <QPushButton>
#include <QDir>
#include <QProcess>


const char* POS_MARKER = "_p";
const char* WL_MARKER = "_w";
const char* TP_MARKER = "_t";
const char* Z_MARKER = "_z";

QMutex Tools::m_createDirectoryMutex;
//ImageProcessingTools Tools::m_imageProcessingTools;



// Little class to expose Qt sleep function
struct Sleeper : public QThread {
public:

	// Expose sleep method
	static void msleep(unsigned long msecs) {
		QThread::msleep(msecs);
	}

};



float Tools::calcDistance( const QPoint& _p1, const QPoint& _p2 )
{
	// Use QLineF to get distance
	return QLineF(_p1, _p2).length();
}

float Tools::calcDistance( const QPointF& _p1, const QPointF& _p2 )
{
	// Use QLineF to get distance
	return QLineF(_p1, _p2).length();
}

QVector<QRgb> Tools::getColorTableGrayIndexed8()
{
	static QVector<QRgb> colorTableGrayIndexed8;
	if(colorTableGrayIndexed8.size() == 0) {
		colorTableGrayIndexed8.resize(256);
		for(unsigned int i = 0; i < colorTableGrayIndexed8.size(); ++i)
			colorTableGrayIndexed8[i] = qRgb(i,i,i);
	}

	return colorTableGrayIndexed8;
}

QVector<QRgb> Tools::getColorTableGrayIndexed8(int blackPoint, int whitePoint)
{
	// Create table
	static QVector<QRgb> colorTableIndexed8;	
	if(colorTableIndexed8.size() == 0)
		colorTableIndexed8.resize(256);

	// Set values
	for(int i = 0; i < blackPoint; ++i)
		colorTableIndexed8[i] = qRgb(0,0,0);
	float fbp = blackPoint, fwp = whitePoint;
	for(int i = blackPoint; i < whitePoint; ++i) {
		int grayLevel = ((static_cast<float>(i) - fbp) / (fwp - fbp)) * 255.0f + 0.5f;
		colorTableIndexed8[i] = qRgb(grayLevel,grayLevel,grayLevel);
	}
	for(int i = whitePoint; i < 256; ++i)
		colorTableIndexed8[i] = qRgb(255,255,255);

	return colorTableIndexed8;
}

QVector<QRgb> Tools::getColorTableGrayIndexed8(int blackPoint, int whitePoint, QColor color)
{
	// Create table
	static QVector<QRgb> colorTableIndexed8;	
	if(colorTableIndexed8.size() == 0)
		colorTableIndexed8.resize(256);

	// Get r, g, b
	int r = color.red(),
		g = color.green(),
		b = color.blue();

	// Set values
	for(int i = 0; i < blackPoint; ++i)
		colorTableIndexed8[i] = qRgba(0,0,0,0);		// Transparent
	float fbp = blackPoint, fwp = whitePoint;
	for(int i = blackPoint; i < whitePoint; ++i) {
		int grayLevel = ((static_cast<float>(i) - fbp) / (fwp - fbp)) * 255.0f + 0.5f;
		colorTableIndexed8[i] = qRgba(r,g,b, grayLevel);
	}
	for(int i = whitePoint; i < 256; ++i)
		colorTableIndexed8[i] = qRgba(r,g,b, 255);	// Full color

	return colorTableIndexed8;
}

int Tools::calcPointFDistanceRectFToBorder( const QPointF& _point, const QRectF& _rect )
{
	int a = qMin(qAbs(_point.x() - _rect.left()), qAbs(_point.x() - _rect.right()));
	int b = qMin(qAbs(_point.y() - _rect.top()), qAbs(_point.y() - _rect.bottom()));
	return qMin(a, b);
}

QString Tools::formatByteNumber( qint64 _numBytes )
{
	// Check input
	if(_numBytes <= 0)
		return QString("0 B");

	double tmp = _numBytes;
	QStringList extensions;
	extensions << "B" << "KB" << "MB" << "GB" << "TB";
	QString extension;
	for(int i = 0; i < 5; ++i) {
		if(tmp < 1000 || i >= 4) {
			extension = extensions[i];
			break;
		}

		tmp /= 1024.0;
	}

	return QString("%1 %2").arg(tmp, 0, 'f', 2).arg(extension);
}

int Tools::getTagLength( const QString& string, const QString& tag )
{
	int len = 0;
	int curIndex = string.lastIndexOf(tag);
	if(curIndex >= 0) {
		curIndex += tag.length();
		while(curIndex < string.length() && string[curIndex++].isDigit())
			++len;
	}
	return len;
}

QCursor Tools::createCircleCursor (int diameter, QColor color, int thickness)
{
	int diam = diameter;

	//there seems to be no restriction on cursor icon sizes... (neither Windows nor Linux)
	//thus, simply create a cursor with the desired width
	//add 2 to receive a nice circle; otherwise flattened at the right & bottom margin
	int width = diam + 2;

	//create pixmap with a transparency mask
	QPixmap mousePix (width, width);
	mousePix.fill (Qt::transparent);
	QPainter p (&mousePix);
	p.setPen (QPen (color, thickness));  //non-transparent
	p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
	p.drawPoint (diam / 2, diam / 2);         //draw center point
	p.end();

	//set all pixels transparent but these on the circle line with the given radius
	QBitmap mask (width, width);
	mask.fill (Qt::color0); //transparent
	p.begin (&mask);
	p.setPen (QPen (Qt::color1, thickness));  //non-transparent
	p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
	p.drawPoint (diam / 2, diam / 2);         //draw center point
	p.end();

	mousePix.setMask (mask);

	//the hot spot is set in the middle automatically
	return QCursor (mousePix);
}

void Tools::msleep( unsigned long ms )
{
	Sleeper::msleep(ms);
}

void Tools::displayMessageBoxWithOpenFolder(QString _message, QString _caption, QString _folder, bool _folderIncludesFile /*= false*/, const QWidget* _parent /*= 0*/)
{
	// Show messagebox with possibility to open folder
	QMessageBox msgBox;

	// Default caption
	if(_caption.isEmpty())
		_caption = "TTT";

	// Include message
	msgBox.setWindowTitle(_caption);
	msgBox.setText(_message);

	// Add buttons
	QPushButton *okButton = msgBox.addButton("Ok", QMessageBox::AcceptRole);
	msgBox.setDefaultButton(okButton);
	QPushButton *openFolderButton = msgBox.addButton("Open Folder", QMessageBox::ActionRole);

	// Display messagebox
	msgBox.exec();

	// Open folder if desired
	if(msgBox.clickedButton() == openFolderButton) {
		QString nativeMovieFolder = QDir::toNativeSeparators(_folder);
		QString commandLineArguments;
		if(_folderIncludesFile)
			// Opens the folder and selects the file
			commandLineArguments = QString("/select,") + nativeMovieFolder;
		else
			// Just open the folder
			commandLineArguments = nativeMovieFolder;

		//QProcess::execute("Explorer.exe", QStringList(commandLineArguments));
		QProcess::execute(QString("Explorer.exe ") + commandLineArguments);
	}
}

QImage Tools::openGrayScaleImage(const QString& imageFileName, int bp, int wp, QString* outError)
{
	QImage img;
	if(imageFileName.toLower().right(4) == ".tif") {
		//qDebug() << "Loading tiff " << imageFileName;

		// Use own tiff loader
		QFile device(imageFileName);
		if(device.open(QIODevice::ReadOnly)) {
			CatTiffHandler tiffLoader(bp, wp);
			tiffLoader.setDevice(&device);
			if(!tiffLoader.read(&img)) {
				if(outError)
					*outError = "Tiff import failed.";
				return QImage();
			}
			//// DEBUG
			//else {
			//	img.save(imageFileName.left(imageFileName.length() - 3) + "png");
			//}
		}
		else {
			if(outError)
				*outError = QString("Cannot open file: ") + device.errorString();
			return QImage();
		}
	}
	else if(imageFileName.toLower().right(4) == ".png") {
		// Use own PNG loader
		int bitDepth, width, height;
		unsigned char* imageData = 0;
		int errCode = openPngGrayScaleImage(QDir::toNativeSeparators(imageFileName).toLatin1(), &imageData, bitDepth, width, height, true);	// Open background image, too

		// Check for error
		if(errCode || !imageData) {
			if(imageData)
				delete[] imageData;
			if(outError)
				*outError = QString("Cannot open file, error code: %1").arg(errCode);
			return QImage();
		}

		// Convert 
		if(bitDepth == 8) {
			// Just copy
			img = QImage(width, height, QImage::Format_Indexed8);
			if(img.isNull()) {
				if(outError)
					*outError = QString("Cannot allocate memory for image.");
				delete[] imageData;
				return QImage();
			}
			img.setColorTable(getColorTableGrayIndexed8());
			for(int y = 0; y < height; ++y)
				memcpy(img.scanLine(y), imageData + y*width, width);
		}
		else if(bitDepth == 16) {
			// Need to scale down to 8 bit
			img = QImage(width, height, QImage::Format_Indexed8);
			if(img.isNull()) {
				if(outError)
					*outError = QString("Cannot allocate memory for image.");
				delete[] imageData;
				return QImage();
			}
			img.setColorTable(getColorTableGrayIndexed8());

			// Determine bp and wp if necessary
			unsigned short* imageDataShort = reinterpret_cast<unsigned short*>(imageData);
			if(bp < 0 || wp < 0 || wp < bp) {
				bp = 0xFFFF;
				wp = 0;
				for(int y = 0; y < height; ++y) {
					unsigned short* curScanLine = imageDataShort + y*width;
					for(int x = 0; x < width; ++x) {
						bp = std::min(curScanLine[x], static_cast<unsigned short>(bp));
						wp = std::max(curScanLine[x], static_cast<unsigned short>(wp));
					}
				}
			}
			assert(wp >= bp);

			// Convert data
			float factor =  255.0f / (wp - bp);
			for(int y = 0; y < height; ++y) {
				unsigned char* curScanLineDst = img.scanLine(y);
				const unsigned short* curScanLineSrc = imageDataShort + y*width;
				for(int x = 0; x < width; ++x) {
					int curval = curScanLineSrc[x];
					curval = (curval - bp) * factor + 0.5f;
					curval = std::max(curval, 0);
					curval = std::min(curval, 255);
					curScanLineDst[x] = curval;
				}
			}
		}
		else if(bitDepth == 1) {
			// Scale up
			QImage tmp(imageData, width, height, QImage::Format_Mono);
			img = tmp.convertToFormat(QImage::Format_Indexed8);
			img.setColorTable(getColorTableGrayIndexed8());
		}
		else {
			if(outError)
				*outError = QString("Bitdepth not supported: %1").arg(bitDepth);
			delete[] imageData;
			return QImage();
		}
		delete[] imageData;
	}
	else {
		// Image is not tiff, so use QTs default implementation
		QImageReader imgReader(imageFileName);
		img = imgReader.read();
		if(img.isNull()) {
			if(outError)
				*outError= imgReader.errorString();
			return QImage();
		}
	}

	return img;
}

int Tools::loadSegmentationFromPNG(const QString& imageFileName, std::vector<Blob>& outSegmentationData, int expectedSizeX, int expectedSizeY, int foreGroundColor, int foreGoundPixelConnectivity)
{
	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Load data
	unsigned char* imageData = nullptr;
	int bitDepth, sizeX, sizeY;
	int err = openPngGrayScaleImage(imageFileName.toLatin1(), &imageData, bitDepth, sizeX, sizeY);
	if(err != 0 || !imageData || sizeX <= 0 || sizeY <= 0) {
		gErr << "Error: found file, but cannot open segmentation file " << imageFileName << " - error: " << err << ".\n";
		delete[] imageData;
		return 2;
	}

	// Check image size
	if(expectedSizeX >= 0 && expectedSizeY >= 0) {
		if(sizeX != expectedSizeX || sizeY != expectedSizeY) {
			delete[] imageData;
			return 3;
		}
	}

	// Convert to list of blobs
	if(bitDepth == 16) {
		// Assume label map with 0 for background - convert to 32 bit label map and extract blobs
		const unsigned short* labelMap16 = reinterpret_cast<unsigned short*>(imageData);
		unsigned int* labelMap32 = new unsigned int[sizeX * sizeY];
		for(int y = 0; y < sizeY; ++y) {
			for(int x = 0; x < sizeX; ++x) 
				labelMap32[y * sizeX + x] = labelMap16[y * sizeX + x];
		}
		ImageProcessingTools imageProcessingTools;
		outSegmentationData = imageProcessingTools.convertUInt32LabelMapToBlobs(labelMap32, sizeX, sizeY);
		delete[] labelMap32;
	}
	else if(bitDepth == 8) {
		// Assume mask image with 0 for background and provided value for foreground
		cv::Mat cvImage(sizeY, sizeX, CV_8UC1, imageData);	// no deep copy
		ImageProcessingTools imageProcessingTools;
		outSegmentationData = imageProcessingTools.convertBinaryMatToBlobs(cvImage, foreGroundColor, foreGoundPixelConnectivity);
	}
	else {
		// Unexpected bit depth
		gErr << "Error: segmentation image " << imageFileName << " has unexpected bit depth: " << bitDepth << ".\n";
		delete[] imageData;
		return 2;
	}

	// Clean up
	delete[] imageData;

	// Success
	return 0;
}
