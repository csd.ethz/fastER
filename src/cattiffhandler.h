/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cattiffhandler_h__
#define cattiffhandler_h__

#include <QImageIOHandler>



class CatTiffHandler : public QImageIOHandler
{
public:
	CatTiffHandler(int blackPoint = -1, int whitePoint = -1);

	bool canRead() const;
	bool read(QImage *image);
	bool write(const QImage &image);

	QByteArray name() const;

	static bool canRead(QIODevice *device);

	QVariant option(ImageOption option) const;
	void setOption(ImageOption option, const QVariant &value);
	bool supportsOption(ImageOption option) const;

	enum Compression {
		NoCompression = 0,
		LzwCompression = 1
	};
private:
	void convert32BitOrder(void *buffer, int width);
	void convert32BitOrderBigEndian(void *buffer, int width);
	int compression;

	// Blackpoint/whitepoint for 16bit grayscale
	int m_blackPoint;
	int m_whitePoint;
};

#endif // cattiffhandler_h__
