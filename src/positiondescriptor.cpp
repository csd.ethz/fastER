/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positiondescriptor.h"

// Qt
#include <QStringList>
#include <QDir>

// Project
#include "tools.h"
#include "experiment.h"
#include "exceptionbase.h"
#include "settings.h"
#include "pngio.h"


PositionDescriptor::PositionDescriptor(const QString& _folder, int _posNumber, Experiment *_experiment)
	: folder(_folder), posNumber(_posNumber)
{
	if(folder.right(1) != "/")
		folder.append('/');

	// Calc basename
	m_baseName = folder.left(folder.length() - 1);
	int i = m_baseName.lastIndexOf('/');
	if(i != -1) {
		m_baseName = m_baseName.mid(i+1);
	}

	experiment = _experiment;
	positionInformationAvailalbe = false;
	m_numExportedTreeFragments = 0;
	//positionInitialized = false;
}

PositionDescriptor::~PositionDescriptor()
{
	//// Clear images
	//for(int i = 0; i < images.size(); ++i) 
	//	delete images[i];
	//images.clear();
}

//
//void PositionDescriptor::setPositionInformation( float _left, float _top )
//{
//	left = _left;
//	top = _top;
//
//	positionInformationAvailalbe = true;
//}

//QPointF PositionDescriptor::transformLocalToGlobalCoords( const QPointF& _coords, int _wl ) const
//{
//	// Return value
//	float x = _coords.x();
//	float y = _coords.y();
//
//	// Transform to micrometer
//	float mmpp = experiment->getMicroMetersPerPixel(_wl);
//	x *= mmpp;
//	y *= mmpp;
//
//	// Transform to global
//	if(Settings::get<bool>("invertedCoordinates")) {
//		// Inverted style: x/y increase to left/top
//		x = left - x;
//		y = top - y;
//	}
//	else {
//		x += left;
//		y += top;
//	}
//
//	return QPointF(x, y);
//}
//
//
//QPoint PositionDescriptor::transformGlobalToLocalCoords( const QPointF& _coords ) const
//{
//	float x, y;
//
//	// Transform to local
//	if(Settings::get<bool>(Settings::KEY_INVERTEDCOORDINATES)) {
//		x = left - _coords.x();
//		y = top - _coords.y();
//	}
//	else {
//		x = _coords.x() - left;
//		y = _coords.y() - top;
//	}
//
//	// Transform to pixels
//	x /= experiment->getMicroMetersPerPixel();
//	y /= experiment->getMicroMetersPerPixel();
//
//	return QPoint(x + 0.5f, y + 0.5f);
//}


//const QList<PositionDescriptor*>& PositionDescriptor::getAdjacentPositions() const
//{
//	return adjacentPositions;
//}
//
//void PositionDescriptor::addAdjacentPosition( PositionDescriptor* _pos )
//{
//	if(!_pos || _pos == this)
//		return;
//
//	adjacentPositions.append(_pos);
//}

//QRectF PositionDescriptor::getGlobalImageRect( unsigned int _wl /*= 0*/ ) const
//{
//	// Check wavelength
//	if(_wl == 0) {
//		// Calc wl0 image rect
//		float mmpp = experiment->getMicroMetersPerPixel();
//		float w = experiment->getImageSizeX() * mmpp,
//			h = experiment->getImageSizeY() * mmpp;
//
//		// Check if system is inverted
//		if(Settings::get<bool>(Settings::KEY_INVERTEDCOORDINATES)) 
//			// Inverted: origin is on bottom/right but position coordinates (top and left) still refer to top/left corner of position
//			return QRectF(left-w, top-h, w, h);
//		else
//			return QRectF(left, top, w, h);
//	}
//
//	// Return invalid rect
//	return QRectF();
//}
//
//const unsigned short* PositionDescriptor::getGainImage( int wl, bool loadIfNecessary )
//{
//	QMutexLocker lock(&m_gainImagesMutex);
//
//	// Make sure array is long enough
//	if(wl >= m_gainImages.size())
//		m_gainImages.resize(qMax(1,wl+1));
//
//	// Check if exists
//	unsigned short* data = m_gainImages[wl].get();
//	if(data)
//		return data;
//
//	// Does not exist
//	if(!loadIfNecessary)
//		return 0;
//
//	// Try to load
//	QString path = QString("%1/gain_w%2.png").arg(getBackgroundFolder()).arg(wl, 2, 10, QChar('0'));
//	int bitDepth;
//	int width, height;
//	if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&data, bitDepth, width, height, true) != 0) {
//		delete[] data;
//		return 0;
//	}
//
//	// Success
//	m_gainImages[wl] = std::unique_ptr<unsigned short[]>(data);
//	return data;
//}
