/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmsegmentationpreview.h"

// Qt
#include <QImage>
#include <QMessageBox>
#include <QElapsedTimer>

// Project
#include "settings.h"
#include "imageviewimageitem.h"
#include "exceptionbase.h"
#include "logging.h"
#include "changecursorobject.h"
#include "jobsegmentation.h"
#include "frmmainwindow.h"
#include "experiment.h"

//// Debug
//#include <Windows.h>
//#include <Psapi.h>


FrmSegmentationPreview::FrmSegmentationPreview( QWidget* parent /*= 0*/ )
	: QWidget(parent)
{
	// Init GUI
	m_ui.setupUi(this);
	//connect(m_ui.pbtRefresh, SIGNAL(clicked()), this, SLOT(updateSegmentationResults()));
	connect(m_ui.bgrDisplay, SIGNAL(buttonReleased(int)), this, SLOT(visualizeSegmentation()));
	//connect(m_ui.pbtShortHelp, SIGNAL(clicked()), this, SLOT(showShortInstructions()));
	connect(m_ui.pbtRefresh, SIGNAL(clicked()), this, SLOT(refreshSegmentationButtonClicked()));

	m_channelForPreview = -1;
	m_segmentationResultsImageSizeX = 0;
	m_segmentationResultsImageSizeY = 0;
	m_segmentationResultsLabelMap = nullptr;
	m_ui.lblPixelValue->setVisible(false);
}

void FrmSegmentationPreview::setImage(const QImage& image, const PictureIndex& pi)
{
	// Clear segmentation results
	clearSegmentationResults();

	// Convert and save original unchanged image
	m_originalImagePictureIndex = pi;
	m_originalImage = image.convertToFormat(QImage::Format_Indexed8);
	m_ui.imageView->getImageViewItem()->setImage(m_originalImage);
}


void FrmSegmentationPreview::updateSegmentationResults(const QString& saveResultToPath)
{
	try {

		// Check if faster segmentation is configured (if faster is used)
		if(Settings::getSegmentationOperations()->containsOperation("fastER") && (g_FrmMainWindow->getNumLabeledCellExtremalRegions() == 0)) {
			//m_ui.imageView->getImageViewItem()->setImage(QImage());
			QString msg = "<h3>Label at least one cell to see segmentation results in this window.</h3>";
			//QString msg = "<h3>Quick Instructions</h3>" 
			//	"<p><ul>"
			//	"<li><b>Mouse wheel:</b> change zoom"
			//	"<li><b>Right mouse button:</b> hold and drag to scroll image"
			//	"<li><b>Left mouse button:</b> label cell"
			//	"<li><b>Ctrl+left mouse button:</b> label background"
			//	"<li><b>Shift+left mouse button:</b> erase labels"
			//	"<li><b>Ctrl+mouse wheel:</b> change brush size"
			//	"</ul>"
			//	"Label at least one cell and one background region to see segmentation results.<br>"
			//	"You need to label at least 10 cells to obtain reasonable results, and segmentation quality will"
			//	"<br>improve as you add more labels. If segmentation still looks bad after labeling 20 cells or more, check"
			//	"<br>the manual for troubleshooting."
			//	"<br><br>You can access these instructions any time by clicking on the \"Quick Instructions\" button on the bottom right.";
			//if(g_FrmMainWindow->getNumForegroundLabels() > 0 && g_FrmMainWindow->getNumBackgroundLabels() > 0 && g_FrmMainWindow->getNumLabeledBackgroundExtremalRegions() == 0)
			//	msg += "<br><br><b>Note: no candidate regions found in the labeled background areas - keep labeling background to see segmentation results!</b>";
			//msg += "</p>";
			m_ui.imageView->showMessage(msg, true);

			// Clear segmentation results
			clearSegmentationResults();

			return;
		}
		m_ui.imageView->hideMessage();

		// Check if we have an image
		if(m_originalImage.isNull())
			return;

		ChangeCursorObject cc;

		// Create a copy to work on
		if(m_imageBuffer.rows != m_originalImage.height() || m_imageBuffer.cols != m_originalImage.width())
			m_imageBuffer = cv::Mat(m_originalImage.height(), m_originalImage.width(), CV_8UC1);
		unsigned char* originalImageData = m_originalImage.bits();
		for(int y = 0; y < m_originalImage.height(); ++y) {
			//for(int x = 0; x < m_originalImage.width(); ++x) {
			//	m_imageBuffer.data[y*m_originalImage.width() + x] = originalImageData[y*m_originalImage.bytesPerLine() + x];
			//}

			memcpy(m_imageBuffer.data + y * m_originalImage.width(), originalImageData + y * m_originalImage.bytesPerLine(), m_originalImage.width());
		}

		// Measure time
		QElapsedTimer t;
		t.start();

		// Determine connectivity
		SegmentationOperationList* segOperations = Settings::getSegmentationOperations().get();
		int connectivity = segOperations ? segOperations->getPixelConnectivity() : 4;

		// Get current segmentation pipeline and run it on current image
		JobSegmentation job(0, Settings::getSegmentationOperations(), m_imageBuffer.data, m_imageBuffer.cols, m_imageBuffer.rows, &m_faster, &m_imageProcessingTools, m_originalImagePictureIndex, true);
		job.run(nullptr);

		// Measure time
		int elapsedTimeMs = t.elapsed();

		// Clear segmentation results
		clearSegmentationResults();

		// Get segmentation as blobs
		if(job.getImageDataType() != IDT_UInt8Gray) {
			// Image type not graylevel anymore -> has been segmented
			m_processedImage = QImage();
			m_segmentationResultsLabelMap = reinterpret_cast<unsigned int*>(job.getImageDataCopy(IDT_UInt32LabelMap));			
			m_segmentationResultsImageSizeX = job.getImageSizeX();
			m_segmentationResultsImageSizeY = job.getImageSizeY();
			m_segmentationResultsList = m_imageProcessingTools.convertUInt32LabelMapToBlobs(m_segmentationResultsLabelMap, m_segmentationResultsImageSizeX, m_segmentationResultsImageSizeY);
		}
		else {
			// Image not segmented -> clear segmentation results, but store processed image

			unsigned char* segResults = job.getImageDataCopy(IDT_UInt8Gray);	
			qDebug() << "Image data1: " << ((void*)segResults) << " - first: " << (segResults)[0];
			QImage tmp(segResults, job.getImageSizeX(), job.getImageSizeY(), QImage::Format_Indexed8);
			tmp.setColorTable(Tools::getColorTableGrayIndexed8());
			m_processedImage = tmp.copy();
			delete[] segResults;
		}

		// Update display
		m_ui.lblFoundCells->setText(QString::number(m_segmentationResultsList.size()));
		visualizeSegmentation(saveResultToPath);

		// Display message 
		if(g_FrmMainWindow->getFastERLogging()) {
			// Done
			QString msg;
			QTextStream s(&msg);
			bool darkOnBright;
			int minSize;
			int maxSize;
			double minProbability;
			bool disableErSplitting;
			Settings::getFastERParameters(darkOnBright, minSize, maxSize, minProbability, disableErSplitting);
			s << "Segmentation executed [Checked regions: " << m_faster.getNumCheckedRegions() << ", "; 
			s << "Too small: " << m_faster.getNumCheckedRegionsTooSmall() << "(minSize: " << minSize << "), "; 
			s << "Too big: " << m_faster.getNumCheckedRegionsTooBig() << "(maxSize: " << maxSize << "), "; 
			s << "Accepted by SVM: " << m_faster.getNumAcceptedRegionsBySvm() << ", ";
			s << "Rejected by SVM: " << m_faster.getNumRejectedRegionsBySvm() << ", ";
			s << "SVM used: " << (m_faster.getNumCheckedRegions() - m_faster.getNumCheckedRegionsTooBig() - m_faster.getNumCheckedRegionsTooSmall()) << ", "; 
			s << "Runtime (fastER only): " << elapsedTimeMs << "ms, ";
			s << "Found " << m_segmentationResultsList.size() << " regions, ";
			s << "DisableErSplitting: " << (disableErSplitting ? "on, " : "off, ");
			s << "Number of used support vectors: " << m_faster.getNumSupportVectors() << "]\n";
			gLog << msg;
		}

		//// Debug
		//if(blobs.size()) {
		//	double minorAxisLength, majorAxisLength, centroidX, centroidY, eccentricity, meanIntensity, intensityIntegral, heterogeneity;
		//	Statistics::calculateBlobFeatures(&blobs[0], minorAxisLength, majorAxisLength, centroidX, centroidY, eccentricity, m_originalImage.bits(), m_originalImage.bytesPerLine(), m_originalImage.width(), m_originalImage.height(), meanIntensity, intensityIntegral, heterogeneity);
		//}
	}
	catch(ExceptionBase& e) {
		gErr << "Exception in JobRunner::run(): " << e.what() << " - source: " << e.where() << "\n";
	}

}

FrmSegmentationPreview::~FrmSegmentationPreview()
{
}

void FrmSegmentationPreview::visualizeSegmentation(const QString& saveResultToPath )
{
	//if(!m_segmentationResults.size() && m_segmentationImage.isNull()) {
	//	return;
	//}

	// Just show bw mask if desired
	if(m_ui.optProcessedImage->isChecked()) {
		if(m_processedImage.isNull()) {
			QImage bw(m_segmentationResultsImageSizeX, m_segmentationResultsImageSizeY, QImage::Format_Indexed8);
			bw.setColorTable(Tools::getColorTableGrayIndexed8());
			bw.fill(0);
			unsigned char* data = bw.bits();
			int bpl = bw.bytesPerLine();
			for(auto itBlob = m_segmentationResultsList.cbegin(); itBlob != m_segmentationResultsList.cend(); ++itBlob) {
				const std::vector<Blob::pixel_type>& pixels = itBlob->getPixels();
				for(auto itPixel = pixels.cbegin(); itPixel != pixels.cend(); ++itPixel) {
					data[itPixel->y * bpl + itPixel->x] = 255;
				}
			}
			m_ui.imageView->getImageViewItem()->setImage(bw);

			// Export if desired
			if(!saveResultToPath.isEmpty()) {
				if(bw.save(saveResultToPath))
					QMessageBox::information(this, "Note", QString("Segmentation preview exported to:\n\n") + saveResultToPath);
			}
		}
		else {
			qDebug() << "Image data2: " << ((void*)m_processedImage.bits()) << " - first: " << (m_processedImage.bits())[0];
			m_ui.imageView->getImageViewItem()->setImage(m_processedImage);

			// Export if desired
			if(!saveResultToPath.isEmpty()) {
				if(m_processedImage.save(saveResultToPath))
					QMessageBox::information(this, "Note", QString("Segmentation preview exported to:\n\n") + saveResultToPath);
			}
		}

		return;
	}

	// Make sure correct image to project segmentation on is displayed
	QImage previewImage;
	if(m_channelForPreview >= 0 && m_channelForPreview != m_originalImagePictureIndex.channel) {
		PictureIndex previewPicIndex = m_originalImagePictureIndex;
		previewPicIndex.channel = m_channelForPreview;
		QImage tmp;
		int err = g_FrmMainWindow->experiment()->getImage(previewPicIndex, tmp);
		if(err == 0 && !tmp.isNull())
			previewImage = tmp;
	}
	if(previewImage.isNull())
		previewImage = m_originalImage;
	m_ui.imageView->getImageViewItem()->setImage(previewImage);

	// If display mode is "none", show no segmentation
	if(m_ui.optNone->isChecked()) {
		if(!saveResultToPath.isEmpty()) {
			m_ui.imageView->getImageViewItem()->getImageDisplaySettings().applyDisplaySettings(previewImage);
			if(previewImage.save(saveResultToPath))
				QMessageBox::information(this, "Note", QString("Segmentation preview exported to:\n\n") + saveResultToPath);
		}
		return;
	}

	// Determine foreground color
	unsigned int foreGroundColor = qRgba(255, 255, 0, 255);

	// Create transparent overlay image and add blobs
	//QImage overlayImage = QImage(m_segmentationResults.size(), QImage::Format_ARGB32);
	//int overlayBytesPerLine = overlayImage.bytesPerLine();
	//unsigned char* overlayData = overlayImage.bits();
	//overlayImage.fill(qRgba(0, 0, 0, 0));
	//int bytesPerLine = m_segmentationResults.bytesPerLine();
	//const unsigned char* segData = m_segmentationResults.bits();
	//for(int y = 0; y < m_segmentationResults.height(); ++y) {
	//	for(int x = 0; x < m_segmentationResults.width(); ++x) {
	//		if(segData[y*bytesPerLine + x] == 255)
	//			*(((unsigned int*)(overlayData+y*overlayBytesPerLine))+x) = foreGroundColor;
	//	}
	//}

	// Show only perimeters or overlay of found blobs
	if(m_ui.optPerimeter->isChecked() || m_ui.optBlobs->isChecked()) {
		QImage overlayImage = ImageProcessingTools::visualizeLabelMap(m_segmentationResultsLabelMap, m_segmentationResultsImageSizeX, m_segmentationResultsImageSizeY, m_ui.optPerimeter->isChecked());
		m_ui.imageView->getImageViewItem()->setOverlayImage(overlayImage);

		// Export if desired
		if(!saveResultToPath.isEmpty()) {
			if(!m_originalImage.isNull() && !overlayImage.isNull()) {
				m_ui.imageView->getImageViewItem()->getImageDisplaySettings().applyDisplaySettings(m_originalImage);
				QPixmap exportImage = QPixmap::fromImage(m_originalImage);
				QPainter p(&exportImage);
				p.drawImage(QPointF(0, 0), overlayImage);

				QImage finalImage = exportImage.toImage();
				if(finalImage.save(saveResultToPath))
					QMessageBox::information(this, "Note", QString("Segmentation preview exported to:\n\n") + saveResultToPath);
			}
		}
	}
}

//void FrmSegmentationPreview::showShortInstructions()
//{
//	// Show simple help message box
//	QString msg = "<p>Label at least one cell and one background region to see segmentation results:<ul>"
//		"<li>Mouse wheel: change zoom"
//		"<li>Right mouse button: hold and drag to scroll image"
//		"<li>Left mouse button: label cell"
//		"<li>Ctrl+left mouse button: label background"
//		"<li>Shift+left mouse button: erase labels"
//		"<li>Ctrl+mouse wheel: change brush size"
//		"</ul>"
//		"You need to label at least 10 cells to obtain reasonable results, and segmentation quality will "
//		"improve as you add more labels. If segmentation still fails after labeling 20 cells or more, check "
//		"the manual for troubleshooting.";
//	QMessageBox::information(this, "Instructions", msg);
//}

void FrmSegmentationPreview::refreshSegmentationButtonClicked()
{
	if(g_FrmMainWindow && g_FrmMainWindow->m_frmSegPreview) {
		// Do full update (re-extract samples, learn SVM, etc.)

		// Update label statistics
		g_FrmMainWindow->updateLabelStatistics();

		// Get samples from labels
		g_FrmMainWindow->extractSamplesForLabels(true);

		// Train SVM and update segmentation
		g_FrmMainWindow->learnParametersSvm();
		g_FrmMainWindow->applySegmentationSettingsAndUpdatePreview();
	}
}

void FrmSegmentationPreview::showPixelValue(int x, int y, int grayLevel)
{
	if(grayLevel < 0) {
		m_ui.lblPixelValue->setVisible(false);
		return;
	}

	m_ui.lblPixelValue->setText(QString("x=%1, y=%2, value=%3").arg(x).arg(y).arg(grayLevel));
	m_ui.lblPixelValue->setVisible(true);
}

//QImage FrmSegmentationPreview::createOverlayImage(bool drawOnlyPerimeters) const
//{
//	// Time
//	QElapsedTimer td;
//	td.start();
//
//// Create empty image
//QImage img = QImage(m_segmentationResultsImageSizeX, m_segmentationResultsImageSizeY, QImage::Format_ARGB32);
//int bpl = img.bytesPerLine();
//img.fill(qRgba(0, 0, 0, 0));
//unsigned char* data = img.bits();
//for(auto itRegion = m_segmentationResults.cbegin(); itRegion != m_segmentationResults.cend(); ++itRegion) {
//	// Determine color: random, but not too dark
//	const int offset = 100;
//	int r1 = qrand() % (255 - offset);
//	int r2 = qrand() % (255 - offset);
//	int r3 = qrand() % (255 - offset);
//	unsigned int color = qRgba(offset+r1, offset+r2, offset+r3, 255);

//	const std::vector<Blob::pixel_type>& pixels = itRegion->getPixels();
//	for(auto itPixel = pixels.cbegin(); itPixel != pixels.cend(); itPixel++) {
//		if(drawOnlyPerimeters) {
//			// Only draw perimeter pixels: check if there is at least one neighbor which does not belong to current region
//			int deltaX[] = {1, -1, 0, 0, 1, 1, -1, -1};
//			int deltaY[] = {0, 0, 1, -1, 1, -1, 1, -1};
//			bool pixelHasNeighborOutsideRegion = false;
//			for(int iNeighbor = 0; iNeighbor < 8; ++iNeighbor) {
//				int xN = itPixel->x + deltaX[iNeighbor];
//				int yN = itPixel->y + deltaY[iNeighbor];
//				if(xN >= 0 && yN >= 0 && xN < m_segmentationResultsImageSizeX && yN < m_segmentationResultsImageSizeY) {
//					if(std::find(pixels.cbegin(), pixels.cend(), Blob::pixel_type(xN, yN)) == pixels.cend()) {
//						// Neighbor is valid pixel and does not belong to region
//						pixelHasNeighborOutsideRegion = true;
//						break;
//					}
//				}
//			}
//			if(!pixelHasNeighborOutsideRegion)
//				continue;
//		}

//		// Set pixel
//		*(((unsigned int*)(data+itPixel->y*bpl))+itPixel->x) = color;
//	}
//}
//
//	gLog << "FrmSegmentationPreview::createOverlayImage() - elapsed time: " << td.elapsed() << "ms \n";
//
//	return img;
//}
