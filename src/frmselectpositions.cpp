/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmselectpositions.h"

// Qt
#include<QMessageBox>
#include<QSignalMapper>

// Project
#include "frmmainwindow.h"
#include "settings.h"
#include "experiment.h"


FrmSelectPositions::FrmSelectPositions(QWidget* parent /*= 0*/)
	: QWidget(parent)
{
	// Init GUI
	m_ui.setupUi(this);
	m_ui.positionDisplay->setEnabled(true);

	// Init variables
	m_needInitialization = true;

	// Signals/slots
	connect(m_ui.positionDisplay, SIGNAL (positionLeftClicked (int)), this, SLOT (positionLeftClicked (int)));
	connect(m_ui.pbtSelectRegion, SIGNAL(clicked(bool)), m_ui.positionDisplay, SLOT(setRegionSelectionMode(bool)));
	connect(m_ui.positionDisplay, SIGNAL(zoomChanged(float)), this, SLOT(positionDisplayZoomChanged(float)));
	connect(m_ui.pbtUpdateThumbnails, SIGNAL(clicked()), this, SLOT(updateThumbnails()));
	connect(m_ui.pbtCancel, SIGNAL(clicked()), this, SLOT(close()));
	connect(m_ui.pbtOk, SIGNAL(clicked()), this, SLOT(okClicked()));
	connect(m_ui.chkInvertedSystem, SIGNAL(clicked(bool)), this, SLOT(setInvertedSystem(bool)));

	connect (m_ui.cboOcularFactor, SIGNAL (activated (const QString&)), this, SLOT (setOcularFactor (const QString&)));
	QSignalMapper* tvSignalMapper = new QSignalMapper(this);
	connect ( m_ui.optTVFactor040, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping ( m_ui.optTVFactor040, "040");
	connect ( m_ui.optTVFactor050, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping ( m_ui.optTVFactor050, "050");
	connect ( m_ui.optTVFactor063, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping ( m_ui.optTVFactor063, "063");
	connect ( m_ui.optTVFactor070, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping ( m_ui.optTVFactor070, "070");
	connect ( m_ui.optTVFactor1, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping ( m_ui.optTVFactor1, "1");
	connect ( tvSignalMapper, SIGNAL (mapped (const QString &)), this, SLOT (setTVAdapterFactor (const QString &)));		

}

void FrmSelectPositions::showEvent( QShowEvent* ev )
{
	if(!ev->spontaneous()) {
		// Update display
		if(m_needInitialization) {
			m_ui.positionDisplay->initialize(g_FrmMainWindow->experiment(), true, false, true);
			setTvAdapterFactorAndOcularFactorInGui();
			m_needInitialization = false;
		}
	}
}

void FrmSelectPositions::positionLeftClicked( int _index )
{
	// Toggle selection
	if(m_ui.positionDisplay->isPositionSelected(_index))
		m_ui.positionDisplay->deSelectPosition(_index);
	else
		m_ui.positionDisplay->selectPosition(_index);
}

void FrmSelectPositions::positionDisplayZoomChanged( float _scaling )
{
	// Update zoom label
	m_ui.lblZoom->setText(QString("%1%").arg(static_cast<int>(_scaling*100+0.5f)));
}

void FrmSelectPositions::setTvAdapterFactorAndOcularFactorInGui()
{
	// Set ocular factor
	int ocular_factor = Settings::get<int>("ocularFactor");
	m_ui.cboOcularFactor->setCurrentText (QString ("%1x").arg (ocular_factor));

	// Set tv factor
	double tvFactor = Settings::get<double>("tvFactor");
	if (qFuzzyCompare(tvFactor, 1.0))
		m_ui.optTVFactor1->setChecked (true);
	else if (qFuzzyCompare(tvFactor, 0.70))
		m_ui.optTVFactor070->setChecked (true);
	else if (qFuzzyCompare(tvFactor, 0.63))
		m_ui.optTVFactor063->setChecked (true);
	else if (qFuzzyCompare(tvFactor, 0.50))
		m_ui.optTVFactor050->setChecked (true);
	else if (qFuzzyCompare(tvFactor, 0.40))
		m_ui.optTVFactor040->setChecked (true);
	else
		m_ui.optTVFactor1->setChecked (true);
}

void FrmSelectPositions::updateThumbnails()
{
	// Set timepoint and wl in position display and update thumbnails if they were actually changed
	bool requiresUpdate = false;
	if(m_ui.positionDisplay->getThumbnailsTimePoint() != m_ui.spbThumbnailsTimePoint->value()) {
		m_ui.positionDisplay->setThumbnailsTimePoint(m_ui.spbThumbnailsTimePoint->value());
		requiresUpdate = true;
	}
	if(m_ui.positionDisplay->getThumbnailsWavelength() != m_ui.spbWaveLength->value()) {
		m_ui.positionDisplay->setThumbnailsWavelength(m_ui.spbWaveLength->value());
		requiresUpdate = true;
	}
	if(!requiresUpdate)
		return;

	// Remember zoom, position (center) and selected positions
	float oldScaling = m_ui.positionDisplay->getCurrentScaling();
	QPointF curCenter = m_ui.positionDisplay->mapToScene(m_ui.positionDisplay->width() / 2, m_ui.positionDisplay->height() / 2);
	QList<int> selectedPositions = m_ui.positionDisplay->getActivePositionNumbers();

	// Call initialize to apply changes
	m_ui.positionDisplay->initialize(g_FrmMainWindow->experiment(), true, false, true);

	// Re-apply selected positions, zoom  and position
	for(auto it = selectedPositions.begin(); it != selectedPositions.end(); ++it) 
		m_ui.positionDisplay->selectPosition(*it);
	float newScaling = m_ui.positionDisplay->getCurrentScaling();
	if(newScaling > 0 && oldScaling > 0)
		m_ui.positionDisplay->scaleView(oldScaling / newScaling);
	if(!curCenter.isNull())
		m_ui.positionDisplay->centerOn(curCenter);
}

void FrmSelectPositions::okClicked()
{
	// Get selected positions
	QList<int> selection = m_ui.positionDisplay->getActivePositionNumbers();
	if(selection.size() == 0) {
		QMessageBox::information(this, "Note", "You must select at least one position.");
		return;
	}

	// Save selection
	QList<QVariant> selectionVariants;
	for(auto it = selection.begin(); it != selection.end(); ++it)
		selectionVariants.append(QVariant(*it));
	Settings::set(Settings::KEY_SELECTEDPOSITIONS, selectionVariants);

	// Update display in main window and select a random image if no image is currently being displayed
	g_FrmMainWindow->updateDisplayOfSelectedPositions();
	if(!g_FrmMainWindow->getCurrentPreviewImage().isValid())
		g_FrmMainWindow->randomPreviewPic();

	// Close form
	close();
}

void FrmSelectPositions::setInvertedSystem(bool val)
{
	// Update experiment
	//Settings::set(Settings::KEY_INVERTEDCOORDINATES, val);
	g_FrmMainWindow->experiment()->setCoordinatesInverted(val);

	// Update position view
	m_ui.positionDisplay->updateDisplay();
}

void FrmSelectPositions::setTVAdapterFactor(const QString& _factor)
{
	double factor = 1.0;
	if(_factor == "040")
		factor = 0.40;
	else if(_factor == "050") 
		factor = 0.50;
	else if (_factor == "063")
		factor = 0.63;
	else if (_factor == "070")
		factor = 0.70;
	else if (_factor == "1")
		factor = 1.0;

	// Update mmpp
	Settings::set("tvFactor", factor);
	g_FrmMainWindow->experiment()->updateMicroMeterPerPixel();

	// Update position view
	m_ui.positionDisplay->updateDisplay();
}

void FrmSelectPositions::setOcularFactor(const QString &_text)
{
	if (! _text.contains ("x"))
		return;

	//cut off the "x" before converting to int, otherwise it would be 0 all the time
	float ocularFactor = _text.left (_text.length() - 1).toInt();

	// Update mmpp
	Settings::set("ocularFactor", ocularFactor);
	g_FrmMainWindow->experiment()->updateMicroMeterPerPixel();

	// Update position view
	m_ui.positionDisplay->updateDisplay();
}
