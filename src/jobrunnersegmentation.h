/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobrunnersegmentation_h__
#define jobrunnersegmentation_h__

// Project
#include "jobrunner.h"

namespace faster {
	class FastERDetector;
}
class ImageProcessingTools;


class JobRunnerSegmentation : public JobRunner {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	JobRunnerSegmentation(JobSet* jobs, JobRunnerMaster* masterJobRunner);

	/**
	 * Destructor.
	 */
	~JobRunnerSegmentation();

	/**
	 * Get detector.
	 */
	faster::FastERDetector* getFastERDetector() const {
		return m_fasterDetector;
	}

	/**
	 * Get processing tools.
	 */
	ImageProcessingTools* getImageProcessingTools() const {
		return m_imageProcessingTools;
	}

private:

	// fastER detector, needed once per thread
	faster::FastERDetector* m_fasterDetector;

	// Image processing tools, needed once per thread
	ImageProcessingTools* m_imageProcessingTools;
};


#endif // jobrunnersegmentation_h__
