/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationlist_h__
#define segmentationoperationlist_h__

// Project
#include "segmentationoperation.h"

// STL
#include <vector>

// QT
#include <QString>


/**
 * A list of SegmentationOperation instances. Always has ownership of all contained operations.
 */
struct SegmentationOperationList {

	// Constructor, initializes variables
	SegmentationOperationList() 
		: maskIndex(0)
	{}

	// Segmentation operations
	std::vector<SegmentationOperation*> operations;

	/**
	 * Destructor. Deletes all operations.
	 */
	~SegmentationOperationList();

	/**
	 * Delete all operations.
	 */
	void clear();

	/**
	 * Return string with all operations and their parameters.
	 */
	QString toString() const;

	/**
	 * Index of segmentation method (used e.g. in segmentation mask files).
	 */
	int maskIndex;

	/**
	 * Get foreground pixel connectivity for this SegmentationOperationList.
	 */
	int getPixelConnectivity() const 
	{
		return getPixelConnectivityFromSegmentationMaskIndex(maskIndex);
	}

	/**
	 * Get if pipeline contains operation with specified name.
	 */
	bool containsOperation(const QString& opName) 
	{
		for(auto it = operations.cbegin(); it != operations.cend(); ++it) {
			if((*it)->getName() == opName)
				return true;
		}
		return false;
	}

	/**
	 * Get operation with specified name, if in pipeline.
	 */
	SegmentationOperation* getOperation(const QString& opName) 
	{
		for(auto it = operations.cbegin(); it != operations.cend(); ++it) {
			if((*it)->getName() == opName)
				return *it;
		}
		return nullptr;
	}

	/**
	 * Delete all operations with specified name from pipeline.
	 */
	void removeOperation(const QString& opName)
	{
		auto it = operations.begin();
		while(it != operations.end()) {
			if((*it)->getName() == opName)
				it = operations.erase(it);
			else
				++it;
		}
	}

	/**
	 * Get foreground pixel connectivity for segmentation mask index.
	 */
	static int getPixelConnectivityFromSegmentationMaskIndex(int _maskIndex)
	{
		// Currently only methods with mask indexes 0 (fastER) and 50 (ilastik import) use 4-connectivity
		if(_maskIndex == 0 || _maskIndex == 50)
			return 4;
		else
			return 8;
	}
};


#endif // segmentationoperationlist_h__
