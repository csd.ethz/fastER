/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef changecursorobject_h__
#define changecursorobject_h__

/**
	@author Oliver Hilsenbeck
	
	Simple class that changes the application cursor when created and restores it when being destroyed.
	This can be used to change the cursor and ensure that it will be changed back, even in case of exceptions.
*/

// Qt
#include <QApplication>
#include <QWidget>

class ChangeCursorObject {
public:

	/**
	 * Constructor, changes cursor
	 * @param _cursor the desired cursor, WaitCursor by default
	 */
	ChangeCursorObject( const QCursor& cursor = QCursor(Qt::WaitCursor), QWidget* widget = nullptr ) ;

	/**
	 * Destructor, restored cursor
	 */
	~ChangeCursorObject() {
		// Restore cursor
		restoreCursor();
	}

	/**
	 * Restore cursor, can be used to restore cursor before destruction of object.
	 */
	void restoreCursor();

private:

	// Widget whose cursor was changed, can be nullptr
	QWidget* m_widget;

	// Old cursor of widget (only set if m_widget is set)
	QCursor m_oldCursor;

	// If old cursor has been restored already
	bool m_cursorRestored;

	// Wait cursor handle and previous cursor handle (only used on Windows for Qt::WaitCursor cursors)
#ifdef Q_OS_WIN
	HCURSOR m_waitCursorHandle;
	HCURSOR m_previousCursorHandle;
#endif
};


#endif // changecursorobject_h__
