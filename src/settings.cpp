/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "settings.h"

// Project
#include "segmentationoperationlist.h"
#include "segmentationoperationfillholes.h"
#include "segmentationoperationsizefilter.h"
#include "segmentationoperationfaster.h"
#include "faster.h"

// STL
#include <cassert>


// Static elements
Settings* Settings::m_inst = 0;
const char* Settings::KEY_VERBOSE = "verbose";
const char* Settings::KEY_TVFACTOR = "tvFactor";
const char* Settings::KEY_OCULARFACTOR = "ocularFactor";
const char* Settings::KEY_SELECTEDPOSITIONS = "restrictPositions";
const char* Settings::KEY_STARTFRAME = "startFrame";
const char* Settings::KEY_STOPFRAME = "stopFrame";
const char* Settings::KEY_SCANBINFACTOR = "scanBinFactor";
const char* Settings::KEY_TRACKINGWL = "trackingWl";
const char* Settings::KEY_TRACKINGZINDEX = "trackingZIndex";
const char* Settings::KEY_TRACKINGAREA = "trackingArea";
const char* Settings::KEY_CHANGELOGVERSION = "showChangelogOnlyIfNewerThan";
const char* Settings::KEY_MEDIANPREPROCESSKERNELSIZE = "MEDIANPREPROCESSKERNELSIZE";
const char* Settings::KEY_ACTIVECONTOURCHANNEL = "ACTIVECONTOURCHANNEL";
const char* Settings::KEY_DENOISING_SIGMACOLOR = "KEY_DENOISING_SIGMACOLOR";
const char* Settings::KEY_DENOISING_SIGMASPACE = "KEY_DENOISING_SIGMASPACE";
const char* Settings::KEY_DENOISING_KERNELSIZE = "KEY_DENOISING_KERNELSIZE";
const char* Settings::KEY_DENOISING_SIGMACOLORFACTOR = "KEY_DENOISING_SIGMACOLORFACTOR";
const char* Settings::KEY_CONSTRAIN_SEGM_POS_SAMPLES = "KEY_CONSTRAIN_SEGM_POS_SAMPLES";
const char* Settings::KEY_SHOWHELPATSTART = "KEY_SHOWHELPATSTART";




Settings::Settings()
{
	// Set defaults
	addDefaultSettings();
	m_svmModel = 0;
	m_svmModelReady = false;
}

void Settings::addDefaultSettings()
{
	addDefaultSetting(KEY_VERBOSE, false, "Verbose mode");
	addDefaultSetting(KEY_TVFACTOR, QVariant(QVariant::Double), "TV factor");
	addDefaultSetting(KEY_OCULARFACTOR, QVariant(QVariant::Double), "Ocular factor");
	addDefaultSetting(KEY_SELECTEDPOSITIONS, QList<QVariant>(), "Restrict the number of positions to analyze (optional)");
	addDefaultSetting(KEY_STARTFRAME, QVariant(QVariant::Int), "Starting frame number (optional)");
	addDefaultSetting(KEY_STOPFRAME, QVariant(QVariant::Int), "Stopping frame number (optional)");	// Debug
	addDefaultSetting(KEY_SCANBINFACTOR, 1, "ScanBinFactor (optional)");
	addDefaultSetting(KEY_TRACKINGWL, 0, "Channel used for tracking (default: 0)");
	addDefaultSetting(KEY_TRACKINGZINDEX, 1, "Z-index used for tracking (default: 1)");
	addDefaultSetting(KEY_TRACKINGAREA, QList<QVariant>(), "Restrict the area to analyze (optional)");
	addDefaultSetting(KEY_CHANGELOGVERSION, QString("1.0.0"), "Display version info only for program versions newer than this (optional)");
	addDefaultSetting(KEY_MEDIANPREPROCESSKERNELSIZE, 3, "Kernel size of median filter preprocessing, set to 0 to disable (default: 3)");
	addDefaultSetting(KEY_ACTIVECONTOURCHANNEL, -1, "Image channel used for active contour - set to negative value to use original image data (default: -1)");
	addDefaultSetting(KEY_DENOISING_SIGMACOLOR, 10.0, "Bilateral filter sigma color.");
	addDefaultSetting(KEY_DENOISING_SIGMASPACE, 10.0, "Bilateral filter sigma space.");
	addDefaultSetting(KEY_DENOISING_KERNELSIZE, 7, "Bilateral filter kernel size.");
	addDefaultSetting(KEY_DENOISING_SIGMACOLORFACTOR, 1.0, "Factor to increase denoising strength.");
	addDefaultSetting(KEY_CONSTRAIN_SEGM_POS_SAMPLES, true, "Constrain segmentation to contain positive training samples.");

	// Default faster settings
	m_fasterMinSize = 10;
	m_fasterMaxSize = 100;
	m_fasterDob = true;
	m_fasterMinProb = 0.5;
	m_disableErSplitting = false;
}

void Settings::cleanUp()
{
	delete m_inst;
	m_inst = 0;
}

QString Settings::getDescription( const QString& key )
{
	if(!m_inst)
		m_inst = new Settings();
	return m_inst->m_descriptions.value(key);
}

Settings::~Settings()
{

}

void  Settings::trainSVM( std::vector<double>& posObs, std::vector<double>& negObs, const double statsMean[], const double statsStandardDeviation[], bool autoScale )
{
	if(!m_inst)
		m_inst = new Settings();

	// Set stats
	std::copy(statsMean, statsMean + faster::NUM_FEATURES, m_inst->m_statsMean);
	std::copy(statsStandardDeviation, statsStandardDeviation + faster::NUM_FEATURES, m_inst->m_statsStandardDeviation);

	// If not enough observations are provided, set flag that svm cannot be used
	if(posObs.size() == 0 || negObs.size() == 0) {
		m_inst->m_svmModelReady = false;
		return;
	}

	// Scale data
	if(autoScale) {
		for(int i = 0; i < posObs.size()/faster::NUM_FEATURES; ++i) {
			int offset = i*faster::NUM_FEATURES;
			for(int c = 0; c < faster::NUM_FEATURES; ++c) {
				posObs[offset+c] -= m_inst->m_statsMean[c];
				if(m_inst->m_statsStandardDeviation[c] > 1e-50)
					posObs[offset+c] /= m_inst->m_statsStandardDeviation[c];
			}
		}
		for(int i = 0; i < negObs.size()/faster::NUM_FEATURES; ++i) {
			int offset = i*faster::NUM_FEATURES;
			for(int c = 0; c < faster::NUM_FEATURES; ++c) {
				negObs[offset+c] -= m_inst->m_statsMean[c];
				if(m_inst->m_statsStandardDeviation[c] > 1e-50)
					negObs[offset+c] /= m_inst->m_statsStandardDeviation[c];
			}
		}
	}

	// Check input
	assert(posObs.size() % faster::NUM_FEATURES == 0 && negObs.size() % faster::NUM_FEATURES == 0);
	svm_problem* problem = &m_inst->m_svmTrainingProblem;

	// Determine number of samples
	int numSamplesPos = posObs.size() / faster::NUM_FEATURES;
	int numSamplesNeg = negObs.size() / faster::NUM_FEATURES;
	int numSamplesTotal = numSamplesPos + numSamplesNeg;

	// Put observations in weird SVMLib form
	m_inst->m_svmNodesArray.resize(numSamplesTotal);
	m_inst->m_svmTrainingData.resize(numSamplesTotal * (faster::NUM_FEATURES+1));
	int iSvmNodes = 0;
	int iSample = 0;

	// Positive observations
	for(int i = 0; i < numSamplesPos; ++i) {
		// Add nodes
		int offset = i*faster::NUM_FEATURES;
		for(int index = 1; index <= faster::NUM_FEATURES; ++index) {
			m_inst->m_svmTrainingData[iSvmNodes].index = index;
			m_inst->m_svmTrainingData[iSvmNodes++].value = posObs[offset + index - 1];
		}
		m_inst->m_svmTrainingData[iSvmNodes].index = -1;
		m_inst->m_svmTrainingData[iSvmNodes++].value = 0;

		// Add entry to nodes array
		m_inst->m_svmNodesArray[iSample] = m_inst->m_svmTrainingData.data() + iSample*(faster::NUM_FEATURES+1);
		++iSample;
	}

	// Negative observations
	for(int i = 0; i < numSamplesNeg; ++i) {
		// Add nodes
		int offset = i*faster::NUM_FEATURES;
		for(int index = 1; index <= faster::NUM_FEATURES; ++index) {
			m_inst->m_svmTrainingData[iSvmNodes].index = index;
			m_inst->m_svmTrainingData[iSvmNodes++].value = negObs[offset + index - 1];
		}
		m_inst->m_svmTrainingData[iSvmNodes].index = -1;
		m_inst->m_svmTrainingData[iSvmNodes++].value = 0;

		// Add entry to nodes array
		m_inst->m_svmNodesArray[iSample] = m_inst->m_svmTrainingData.data() + iSample*(faster::NUM_FEATURES+1);
		++iSample;
	}

	// Create array of target values
	m_inst->m_svmTargetValues.resize(0);
	m_inst->m_svmTargetValues.reserve(numSamplesTotal);
	for(int i = 0; i < numSamplesPos; ++i)
		m_inst->m_svmTargetValues.push_back(1);
	for(int i = 0; i < numSamplesNeg; ++i)
		m_inst->m_svmTargetValues.push_back(-1);

	// Now fill svm_problem thing
	problem->l = numSamplesTotal;
	problem->y = m_inst->m_svmTargetValues.data();
	problem->x = m_inst->m_svmNodesArray.data();

	// Set parameters
	svm_parameter* p = &m_inst->m_svmParameters;
	p->svm_type = C_SVC;

	//p->kernel_type = LINEAR;
	//p->degree = 3;
	//p->coef0 = 0;
	//p->gamma = 1.0 / faster::NUM_FEATURES;

	//p->kernel_type = SIGMOID;
	//p->degree = 3;
	//p->coef0 = 0;
	//p->gamma = 1.0 / faster::NUM_FEATURES;

	//p->kernel_type = POLY;
	//p->degree = 3;
	//p->coef0 = 0;
	//p->gamma = 1.0 / faster::NUM_FEATURES;

	p->kernel_type = RBF;
	p->degree = 3;
	p->coef0 = 0;
	p->gamma = 1.0 / faster::NUM_FEATURES;
	//p->gamma = 1;

	p->cache_size = 1000;
	p->eps = 0.001;
	p->C = 1.0;
	p->shrinking = 0;
	p->probability = 1;

	// Weights
	p->nr_weight = 2;
	int weightLabels[2];
	weightLabels[0] = -1;
	weightLabels[1] = 1;
	p->weight_label = weightLabels;
	double weights[2];
	weights[0] = numSamplesTotal / (2.0*numSamplesNeg);
	weights[1] = numSamplesTotal / (2.0*numSamplesPos);
	p->weight = weights;


	// Check parameters
	const char* err = svm_check_parameter(problem, p);
	if(err) {
		m_inst->m_svmModelReady = false;
		gErr << "Error in trainSVM(): " << err << "\nSVM cannot be used.\n";
		return;
	}

	// Train model
	m_inst->m_svmModel = svm_train(problem, p);
	m_inst->m_svmModelReady = true;
}

void Settings::applyFastERParameters( SegmentationOperationFastER* op /*= NULL*/ )
{
	if(!m_inst)
		m_inst = new Settings();

	if(op) {
		op->darkOnBright = m_inst->m_fasterDob;
		op->minSize = m_inst->m_fasterMinSize;
		op->maxSize = m_inst->m_fasterMaxSize;
		op->minProbability = m_inst->m_fasterMinProb;
		op->disableErSplitting = m_inst->m_disableErSplitting;
		op->constrainPosSamples = get<bool>(KEY_CONSTRAIN_SEGM_POS_SAMPLES);
	}
	else {
		if(m_inst->m_segmentationOperations) {
			for(auto it = m_inst->m_segmentationOperations->operations.begin(); it != m_inst->m_segmentationOperations->operations.end(); ++it) {
				SegmentationOperationFastER* opFastER = qobject_cast<SegmentationOperationFastER*>(*it);
				if(opFastER) {
					opFastER->darkOnBright = m_inst->m_fasterDob;
					opFastER->minSize = m_inst->m_fasterMinSize;
					opFastER->maxSize = m_inst->m_fasterMaxSize;
					opFastER->minProbability = m_inst->m_fasterMinProb;
					opFastER->disableErSplitting = m_inst->m_disableErSplitting;
					opFastER->constrainPosSamples = get<bool>(KEY_CONSTRAIN_SEGM_POS_SAMPLES);
				}
			}
		}
	}
}

std::unordered_set<faster::RegionID, faster::RegionIDHasher>& Settings::getConstrainedRegionsPositive(const PictureIndex& pi)
{
	if(!m_inst)
		m_inst = new Settings();

	return m_inst->m_constrainedPositiveSamples[pi];
}

void Settings::applyLearnedDenoisingParameters(SegmentationOperationBilateralFilter* op)
{
	if(!m_inst)
		m_inst = new Settings();

	double sigmaColor = Settings::get<double>(Settings::KEY_DENOISING_SIGMACOLOR);
	sigmaColor *= Settings::get<double>(Settings::KEY_DENOISING_SIGMACOLORFACTOR);

	if(op) {
		op->sigmaSpace = Settings::get<double>(Settings::KEY_DENOISING_SIGMASPACE);
		op->sigmaColor = sigmaColor;
		op->pixelNeighborhoodDiameter = Settings::get<int>(Settings::KEY_DENOISING_KERNELSIZE);
	}
	else {
		if(m_inst->m_segmentationOperations) {
			for(auto it = m_inst->m_segmentationOperations->operations.begin(); it != m_inst->m_segmentationOperations->operations.end(); ++it) {
				op = qobject_cast<SegmentationOperationBilateralFilter*>(*it);
				if(op) {
					op->sigmaSpace = Settings::get<double>(Settings::KEY_DENOISING_SIGMASPACE);
					op->sigmaColor = sigmaColor;
					op->pixelNeighborhoodDiameter = Settings::get<int>(Settings::KEY_DENOISING_KERNELSIZE);
				}
			}
		}
	}
}
