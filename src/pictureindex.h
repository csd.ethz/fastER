/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef pictureindex_h__
#define pictureindex_h__

// Project
#include "exceptionbase.h"

// Qt
#include <QtGlobal>
#include <QDebug>
#include <QDataStream>


/**
 * Index to refer to an image by its position number, time point, channel, z-index (and maybe other values in the future).
 *
 * This class is not being used everywhere yet, but should be used in new code.
 */
struct PictureIndex {

	// Position
	int positionNumber;

	// Time point
	int timePoint;

	// Channel
	int channel;

	// zIndex
	int zIndex;

	// Constructor
	explicit PictureIndex(int _positionNumber = -1, int _timePoint = -1, int _channel = -1, int _zIndex = -1)
		: positionNumber(_positionNumber), timePoint(_timePoint), channel(_channel), zIndex(_zIndex)
	{}

	// Check if valid
	bool isValid() const 
	{
		// At least one value must be set
		return positionNumber >= 0 || timePoint >= 0 || channel >= 0 || zIndex >= 0;
	}

	// Comparison
	bool operator==(const PictureIndex& other) const 
	{
		return positionNumber == other.positionNumber && 
			timePoint == other.timePoint &&
			channel == other.channel &&
			zIndex == other.zIndex;
	}
	bool operator!=(const PictureIndex& other) const 
	{
		return positionNumber != other.positionNumber ||
			timePoint != other.timePoint ||
			channel != other.channel ||
			zIndex != other.zIndex;
	}

	// Convert to human readable string
	QString toString() const
	{
		return QString("[pos=%1, tp=%2, ch=%3, z=%4]").arg(positionNumber).arg(timePoint).arg(channel).arg(zIndex);
	}

	// Convert to string with markers
	QString toStringWithMarkers() const 
	{
		return QString("p%1_t%2_c%3_z%4").arg(positionNumber, 4, 10, QChar('0')).arg(timePoint, 5, 10, QChar('0')).arg(channel, 2, 10, QChar('0')).arg(zIndex, 2, 10, QChar('0'));
	}

};

/**
 * Hash function.
 */
inline unsigned int qHash(const PictureIndex& pi) {
	return qHash(pi.positionNumber) ^ qHash(pi.timePoint) ^ qHash(pi.channel) ^ qHash(pi.zIndex);
}

/**
 * Min/max functions.
 */
inline PictureIndex qMin(const PictureIndex& value1, const PictureIndex& value2)
{
	return PictureIndex(qMin(value1.positionNumber, value2.positionNumber), 
		qMin(value1.timePoint, value2.timePoint), 
		qMin(value1.channel, value2.channel), 
		qMin(value1.zIndex, value2.zIndex));
}
inline PictureIndex qMax(const PictureIndex& value1, const PictureIndex& value2)
{
	return PictureIndex(qMax(value1.positionNumber, value2.positionNumber), 
		qMax(value1.timePoint, value2.timePoint), 
		qMax(value1.channel, value2.channel), 
		qMax(value1.zIndex, value2.zIndex));
}

/**
 * Write to QDataStream.
 */
inline QDataStream &operator<<(QDataStream& stream, const PictureIndex& pi)
{
	// Magic number + version
	stream << (quint32)0x041D2A7B;
	stream << (qint32)1;

	// Data
	stream << (quint32)pi.positionNumber;
	stream << (quint32)pi.timePoint;
	stream << (quint32)pi.channel;
	stream << (quint32)pi.zIndex;

	return stream;
}

/**
 * Read from QDataStream.
 */
inline QDataStream &operator>>(QDataStream& stream, PictureIndex& pi)
{
	// Read magic number + version
	quint32 magic, version;
	stream >> magic;
	stream >> version;
	if(magic != (quint32)0x041D2A7B || version > 1) {
		throw ExceptionBase(__FUNCTION__, QString("File is invalid or version too new (magic: %1, version: %2).").arg(magic).arg(version));
	}

	// Read data
	quint32 tmp;
	stream >> tmp;
	pi.positionNumber = tmp;
	stream >> tmp;
	pi.timePoint = tmp;
	stream >> tmp;
	pi.channel = tmp;
	stream >> tmp;
	pi.zIndex = tmp;

	return stream;
}

/**
 * Debug output.
 */
inline QDebug operator<<(QDebug dbg, const PictureIndex& pi)
{
	dbg.nospace() << "(pos=" << pi.positionNumber << ", tp=" << pi.timePoint << ", ch=" << pi.channel << ", z=" << pi.zIndex << ")";

	return dbg.space();
}

#endif // pictureindex_h__
