/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmabout.h"

// Project
#include "frmmainwindow.h"

// Qt
#include <QFile>
#include <QPlainTextEdit>
#include <QScrollBar>


FrmAbout::FrmAbout(QWidget* parent /*= 0*/)
	: QDialog(parent)
{
	m_ui.setupUi(this);

	// Update version
	QString version = QString("v%1.%2.%3").arg(FrmMainWindow::VERSION_MAJOR).arg(FrmMainWindow::VERSION_MINOR_1).arg(FrmMainWindow::VERSION_MINOR_2);

	// Add OS
#ifdef Q_OS_MAC
	version += " - Mac OS";
#elif defined Q_OS_WIN
	version += " - Windows";
#elif defined Q_OS_LINUX
	version += " - Linux";
#endif

	// Add architecture
	bool is32bit = sizeof(int*) == 4;
	if(is32bit)
		version += " - 32bit";
	else
		version += " - 64bit";

	m_ui.lblVersion->setText("<html><head/><body><p align=\"center\">" + version + "</p></body></html>");

	// Update Qt version
	m_ui.lblQtVersion->setText(m_ui.lblQtVersion->text().arg(QT_VERSION_STR));

	// Signals/slots
	connect(m_ui.lblLibSvm, SIGNAL(linkActivated(const QString&)), this, SLOT(displayTextResource(const QString&)));
}

void FrmAbout::displayTextResource(const QString& resourceName)
{
	// Open resource
	QFile f(QString(":/%1").arg(resourceName));
	if(!f.open(QIODevice::ReadOnly))
		return;

	// Display
	QDialog dialog;
	QVBoxLayout* verticalLayout = new QVBoxLayout(&dialog);
	QPlainTextEdit* pteText = new QPlainTextEdit(&dialog);
	pteText->document()->setPlainText(f.readAll());
	pteText->moveCursor(QTextCursor::Start);
	pteText->setReadOnly(true);
	verticalLayout->addWidget(pteText);
	dialog.resize(600, 300);
	dialog.exec();
}

