/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmbasicsegmentationsettings_h__
#define frmbasicsegmentationsettings_h__

#include "ui_frmBasicSegmentationSettings.h"


class FrmMainWindow;


/**
 * Form to display basic segmentation settings like minsize, maxsize for fastER.
 */
class FrmBasicSegmentationSettings : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmBasicSegmentationSettings(QWidget* parent = 0);

	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(200, 217);
	}

private slots:

	/**
	 * Learn automatically checkboxes changed.
	 */
	void autoLearnDobToggled(bool newVal);
	void autoLearnSizeToggled(bool newVal);
	
	// Restrict to binary checkbox toggled
	void updateSegmentationFull();

	// Enable/disable/strong denoising
	void denoisingSettingChanged();

	// Enable/disable constraining segmentation samples
	void constrainPositiveSamplesClicked(bool newVal);

private:

	// GUI
	Ui::BasicSegmentationSettings m_ui;

	friend FrmMainWindow;
};


#endif // frmbasicsegmentationsettings_h__
