/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef fastdirectorylisting_h__
#define fastdirectorylisting_h__

// Qt includes
#include <QStringList>
#include <QDir>
#include <QByteArray>

/**
 * @author Oliver Hilsenbeck
 *
 * This class provides functions to list files as fast as possible, especially
 * faster than QDir::entryList() functions.
 * Implemented using windows API
 */
class FastDirectoryListing {
public:

	/**
	 * Return unsorted list of files 
	 * @param _path the path to search in, e.g. "C:\someFolder"
	 * @param _fileExtensions case-insensitive file extensions to use as filter, e.g. (".cpp",".h") (this means you can specify arbitrary file endings, like w_0.jpg, and not just parts of the actual file extension)
	 * @return unsorted(!) list of found filenames (without path)
	 */
	static QStringList listFiles(QDir _path, QStringList _fileExtensions);

private:

	/**
	 * Helper function to check extension of a filename
	 * @param _fileName the file name
	 * @param _fileExtensions the accepted file extensions
	 * @return true if _fileName ends with a string provided in _fileExtensions
	 */
	static bool checkFileExtension(const QByteArray& _fileName, const QStringList& _fileExtensions);

	/**
	 * Function based on QDir::entrylist, used in case of errors
	 * @param _path the path to search in
	 * @param _fileExtensions file extensions to use as filter
	 * @return UNSORTED list of filenames without path that have been found
	 */
	static QStringList listFilesWithQDir(QDir _path, QStringList _fileExtensions);
};


#endif // fastdirectorylisting_h__