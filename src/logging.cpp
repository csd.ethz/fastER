/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "logging.h"

// Project
#include "frmconsole.h"

// Windows
#ifdef _MSC_VER
#include <Windows.h>
#endif

QElapsedTimer gDbgTimer;

Logging gLog(Logging::LOG_STANDARD);
Logging gErr(Logging::LOG_ERROR);
Logging gDbg(Logging::LOG_DBG);

//Logging& Logging::log()
//{
//	if(!m_inst)
//		m_inst = new Logging();
//	return *m_inst;
//}

Logging& Logging::operator<<( const QString& s )
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(s));
#endif
	
	// Console output
	FrmConsole::out(s);

	return *this;
}

Logging& Logging::operator<<( const QStringList& s )
{
	return *this << s.join("");
}

Logging& Logging::operator<<(const char* s)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(s);
#endif

	// Console output
	FrmConsole::out(QString(s));

	return *this;
}

Logging& Logging::operator<<(char ch)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString(ch)));
#endif

	// Console output
	FrmConsole::out(QString(ch));

	return *this;
}

Logging& Logging::operator<<(short s)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(s)));
#endif

	// Console output
	FrmConsole::out(QString::number(s));

	return *this;
}

Logging& Logging::operator<<(int i)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(i)));
#endif

	// Console output
	FrmConsole::out(QString::number(i));

	return *this;
}

Logging& Logging::operator<<(unsigned int i)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(i)));
#endif

	// Console output
	FrmConsole::out(QString::number(i));

	return *this;
}

Logging& Logging::operator<<(long long i)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(i)));
#endif

	// Console output
	FrmConsole::out(QString::number(i));

	return *this;
}

Logging& Logging::operator<<(unsigned long long i)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(i)));
#endif

	// Console output
	FrmConsole::out(QString::number(i));

	return *this;
}

Logging& Logging::operator<<(float f)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(f)));
#endif

	// Console output
	FrmConsole::out(QString::number(f));

	return *this;
}

Logging& Logging::operator<<(double d)
{
	// Debug output
#ifdef _MSC_VER
	OutputDebugStringA(qPrintable(QString::number(d)));
#endif

	// Console output
	FrmConsole::out(QString::number(d));

	return *this;
}

Logging::Logging(Type t)
	: m_type(t)
{

}
