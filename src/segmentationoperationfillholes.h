/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationfillholes_h__
#define segmentationoperationfillholes_h__


#include "segmentationoperation.h"


/**
 * Fill holes of blobs.
 */
class SegmentationOperationFillHoles : public SegmentationOperation {

	Q_OBJECT

public:

	/**
	 * Reimplemented.
	 */

	void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner);

	/**
	 * Reimplemented.
	 */
	QString getName() const
	{
		return "Fill holes";
	}

	/**
	 * Reimplemented.
	 */
	QHash<QString, QVariant> getParameters() const
	{
		return QHash<QString, QVariant>();
	}

	/**
	 * Reimplemented.
	 */
	void setParameters(const QHash<QString, QVariant>& parameters)
	{}
};


#endif // segmentationoperationfillholes_h__
