/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "imageviewimageitem.h"

// Qt
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QDebug>

// Project
#include "imageview.h"
#include "logging.h"
#include "settings.h"
#include "segmentationoperationlist.h"


const QColor ImageViewImageItem::COLOR_FOREGROUND = QColor(255, 255, 0, MARKINGS_TRANSPARENCY);
const QColor ImageViewImageItem::COLOR_BACKGROUND = QColor(0, 0, 255, MARKINGS_TRANSPARENCY);


void ImageViewImageItem::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget )
{
	if(!m_imagePaintingForeground.isNull() && !m_imageOriginal.isNull()) {
		// Draw original image
		painter->drawImage(0, 0, m_imageOriginal);

		// Draw markings
		if(!m_hideMarks) {
			painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
			painter->drawImage(0, 0, m_imagePaintingForeground);
			painter->drawImage(0, 0, m_imagePaintingBackground);
		}

		// Draw overlay image
		if(!m_overlayImage.isNull())
			painter->drawImage(0, 0, m_overlayImage);
	}
}

void ImageViewImageItem::setImage(const QImage& image, bool keepMarkings /*= false*/)
{
	// Change image and apply display settings
	prepareGeometryChange();
	m_imageOriginal = image.convertToFormat(QImage::Format_Indexed8);
	m_imageDisplaySettings.applyDisplaySettings(m_imageOriginal);

	// Reset markings
	if(!keepMarkings || m_imagePaintingForeground.isNull() || m_imageOriginal.size() != m_imagePaintingForeground.size()) {
		// Markings are put on m_imagePainting's, which are drawn on m_imageOriginal and fully transparent in the beginning
		m_imagePaintingForeground = QImage(m_imageOriginal.size(), QImage::Format_ARGB32);
		m_imagePaintingForeground.fill(QColor(255,255,255,0));
		m_imagePaintingBackground = QImage(m_imageOriginal.size(), QImage::Format_ARGB32);
		m_imagePaintingBackground.fill(QColor(255,255,255,0));

		m_paintedBlobs.clear();
		m_paintedBackground.clear();
	}

	// Remove overlay image
	m_overlayImage = QImage();

	// Update shown region
	QRectF sceneRect = m_imageOriginal.rect();
	m_parentImageView->setSceneRect(sceneRect);
	scene()->setSceneRect(sceneRect);

	// Redraw everything
	update();
}

void ImageViewImageItem::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
	// Check if painting event
	if(!m_parentImageView->getPaintable() || m_imagePaintingForeground.isNull() || event->button() != Qt::LeftButton)
		return;

	// Paint or erase circle
	int diameterScene = m_brushDiameter;
	QPoint posScene = event->scenePos().toPoint();
	//posScene -= QPoint(1,1);
	if(event->modifiers() & Qt::ShiftModifier) {
		// Erase
		eraseCircle(m_imagePaintingForeground, posScene, diameterScene);
		eraseCircle(m_imagePaintingBackground, posScene, diameterScene);
	}
	else if(event->modifiers() & Qt::ControlModifier) {
		// Background
		drawCircle(m_imagePaintingBackground, posScene, diameterScene, COLOR_BACKGROUND);

		// Avoid overlay of background and foreground labels
		eraseCircle(m_imagePaintingForeground, posScene, diameterScene);
	}
	else {
		// Foreground
		drawCircle(m_imagePaintingForeground, posScene, diameterScene, COLOR_FOREGROUND);

		// Avoid overlay of background and foreground labels
		eraseCircle(m_imagePaintingBackground, posScene, diameterScene);
	}
	
	update(posScene.x() - diameterScene/2, posScene.y() - diameterScene/2, diameterScene+1, diameterScene+1);

	// Go into painting mode until mouse button is released
	m_painting = true;
}

void ImageViewImageItem::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
	// Process markings after finished painting 
	if(m_painting) {
		m_painting = false;

		// DEBUG
		qDebug() << "start ImageViewImageItem::mouseReleaseEvent()";
		gDbgTimer.start();

		// Process user labels
		processMarkings();

		// DEBUG
		qDebug() << "stop ImageViewImageItem::mouseReleaseEvent()";
		qDebug() << "elapsed time: " << gDbgTimer.elapsed() << "ms";
	}

	// This is a workaround: when two image views are synchronous in dock widgets, moving the mouse while pressing
	// the right mouse button can lead to the appearance of artifacts -> removed when mouse is released (ToDo: 
	// find and fix true cause)
	update();
}

void ImageViewImageItem::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
	QPoint posScene = event->scenePos().toPoint();

	// Update cursor position (needed because no hover events during mouse grabbing)
	if(m_brushCursor)
		updateCursorPosition(posScene);

	// Remaining part of function handles painting events
	if(!m_painting)
		return;
	int diameterScene = m_brushDiameter;
	QPoint lastPosScene = event->lastScenePos().toPoint();

	//posScene -= QPoint(1,1);
	//lastPosScene -= QPoint(1,1);

	if(event->modifiers() & Qt::ShiftModifier) {
		// Erase
		eraseLine(m_imagePaintingForeground, lastPosScene, posScene, diameterScene);
		eraseCircle(m_imagePaintingForeground, lastPosScene, diameterScene);
		eraseLine(m_imagePaintingBackground, lastPosScene, posScene, diameterScene);
		eraseCircle(m_imagePaintingBackground, lastPosScene, diameterScene);
	}
	else if(event->modifiers() & Qt::ControlModifier) {
		// Background
		drawLine(m_imagePaintingBackground, lastPosScene, posScene, diameterScene, COLOR_BACKGROUND);
		drawCircle(m_imagePaintingBackground, lastPosScene, diameterScene, COLOR_BACKGROUND);

		// Avoid overlay of background and foreground labels
		eraseLine(m_imagePaintingForeground, lastPosScene, posScene, diameterScene);
		eraseCircle(m_imagePaintingForeground, lastPosScene, diameterScene);
	}
	else {
		// Foreground
		drawLine(m_imagePaintingForeground, lastPosScene, posScene, diameterScene, COLOR_FOREGROUND);
		drawCircle(m_imagePaintingForeground, lastPosScene, diameterScene, COLOR_FOREGROUND);

		// Avoid overlay of background and foreground labels
		eraseLine(m_imagePaintingBackground, lastPosScene, posScene, diameterScene);
		eraseCircle(m_imagePaintingBackground, lastPosScene, diameterScene);
	}

	// Redraw
	int xMin = std::min(posScene.x(), lastPosScene.x()),
		yMin = std::min(posScene.y(), lastPosScene.y()),
		xMax = std::max(posScene.x(), lastPosScene.x()),
		yMax = std::max(posScene.y(), lastPosScene.y());
	update(xMin - diameterScene/2, yMin - diameterScene/2, xMax - xMin + diameterScene + 1, yMax - yMin + diameterScene + 1);
}

void ImageViewImageItem::drawCircle( QImage& img, QPoint pos, int diameter, QColor color )
{
	//gDbg << "Circle: " << pos.x() << " - " << pos.y() << "\n";

	// Draw semi-transparently on img, ignoring destination color
	QPainter p(&img);
	p.setPen(QPen(color, 1, Qt::SolidLine, Qt::RoundCap));
	p.setBrush(QBrush(color));
	p.setCompositionMode(QPainter::CompositionMode_Source);
	if(diameter > 0)
		p.drawEllipse(pos.x()-diameter/2, pos.y()-diameter/2, diameter, diameter);
	else
		p.drawPoint(pos);
}

void ImageViewImageItem::eraseCircle( QImage& img, QPoint pos, int diameter )
{
	// Draw transparently on img, ignoring destination color
	QPainter p(&img);
	QColor col = QColor(0,0,0,0);
	p.setPen(QPen(QBrush(col), 1, Qt::SolidLine, Qt::RoundCap));
	p.setBrush(QBrush(col));
	p.setCompositionMode(QPainter::CompositionMode_Source);
	if(diameter > 0)
		p.drawEllipse(pos.x()-diameter/2, pos.y()-diameter/2, diameter, diameter);
	else
		p.drawPoint(pos);
}

void ImageViewImageItem::drawLine(QImage& img, QPoint posStart, QPoint posEnd, int width, QColor color)
{
	//gDbg << "Start: " << posStart.x() << " - " << posStart.y() << "\n";
	//gDbg << "End: " << posEnd.x() << " - " << posEnd.y() << "\n";

	// Draw semi-transparently on img, ignoring destination color
	QPainter p(&img);
	p.setCompositionMode(QPainter::CompositionMode_Source);
	p.setPen(QPen(color, width+1, Qt::SolidLine, Qt::FlatCap));
	p.drawLine(posStart, posEnd);
}

void ImageViewImageItem::eraseLine( QImage& img, QPoint posStart, QPoint posEnd, int width )
{
	// Draw transparently on img, ignoring destination color
	QPainter p(&img);
	QColor col = QColor(0,0,0,0);
	p.setPen(QPen(QBrush(col), width, Qt::SolidLine, Qt::RoundCap));
	p.setCompositionMode(QPainter::CompositionMode_Source);
	p.drawLine(posStart, posEnd);
}

void ImageViewImageItem::processMarkings()
{
	QElapsedTimer timer;
	timer.start();

	// Determine pixel connectivity and fill holes
	SegmentationOperationList* segOperations = Settings::getSegmentationOperations().get();
	int connectivity = segOperations ? segOperations->getPixelConnectivity() : 4;
	m_imagePaintingForeground = m_segConverter.fillHoles(m_imagePaintingForeground, qRgba(COLOR_FOREGROUND.red(), COLOR_FOREGROUND.green(), COLOR_FOREGROUND.blue(), COLOR_FOREGROUND.alpha()), qRgba(0, 0, 0, 0), connectivity);
	update();

	// Convert foreground markings to blob list
	m_paintedBlobs = m_segConverter.convertBinaryQImageToBlobs(m_imagePaintingForeground, qRgba(COLOR_FOREGROUND.red(), COLOR_FOREGROUND.green(), COLOR_FOREGROUND.blue(), COLOR_FOREGROUND.alpha()), connectivity);

	// Convert background markings to pixel list
	m_paintedBackground = m_segConverter.convertBinaryQImageToPixelList(m_imagePaintingBackground, qRgba(COLOR_BACKGROUND.red(), COLOR_BACKGROUND.green(), COLOR_BACKGROUND.blue(), COLOR_BACKGROUND.alpha()));

	// Emit signal
	emit m_parentImageView->markingFinished();

	gLog << "MarkingFinished(): elapsed time: " << timer.elapsed() << " ms\n";
}

QPixmap ImageViewImageItem::createBrushCursor( int diameter, QColor color )
{
	// Set some transparency
	color.setAlpha(ImageViewImageItem::MARKINGS_TRANSPARENCY);

	// Create pixmap with circle
	QPixmap pixmap(diameter+2, diameter+2);
	pixmap.fill(Qt::transparent);
	QPainter p(&pixmap);
	p.setPen(QPen(color, 1, Qt::SolidLine, Qt::RoundCap));
	p.setBrush(QBrush(color));
	p.setCompositionMode(QPainter::RasterOp_SourceOrDestination);
	if(diameter > 0)
		p.drawEllipse(0, 0, diameter, diameter);
	else
		p.drawPoint(0, 0);
	p.end();

	return pixmap;
}

ImageViewImageItem::~ImageViewImageItem()
{
	//if(m_brushCursor) {
	//	scene()->removeItem(m_brushCursor);
	//	delete m_brushCursor;
	//}
}

void ImageViewImageItem::wheelEvent( QGraphicsSceneWheelEvent* event )
{
	if(event->modifiers() & Qt::ControlModifier && m_parentImageView->getPaintable()) {
		// Change painting brush size (making sure it is an even number)
		int delta = event->delta() / 60;
		m_brushDiameter = std::min((std::max(m_brushDiameter + delta, 0)/2) * 2, 100);
		updateCursor();
	}
	else
		event->ignore();
}

void ImageViewImageItem::updateCursor()
{
	if(m_parentImageView->getPaintable()) {
		// Create brush cursor
		if(!m_brushCursor) {
			m_brushCursor = new QGraphicsPixmapItem(this);
			//scene()->addItem(m_brushCursor);
		}
		m_brushCursor->setPixmap(createBrushCursor(m_brushDiameter, m_brushColor));
		if(!m_cursorScenePosition.isNull())
			updateCursorPosition(m_cursorScenePosition);
	}
	else {
		// Remove brush cursor
		if(m_brushCursor) {
			scene()->removeItem(m_brushCursor);
			delete m_brushCursor;
			m_brushCursor = 0;
		}
	}
}

void ImageViewImageItem::hoverEnterEvent( QGraphicsSceneHoverEvent* event )
{
	// Show cursor and move cursor
	if(m_brushCursor && m_parentImageView->m_allowPainting) {
		m_brushCursor->setVisible(true);
		
		QPoint posScene = event->scenePos().toPoint();
		updateCursorPosition(posScene);
	}
}

void ImageViewImageItem::hoverLeaveEvent( QGraphicsSceneHoverEvent* event )
{
	// Mouse position and gray level not available anymore
	emit m_parentImageView->mousePositionChanged(-1, -1, -1);

	// Hide cursor
	if(m_brushCursor) {
		m_brushCursor->setVisible(false);

		QPoint scenePos = event->scenePos().toPoint();
		emit m_parentImageView->brushPositionChanged(scenePos.x(), scenePos.y(), 0);
	}
}

void ImageViewImageItem::hoverMoveEvent( QGraphicsSceneHoverEvent* event )
{
	// Emit signal with mouse position and pixel gray level
	int grayLevel = -1;
	QPointF scenePos = event->scenePos();
	if(!m_imageOriginal.isNull() && scenePos.x() < m_imageOriginal.width() && scenePos.y() < m_imageOriginal.height() && scenePos.x() >= 0 && scenePos.y() >= 0)
		grayLevel = m_imageOriginal.pixelIndex(scenePos.x(), scenePos.y());
	emit m_parentImageView->mousePositionChanged(scenePos.x(), scenePos.y(), grayLevel);

	// Move cursor
	if(m_brushCursor && m_parentImageView->m_allowPainting) {
		QPoint posScene = event->scenePos().toPoint();
		updateCursorPosition(posScene);
	}
}

void ImageViewImageItem::keyPressEvent( QKeyEvent* event )
{
	updateCursorColorAfterKeyEvent(event);
}

void ImageViewImageItem::keyReleaseEvent( QKeyEvent* event )
{
	updateCursorColorAfterKeyEvent(event);
}

void ImageViewImageItem::updateCursorColorAfterKeyEvent( QKeyEvent* event )
{
	// Change painting cursor
	if(event->modifiers() & Qt::ControlModifier) {
		m_brushColor = ImageViewImageItem::COLOR_BACKGROUND;
	}
	else if(event->modifiers() & Qt::ShiftModifier) {
		m_brushColor = QColor(Qt::white);
	}
	else {
		m_brushColor = ImageViewImageItem::COLOR_FOREGROUND;
	}
	if(m_parentImageView->getPaintable())
		updateCursor();
}

void ImageViewImageItem::addLabels( const std::vector<Blob>& labels )
{
	// Add labels to picture and to m_paintedBlobs
	if(!m_imagePaintingForeground.isNull()) {
		for(auto it = labels.begin(); it != labels.end(); ++it) {
			const auto& pixels = it->getPixels();
			for(auto px = pixels.begin(); px != pixels.end(); ++px) {
				m_imagePaintingForeground.setPixel(px->x, px->y, qRgba(COLOR_FOREGROUND.red(), COLOR_FOREGROUND.green(), COLOR_FOREGROUND.blue(), COLOR_FOREGROUND.alpha()));
			}
		}
		m_paintedBlobs.insert(m_paintedBlobs.end(), labels.begin(), labels.end());
	}
}

void ImageViewImageItem::addBackgroundLabels( const std::vector<cv::Point2i>& labels )
{
	// Add labels to picture and to m_paintedBlobs
	if(!m_imagePaintingBackground.isNull()) {
		for(auto it = labels.begin(); it != labels.end(); ++it) {
			m_imagePaintingBackground.setPixel(it->x, it->y, qRgba(COLOR_BACKGROUND.red(), COLOR_BACKGROUND.green(), COLOR_BACKGROUND.blue(), COLOR_BACKGROUND.alpha()));
		}

		m_paintedBackground.insert(m_paintedBackground.begin(), labels.begin(), labels.end());
	}
}

void ImageViewImageItem::updateCursorPosition( QPoint scenePos )
{
	// Brush position
	emit m_parentImageView->brushPositionChanged(scenePos.x(), scenePos.y(), m_brushCursor);

	m_cursorScenePosition = scenePos;
	if(m_brushDiameter > 0) 
		scenePos -= QPoint(m_brushDiameter/2, m_brushDiameter/2);

	m_brushCursor->setPos(scenePos);
}

void ImageViewImageItem::displayBrushCursor( int posX, int posY, QGraphicsPixmapItem* brush )
{
	// If image view is paintable, it has a brush already anyways
	if(m_parentImageView->m_allowPainting)
		return;

	if(brush) {
		// Update brush here
		if(!m_brushCursor) {
			m_brushCursor = new QGraphicsPixmapItem(this);
		}
		m_brushCursor->setPixmap(brush->pixmap());
		m_brushCursor->setVisible(brush->isVisible());
		m_brushCursor->setPos(brush->scenePos());
	}
	else {
		// Remove brush here
		if(m_brushCursor) {
			m_brushCursor->setVisible(false);
		}
	}
}

void ImageViewImageItem::clearLabels()
{
	// Markings are put on m_imagePainting's, which are drawn on m_imageOriginal and fully transparent in the beginning
	m_imagePaintingForeground = QImage(m_imageOriginal.size(), QImage::Format_ARGB32);
	m_imagePaintingForeground.fill(QColor(255,255,255,0));
	m_imagePaintingBackground = QImage(m_imageOriginal.size(), QImage::Format_ARGB32);
	m_imagePaintingBackground.fill(QColor(255,255,255,0));

	m_paintedBlobs.clear();
	m_paintedBackground.clear();

	update();
}
