/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "experiment.h"


int Experiment::readSegmentationQuantificationForMultipleFrames(const QVector<PictureIndex>& pis, int segmentationMethod, QHash<PictureIndex, QVector<QString>>& outColumns, QHash<PictureIndex, QVector<QVector<QVariant>>>& outMatrices, int& outMaxNumRows, QSet<QString>& outAllRows, QStringList& outErrors, int& outNumberOfImagesWithoutQuantification, QProgressDialog* progressBar /*= 0*/)
{
	outMaxNumRows = 0;
	int counterSuccess = 0;
	for(auto it = pis.cbegin(); it != pis.cend(); ++it) {
		if(progressBar)
			progressBar->setValue(progressBar->value() + 1);

		const PictureIndex& pi = *it;
		if(!hasImage(pi))
			continue;

		// Get csv data
		QString csvDataString;
		int err = readSegmentationQuantification(pi, csvDataString, segmentationMethod);
		if(err || csvDataString.isEmpty()) {
			if(err == 1)
				++outNumberOfImagesWithoutQuantification;
			else
				outErrors.push_back(QString("Cannot load quantification for image %1 (error %2)").arg(pi.toString()).arg(err));
			continue;
		}

		// Parse csv data
		err = Tools::parseCsvData(csvDataString, ";", outMatrices[pi], &outColumns[pi]);
		if(err)  {
			outErrors.push_back(QString("Cannot parse quantification for image %1 (error %2)").arg(pi.toString()).arg(err));
			continue;
		}
		outMaxNumRows = std::max(outMaxNumRows, outMatrices[pi].size());
		outAllRows.unite(QSet<QString>::fromList(outColumns[pi].toList()));
		++counterSuccess;
	}

	return counterSuccess;
}