/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobrunner_h__
#define jobrunner_h__

// OpenCV
#include "opencv2/core/core.hpp"

// Qt
#include <QThread>

// Project
#include "jobset.h"
#include "job.h"
#include "exceptionbase.h"
#include "logging.h"

class JobRunnerMaster;

/**
 * Represents one thread running Job instances. This class and derived classes provide objects
 * that are needed exactly once per thread.
 */
class JobRunner : public QThread {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	JobRunner(JobSet* jobset, JobRunnerMaster* masterJobRunner)
		: m_jobs(jobset), m_masterJobRunner(masterJobRunner)
	{}

	/**
	 * Get master job runner. Can be 0, but only when there is only one thread (i.e. one JobRunner instance).
	 */
	JobRunnerMaster* getMasterJobRunner() const 
	{
		return m_masterJobRunner;
	}

protected:

	/**
	 * Thread entry point.
	 */
	void run()
	{
		try {
			// Run jobs
			while(Job* job = m_jobs->popJob(this)) {
				job->run(this);
				m_jobs->jobFinished(this, job);
			}
		}
		catch(ExceptionBase& e) {
			gErr << "Exception in JobRunner::run(): " << e.what() << " - source: " << e.where() << "\n";
		}
		catch(cv::Exception& e) {
			gErr << "OpenCV exception in JobRunner::run(): " << e.what() << " - source: " << QString::fromStdString(e.func) << "\n";
		}
	}

private:

	// Master JobRunner
	JobRunnerMaster* m_masterJobRunner;

	// Job source
	JobSet* m_jobs;

};



#endif // jobrunner_h__
