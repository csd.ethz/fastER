/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmselectexperiment_h__
#define frmselectexperiment_h__

#include "ui_frmSelectExperiment.h"

// Project
#include "experiment.h"

// Qt
#include <QTimer>
#include <QSharedPointer>

class FrmImportExperimentData;



/**
 * Experiment selection form. Selected experiment is saved in Settings.
 */
class FrmSelectExperiment : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmSelectExperiment(QWidget* parent = 0);

signals:

	/**
	 * User selected experiment.
	 * @param experiment instance of a derived class of Experiment
	 * @param selectPositions if user wants to restrict the positions to be used.
	 */
	void experimentSelected(QSharedPointer<Experiment> experiment, bool selectPositions);

	/**
	 * Selection window closed.
	 */
	void experimentSelectionClosed();

protected:

	/**
	 * Close event.
	 */
	void closeEvent(QCloseEvent* event);

private slots:

	/**
	 * Select experiment clicked.
	 */
	void changeExperimentClicked();

	/**
	 * Experiment selection text edited.
	 */
	void experimentSelectionEdited();

	/**
	 * Experiment selection text editing finished.
	 */
	void experimentSelectionEditingFinished();

	/**
	 * Check if currently selected experiment is valid and enable/disable buttons accordingly.
	 */
	bool checkExperiment();

	/**
	 * Open selected experiment.
	 */
	void openExperimentComplete();
	void openExperimentAndSelectPositions();
	void openExperimentImport();

private:

	// Open experiment internal
	void openExperimentInternal(bool selectPositions);

	// GUI
	Ui::SelectExperiment m_ui;

	// Timer to check experiment after manual entering experiment name
	QTimer m_experimentSelectionEditTimer;

	// Window for importing experiments
	FrmImportExperimentData* m_frmImportExperimentData;
};


#endif // frmselectexperiment_h__
