/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "blob.h"


void Blob::calcAreaConvex()
{
	if(m_areaConvex != 0.0 || m_pixels.size() == 0)
		return;

	/*
	std::vector<cv::Point2i> convexHull;
	cv::Mat pixelsMat(m_pixels);
	cv::convexHull(pixelsMat, convexHull);
	m_areaConvex = cv::contourArea(cv::Mat(convexHull));
	*/

	cv::Mat pixelsMat(m_pixels);
	cv::Mat out;
	cv::convexHull(pixelsMat, out);
	m_areaConvex = cv::contourArea(out);
}
