/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Class describing a position of an experiment
 */

/**
 * Copyright (C) 2011 Oliver Hilsenbeck
 */

#ifndef positiondescriptor_h__
#define positiondescriptor_h__

// Qt
#include <QString>
#include <QList>
#include <QPoint>
#include <QPointF>
#include <QRectF>

// Project
#include "experiment.h"

// Forward declarations
class ImageDescriptor;


class PositionDescriptor {

public:

	/**
	 * Constructor.
	 */
	PositionDescriptor(const QString& _folder, int _posNumber, Experiment *_experiment);

	/**
	 * Destructor, deletes image descriptors.
	 */
	~PositionDescriptor();

	///**
	// * Get folder for tree fragments.
	// */
	//QString getTreeFragmentsFolder() const
	//{
	//	//return experiment->getPath() + "TTTrack/TreeFragments/" + m_baseName + "/";
	//	return experiment->getPath() + "TTTrack/TreeFragments/";
	//}

	//// Set position information
	//void setPositionInformation(float _left, float _top);

	//// Get position information
	//bool isPositionInformationAvailable() const { return positionInformationAvailalbe; }
	//float getLeft() const { return left; }
	//float getTop() const { return top; }

	//// Transform picture local pixel coordinates to experiment global micrometer coordinates; must be thread-safe
	//QPointF transformLocalToGlobalCoords(const QPointF& _coords, int _wl) const;

	//// Opposite of transformLocalToGlobalCoords()
	//QPoint transformGlobalToLocalCoords(const QPointF& _coords) const;

	//// Add adjacent position
	//void addAdjacentPosition(PositionDescriptor* _pos);

	//// Get adjacent positions
	//const QList<PositionDescriptor*>& getAdjacentPositions() const;

	//// Get image rect
	//QRectF getGlobalImageRect(unsigned int _wl = 0) const;

	//// Get number of exported tree fragments
	//int getNumExportedTreeFragments() const 
	//{
	//	return m_numExportedTreeFragments;
	//}

	//// Set number of exported tree fragments
	//void setNumExportedTreeFragments(int numExportedTreeFragments)
	//{
	//	m_numExportedTreeFragments = numExportedTreeFragments;
	//}

	///**
	// * Get gain image (thread-safe).
	// * @param wl wavelength of gain image
	// * @param loadIfNecessary if image should be loaded if necessary
	// * @return pointer to gain image (must not be deleted by caller!) or 0
	// */
	//const unsigned short* getGainImage(int wl, bool loadIfNecessary);

	//// Convert image file name to segmentation file name
	//static QString convertImageNameToSegmentationName(const QString& imageName)
	//{
	//	return imageName.left(imageName.lastIndexOf('.')) + "_m00_mask.png";
	//}

private:

	/*
	// Convert timepoint to images index
	int timePointToIndex(int _tp) const;
	*/

	// Experiment descriptor
	Experiment *experiment;

	/*
	// If image descriptors have been created for this position (will only be done for selected positions)
	bool positionInitialized;
	*/

	// Complete path (with '/' at end)
	QString folder;
	QString m_baseName;

	// Position number
	const int posNumber;

	// Position information
	bool positionInformationAvailalbe;
	float left;
	float top;

	// Number of tree fragments exported to position
	int m_numExportedTreeFragments;

	// Adjacent positions
	QList<PositionDescriptor*> adjacentPositions;

	// Gain images for different wavelengths, only loaded on demand
	std::vector<std::unique_ptr<unsigned short[]>> m_gainImages;
	QMutex m_gainImagesMutex;
};


#endif // positiondescriptor_h__