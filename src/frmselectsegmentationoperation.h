/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmselectsegmentationoperation_h__
#define frmselectsegmentationoperation_h__

#include "ui_frmSelectSegmentationOperation.h"

// Qt
#include <QCloseEvent>



/**
 * Select a segmentation operation.
 */
class FrmSelectSegmentationOperation : public QDialog {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmSelectSegmentationOperation(QWidget* parent = 0) 
		: QDialog(parent)
	{
		m_ui.setupUi(this);
		connect(m_ui.bgrSelection, SIGNAL(buttonReleased(int)), this, SLOT(selectionChanged(int)));
		connect(m_ui.pbtCancel, SIGNAL(clicked()), this, SLOT(cancel()));
		connect(m_ui.pbtOk, SIGNAL(clicked()), this, SLOT(close()));
		setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint | Qt::Popup) & ~Qt::WindowContextHelpButtonHint & ~Qt::WindowCloseButtonHint );

		// Ids for result
		m_ui.bgrSelection->setId(m_ui.optStdDev, 0);
		m_ui.bgrSelection->setId(m_ui.optBlur, 1);
		m_ui.bgrSelection->setId(m_ui.optFillHoles, 2);
		m_ui.bgrSelection->setId(m_ui.optFastER, 3);
		m_ui.bgrSelection->setId(m_ui.optSize, 4);
		m_ui.bgrSelection->setId(m_ui.optMedianFilter, 5);
		m_ui.bgrSelection->setId(m_ui.optMorphologyTransform, 6);
		setResult(0);
	}

protected:

	// Override to avoid call to reject() by QDialog implementation
	void closeEvent(QCloseEvent *e)
	{
		e->accept();
	}

private slots:

	// Selection changed
	void selectionChanged(int r)
	{
		setResult(r);
	}

	// Cancel
	void cancel()
	{
		setResult(-1);
		close();
	}

private:

	// GUI
	Ui::SelectSegmentationOperation m_ui;

};


#endif // frmselectsegmentationoperation_h__
