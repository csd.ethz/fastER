/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmdisplaysettings_h__
#define frmdisplaysettings_h__

#include "ui_frmDisplaySettings.h"


class FrmMainWindow;


/**
 * Dialog to adjust image display settings.
 */
class FrmColorTransformation : public QDialog {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmColorTransformation(QWidget* parent = 0);

	///**
	// * Update display according to provided settings.
	// */
	//void updateDisplay(const ImageDisplaySettings& settings);

	/**
	 * Apply currently set settings to provided settings object.
	 */
	void applySettings(ImageDisplaySettings& settings);

	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(200, 102);
	}

signals:

	/**
	 * Settings were changed.
	 */
	void settingsChanged();

private slots:

	/**
	 * Reset settings.
	 */
	void reset();

private:

	// Ui
	Ui::frmDisplaySettings ui;

};


#endif // frmdisplaysettings_h__
