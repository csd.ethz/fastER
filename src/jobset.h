/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jobset_h__
#define jobset_h__


class Job;
class JobRunner;

// Project
#include "logging.h"

// QT
#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <QString>
#include <QTime>


/**
 * A source for Jobs.
 */
class JobSet : public QObject {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	JobSet()
	{
		m_numJobsFinished = 0;
		m_lastFinishedSignalEmission.start();	// Make sure, m_lastFinishedSignalEmission.elapsed() returns not 0
	}

	/**
	 * Get next job. Returns 0 if there is no job left. Must be thread safe.
	 */
	virtual Job* popJob(JobRunner* jobRunner) = 0;

	/**
	 * Called by Job executing thread after job has been finished. Must be thread safe.
	 * Default implementation emits jobFinishedSignal signal.
	 */
	virtual void jobFinished(JobRunner* jobRunner, Job* job)
	{
		QMutexLocker lock(&m_numJobsFinishedMutex);
		++m_numJobsFinished;
		// Do not emit signals more than 10 times per second as this can actually make the GUI thread freeze
		if(m_lastFinishedSignalEmission.elapsed() > 100) {
			emit jobFinishedSignal(m_numJobsFinished);
			m_lastFinishedSignalEmission.start();
		}
	}

	/**
	 * Get total number of jobs. Returns -1 if number is not available.
	 */
	virtual int getNumJobsTotal() const {
		return -1;
	}

	/**
	 * Get number of jobs already processed.
	 */
	virtual int getNumJobsProcessed() const {
		return -1;
	}

	/**
	 * Request cancel. Returns false if not supported.
	 */
	virtual bool requestCancel() {
		return false;
	}

	/**
	 * Get description of jobset.
	 */
	virtual QString getDescription() const {
		return "";
	}

signals:

	/**
	 * Emitted after a job has been finished. Note: there is no guarantee that there will
	 * be a signal for every single finished job (too man signals too fast would freeze
	 * a connected GUI thread).
	 */
	void jobFinishedSignal(int numJobsFinished);

private:

	// Number of finished jobs
	int m_numJobsFinished;
	QMutex m_numJobsFinishedMutex;
	QTime m_lastFinishedSignalEmission;


};


#endif // jobset_h__
