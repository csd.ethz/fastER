/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmbasicsegmentationsettings.h"

// Project
#include "frmmainwindow.h"
#include "segmentationoperationlist.h"
#include "settings.h"


FrmBasicSegmentationSettings::FrmBasicSegmentationSettings(QWidget* parent /*= 0*/)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	// Init ui
	m_ui.chkConstrainPositiveSamples->setChecked(Settings::get<bool>(Settings::KEY_CONSTRAIN_SEGM_POS_SAMPLES));

	// Signals/slots
	connect(m_ui.chkLearnDOB, SIGNAL(toggled(bool)), this, SLOT(autoLearnDobToggled(bool)));
	connect(m_ui.chkLearnSize, SIGNAL(toggled(bool)), this, SLOT(autoLearnSizeToggled(bool)));
	connect(m_ui.chkEnforceBinaryMask, SIGNAL(toggled(bool)), this, SLOT(updateSegmentationFull()));
	connect(m_ui.optDenoiseOff, SIGNAL(toggled(bool)), this, SLOT(denoisingSettingChanged()));
	connect(m_ui.optDenoiseNormal, SIGNAL(toggled(bool)), this, SLOT(denoisingSettingChanged()));
	connect(m_ui.optDenoiseStrong, SIGNAL(toggled(bool)), this, SLOT(denoisingSettingChanged()));
	//connect(m_ui.chkNoDenoising, SIGNAL(toggled(bool)), this, SLOT(disableDenoisingToggled(bool)));
	connect(m_ui.chkConstrainPositiveSamples, SIGNAL(clicked(bool)), this, SLOT(constrainPositiveSamplesClicked(bool)));
}

void FrmBasicSegmentationSettings::autoLearnDobToggled( bool newVal )
{
	m_ui.optDOB->setEnabled(!newVal);
	m_ui.optBOD->setEnabled(!newVal);
}

void FrmBasicSegmentationSettings::autoLearnSizeToggled( bool newVal )
{
	m_ui.spbMinSize->setEnabled(!newVal);
	m_ui.spbMaxSize->setEnabled(!newVal);
}

void FrmBasicSegmentationSettings::updateSegmentationFull()
{
	if(g_FrmMainWindow && g_FrmMainWindow->m_frmSegPreview) {
		// Do full update (re-extract samples, learn SVM, etc.)

		// Update label statistics
		g_FrmMainWindow->updateLabelStatistics();

		// Get samples from labels
		g_FrmMainWindow->extractSamplesForLabels(true);

		// Train SVM and update segmentation
		g_FrmMainWindow->learnParametersSvm();
		g_FrmMainWindow->applySegmentationSettingsAndUpdatePreview();
	}
}

void FrmBasicSegmentationSettings::denoisingSettingChanged()
{
	// Add or remove operation from list
	SegmentationOperationList* segOperations = Settings::getSegmentationOperations().get();
	if(!segOperations)
		return;

	if(!m_ui.optDenoiseOff->isChecked()) {
		// Enable if off
		if(!segOperations->containsOperation(SegmentationOperationBilateralFilter::SEGOP_NAME)) {
			segOperations->operations.insert(segOperations->operations.begin(), new SegmentationOperationBilateralFilter());
		}

		// Enable/disable strong denoising
		if(m_ui.optDenoiseStrong->isChecked()) {
			Settings::set(Settings::KEY_DENOISING_SIGMACOLORFACTOR, 2.0);
		}
		else if(m_ui.optDenoiseNormal->isChecked()) {
			Settings::set(Settings::KEY_DENOISING_SIGMACOLORFACTOR, 1.0);
		}
	}
	else {
		// Disable (i.e. remove from pipeline)
		segOperations->removeOperation(SegmentationOperationBilateralFilter::SEGOP_NAME);
	}

	// Refresh segmentation
	updateSegmentationFull();
}

void FrmBasicSegmentationSettings::constrainPositiveSamplesClicked(bool newVal)
{
	// Update settings
	Settings::set(Settings::KEY_CONSTRAIN_SEGM_POS_SAMPLES, newVal);

	// Update segmentation
	g_FrmMainWindow->applySegmentationSettingsAndUpdatePreview();
}