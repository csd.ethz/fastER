/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmoriginalimageview_h__
#define frmoriginalimageview_h__

#include "ui_frmOriginalImageView.h"

class FrmMainWindow;

class FrmOriginalImageView : public QWidget {

	Q_OBJECT

public:
	
	/**
	 * Constructor.
	 */
	FrmOriginalImageView(QWidget* parent = 0)
		: QWidget(parent)
	{
		m_ui.setupUi(this);

		///**
		// * Debug: hide some controls for thesis screenshots.
		// */
		//m_ui.label_20->setVisible(false);
		//m_ui.liePreviewPic->setVisible(false);
		//m_ui.pbtSelectPreviewPic->setVisible(false);
		//m_ui.pbtMinus10->setVisible(false);
		//m_ui.pbtPlus10->setVisible(false);
	}

	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(2000, 400);
	}

	/**
	 * Get if segmentation should be refreshed automatically.
	 */
	bool autoRefreshSegmentation() const 
	{
		return m_ui.chkAutoRefreshSegmentation->isChecked();
	}

private:

	// GUI
	Ui::OriginalImageView m_ui;

	friend FrmMainWindow;
};


#endif // frmoriginalimageview_h__
