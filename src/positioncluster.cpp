/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positioncluster.h"

// Project
#include "experiment.h"


PositionCluster::PositionCluster( Experiment* experiment, int position )
	: m_experiment(experiment), m_dimensions(experiment->getGlobalImageRect(PictureIndex(position, 1, 0, 1)))
{
	m_positions.push_back(position);
}

void PositionCluster::addPosition( int position )
{
	// Add position and update bounding rect
	m_positions.push_back(position);
	m_dimensions = m_dimensions.united(m_experiment->getGlobalImageRect(PictureIndex(position, 1, 0, 1)));
}

bool PositionCluster::intersects( int position ) const
{
	return m_dimensions.intersects(m_experiment->getGlobalImageRect(PictureIndex(position, 1, 0, 1)));
}

void PositionCluster::merge( const PositionCluster* other )
{
	// Merge
	m_positions.insert(m_positions.end(), other->m_positions.begin(), other->m_positions.end());
	m_dimensions = m_dimensions.united(other->m_dimensions);
}

