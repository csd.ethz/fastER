/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationsizefilter.h"

// OpenCV
#include "opencv2/core/core.hpp"

// Project
#include "exceptionbase.h"
#include "imageprocessingtools.h"
#include "jobsegmentation.h"
#include "logging.h"


void SegmentationOperationSizeFilter::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	try {

		if(*imageDataType == IDT_UInt8Gray || *imageDataType == IDT_UInt8Binary) {
			// Run size filter
			unsigned char fgColor = 255;
			unsigned char bgColor = 0;
			QImage img(*imageData, sizeX, sizeY, sizeX, QImage::Format_Indexed8);
			jobSegmentation->getImageProcessingTools()->sizeFilter<unsigned char>(img, fgColor, bgColor, minSize, maxSize, jobSegmentation->getPixelConnectivity());
		}
		else if(*imageDataType == IDT_UInt32LabelMap) {
			// Convert to blobs
			std::vector<Blob> blobs = jobSegmentation->getImageProcessingTools()->convertUInt32LabelMapToBlobs(reinterpret_cast<unsigned int*>(*imageData), sizeX, sizeY);

			// Remove blobs with wrong size
			auto itBlob = blobs.begin();
			while(itBlob != blobs.end()) {
				if(itBlob->getPixels().size() <= maxSize && itBlob->getPixels().size() >= minSize)
					++itBlob;
				else
					itBlob = blobs.erase(itBlob);
			}

			// Write back results
			memset(*imageData, 0, sizeX*sizeY*sizeof(unsigned int));
			unsigned int* dst = reinterpret_cast<unsigned int*>(*imageData);
			for(int iBlob = 0; iBlob < blobs.size(); ++iBlob) {
				const auto& blobPixels = blobs[iBlob].getPixels();
				for(auto itPixel = blobPixels.begin(); itPixel != blobPixels.end(); ++itPixel) {
					dst[itPixel->y * sizeX + itPixel->x] = iBlob + 1;
				}
			}
		}
		else
			throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));

	}
	catch(cv::Exception& e) {
		throw ExceptionBase(__FUNCTION__, QString("OpenCV Exception: %1").arg(e.what()));
	}
}
