/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Various useful functions
 */

#ifndef tools_h__
#define tools_h__

// Project
#include "cattiffhandler.h"
#include "logging.h"
#include "imageprocessingtools.h"

// Qt
#include <QImageReader>
#include <QString>
#include <QVector>
#include <QRgb>
#include <QRectF>
#include <QCursor>
#include <QColor>
#include <QImage>
#include <QDebug>
#include <QDir>
#include <QMutex>

// OpenCV
#include "opencv2/core/core.hpp"

// Forwards
class QPoint;
class QPointF;

/**
* Write cv::Point2i to QDataStream.
*/
inline QDataStream &operator<<(QDataStream& stream, const cv::Point2i& p)
{
	stream << (quint32)p.x;
	stream << (quint32)p.y;

	return stream;
}

/**
* Read blob from QDataStream.
*/
inline QDataStream &operator>>(QDataStream& stream, cv::Point2i& p)
{
	// Read data
	quint32 x,y;
	stream >> x;
	stream >> y;
	p.x = x;
	p.y = y;

	return stream;
}

// Separators (getNumberFromPath assumes that all separators have length 2)
extern const char* POS_MARKER;
extern const char* WL_MARKER;
extern const char* TP_MARKER;
extern const char* Z_MARKER;

class Tools {
public:

	// Get position number from directory name, e.g. 5 for '100709DL1_p005'
	// Returns -1 in case of error
	static int getPosNumberFromDirName(const QString& _dir)
	{
		// Extract number after POS_MARKER
		return getNumberFromPath(_dir, POS_MARKER);
	}

	// Get wavelength number from filename, e.g. 0 for '100709DL1_p002_t00003_w0.jpg'
	// Returns -1 in case of error
	static int getWlNumberFromFileName(const QString& _fileName)
	{
		// Extract number after WL_MARKER
		return getNumberFromPath(_fileName, WL_MARKER);
	}

	// Get z-index number from filename, e.g. 1 for '130627DL8_p0013_t00001_z001_w00.png'
	// Returns -1 in case of error
	static int getZIndexFromFileName(const QString& _fileName)
	{
		// Extract number after Z_MARKER
		return getNumberFromPath(_fileName, Z_MARKER);
	}

	// Get timepoint number from filename, e.g. 3 for '100709DL1_p002_t00003_w0.jpg'
	// Returns -1 in case of error
	static int getTpNumberFromFileName(const QString& _fileName)
	{
		// Extract number after TP_MARKER
		return getNumberFromPath(_fileName, TP_MARKER);
	}

	// Get distance of two points
	static float calcDistance(const QPoint& _p1, const QPoint& _p2);
	static float calcDistance(const QPointF& _p1, const QPointF& _p2);

	// Return a color table for grayscale QImage pictures using image format Indexed8
	static QVector<QRgb> getColorTableGrayIndexed8();

	// Return a color table for grayscale QImage pictures using image format Indexed8 with blackpoint and whitepoint
	static QVector<QRgb> getColorTableGrayIndexed8(int blackPoint, int whitePoint);

	// Return a color table for grayscale QImage pictures using image format Indexed8 with blackpoint and whitepoint, and with specified color translation
	// Black pixels are transparent, pixels between bp and wp get according opacity, pixels above wp are fully opaque
	static QVector<QRgb> getColorTableGrayIndexed8(int blackPoint, int whitePoint, QColor color);

	/**
	 * Return distance of _point to borders of _rect (_point can be inside or outside of _rect)
	 */
	static int calcPointFDistanceRectFToBorder(const QPointF& _point, const QRectF& _rect);

	/**
	 * Convert number of bytes to string with at most 2 digits after and 2 digits before the comma
	 * @param _size size in bytes
	 * @return string, e.g. "4.5GB"
	 */
	static QString formatByteNumber(qint64 _numBytes);

	/**
	 * Get length of number in a string after the last occurrence of a provided tag.
	 * @param string the string in which the tag should be searched, e.g. '100709DL1_p002_t00003_w0.jpg'.
	 * @param tag the tag to look for, e.g. '_p'.
	 * @return length of number after the tag, e.g. 3 for the example.
	 */
	static int getTagLength(const QString& string, const QString& tag);

	/**
	 * Create a circle cursor.
	 * @param diameter diameter in pixels.
	 * @param color color.
	 * @param thickness thickness in pixels.
	 */
	static QCursor createCircleCursor (int diameter, QColor color, int thickness);

	/**
	 * Sleep function. Holds execution of calling thread.
	 * @param ms how long.
	 */
	static void msleep(unsigned long ms);

	/**
	 * Display a messagebox with a button to open a specified folder (useful for exports of any kind)
	 * @param _message the message to display
	 * @param _caption the caption to display
	 * @param _folder the absolute path of the directory to be opened
	 * @param _folderIncludesFile if set to true, folder will be opened and file will be selected
	 * @param _parent parent window if desired
	 */
	static void displayMessageBoxWithOpenFolder(QString _message, QString _caption, QString _folder, bool _folderIncludesFile = false, const QWidget* _parent = 0);

	/**
	 * Apply background correction to image.
	 */
	static QVector<double> applyBackgroundCorrection(const QImage& imageData, const unsigned short* backgroundImage, const unsigned short* gainImage)
	{
		assert(gainImage);
#ifdef _DEBUG
		// Stupid workaround for VS2010 QT plugin for debugging (QVector entries cannot be seen in variables window)
		std::vector<double> resultImage;
#else
		QVector<double> resultImage;
#endif
		resultImage.resize(imageData.width()*imageData.height());
		int i = 0;
		for(int y = 0; y < imageData.height(); ++y) {
			const unsigned char* scanLine = imageData.scanLine(y);
			for(int x = 0; x < imageData.width(); ++x) {
				// First scale all values to range [0.0, 1.0]
				double pixelValue = static_cast<double>(*(scanLine++)) / 255.0;
				double backgroundValue = static_cast<double>(*(backgroundImage++)) / 65535.0; 
				double gainValue = static_cast<double>(*(gainImage++)) / 255.0;		// Gain is only divided by 255.0, as it is stored already scaled 

				// Run background correction with gain
				resultImage[i] = pixelValue - backgroundValue;
				if(gainValue != 0.0)	// This actually happens sometimes..
					resultImage[i] /= gainValue;
				else
					resultImage[i] = std::numeric_limits<double>::quiet_NaN();

				++i;
			}
		}
#ifdef _DEBUG
		return QVector<double>::fromStdVector(resultImage);
#else
		return resultImage;
#endif
	}

	/**
	 * Apply background correction to image using AMT single formula (I/B-1)
	 */
	static QVector<double> applyBackgroundCorrectionAMTSingle(const QImage& imageData, const unsigned short* backgroundImage)
	{
#ifdef _DEBUG
		// Stupid workaround for VS2010 QT plugin for debugging (QVector entries cannot be seen in variables window)
		std::vector<double> resultImage;
#else
		QVector<double> resultImage;
#endif
		resultImage.resize(imageData.width()*imageData.height());
		int i = 0;
		for(int y = 0; y < imageData.height(); ++y) {
			const unsigned char* scanLine = imageData.scanLine(y);
			for(int x = 0; x < imageData.width(); ++x) {
				// First scale all values to range [0.0, 1.0]
				double pixelValue = static_cast<double>(*(scanLine++)) / 255.0;
				double backgroundValue = static_cast<double>(*(backgroundImage++)) / 65535.0; 

				// No gain exists, so use formula of AMTSingle
				resultImage[i] = pixelValue / backgroundValue - 1.0;
	
				++i;
			}
		}
#ifdef _DEBUG
		return QVector<double>::fromStdVector(resultImage);
#else
		return resultImage;
#endif
	}

	// Simple background correction: just subtracts background from signal
	static QVector<double> substractBackground(const QImage& imageData, const unsigned short* backgroundImage) 
	{
#ifdef _DEBUG
		// Stupid workaround for VS2010 QT plugin for debugging (QVector entries cannot be seen in variables window)
		std::vector<double> resultImage;
#else
		QVector<double> resultImage;
#endif
		resultImage.resize(imageData.width()*imageData.height());
		int i = 0;
		for(int y = 0; y < imageData.height(); ++y) {
			const unsigned char* scanLine = imageData.scanLine(y);
			for(int x = 0; x < imageData.width(); ++x) {
				// First scale all values to range [0.0, 1.0]
				double pixelValue = static_cast<double>(*(scanLine++)) / 255.0;
				double backgroundValue = static_cast<double>(*(backgroundImage++)) / 65535.0; 

				// Very stupid formula
				resultImage[i] = pixelValue - backgroundValue;

				++i;
			}
		}
#ifdef _DEBUG
		return QVector<double>::fromStdVector(resultImage);
#else
		return resultImage;
#endif
	}

	// Helper function to extract numbers from path strings: will return number after last occurence of separator in provided string
	static int getNumberFromString(const QString& s, const QString& separator) 
	{
		// Find separator
		int index = s.lastIndexOf(separator);
		if(index != -1) {
			// Skip separator
			index += separator.length();

			// Extract number
			QString numberString;
			int number = 0;
			while(index < s.size() && s[index].isDigit())
				number = number * 10 + s[index++].digitValue();

			// Done
			return number;
		}

		return -1;
	}

	/**
	 * Check if directory exists and if not, create it.
	 * @return true if directory was created successfully or already exists.
	 */
	static bool createDirectoryThreadSafe(const QString& path, bool pathContainsFileName = false)
	{
		QString s = QDir::fromNativeSeparators(path);
		
		// Remove file name
		if(pathContainsFileName) {
			int idx = s.lastIndexOf("/");
			if(idx > 0)
				s = s.left(idx);
		}

		// Create folder
		if(!QDir(s).exists()) {
			// Synchronization
			m_createDirectoryMutex.lock();

			// Check again, to make sure no other thread created the directory in the mean time
			if(!QDir(s).exists()) {
				if(!QDir(s).mkpath(s)) {
					m_createDirectoryMutex.unlock();
					return false;
				}
			}

			m_createDirectoryMutex.unlock();
		}

		return true;
	}

	/**
	 * Open image, with support for 16 bit tif and png images (will be converted to 8 bit).
	 */
	static QImage openGrayScaleImage(const QString& imageFileName, int bp = -1, int wp = -1, QString* outError = 0);

	/**
	 * Load segmentation from PNG grayscale image (1 bit bw mask, 8 bit bw mask, or 16 bit label map) and convert
	 * to list of blobs.
	 * @return 0 if successful, 1 if file was not found, 2 if file was found but could not be read, 3 if segmentation image has unexpected size.
	 */
	static int loadSegmentationFromPNG(const QString& imageFileName, std::vector<Blob>& outSegmentationData, int sizeX, int sizeY, int foreGroundColor = 255, int foreGoundPixelConnectivity = 4);

	/**
	 * Parse csv data. For numeric data, consider using Tools::parseCsvDataDouble() instead.
	 * @param data string containing csv data.
	 * @param sep separator used in csv file.
	 * @param outDataRows data, numbers are automatically parsed.
	 * @param outColumnNames if not null, entries of first row will be returned as column names.
	 * @param columnsToFind can be used to provide column names to look for, the corresponding indexes will be stored in outColumnsToFindIndexes.
	 * @param outColumnsToFindIndexes if column names to look for were provided in columnsToFind, their column indexes in the returned data will be stored here (or -1 for not found columns).
	 * @return 0 if successful or error code.
	 */
	static int parseCsvData(QString& data, 
							const QString sep, 
							QVector<QVector<QVariant>>& outDataRows, 
							QVector<QString>* outColumnNames = nullptr, 
							const QVector<QString>* columnsToFind = nullptr,
							QVector<int>* outColumnsToFindIndexes = nullptr)
	{
		QTextStream s(&data, QIODevice::ReadOnly);
		s.setLocale(QLocale("C"));

		// Parse data line by line
		bool parseHeader = outColumnNames != nullptr;
		int numColumns = -1;
		QVector<QVariant> firstDataLine;
		while(!s.atEnd()) {
			// Read line, remove whitespaces
			QString line = s.readLine().trimmed();
			if(line.isEmpty())
				continue;

			// Split it
			QList<QString> split = line.split(sep, QString::KeepEmptyParts, Qt::CaseSensitive);
			if(numColumns == -1) {
				numColumns = split.size();
			}
			else {
				if(split.size() != numColumns)
					// Line has invalid number of columns
					return 1;
			}

			// Parse line
			if(parseHeader) {
				// Parse header
				parseHeader = false;
				outColumnNames->resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					(*outColumnNames)[c] = split[c];
				}
			}
			else {
				// Parse data row
				outDataRows.push_back(QVector<QVariant>());
				QVector<QVariant>& newLine = outDataRows.back();
				newLine.resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					if(firstDataLine.size()) {
						// Use format of first line
						if(firstDataLine[c].type() == QVariant::Double) {
							// Treat as double
							bool ok = false;
							double d = split[c].toDouble(&ok);
							if(!ok)
								// Value should be double but cannot be parsed
								return 2;
							newLine[c] = d;
						}
						else {
							// Must be string
							newLine[c] = split[c];
						}
					}
					else {
						// Determine format
						bool isDouble = false;
						double d = split[c].toDouble(&isDouble);
						if(isDouble)
							// Treat as double
							newLine[c] = d;
						else
							// Treat as string
							newLine[c] = split[c];
					}
				}
				if(firstDataLine.size() == 0)
					firstDataLine = newLine;
			}
		}

		// Look for columns
		if(outColumnNames && columnsToFind && outColumnsToFindIndexes) {
			outColumnsToFindIndexes->resize(columnsToFind->size());
			for(int c = 0; c < columnsToFind->size(); ++c) {
				(*outColumnsToFindIndexes)[c] = outColumnNames->indexOf((*columnsToFind)[c]);
			}
		}	

		return 0;
	}

	/**
	 * Parse csv data that contains only doubles (i.e. only numeric values).
	 * @param data string containing csv data.
	 * @param sep separator used in csv file.
	 * @param outDataRows data, numbers are automatically parsed.
	 * @param outColumnNames if not null, entries of first row will be returned as column names.
	 * @param columnsToFind can be used to provide column names to look for, the corresponding indexes will be stored in outColumnsToFindIndexes.
	 * @param outColumnsToFindIndexes if column names to look for were provided in columnsToFind, their column indexes in the returned data will be stored here (or -1 for not found columns).
	 * @return 0 if successful or error code.
	 */
	static int parseCsvDataDouble(QString& data, 
							const QString sep, 
							QVector<QVector<double>>& outDataRows, 
							QVector<QString>* outColumnNames = nullptr, 
							const QVector<QString>* columnsToFind = nullptr,
							QVector<int>* outColumnsToFindIndexes = nullptr)
	{
		QTextStream s(&data, QIODevice::ReadOnly);
		s.setLocale(QLocale("C"));

		// Parse data line by line
		bool parseHeader = outColumnNames != nullptr;
		int numColumns = -1;
		while(!s.atEnd()) {
			// Read line, remove whitespaces
			QString line = s.readLine().trimmed();
			if(line.isEmpty())
				continue;

			// Split it
			QList<QString> split = line.split(sep, QString::KeepEmptyParts, Qt::CaseSensitive);
			if(numColumns == -1) {
				numColumns = split.size();
			}
			else {
				if(split.size() != numColumns)
					// Line has invalid number of columns
					return 1;
			}

			// Parse line
			if(parseHeader) {
				// Parse header
				parseHeader = false;
				outColumnNames->resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					(*outColumnNames)[c] = split[c];
				}
			}
			else {
				// Parse data row
				outDataRows.push_back(QVector<double>());
				QVector<double>& newLine = outDataRows.back();
				newLine.resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();

					// Treat as double
					bool ok = false;
					double d = split[c].toDouble(&ok);
					if(!ok)
						// Value should be double but cannot be parsed
						return 2;
					newLine[c] = d;
				}
			}
		}

		// Look for columns
		if(outColumnNames && columnsToFind && outColumnsToFindIndexes) {
			outColumnsToFindIndexes->resize(columnsToFind->size());
			for(int c = 0; c < columnsToFind->size(); ++c) {
				(*outColumnsToFindIndexes)[c] = outColumnNames->indexOf((*columnsToFind)[c]);
			}
		}	

		return 0;
	}

private:
	// Helper function to extract numbers from path strings (separator must have length 2)
	static int getNumberFromPath(const QString& _dir, const char* SEPARATOR)
	{
		// Find position marker
		int index = _dir.lastIndexOf(SEPARATOR);
		if(index != -1) {
			// Skip separator
			index += 2;

			// Extract position
			QString position;
			while(index < _dir.size() && _dir[index].isDigit()) 
				position += _dir[index++];

			// Convert to int
			bool ok;
			int intPos = position.toInt(&ok);

			// Check for error
			if(!ok)
				return -1;

			// Return position number
			return intPos;
		}

		return -1;
	}

	// Create directory mutex
	static QMutex m_createDirectoryMutex;

	//// Helper
	//static ImageProcessingTools m_imageProcessingTools;
};



#endif // tools_h__