/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef faster_defs_h__
#define faster_defs_h__

// STL
//#include <malloc.h>
#include <stdlib.h>
#include <cassert>

//////////////////////////////////////////////////////////////////////////
// Compiler specific global definitions
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Microsoft Visual C++

// Intrinsics
#include <intrin.h>

// Fundamental data types requiring special care
typedef __int64 fasterInt64;
typedef unsigned __int64 fasterUInt64;

#else 
//#ifdef __GNUC__
//// GNU GCC
//#warning Note: Aligned memory is not yet supported for GCC.
//
//#include <inttypes.h>
//
//// Fundamental data types requiring special care
//typedef int64_t fasterInt64;
//typedef uint64_t fasterUInt64;
//
//#else
// Unknown compiler
#warning Unknown compiler - memory alignment and bitscan optimizations disabled.

// Fundamental data types requiring special care
typedef long long fasterInt64;
typedef unsigned long long fasterUInt64;

//#endif
#endif

namespace faster {

	//////////////////////////////////////////////////////////////////////////
	// Compile time settings
	//////////////////////////////////////////////////////////////////////////

	// If allocated memory should be aligned according to cache lines to avoid false sharing. Increases speed when using 
	// multi-threading but also slightly increases memory consumption.
	const bool FASTER_ALIGN_MEMORY = true;

	// Cache line size, does nothing if FASTER_ALIGN_MEMORY is false.
	const unsigned int FASTER_CACHE_LINE_SIZE = 64;

	//////////////////////////////////////////////////////////////////////////
	// Namespace global definitions
	//////////////////////////////////////////////////////////////////////////

	// Number of gray levels, must be dividable by 64
	static const unsigned int NUM_GRAY_LEVELS = 256;

	//////////////////////////////////////////////////////////////////////////
	// Compiler specific namespace global definitions
	//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER


	/**
	 * Check if code is compiled for 64 bit architecture
	 * @return true if 64 bit architecture
	 */
	inline bool is64BitArchitecture()
	{
#ifdef _WIN64
		return true;
#else
		return false;
#endif
	}

	/**
	 * Memory allocation function. If FASTER_ALIGN_MEMORY is true, memory will be aligned.
	 * @param size number of bytes to allocate.
	 * @param alignment desired memory alignment, has no effect if FASTER_ALIGN_MEMORY is false.
	 * @return pointer to allocated memory block
	 */
	inline void* faster_malloc(size_t size, size_t alignment)
	{
		if(faster::FASTER_ALIGN_MEMORY)
			return _aligned_malloc(size, alignment);
		else
			return malloc(size);
	}

	/**
	 * Free memory that was allocated using faster_malloc()
	 * @param p pointer to memory block to be freed
	 */
	inline void faster_free(void* p)
	{
		if(faster::FASTER_ALIGN_MEMORY)
			_aligned_free(p);
		else
			free(p);
	}

	/**
	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
	 * @param lowerBound reference to lower bound variable (below this index, no bit is set in bitMask)
	 * @return index of first found bit that is set or -1 if no bit is set
	 */
	inline int faster_bitScanReverse256(fasterUInt64* bitMask, unsigned int& lowerBound)
	{
		unsigned long tmp;

		// In debug mode, ensure that lower bound is correct
#ifdef _DEBUG
		for(int i = 0; i < lowerBound / 64; ++i)
			assert(bitMask[i] == 0);
#endif

		// Find lowest bit
#ifdef _WIN64
		// x64, use _BitScanForward64 on 64 bit array
		for(int i = lowerBound / 64; i < NUM_GRAY_LEVELS / 64; ++i) {	
			if(_BitScanForward64(&tmp, bitMask[i])) {
				int grayLevel = tmp + i * 64;
				lowerBound = grayLevel;
				return grayLevel;
			}
		}

		// Nothing found
		return -1;
#else
		// x86, use _BitScanForward treating bit mask as 32 bit array
		unsigned long* pTmp = (unsigned long*)bitMask;
		for(int i = lowerBound / 32; i < NUM_GRAY_LEVELS / 32; ++i) {
			if(_BitScanForward(&tmp, pTmp[i])) {
				int grayLevel = tmp + i * 32;
				lowerBound = grayLevel;
				return grayLevel;
			}
		}

		// Nothing found
		return -1;
#endif
	}



#else
//#ifdef __GNUC__
//
//	/**
//	 * Check if code is compiled for 64 bit architecture
//	 * @return true if 64 bit architecture
//	 */
//	inline bool is64BitArchitecture()
//	{
//		if(sizeof(int*) == 8)
//			return true;
//		else
//			return false;
//	}
//
//	/**
//	 * Memory allocation function. If FASTER_ALIGN_MEMORY is true, memory will be aligned.
//	 * @param size number of bytes to allocate.
//	 * @param alignment (ignored).
//	 * @return pointer to allocated memory block
//	 */
//	inline void* faster_malloc(size_t size, size_t alignment)
//	{
//		return malloc(size);
//	}
//
//	/**
//	 * Free memory that was allocated using faster_malloc()
//	 * @param p pointer to memory block to be freed
//	 */
//	inline void faster_free(void* p)
//	{
//		free(p);
//	}
//
//	/**
//	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
//	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
//	 * @return index of first found bit that is set or -1 if no bit is set
//	 */
//	inline int faster_bitScanReverse256(fasterUInt64* bitMask)
//	{
//		unsigned int tmp;
//		// Check if 64 bit system
//		if(is64BitArchitecture()) {
//			// x64
//			for(int i = 0; i < NUM_GRAY_LEVELS / 64; ++i) {
//				tmp = __builtin_ffs(bitMask[i]);
//				if(tmp)
//					return tmp + i * 64;
//			}
//
//			// Nothing found
//			return -1;
//		}
//		else {
//			// x86
//			unsigned long* pTmp = (unsigned long*)bitMask;
//			for(int i = 0; i < NUM_GRAY_LEVELS / 32; ++i) {
//				tmp = __builtin_ffs(pTmp[i]);
//				if(tmp)
//					return tmp + i * 32;
//			}
//
//			// Nothing found
//			return -1;
//		}
//	}
//#else

	/**
	 * Check if code is compiled for 64 bit architecture
	 * @return true if 64 bit architecture
	 */
	inline bool is64BitArchitecture()
	{
		if(sizeof(int*) == 8)
			return true;
		else
			return false;
	}

	/**
	 * Memory allocation function. If FASTER_ALIGN_MEMORY is true, memory will be aligned.
	 * @param size number of bytes to allocate.
	 * @param alignment (ignored).
	 * @return pointer to allocated memory block
	 */
	inline void* faster_malloc(size_t size, size_t alignment)
	{
		return malloc(size);
	}

	/**
	 * Free memory that was allocated using faster_malloc()
	 * @param p pointer to memory block to be freed
	 */
	inline void faster_free(void* p)
	{
		free(p);
	}

	/**
	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
	 * @param lowerBound reference to lower bound variable (below this index, no bit is set in bitMask)
	 * @return index of first found bit that is set or -1 if no bit is set
	 */
	inline int faster_bitScanReverse256(fasterUInt64* bitMask, unsigned int& lowerBound)
	{

		// In debug mode, ensure that lower bound is correct
#ifdef _DEBUG
		for(int i = 0; i < lowerBound / 64; ++i)
			assert(bitMask[i] == 0);
#endif

		// Check if 64 bit system
		if(is64BitArchitecture()) {
			fasterUInt64 mask = -1;
			for(int i = lowerBound / 64; i < NUM_GRAY_LEVELS / 64; ++i) {
				if(bitMask[i] & mask) {
					for(int j = 0; j < 64; ++j) {
						if(bitMask[i] & ((fasterUInt64)1 << j)) {
							int grayLevel = j + i * 64;
							lowerBound = grayLevel;
							return grayLevel;
						}
					}
				}
			}
		}
		else {
			// We could also use the previous loop, but in 32bit systems, this is much faster actually.
			unsigned int mask = -1;
			unsigned int *tmp = (unsigned int*)bitMask;
			for(int i = lowerBound/32; i < NUM_GRAY_LEVELS / 32; ++i) {
				if(tmp[i] & mask) {
					for(int j = 0; j < 32; ++j) {
						if(tmp[i] & ((unsigned int)1 << j)) {
							int grayLevel = j + i * 32;
							lowerBound = grayLevel;
							return grayLevel;
						}
					}
				}
			}
		}

		// Nothing found
		return -1;
	}

//#endif
#endif

	/**
	 * Set specified bit in specified bitmask
	 * @param bitmask pointer to bitmask (not checked if valid)
	 * @param bitNumber number of bit to set starting with least significant bit of bitmask[0], 
	 *		  must be in [0, 8*size of bitmask - 1] (not checked)
	 */
	inline void setBit(unsigned char* bitmask, unsigned int bitNumber)
	{
		bitmask[bitNumber / 8] |= (unsigned char)1 << (bitNumber % 8);
	}

	/**
	 * Check if bit is set
	 * @param bitmask pointer to bitmask (not checked if valid)
	 * @param bitNumber number of bit to test starting with least significant bit of bitmask[0] (not checked if valid)
	 * @return 0 if bit is not set
	 */
	inline int testBit(unsigned char* bitmask, unsigned int bitNumber)
	{
		return bitmask[bitNumber / 8] & (unsigned char)1 << (bitNumber % 8);
	}

}
#endif // faster_defs_h__