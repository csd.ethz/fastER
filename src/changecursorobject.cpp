/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "changecursorobject.h"

// Qt
#include <QDebug>



// Windows (required as work-around for problem (in Qt5.2.1) that caliing QApplication::processEvents() not excluding user events is required for setOverrideCursor() to take effect, but not excluding user events can result in recursive calls to user event handlers which can cause application crashes etc.)
#ifdef Q_OS_WIN
#include <Windows.h>
#endif

ChangeCursorObject::ChangeCursorObject(const QCursor& cursor /*= QCursor(Qt::WaitCursor)*/, QWidget* widget /*= nullptr */)
	: m_widget(widget), m_cursorRestored(false)
{
	// Init more variables
#ifdef Q_OS_WIN
	m_waitCursorHandle = NULL;
	m_previousCursorHandle = NULL;
#endif

	// Change cursor
	if(m_widget) {	
		m_oldCursor = m_widget->cursor();

		// Change cursor only when necessary
		if(m_oldCursor.shape() == cursor.shape()) {
			//  Nothing to do
			m_cursorRestored = true;
		}
		else {
			m_widget->setCursor(cursor);				

			// Required in QT5 for cursor change to take effect
			QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
		}
	}
	else {
		// Change cursor only when necessary
		QCursor* curCursor = QApplication::overrideCursor();
		if(curCursor && curCursor->shape() == cursor.shape()) {
			//  Nothing to do
			m_cursorRestored = true;
		}
		else {

/* 
Explanation for the following #ifdef block:
	QApplication::setOverrideCursor() sometimes only works when calling
	QApplication::processEvents() afterwards (without ExcludeUserInputEvents flag) for unknown 
	reasons (bug in Qt5.21?), but without ExcludeUserInputEvents processEvents() 
	can have side effects (recursive calls to e.g. click handlers after accidental double-clicks resulting in 
	multi-threading like problems including program crashes).
	More details: after calling setOverrrideCursor, the program receives a
	WM_MOUSEMOVE event and only after calling DispatchMessage for this event,
	the cursor is actually changed - no workaround with Qt seems possible.
*/
#ifdef Q_OS_WIN
			
			if(cursor.shape() == Qt::WaitCursor) {
				// Change cursor using Windows API (see comment above for details)
				m_waitCursorHandle = (HCURSOR)LoadImage(0, IDC_WAIT, IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE | LR_SHARED);
				m_previousCursorHandle = SetCursor(m_waitCursorHandle);
			}
			else {
				QApplication::setOverrideCursor(cursor);
				QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);	
			}
#else
			// No Windows, so use default way (should be no problem on other OSs)
			QApplication::setOverrideCursor(cursor);
			QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);	
#endif
		}
	}
}

void ChangeCursorObject::restoreCursor()
{
	//qDebug() << this << ": ChangeCursorObject::restoreCursor() - m_oldCursor=" << m_oldCursor.shape() << ", m_widget=" << reinterpret_cast<unsigned long long>(m_widget) << ", m_cursorRestored=" << m_cursorRestored;
	if(m_cursorRestored)
		return;

	// Check if cursor was changed using Windows API
#ifdef Q_OS_WIN
	if(m_waitCursorHandle != NULL) {
		SetCursor(m_previousCursorHandle);
		return;
	}
#endif

	// Restore cursor
	if(m_widget)
		m_widget->setCursor(m_oldCursor);
	else
		QApplication::restoreOverrideCursor();

	m_cursorRestored = true;
}
