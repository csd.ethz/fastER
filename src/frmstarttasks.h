/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmstarttasks_h__
#define frmstarttasks_h__

#include "ui_frmStartTasks.h"


class FrmMainWindow;


class FrmStartTasks : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor
	 */
	FrmStartTasks(QWidget* parent = 0) 
		: QWidget(parent)
	{
		m_ui.setupUi(this);
	}

	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(200, 276);
	}


private:

	// GUI
	Ui::StartTasks m_ui;


	friend FrmMainWindow;
};


#endif // frmstarttasks_h__
