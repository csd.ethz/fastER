/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef experimentexternalindividualimages_h__
#define experimentexternalindividualimages_h__


// Project
#include "experiment.h"
#include "pictureindex.h"
#include "tools.h"
#include "exceptionbase.h"
#include "logging.h"

// Qt
#include <QString>
#include <QHash>
#include <QPair>
#include <QVector>
#include <QSharedPointer>
#include <QDebug>
#include <QList>
#include <QSet>

// STL
#include <algorithm>


/**
 * Experiment consisting of one image per PictureIndex, but not using arbitrary file structure.
 */
class ExperimentExternalIndividualImages : public Experiment {

	Q_OBJECT

public:

	/**
	 * Constructor. Initializes experiment, can throw ExceptionBase if experiment is invalid.
	 */
	ExperimentExternalIndividualImages(const QString& path, int blackPoint = -1, int whitePoint = -1)
		: Experiment(path)
	{
		m_picSizeX = -1;
		m_picSizeY = -1;
		m_blackPoint = blackPoint;
		m_whitePoint = whitePoint;
	}


	/**
	 * Add mapping of file to PictureIndex and vice versa - existing mappings will be replaced. 
	 * The fileName should use '/' separators.
	 * Not thread-safe.
	 * Can throw.
	 */
	void addImageMapping(const PictureIndex& pi, const QString& fileName)
	{
		// Convert full path to experiment relative path
		QString expRelPath = fullPathToExpRelPath(fileName);

		//qDebug() << pi << " - " << expRelPath;

		// Update index
		m_imagePaths[pi] = expRelPath;
		m_imageIndexes[expRelPath] = pi;

		// Update min/max PI
		m_maxPictureIndex = m_maxPictureIndex.isValid() ? qMax(m_maxPictureIndex, pi) : pi;
		m_minPictureIndex = m_minPictureIndex.isValid() ? qMin(m_minPictureIndex, pi) : pi;

		// Update image sizes
		if(m_picSizeX < 0 && pi.channel == 0) {
			QString fullImagePath = m_path + fileName;
			QString errString;
			QImage img = Tools::openGrayScaleImage(fullImagePath, m_blackPoint, m_whitePoint, &errString);
			if(img.isNull())
				throw ExceptionBase(__FUNCTION__, QString("Experiment initialization failed: cannot open image %1 - details: %2").arg(fullImagePath).arg(errString));
			m_picSizeX = img.width();
			m_picSizeY = img.height();
		}

		// Add position
		if(!m_positionNumbersSet.contains(pi.positionNumber)) {
			m_positionNumbersSet.insert(pi.positionNumber);
			m_positionNumbers.push_back(pi.positionNumber);
			qSort(m_positionNumbers);
		}
	}

	/**
	 * Clear index.
	 * Not thread-safe.
	 */
	void removeAllMappings()
	{
		m_imagePaths.clear();
		m_imageIndexes.clear();
		m_minPictureIndex = PictureIndex();
		m_maxPictureIndex = PictureIndex();
	}

	/**
	 * Map PictureIndex to actual file path.
	 * @return experiment relative path to image or empty string if image does not exist.
	 */
	QString mapPictureIndex(const PictureIndex& pi) const
	{
		return m_imagePaths.value(pi, QString());
	}

	/**
	 * Map experiment relative path of image to PictureIndex.
	 * @return PictureIndex of image or invalid PictureIndex if image is not found.
	 */
	PictureIndex mapImageFileName(const QString& imageExperimentRelativePath)
	{
		return m_imageIndexes.value(imageExperimentRelativePath, PictureIndex());
	}

	/**
	 * @reimplemented.
	 */
	std::vector<PictureIndex> getAllPictureIndexesInPosition(int position);
	int getImage(const PictureIndex& pi, QImage& outImage);
	int getImage(const PictureIndex& pi, cv::Mat& outImage);
	bool hasImage(const PictureIndex& pi) const;
	PictureIndex getRandomImage(bool allowDifferentChannel);
	int getBackgroundImage(const PictureIndex& pi, std::shared_ptr<unsigned char>& outImage, int& outBitDepth, int& outWidth, int& outHeight);
	int getGainImage(const PictureIndex& pi, bool loadIfNecessary, std::shared_ptr<unsigned char>& outImag, int& outBitDepth, int& outWidth, int& outHeight);
	int getSegmentationData(const PictureIndex& pi, std::vector<Blob>& outSegmentationData, int segmentationMethod, int expectedSizeX, int expectedSizeY);
	int writeSegmentationData(const PictureIndex& pi, const QImage& maskImage, int segmentationMethod = 0);
	int writeSegmentationData(const PictureIndex& pi, unsigned short* labelMap, int sizeX, int sizeY, int segmentationMethod = 0);
	int writeSegmentationQuantification(const PictureIndex& pi, const QString& quantificationData, int segmentationMethod = 0);
	int readSegmentationQuantification(const PictureIndex& pi, QString& outData, int segmentationMethod = 0);
	PictureIndex getMinPictureIndex() const;
	PictureIndex getMaxPictureIndex() const;
	int getImageSizeX(const PictureIndex& pi) const;
	int getImageSizeY(const PictureIndex& pi) const;
	PictureIndex converFileNameToPictureIndex(const QString& fileName) const
	{
		return getPictureIndexFromFileName(fileName);
	}
	QString getImageFileNameRelative(const PictureIndex& pi, bool includeFileExtension = true) const
	{
		// Get relative file name
		QString fileName = m_imagePaths.value(pi, QString());
		if(!includeFileExtension) {
			int idx = fileName.lastIndexOf('.');
			if(idx >= 0)
				fileName = fileName.left(idx);
		}
		return fileName;
	}

private:

	/**
	 * Mapping of file names to PictureIndex and vice versa.
	 * @return empty string/invalid PictureIndex if specified PictureIndex/file name does not exist.
	 */
	QString getImageExperimentRelativeFileName(const PictureIndex& pi, bool addExtension = true) const
	{
		// This must be set
		assert(pi.positionNumber >= 0 && pi.timePoint >= 0 && pi.channel >= 0);

		// Get relative file name
		QString imageFileName = m_imagePaths.value(pi, QString());
		if(imageFileName.isEmpty()) {
			return QString();
		}
		
		// Remove extension
		if(!addExtension) {
			int idx = imageFileName.lastIndexOf(".");
			if(idx > 0)
				imageFileName = imageFileName.left(idx);
		}

		return imageFileName;
	}
	QString getBackgroundImageExperimentRelativeFileName(const PictureIndex& pi) const
	{
		// Get file name and position name
		QString expRelImageFileName = getImageExperimentRelativeFileName(pi, false);
		if(expRelImageFileName.isEmpty())
			return "";

		// Background images are in background folder and always have extension png
		QString imageFileName = "background/";
		imageFileName += expRelImageFileName;
		imageFileName += ".png";

		return imageFileName;
	}
	QString getGainImageExperimentRelativeFileName(const PictureIndex& pi) const
	{
		// Background images are in background folder and always have extension png
		QString imageFileName = "background/";

		// Add position folder
		QString expRelImageFileName = getImageExperimentRelativeFileName(pi, false);
		if(expRelImageFileName.isEmpty())
			return "";
		QString posFolder = getPositionNameByExpRelativeFileName(expRelImageFileName);
		imageFileName += posFolder;
		imageFileName += '/';

		// Add file name
		imageFileName += QString("gain_w%1.png").arg(pi.channel, 2, 10, QChar('0'));

		return imageFileName;
	}
	QString getSegmentationImageExperimentRelativeFileName(const PictureIndex& pi, int segmentationMethodIndex, bool addExtension = true) const
	{
		// This must be set
		assert(pi.positionNumber >= 0 && pi.timePoint >= 0 && pi.channel >= 0);

		// Add position folder with segmentation subfolder
		QString expRelImageFileName = getImageExperimentRelativeFileName(pi, false);
		if(expRelImageFileName.isEmpty())
			return "";
		QString fileName;
		QString posFolder = getPositionNameByExpRelativeFileName(expRelImageFileName, &fileName);
		QString imageFileName = posFolder;
		imageFileName += "/segmentation/";

		// Add file name
		imageFileName += fileName;
		imageFileName += QString("_m%1_mask").arg(segmentationMethodIndex, 2, 10, QChar('0'));
		if(addExtension)
			imageFileName += ".png";

		return imageFileName;
	}
	PictureIndex getPictureIndexFromFileName(const QString& fileName) const
	{
		return m_imageIndexes.value(fileName);
	}
	QString fullPathToExpRelPath(const QString& imgPath) const
	{
		// Cut off everything until the last occurrence of "/<expName>/"
		// (needed for backward compatibility: conversion of image paths
		// that were created when experiment was in a different location)
		QString imgPathConv = QDir::fromNativeSeparators(imgPath);
		QString expNamePart = "/" + m_name + "/";
		int idx = imgPathConv.lastIndexOf(expNamePart);
		if(idx >= 0)
			return imgPathConv.mid(idx + expNamePart.length());
		else
			return imgPath;
	}
	QString getPositionNameByExpRelativeFileName(const QString& expRelPath, QString* outFileName = 0) const
	{
		// Extract part left of file name
		QString s = QDir::fromNativeSeparators(expRelPath);
		int idx = s.lastIndexOf('/');
		QString posName;
		if(idx > 0) {
			posName = s.left(idx);
			if(outFileName)
				*outFileName = s.mid(idx + 1);
		}
		else {
			posName = ".";
			if(outFileName)
				*outFileName = expRelPath;
		}

		return posName;
	}

	// Index: PictureIndex to relative file name
	QHash<PictureIndex, QString> m_imagePaths;

	// Index: relative file name to PictureIndex
	QHash<QString, PictureIndex> m_imageIndexes;

	// Minimal and maximal PictureIndex of the experiment (combination of min/max pos, tp, etc., so no image has to exist at either of the two)
	PictureIndex m_minPictureIndex;
	PictureIndex m_maxPictureIndex;

	// Gain images for different positions and wavelengths, only loaded on demand
	QHash<PictureIndex, std::shared_ptr<unsigned char>> m_gainImages;
	QMutex m_gainImagesMutex;

	// WL0 image size
	int m_picSizeX;
	int m_picSizeY;

	// Blackpoint/whitepoint for image conversion to 8 bit (-1 if not specified)
	int m_blackPoint;
	int m_whitePoint;

	// Position numbers - only used to efficiently update m_positionNumbers when an image is added
	QSet<int> m_positionNumbersSet;
};


#endif // experimentexternalindividualimages_h__
