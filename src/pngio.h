/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef pngwriter_h__
#define pngwriter_h__

/**
 * Important:
 * PNG functions will crash if compiled using a different CRT library version (e.g. debug
 * instead of release) to the one used for compiling libpng.
 *
 * Version history:
 *		140429 (2.01): removed inclusion of Windows.h; replaced LPCSTR with const char*; changed default value of littleEndian in open function
 *		140428 (2.02): changed acquisition date parameters to pointers
 *		140804 (2.03): added support for png grayscale images with PNG_COLOR_TYPE_PALETTE color type
 *		141017 (2.04): added support for color types PNG_COLOR_TYPE_RGB, PNG_COLOR_TYPE_RGBA and PNG_COLOR_TYPE_GRAY_ALPHA (alpha channel is ignored)
 *		141106 (2.05): fixed bug when reading 1-bit images: pixels are expanded to 8-bit by libpng, but 1 bit was returned as bit depth to caller
 */


/**
 * Save 16 bit image.
 * @param imageData pointer to image data (make sure Endianess of data fits to littleEndian parameter).
 * @param width image width.
 * @param height image height.
 * @param fileName png filename.
 * @param littleEndian if imageData is little Endian or big Endian.
 * @return: 
 *		0=no error
 *		1=open file failed
 *		2=libPNG - png_create_write_struct failed
 *		3=libPNG - png_create_info_struct failed failed
 *		4=libPNG - error during init_io
 *		5=libPNG - error during writing header
 *		6=libPNG - error during writing bytes
 *		7=libPNG - error during end of write
 *		8=invalid parameters
 */
int save16BitImageAsPng(unsigned char* imageData, int width, int height, const char* fileName, bool littleEndian, int acquisitionYear = -1, int acquisitionMonth = -1, int acquisitionDay = -1, int acquisitionHour = -1, int acquisitionMinute = -1, int acquisitionSecond = -1);

/**
 * Save 8 bit image.
 * @param imageData pointer to image data.
 * @param width image width.
 * @param height image height.
 * @param fileName png filename.
 * @return: 
 *		0=no error
 *		1=open file failed
 *		2=libPNG - png_create_write_struct failed
 *		3=libPNG - png_create_info_struct failed failed
 *		4=libPNG - error during init_io
 *		5=libPNG - error during writing header
 *		6=libPNG - error during writing bytes
 *		7=libPNG - error during end of write
 *		8=invalid parameters
 */
int save8BitImageAsPng(unsigned char* imageData, int width, int height, const char* fileName, int acquisitionYear = -1, int acquisitionMonth = -1, int acquisitionDay = -1, int acquisitionHour = -1, int acquisitionMinute = -1, int acquisitionSecond = -1);

/**
 * Open 1, 8 or 16 bit png grayscale image. Caller must delete[] *imageData afterwards.
 * @param littleEndian if true, data will be returned in little Endian, otherwise in big Endian.
 */
int openPngGrayScaleImage(const char* fileName, unsigned char** imageData, int& outBitDepth, int& outWidth, int& outHeight, bool littleEndian = true, int* acquisitionYear = 0, int* acquisitionMonth = 0, int* acquisitionDay = 0, int* acquisitionHour = 0, int* acquisitionMinute = 0, int* acquisitionSecond = 0);


#endif // pngwriter_h__
