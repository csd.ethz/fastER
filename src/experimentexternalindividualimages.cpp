/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "experimentexternalindividualimages.h"

// Qt
#include <QFile>
#include <QDir>
#include <QXmlStreamReader>
#include <QRect>
#include <QRectF>
#include <QImage>
#include <QImageReader>

// Project
#include "pngio.h"
#include "settings.h"
#include "fastdirectorylisting.h"
#include "positioncluster.h"
#include "logging.h"
#include "cattiffhandler.h"


int ExperimentExternalIndividualImages::getImage(const PictureIndex& pi, QImage& outImage)
{
	// Create file name
	QString imageRelativeFileName = getImageExperimentRelativeFileName(pi);
	if(imageRelativeFileName.isEmpty())
		return 1;
	QString imageFileName = m_path + imageRelativeFileName;

	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Try to open
	QString errString;
	outImage = Tools::openGrayScaleImage(imageFileName, m_blackPoint, m_whitePoint, &errString);
	if(outImage.isNull()) {
		gErr << "Error: cannot open image " << imageFileName << " - details: " << errString << '\n';
		return 2;
	}

	// Ensure format is :Format_Indexed8
	if(outImage.format() != QImage::Format_Indexed8) {
		outImage = outImage.convertToFormat(QImage::Format_Indexed8);
		outImage.setColorTable(Tools::getColorTableGrayIndexed8());
	}

	// Success
	return 0;
}

int ExperimentExternalIndividualImages::getImage(const PictureIndex& pi, cv::Mat& outImage)
{
	// Create file name
	QString imageRelativeFileName = getImageExperimentRelativeFileName(pi);
	if(imageRelativeFileName.isEmpty())
		return 1;
	QString imageFileName = m_path + imageRelativeFileName;

	// Check if exists
	if(!QFile::exists(imageFileName))
		return 1;

	// Try to open
	outImage = cv::imread(imageFileName.toStdString(), CV_LOAD_IMAGE_UNCHANGED);
	if(!outImage.data)
		return 2;

	// Success
	return 0;
}

bool ExperimentExternalIndividualImages::hasImage(const PictureIndex& pi) const
{
	// Create file name
	QString expRelFile = getImageExperimentRelativeFileName(pi);
	if(expRelFile.isEmpty())
		return false;
	QString imageFileName = m_path + expRelFile;

	// Check if exists
	return QFile::exists(imageFileName);
}

PictureIndex ExperimentExternalIndividualImages::getRandomImage(bool allowDifferentChannel)
{
	// Get all image indexes
	QList<PictureIndex> allImages = m_imagePaths.keys();
	if(allImages.size() == 0)
		return PictureIndex();

	// Determine required wl
	int desiredWl = Settings::get<int>(Settings::KEY_TRACKINGWL);

	// Select one randomly
	while(allImages.size()) {
		PictureIndex randomPI = allImages.takeAt(qrand()%allImages.size());
		if(randomPI.channel == desiredWl)
			return randomPI;
	}

	// No image with selected channel exists -> return one with different channel
	allImages = m_imagePaths.keys();
	if(allImages.size() == 0)
		return PictureIndex();
	else
		return allImages[0];
}

int ExperimentExternalIndividualImages::getBackgroundImage(const PictureIndex& pi, std::shared_ptr<unsigned char>& outImage, int& outBitDepth, int& outWidth, int& outHeight)
{
	// Create file name
	QString imageFileName = m_path + getBackgroundImageExperimentRelativeFileName(pi);

	// Check if exists
	if(!QFile::exists(imageFileName)) {
		gLog << "Note: requested background image for image " << pi.toString() << " not found: " << imageFileName << "\n";
		return 1;
	}

	// Try to open
	int bitDepth, width, height;
	unsigned char* backgroundImageData = 0;
	int errCode = openPngGrayScaleImage(QDir::toNativeSeparators(imageFileName).toLatin1(), &backgroundImageData, bitDepth, width, height, true);	// Open background image, too

	// Check for error
	if(errCode || !backgroundImageData) {
		if(backgroundImageData)
			delete[] backgroundImageData;
		return 2;
	}

	// Success
	outImage = std::shared_ptr<unsigned char>(backgroundImageData, std::default_delete<unsigned char[]>());
	outWidth = width;
	outHeight = height;
	outBitDepth = bitDepth;
	return 0;
}

int ExperimentExternalIndividualImages::getGainImage(const PictureIndex& pi, bool loadIfNecessary, std::shared_ptr<unsigned char>& outImag, int& outBitDepth, int& outWidth, int& outHeight)
{
	QMutexLocker lock(&m_gainImagesMutex);

	// Use only position and channel as key, all other values must be default-initialized
	PictureIndex key;
	key.positionNumber = pi.positionNumber;
	key.channel = pi.channel;

	// Check if image exists
	outImag = m_gainImages.value(key);
	if(outImag)
		// Image already loaded
		return 0;

	if(!loadIfNecessary)
		// Image should not be loaded
		return 3;

	// Check if image exists
	QString path = m_path + getGainImageExperimentRelativeFileName(pi);
	if(!QFile::exists(path))
		return 1;

	// Try to load image
	int bitDepth;
	int width, height;
	unsigned char* gainImageData = 0;
	if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&gainImageData, bitDepth, width, height, true) != 0) {
		delete[] gainImageData;
		return 2;
	}

	// Success
	outImag = std::shared_ptr<unsigned char>(gainImageData, std::default_delete<unsigned char[]>());
	m_gainImages[key] = outImag;
	return 0;
}



int ExperimentExternalIndividualImages::getSegmentationData(const PictureIndex& pi, std::vector<Blob>& outSegmentationData, int segmentationMethod, int expectedSizeX, int expectedSizeY)
{
	// Get segmentation file name
	QString imageFileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);

	// Try to open
	return Tools::loadSegmentationFromPNG(imageFileName, outSegmentationData, expectedSizeX, expectedSizeY);
}

int ExperimentExternalIndividualImages::writeSegmentationData(const PictureIndex& pi, const QImage& maskImage, int segmentationMethod /*= 0*/)
{
	// Create output folder
	QString filePath = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);
	if(!Tools::createDirectoryThreadSafe(filePath, true))
		return 2;

	// Save image data
	if(!maskImage.save(filePath))
		return 1;

	return 0;
}

int ExperimentExternalIndividualImages::writeSegmentationData(const PictureIndex& pi, unsigned short* labelMap, int sizeX, int sizeY, int segmentationMethod /*= 0*/)
{
	// Create output folder
	QString filePath = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod);
	if(!Tools::createDirectoryThreadSafe(filePath, true))
		return 2;

	// Save image
	if(save16BitImageAsPng(reinterpret_cast<unsigned char*>(labelMap), sizeX, sizeY, filePath.toLatin1(), true) != 0)
		return 1;

	return 0;
}

int ExperimentExternalIndividualImages::writeSegmentationQuantification(const PictureIndex& pi, const QString& quantificationData, int segmentationMethod /*= 0*/)
{
	// Get file name
	QString fileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod, false);
	if(fileName.isEmpty())
		return 1;
	fileName += ".csv";

	// Create output folder
	if(!Tools::createDirectoryThreadSafe(fileName, true))
		return 2;

	// Write data
	QFile csvFile(fileName); 
	if(!csvFile.open(QIODevice::WriteOnly))
		return 3;
	QTextStream ss(&csvFile);
	ss.setLocale(QLocale("C"));		// Make sure formatting is right
	ss << quantificationData;

	return (ss.status() != QTextStream::WriteFailed) ? 0 : 4;
}

int ExperimentExternalIndividualImages::readSegmentationQuantification(const PictureIndex& pi, QString& outData, int segmentationMethod /*= 0*/)
{
	// Get file name
	QString fileName = m_path + getSegmentationImageExperimentRelativeFileName(pi, segmentationMethod, false);
	if(fileName.isEmpty())
		return 2;
	fileName += ".csv";

	// Check if file exists
	if(!QFile::exists(fileName))
		return 1;

	// Read data
	QFile csvFile(fileName); 
	if(!csvFile.open(QIODevice::ReadOnly))
		return 3;
	QTextStream ss(&csvFile);
	ss.setLocale(QLocale("C"));		// Make sure formatting is right
	outData = ss.readAll();

	return (ss.status() == QTextStream::Ok) ? 0 : 4;
}
PictureIndex ExperimentExternalIndividualImages::getMinPictureIndex() const
{
	return m_minPictureIndex;
}

PictureIndex ExperimentExternalIndividualImages::getMaxPictureIndex() const
{
	return m_maxPictureIndex;
}

int ExperimentExternalIndividualImages::getImageSizeX(const PictureIndex& pi) const
{
	assert(pi.channel == 0 || !pi.isValid());
	return m_picSizeX;
}

int ExperimentExternalIndividualImages::getImageSizeY(const PictureIndex& pi) const
{
	assert(pi.channel == 0 || !pi.isValid());
	return m_picSizeY;
}

std::vector<PictureIndex> ExperimentExternalIndividualImages::getAllPictureIndexesInPosition(int position)
{
	// Look in m_imagePaths
	std::vector<PictureIndex> foundImages;
	const QList<PictureIndex> imagePathKeys = m_imagePaths.keys();
	for(auto it = imagePathKeys.begin(); it != imagePathKeys.end(); ++it) {
		if(it->positionNumber != position)
			continue;
		foundImages.push_back(*it);
	}

	return foundImages;
}

