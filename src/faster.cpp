/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "faster.h"

// C Library
#include <memory.h>

// DEBUG
#include <iostream>
#include <QDebug>
#include <QMessageBox>

namespace faster {

	FastERDetector::FastERDetector()
	{
		// Default parameters
		m_minSize = 10;
		m_maxSize = 1000;

		// Init variables
		m_brightOnDark = false;
		m_heapStacks = 0;
		m_visitedPixels = 0;
		m_backgroundLabelsBitmask = 0;
		m_backgroundLabelsBitmaskSize = 0;
		m_allocImageSizeX = 0;
		m_allocImageSizeY = 0;
		m_imageData = 0;
		m_imageSizeX = 0;
		m_svmModel = 0;
		m_imageSizeY = 0;
		m_componentsCount = 0;
		m_minProbability = 0.5;
		m_sumOfScores = 0.0;
		m_numCheckedRegions = 0;
		m_heapBitMaskLowerBound = 0;
		m_constrainedPositiveRegions = nullptr;

#ifdef _DEBUG
		maxPixelsQueueSize = 0;
		splitStopsDueToNumberOfChildren = 0;
		splitStopsDueToOverlapConflicts = 0;
#endif

		// Initialize components
		initializeComponents();
	}

	FastERDetector::FastERDetector(int imageSizeX, int imageSizeY)
	try	
	{
		// Default parameters
		m_minSize = 10;
		m_maxSize = static_cast<unsigned int>(0.75f * imageSizeX * imageSizeY);

		// Init variables
		m_brightOnDark = false;
		m_heapStacks = 0;
		m_backgroundLabelsBitmask = 0;
		m_visitedPixels = 0;
		m_backgroundLabelsBitmaskSize = 0;
		m_allocImageSizeX = 0;
		m_allocImageSizeY = 0;
		m_imageData = 0;
		m_svmModel = 0;
		m_imageSizeX = imageSizeX;
		m_imageSizeY = imageSizeY;
		m_componentsCount = 0;
		m_minProbability = 0.5;
		m_sumOfScores = 0.0;
		m_numCheckedRegions = 0;
		m_heapBitMaskLowerBound = 0;
		m_constrainedPositiveRegions = nullptr;

#ifdef _DEBUG
		maxPixelsQueueSize = 0;
		splitStopsDueToNumberOfChildren = 0;
		splitStopsDueToOverlapConflicts = 0;
#endif

		// Initialize components
		initializeComponents();

		// Initial allocations
		resetInternalStructures();
	}
	// Handler to avoid memory leak in case of error, automatically re-throws to caller
	catch(const FastERException&) {
		// Cleanup
		faster_free(m_heapStacks);
		faster_free(m_visitedPixels);
	}

	
	faster::FastERDetector::~FastERDetector()
	{
		// Cleanup
		faster_free(m_heapStacks);
		faster_free(m_visitedPixels);
		faster_free(m_imageData);
		faster_free(m_backgroundLabelsBitmask);
	}

	
	void faster::FastERDetector::resetInternalStructures()
	{
		// DEBUG
#ifdef _DEBUG
		processedPixels = std::vector<int>(m_imageSizeX*m_imageSizeY, 0);
		splitStopsDueToNumberOfChildren = 0;
		splitStopsDueToOverlapConflicts = 0;
#endif

		// Check if we have to reallocate m_imageData
		if(!m_imageData || m_imageSizeX * m_imageSizeY > m_allocImageSizeX * m_allocImageSizeY) {
			// Realloc
			faster_free(m_imageData);
			m_imageData = (unsigned char*)faster_malloc(m_imageSizeX * m_imageSizeY, faster::FASTER_CACHE_LINE_SIZE);

			// Check for error
			if(!m_imageData)
				throw FastERException(FastERException::ALLOC_FAILED);
		}

		// Check if we have to reallocate anything else
		if(m_imageSizeX * m_imageSizeY <= m_allocImageSizeX * m_allocImageSizeY)
			return;

		// Free old data
		faster_free(m_heapStacks);
		faster_free(m_visitedPixels);

		// Allocations
		m_heapStacks = (unsigned int*)faster_malloc(m_imageSizeX * m_imageSizeY * BYTES_PER_HEAP_ELEMENT, faster::FASTER_CACHE_LINE_SIZE);
		m_visitedPixels = (unsigned char*)faster_malloc((m_imageSizeX * m_imageSizeY + 7) / 8, faster::FASTER_CACHE_LINE_SIZE);

		// Check for failed allocations
		if(!m_heapStacks || !m_visitedPixels)
			throw FastERException(FastERException::ALLOC_FAILED);

		// Update alloc image sizes
		m_allocImageSizeX = m_imageSizeX;
		m_allocImageSizeY = m_imageSizeY;
	}

	
	int faster::FastERDetector::segmentImage( const svm_model* svmModel, const double statsMean[], const double statsStandardDeviation[],const unsigned char* imageData, int imageSizeX, int imageSizeY, bool darkOnBright, int minSize, int maxSize, double minProbability, const std::unordered_set<RegionID, RegionIDHasher>& constrainedPositiveRegions, bool disableErSplitting )
	{
		//// OH170106
		//disableErSplitting = true;

		// Set vars
		m_svmModel = svmModel;
		m_disableErSplitting = disableErSplitting;
		std::copy(statsMean, statsMean + NUM_FEATURES, m_statsMean);
		std::copy(statsStandardDeviation, statsStandardDeviation + NUM_FEATURES, m_statsStandardDeviation);
		m_numRejectedBySvm = 0;
		m_numAcceptedBySvm = 0;
		m_minProbability = minProbability;
		m_foundRegionsPositive.clear();
		m_foundRegionsNegative.clear();
		m_constrainedPositiveRegions = &constrainedPositiveRegions;

		// segment image
		processImageInternal<MODE_SEGMENTATION>(imageData, imageSizeX, imageSizeY, darkOnBright, minSize, maxSize);

#ifdef _DEBUG
		qDebug() << "segmentImage() finished - splitStopsDueToNumberOfChildren = " << splitStopsDueToNumberOfChildren << ", splitStopsDueToOverlapConflicts = " << splitStopsDueToOverlapConflicts;
#endif

		// Top component now contains segmentation
		assert(m_componentsCount == 2);
		m_foundRegionsPositive = std::move(m_C[1].positiveRegionSeeds);
		return m_foundRegionsPositive.size();
	}


	void FastERDetector::getSamplesFromLabels( const unsigned char* imageData, int imageSizeX, int imageSizeY, bool darkOnBright, const std::vector<std::vector<int> >& foregroundLabels, const std::vector<int>& backgroundPixels, bool randomSubsetFromNegSamples, int minSize, int maxSize, bool disableErSplitting )
	{
		//// OH170106
		//disableErSplitting = true;

		//outTotalNumberOfNegSamples = 0;
		m_foundRegionsPositive.clear();
		m_foundRegionsNegative.clear();
		m_disableErSplitting = disableErSplitting;
		//outNumPosSamples = 0;
		//outNumNegSamples = 0;

		// DEBUG
		//qDebug() << "getSamplesFromLabels(): imageData=" << ((void*)imageData) << " darkOnBright=" << darkOnBright << " foregroundLabels.size()=" << foregroundLabels.size() << " backgroundPixels.size()=" << backgroundPixels.size();

		// Store foreground labels and init map
		m_foregroundLabels = foregroundLabels;
		if(m_foregroundLabelsMap.size() < imageSizeX * imageSizeY)
			m_foregroundLabelsMap.resize(imageSizeX*imageSizeY);
		std::fill(m_foregroundLabelsMap.begin(), m_foregroundLabelsMap.end(), -1);
		for(int iLabel = 0; iLabel < m_foregroundLabels.size(); ++iLabel) {
			for(auto itPixel = m_foregroundLabels[iLabel].begin(); itPixel != m_foregroundLabels[iLabel].end(); ++itPixel) 
				m_foregroundLabelsMap[*itPixel] = iLabel;
		}

		// Prepare background labels mask
		if(m_backgroundLabelsBitmaskSize < imageSizeX * imageSizeY) {
			m_backgroundLabelsBitmask = (unsigned char*)faster_malloc((imageSizeX * imageSizeY + 7) / 8, faster::FASTER_CACHE_LINE_SIZE);
			m_backgroundLabelsBitmaskSize = imageSizeX*imageSizeY;
		}
		memset(m_backgroundLabelsBitmask, 0, (imageSizeX * imageSizeY+7)/8);
		for(auto itPx = backgroundPixels.begin(); itPx != backgroundPixels.end(); ++itPx)
			setBit(m_backgroundLabelsBitmask, *itPx);

		// Prepare lists of found samples
		m_numPosSamples = 0;
		m_positiveSamples = std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>>(foregroundLabels.size(), SubRegionSeed(-1));
		m_positiveSampleScores = std::vector<double>(foregroundLabels.size(), 0.0);
		m_negativeSamples.clear();

		// Prepare observations matrixes
		m_positiveSampleObservations = sample_feature_list_type(foregroundLabels.size()*NUM_FEATURES, 0.0);
		m_negativeSampleObservations.clear();

		// Get labels
		processImageInternal<MODE_GET_SAMPLES>(imageData, imageSizeX, imageSizeY, darkOnBright, minSize, maxSize);
		assert(m_numPosSamples <= foregroundLabels.size());
		if(!m_numPosSamples) {
			m_positiveSampleObservations.clear();
			return;
		}

		// Filter out positive observations for which no region was found
		sample_feature_list_type positiveSampleObservationsFiltered;
		std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> positiveSamplesFiltered;
		positiveSampleObservationsFiltered.resize(m_numPosSamples * NUM_FEATURES);
		positiveSamplesFiltered.resize(m_numPosSamples);
		int iSample = 0;
		for(int iLabel = 0; iLabel < m_foregroundLabels.size(); ++iLabel) {
			if(m_positiveSamples[iLabel].seedER >= 0) {
				// Update m_sumOfScores
				m_sumOfScores += m_positiveSampleScores[iLabel];

				// Copy observation
				positiveSamplesFiltered[iSample] = m_positiveSamples[iLabel];
				for(int iFeature = 0; iFeature < NUM_FEATURES; ++iFeature)
					positiveSampleObservationsFiltered[iSample*NUM_FEATURES + iFeature] = m_positiveSampleObservations[iLabel*NUM_FEATURES + iFeature];

				++iSample;
			}
		}
		m_positiveSampleObservations = std::move(positiveSampleObservationsFiltered);
		//outNumPosSamples = positiveSamplesFiltered.size();
		m_foundRegionsPositive = std::move(positiveSamplesFiltered);

		// Take random subset from neg samples to have same number as positive samples
		//outTotalNumberOfNegSamples = m_negativeSamples.size();
		if(randomSubsetFromNegSamples && m_negativeSamples.size() > m_numPosSamples) {
			std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> negSampleSeedsFiltered;
			negSampleSeedsFiltered.reserve(m_numPosSamples);
			std::vector<int> subSample = getSample(m_numPosSamples, m_negativeSamples.size()-1);
			for(auto itSample = subSample.begin(); itSample != subSample.end(); ++itSample)
				negSampleSeedsFiltered.push_back(m_negativeSamples[*itSample]);
			m_negativeSamples = std::move(negSampleSeedsFiltered);
		}
		//outNumNegSamples = m_negativeSamples.size();
		m_foundRegionsNegative = std::move(m_negativeSamples);

		//// Debug
		//QString msg;
		//for(auto it = m_foundRegionsPositive.begin(); it != m_foundRegionsPositive.end(); ++it)
		//	msg += QString("%1 ").arg(it->first);
		//qDebug() << "Found " << m_foundRegionsPositive.size() << " positive samples: " << msg;
		//msg = "";
		//for(auto it = m_foundRegionsNegative.begin(); it != m_foundRegionsNegative.end(); ++it)
		//	msg += QString("%1 ").arg(it->first);
		//qDebug() << "Found " << m_foundRegionsNegative.size() << " negative samples:" << msg;

#ifdef _DEBUG
		qDebug() << "getSamples() finished - splitStopsDueToNumberOfChildren = " << splitStopsDueToNumberOfChildren << ", splitStopsDueToOverlapConflicts = " << splitStopsDueToOverlapConflicts;
#endif
	}

	template<int MODE>
	void FastERDetector::processImageInternal(const unsigned char* imageData, int imageSizeX, int imageSizeY, bool darkOnBright, int minSize, int maxSize)
	{
		m_numCheckedRegions = 0;
		m_numCheckedRegionsTooSmall = 0;
		m_numCheckedRegionsTooBig = 0;
		m_minSize = minSize;
		m_maxSize = maxSize;
		m_sumOfScores = 0.0;
		m_C[1].positiveRegionSeeds.clear();
		m_C[1].pixelsQueue.clear();
		//m_C[1].pixelsFromDiscardedSubComponents.clear();

		// Check parameters
		if(!imageData || !imageSizeX || !imageSizeY)
			throw FastERException(FastERException::INVALID_PARAMETERS);

		// Store data
		m_imageSizeX = imageSizeX;
		m_imageSizeY = imageSizeY;
		m_brightOnDark = !darkOnBright;

		// Reallocate if necessary
		resetInternalStructures();
		assert(m_imageData);

		// Dark on bright
		if(darkOnBright) {
			// Copy image data
			memcpy(m_imageData, imageData, imageSizeX*imageSizeY);

			// Prepare for processing
			prepareForNextImage();

			// Process image
			processExtremalRegions<MODE>(imageSizeX, imageSizeY);
		}
		else {
			// Invert image
			for(int i = 0; i < imageSizeX * imageSizeY; ++i) 
				m_imageData[i] = ~imageData[i];		// 255 - imageData[i]

			// Prepare for processing
			prepareForNextImage();

			// Process image
			processExtremalRegions<MODE>(imageSizeX, imageSizeY);
		}
	}

	template<int MODE>
	void FastERDetector::processExtremalRegions(int imageSizeX, int imageSizeY)
	{
		// Dummy component
		addComponent<MODE>(faster::NUM_GRAY_LEVELS);

		// Process pixels starting with first
		int currentPixel = 0;
		int currentGrayLevel = m_imageData[currentPixel];
		unsigned int currentEdge = EDGE_UP;
		setBit(m_visitedPixels, currentPixel);

MainLoopStart:

		// Push empty component on stack with currentGrayLevel
		addComponent<MODE>(currentGrayLevel);

		while(1) {
			// Evaluate neighbors (clockwise, EDGE_UP is always last)
			do {
				currentEdge = getNextEdge(currentEdge);

				// Get neighbor pixel index
				int neighbor = getNeighbor(currentPixel, currentEdge);
				if(neighbor != -1) {
					// Check if neighbor already accessed
					if(!testBit(m_visitedPixels, neighbor)) {
						// Mark as accessed
						setBit(m_visitedPixels, neighbor);

						// Check neighbor gray level
						int neighborGrayLevel = m_imageData[neighbor];
						if(neighborGrayLevel >= currentGrayLevel) {
							heapPushPixel(neighbor, neighborGrayLevel, EDGE_UP);
						}
						else {
							// Smaller pixel found - put current pixel back on stack (with current edge, after pop, next edge will be selected)						
							heapPushPixel(currentPixel, currentGrayLevel, currentEdge);

							// Consider neighbor
							currentPixel = neighbor;
							currentGrayLevel = neighborGrayLevel;
							currentEdge = EDGE_UP;

							// Go to outer loop
							goto MainLoopStart;
						}
					}
				}
			} while(currentEdge != EDGE_UP);

			// Add current pixel to current component
			Component* topComponent = &m_C[m_componentsCount - 1];
			assert(topComponent->currentGrayLevel == currentGrayLevel);

			// Add pixel to top components
			topComponent->addPixel<MODE>(this, currentGrayLevel, currentPixel);

			// If top component allows splitting, also enqueue it to add it to correct child later
			if(topComponent->allowSplitting)
				topComponent->enqueuePixel(this, currentPixel);

			// Get next pixel and edge from heap
			if(!heapPopTopPixel(currentPixel, currentEdge))
				return;

			// Get next gray level
			int newGrayLevel = m_imageData[currentPixel];
			if(newGrayLevel != currentGrayLevel) {
				// newGrayLevel must be greater than currentGrayLevel -> process stack
				processStack<MODE>(newGrayLevel);

				// Process next pixel
				currentGrayLevel = newGrayLevel;
			}
		}
	}
	

	void faster::FastERDetector::prepareForNextImage()
	{

		/**
		 * Start by preparing the heap
		 */

		// Set heap stack lengths and bitmasks to 0
		memset(m_heapStacksLengths, 0, sizeof(m_heapStacksLengths));
		memset(m_heapBitMask, 0, sizeof(m_heapBitMask));
		m_heapBitMaskLowerBound = NUM_GRAY_LEVELS;
		memset(m_visitedPixels, 0, (m_allocImageSizeX * m_allocImageSizeY+7)/8);

		// Create histogram of pixel intensities and store it in m_heapStacksIndexes
		memset(m_heapStacksIndexes, 0, sizeof(m_heapStacksIndexes));
		for(int i = 0; i < m_imageSizeX * m_imageSizeY; ++i)
			++m_heapStacksIndexes[m_imageData[i]];

		// Calculate m_heapStacksIndexes values, starting with gray level 0
		int startIndex = 0;
		for(int i = 0; i < NUM_GRAY_LEVELS; ++i) {
			// Remember count of gray level i
			int tmp = m_heapStacksIndexes[i];

			// Set start index for gray level i
			m_heapStacksIndexes[i] = startIndex;

			// Calc start index for gray level i + 1
			startIndex += tmp;
		}

		/**
		 * Clear components stack
		 */
		clearComponents();
	}

	template<int MODE> void FastERDetector::processStack( int newGrayLevel )
	{
		do {
			/**
			 * All children of topComp are now known, so initialize children, add pixels 
			 * of discarded children, add queued pixels, and post-process children
			 */
			Component* topComp = &m_C[m_componentsCount - 1]; 
			topComp->initializeChildren();
			//topComp->addDiscardedRegionPixels<MODE>(this, imageData);
			topComp->addQueuedPixels<MODE>(this);
			topComp->postProcessChildren();

#ifdef _DEBUG
			// Debug checks
			if(topComp->numChildren) {
				assert(topComp->allowSplitting);
				int sumPixelsChildren = 0;
				double sumGradients = 0;
				int numGradients = 0;
				for(auto itChild = topComp->children; itChild != topComp->children + topComp->numChildren; ++itChild) {
					sumPixelsChildren += (*itChild)->stats.numPixels;
					//sumGradients += (*itChild)->stats.sumGradientMagnitudes;
					//numGradients += (*itChild)->stats.numGradients;
				}
				assert(sumPixelsChildren == topComp->completeEr->stats.numPixels);
				//assert(numGradients == topComp->completeEr->stats.numGradients);
				//assert(sumGradients == topComp->completeEr->stats.sumGradientMagnitudes);
			}
#endif

			/**
			 * Evaluate top component
			 */
			if(MODE == MODE_GET_SAMPLES) {
				scoreRegionAgainstLabels(topComp, topComp->completeEr);
				if(topComp->numChildren) {
					scoreRegionAgainstLabels(topComp, topComp->children[0], topComp->children[1], false);
					scoreRegionAgainstLabels(topComp, topComp->children[1], topComp->children[0], true);
				}
			}
			else {
				// Calculate segmentation scores
				double scoreEr = score(topComp->seedPixel, topComp->completeEr, -1);
				double subScoreNormalized = 0.0;
				double scoreSub1 = 0.0;
				double scoreSub2 = 0.0;
				if(topComp->numChildren) {
					assert(MAX_CHILDREN == 2 && topComp->numChildren == 2);
					scoreSub1 = score(topComp->seedPixel, topComp->children[0], 0);
					scoreSub2 = score(topComp->seedPixel, topComp->children[1], 1);

					// Consider children only if both are classified as positive or if one is constrained to be positive
					if((scoreSub1 > m_minProbability && scoreSub2 > m_minProbability) || scoreSub1 == std::numeric_limits<double>::infinity() || scoreSub2 == std::numeric_limits<double>::infinity() ) {
						subScoreNormalized = (scoreSub1 + scoreSub2) / 2.0;
					}
				}

				// Calculate normalized score of existing 
				double sumScoresChildrenNormalized = topComp->maxSumOfScores;

				//// OH170106: following was commented out 
				if(topComp->positiveRegionSeeds.size() > 1)
					sumScoresChildrenNormalized /= topComp->positiveRegionSeeds.size();
				
				// Extract best scoring combination and check if better than current regions
				if(scoreEr >= subScoreNormalized) {
					// ER has best score
					if(scoreEr > m_minProbability && scoreEr > sumScoresChildrenNormalized) {
						topComp->maxSumOfScores = scoreEr;
						topComp->positiveRegionSeeds.clear();
						topComp->positiveRegionSeeds.push_back(SubRegionSeed(topComp->seedPixel));
#ifdef _DEBUG
						topComp->positiveRegionSeeds.back().numPixels = topComp->completeEr->stats.numPixels;
						topComp->positiveRegionSeeds.back().numGradients = topComp->completeEr->stats.numGradients;
						topComp->positiveRegionSeeds.back().sumGradientMagnitudes = topComp->completeEr->stats.sumGradientMagnitudes;
#endif
					}
				}
				else if(subScoreNormalized > sumScoresChildrenNormalized) {
					//qDebug() << "1: x=[" << topComp->children[0]->watersheddingBoundingBox.minX << ", " << topComp->children[0]->watersheddingBoundingBox.maxX << "] y=[" << topComp->children[0]->watersheddingBoundingBox.minY << ", " << topComp->children[0]->watersheddingBoundingBox.maxY << "] size=" << topComp->children[0]->stats.numPixels;
					//qDebug() << "2: x=[" << topComp->children[1]->watersheddingBoundingBox.minX << ", " << topComp->children[1]->watersheddingBoundingBox.maxX << "] y=[" << topComp->children[1]->watersheddingBoundingBox.minY << ", " << topComp->children[1]->watersheddingBoundingBox.maxY << "] size=" << topComp->children[1]->stats.numPixels;

					// Sub1 and Sub2 have best score	
					assert(scoreSub1 > m_minProbability || scoreSub2 > m_minProbability);
					topComp->positiveRegionSeeds.clear();
					if(scoreSub1 > m_minProbability) {
						topComp->maxSumOfScores += scoreSub1;
						topComp->positiveRegionSeeds.push_back(SubRegionSeed(topComp->seedPixel, topComp->children[0]->watersheddingBoundingBox, topComp->children[1]->watersheddingBoundingBox, false));
#ifdef _DEBUG
						topComp->positiveRegionSeeds.back().numPixels = topComp->children[0]->stats.numPixels;
						topComp->positiveRegionSeeds.back().numGradients = topComp->children[0]->stats.numGradients;
						topComp->positiveRegionSeeds.back().sumGradientMagnitudes =topComp->children[0]->stats.sumGradientMagnitudes;
#endif
					}

					if(scoreSub2 > m_minProbability) {
						topComp->maxSumOfScores += scoreSub2;
						topComp->positiveRegionSeeds.push_back(SubRegionSeed(topComp->seedPixel, topComp->children[1]->watersheddingBoundingBox, topComp->children[0]->watersheddingBoundingBox, true));	
#ifdef _DEBUG
						topComp->positiveRegionSeeds.back().numPixels = topComp->children[1]->stats.numPixels;
						topComp->positiveRegionSeeds.back().numGradients = topComp->children[1]->stats.numGradients;
						topComp->positiveRegionSeeds.back().sumGradientMagnitudes =topComp->children[1]->stats.sumGradientMagnitudes;
#endif
					}
					assert(topComp->maxSumOfScores > 0.0);
				}
			}


			/**
			 * Either merge TopC with SecC or raise its graylevel
			 */

			// Compare newGrayLevel with graylevel of second component on stack
			if(newGrayLevel < m_C[m_componentsCount - 2].currentGrayLevel) {
				// No component exists for newGrayLevel, so increase top component's graylevel to newGrayLevel,
				// pixel with newGrayLevel will be added to existing top component then
				assert(topComp->numChildren == 0 || (topComp->numChildren >= 2 && topComp->allowSplitting));
				if(!m_disableErSplitting) {
					if(topComp->numChildren == 0) {
						// Must make topComp->completeEr to topComp's first child
						*topComp->children[0] = *topComp->completeEr;
						topComp->allowSplitting = true;
						topComp->numChildren = 1;
					}
				}

				topComp->setGrayLevel(newGrayLevel);
				return;
			}

			// Merge the two topmost components 
			mergeTopComponents<MODE>();
		} while(newGrayLevel > m_C[m_componentsCount - 1].currentGrayLevel);
	}


	std::vector<int, AlignedAllocator<int>> FastERDetector::fillRegion( int regionIndex, bool darkOnBright, bool* outRegionTouchesImageBorder, bool posSample )
	{
		// Get reference to vector with regions of interest
		const auto& regions = posSample ? m_foundRegionsPositive : m_foundRegionsNegative;
		assert(regionIndex < regions.size());
		assert(regions[regionIndex].numBoundingBoxes == 0 || regions[regionIndex].numBoundingBoxes > 1);

		// Check parameters
		const SubRegionSeed& seed = regions[regionIndex];
		if(!m_imageData || seed.seedER >= m_imageSizeX * m_imageSizeY)
			throw FastERException(FastERException::INVALID_PARAMETERS);

		// Initialize results vector
		int typicalRegionSize = std::max((m_minSize+m_maxSize)/2, 3);
		std::vector<int, AlignedAllocator<int>> pixels;
		pixels.reserve(typicalRegionSize);
		if(outRegionTouchesImageBorder)
			*outRegionTouchesImageBorder = false;

		// Reset bitmask of visited pixels
		memset(m_visitedPixels, 0, (m_allocImageSizeX * m_allocImageSizeY+7)/8);

		// Current number of elements in heap (which we will abuse for this)
		int heapCount = 0;

		// Fill region by using heap similar to processImage()
		int currentPixel = seed.seedER,
			threshold = m_imageData[currentPixel];
		unsigned int currentEdge = EDGE_UP;
		setBit(m_visitedPixels, currentPixel);
		while(1) {
			// Evaluate neighbors (clockwise, EDGE_UP is always last)
			do {
				currentEdge = getNextEdge(currentEdge);

				// Get neighbor pixel index
				int neighbor = getNeighbor(currentPixel, currentEdge);
				if(neighbor != -1) {
					// Check if neighbor already accessible
					if(!testBit(m_visitedPixels, neighbor)) {
						// Mark as accessible
						setBit(m_visitedPixels, neighbor);

						// Check neighbor gray level and ignore pixels outside containing ER (i.e. with higher gray level)
						int neighborGrayLevel = m_imageData[neighbor];
						if(neighborGrayLevel <= threshold) {
							m_heapStacks[heapCount++] = neighbor;
						}
					}
				}
			} while(currentEdge != EDGE_UP);

			int x = currentPixel % m_imageSizeX;
			int y = currentPixel / m_imageSizeX;
			assert(y < m_imageSizeY);

			// Check if pixel does belong to different SubRegion
			bool pixelBelongsToOtherSubRegion = false;
			if(seed.numBoundingBoxes) {
				// First entry in boundingBoxes array always belongs to this SubRegion
				int distToSubRegion = seed.boundingBoxes[0].manhattanDistance(x, y);
				for(int iOtherSubRegion = 1; iOtherSubRegion < seed.numBoundingBoxes; ++iOtherSubRegion) {
					int distToOtherSubRegion = seed.boundingBoxes[iOtherSubRegion].manhattanDistance(x, y);
					if(distToSubRegion >= distToOtherSubRegion) {
						// If other sub-region has higher priority, pixels with equal distance belong to it
						if(seed.boundingBoxPriorities[iOtherSubRegion] || distToSubRegion > distToOtherSubRegion)
							pixelBelongsToOtherSubRegion = true;
					}
				}				
			}

			if(!pixelBelongsToOtherSubRegion)
			{
				// Check if pixel touches border
				if(outRegionTouchesImageBorder && !*outRegionTouchesImageBorder) {
					*outRegionTouchesImageBorder = x == 0 || y == 0 || x == (m_imageSizeX - 1) || y == (m_imageSizeY - 1);
				}

				// Add current pixel to pixels list
				pixels.push_back(currentPixel);
			}

			// Get next pixel from heap
			if(!heapCount) {
				assert(pixels.size() <= m_maxSize && pixels.size() >= m_minSize);

				// In debug mode, quantify gradient features and compare with sufficient statistics
#ifdef _DEBUG
				assert(pixels.size() == seed.numPixels);
				double sumGradientMagnitudes = 0.0;
				int numGradients = 0;
				for(auto itPixel = pixels.cbegin(); itPixel != pixels.cend(); ++itPixel) {
					int x = *itPixel % m_imageSizeX;
					int y = *itPixel / m_imageSizeX;
					const int xSteps[] = {-1, 0, 1, 0};
					const int ySteps[] = {0, -1, 0, 1};
					for(int i = 0; i < 4; ++i) {
						int xNeighbor = x + xSteps[i];
						int yNeighbor = y + ySteps[i];
						if(xNeighbor >= 0 && yNeighbor >= 0 && xNeighbor < m_imageSizeX && yNeighbor < m_imageSizeY) {
							int indexLinear = yNeighbor * m_imageSizeX + xNeighbor;
							if(std::find(pixels.cbegin(), pixels.cend(), indexLinear) == pixels.cend()) {
								// Found neighbor within image boundaries but not within region --> count gradient
								++numGradients;
								double gradient = std::abs(m_imageData[indexLinear] - m_imageData[*itPixel]);
								sumGradientMagnitudes += gradient;
							}
						}
					}
				}
				assert(numGradients == seed.numGradients);
				assert(sumGradientMagnitudes == seed.sumGradientMagnitudes);
#endif

				// Done
				return pixels;
			}

			currentPixel = m_heapStacks[--heapCount];
		}
	}

	void FastERDetector::initializeComponents()
	{
		// Initialize pointers of Components to SubComponents
		int subComponentArrayIndex = 0;
		for(int iComponent = 0; iComponent <= NUM_GRAY_LEVELS; ++iComponent) {
			m_C[iComponent].completeEr = &m_subComponents[subComponentArrayIndex++];
			for(int iSubComponent = 0; iSubComponent < MAX_CHILDREN; ++iSubComponent) 
				m_C[iComponent].children[iSubComponent] = &m_subComponents[subComponentArrayIndex++];
		}
		assert(subComponentArrayIndex == (NUM_GRAY_LEVELS + 1) * (MAX_CHILDREN + 1));
	}
	

	/**
	 * Merge the top component on the stack into the second one
	 */
	template<int MODE> void FastERDetector::mergeTopComponents() 	
	{
		// Get components to merge
		Component* topC = &m_C[m_componentsCount - 1];
		Component* secC = &m_C[m_componentsCount - 2];
		--m_componentsCount;

		// topC must not have queued pixels
		assert(topC->pixelsQueue.size() == 0);
		//assert(topC->pixelsFromDiscardedSubComponents.size() == 0);

		// topC must not have pixels at graylevels >= secC->m_grayLevel
		assert(topC->currentGrayLevel < secC->currentGrayLevel);

		/**
		 * Merge segmentation results.
		 */
		if(topC->positiveRegionSeeds.size()) {
			// TopC has positiveRegionSeeds
			if(secC->positiveRegionSeeds.size()) {
				// Both have positiveRegionSeeds, merge
				secC->positiveRegionSeeds.insert(secC->positiveRegionSeeds.end(), topC->positiveRegionSeeds.begin(), topC->positiveRegionSeeds.end());
				secC->maxSumOfScores += topC->maxSumOfScores;
				topC->positiveRegionSeeds.clear();
			}
			else {
				// Only topC has positiveRegionSeeds, overwrite
				secC->positiveRegionSeeds = std::move(topC->positiveRegionSeeds);
				secC->maxSumOfScores = topC->maxSumOfScores;
			}
		}

		/**
		 *	Merge completeEr of topC into completeEr of secCC.
		 */
		secC->completeEr->merge<MODE>(*topC->completeEr);

		/**
		 * Handle nested sub regions
		 */

		// If secC does not allow splitting (or if splitting is disabled), there is nothing more to do
		if(!secC->allowSplitting || m_disableErSplitting) 
			return;
			
		// Get new sub regions of topC that have to be merged into secC
		SubComponent** newChildren[MAX_CHILDREN];
		int numNewChildren;
		if(topC->numChildren == 0) {
			// TopC becomes sub-region - initialize ws-boundingbox 
			topC->completeEr->watersheddingBoundingBox = topC->completeEr->stats.boundingBox;
			newChildren[0] = &topC->completeEr;
			numNewChildren = 1;
		}
		else {
			// SecC takes over children of topC 
			for(int iChild = 0; iChild < topC->numChildren; ++iChild) 
				newChildren[iChild] = &topC->children[iChild];
			numNewChildren = topC->numChildren;
		}

		// Stop if too many (i.e. more than 2)
		if(secC->numChildren + numNewChildren > MAX_CHILDREN) {
			secC->allowSplitting = false;

#ifdef _DEBUG
			++splitStopsDueToNumberOfChildren;
#endif

			return;
		}

		// Stop if overlap
		for(int iOld = 0; iOld < secC->numChildren; ++iOld) {
			for(int iNew = 0; iNew < numNewChildren; ++iNew) {
				if(secC->children[iOld]->boundingBoxesIntersect(**newChildren[iNew])) {
					secC->allowSplitting = false;

#ifdef _DEBUG
					++splitStopsDueToOverlapConflicts;
#endif

					return;
				}
			}

		}


		// Ok
		for(int i = 0; i < numNewChildren; ++i) {
			(*newChildren[i])->currentGrayLevel = secC->currentGrayLevel;
			std::swap(*(secC->children + secC->numChildren++), *newChildren[i]);
		}
		assert(secC->numChildren <= MAX_CHILDREN);
	}
}
