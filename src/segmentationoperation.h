/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperation_h__
#define segmentationoperation_h__

class JobSegmentation;
class JobRunnerSegmentation;

// Qt
#include <QHash>
#include <QVariant>
#include <QTextStream>

// Project
#include "imageprocessingtools.h"

/**
 * Represents a single segmentation operation, such as applying CLAHE, Blurring, MSER or fastER. Inherits from
 * QObject to allow safe downcasts without requiring C++ RTTI.
 */
class SegmentationOperation : public QObject {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	SegmentationOperation()
		: m_enabled(true)
	{}

	/**
	 * Execute segmentation operation. Must be thread safe if operating on different data and with different JobSegmentation/JobRunner instances.
	 * @param imageData pointer to image data to work on - change *imageData to change the pointer after reallocation to change buffer size (in this case, the old data will be deleted automatically).
	 * @param imageDataType pointer to image data type - change *imageDataType to change the type of image data.
	 * @param originalImageData original image data, i.e. without any previous operations applied on (always IDT_UInt8Gray).
	 * @param sizeX image width (in pixels).
	 * @param sizeY image height (in pixels)
	 * @param jobSegmentation the JobSegmentation instance executing this operation (if available).
	 * @param callingJobRunner the JobRunnerSegmentation (i.e. QThread) instance in which's context this operation is executed (if available, can be NULL otherwise!).
	 */
	virtual void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner) = 0;

	/**
	 * Virtual destructor.
	 */
	virtual ~SegmentationOperation() {}

	/**
	 * Get name of operation for display purposes.
	 */
	virtual QString getName() const = 0;

	/**
	 * Get name and values of parameters.
	 */
	virtual QHash<QString, QVariant> getParameters() const = 0;

	/**
	 * Set parameter values by parameter names.
	 */
	virtual void setParameters(const QHash<QString, QVariant>& parameters) = 0;

	/**
	 * Return string with operation name and parameters.
	 */
	virtual QString toString() const
	{
		QString ret;
		QTextStream s(&ret);
		s << getName() << "[";
		QHash<QString, QVariant> params = getParameters();
		for(auto it = params.begin(); it != params.end(); ++it) 
			s << it.key() << "=" << it.value().toString() << " ";
		s << "]";
		return ret;
	}

	/**
	 * Return if segmentation operation should be disabled in preview mode.
	 */
	virtual bool disableInPreview() const 
	{
		return false;
	}

	/**
	 * Return if segmentation operation is enabled.
	 */
	bool isEnabled() const
	{
		return m_enabled;
	}

private:

	/**
	 * If operation is enabled.
	 */
	bool m_enabled;

};


#endif // segmentationoperation_h__
