/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "jobsegmentation.h"

// Project
#include "segmentationoperation.h"
#include "jobrunnersegmentation.h"


void JobSegmentation::run(JobRunner* callingJobRunner)
{
	// Cast callingJobRunner to JobRunnerSegmentation, if available
	JobRunnerSegmentation* jobRunnerSeg = 0;
	if(callingJobRunner) {
#ifdef _DEBUG
		// In debug mode, use type-safe cast to ensure that callingJobRunner is actually a JobRunnerSegmentation
		jobRunnerSeg = qobject_cast<JobRunnerSegmentation*>(callingJobRunner);
		assert(jobRunnerSeg);
#else
		jobRunnerSeg = static_cast<JobRunnerSegmentation*>(callingJobRunner);
#endif
	}

	// Execute all segmentation operations
	unsigned char** imageDataPtrPtr = &m_imageData;
	for(auto it = m_operationList->operations.begin(); it != m_operationList->operations.end(); ++it) {
		SegmentationOperation* curSegOp = *it;
		if(!curSegOp->isEnabled() || (m_previewMode && curSegOp->disableInPreview()))
			continue;
		
		// Execute operation and remember address of image data
		unsigned char* oldPtr = *imageDataPtrPtr;
		curSegOp->run(imageDataPtrPtr, &m_imageDataType, m_imageDataOriginal, m_imageSizeX, m_imageSizeY, this, jobRunnerSeg);
		
		// Check if address of data has changed -> delete old data
		if(m_imageData != oldPtr)
			delete[] oldPtr;
	}
}
