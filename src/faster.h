/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef faster_h__
#define faster_h__

// Project
#include "faster_defs.h"

// STL
#include <vector>
#include <cassert>
#include <algorithm>
#include <unordered_set>

// LibSVM
#include "svm.h"

// Debug
#include <QDebug>


namespace faster {

	// Number of features calculated for each blob 
	const int NUM_FEATURES = 9;

	// Maximal number of nested regions to consider for each extremal region (more than
	// two are technically possible, but not really supported and may cause worse results)
	const int MAX_CHILDREN = 2;

	/**
	 * Exception class
	 */
	class FastERException {
	public:

		/**
		 * Error types
		 */
		enum ErrorType {
			INVALID_PARAMETERS = 0,
			ALLOC_FAILED = 1,
			INTERNAL_ERROR = 2,
			SVM_INVALIDPARAMETERS = 3,
		};

		/**
		 * Construct exception object, never throws.
		 * @param type error type, see error type constants.
		 */
		FastERException(ErrorType errorType) : m_errorType(errorType) {}

		/**
		 * Get error type, never throws.
		 * @return type of error, see error type constants.
		 */
		ErrorType getType() const {
			return m_errorType;
		}

	private:

		// Type of error
		ErrorType m_errorType;
	};

	/**
	 * Base class for classes with aligned memory and FastERException in case of error
	 */
	class FastERBase {
	public:

		/**
		 * Allocation
		 * @param s number of bytes to allocate. Throws FastERException if allocation fails.
		 * @return valid pointer to allocated block.
		 */
		void* operator new(size_t s) 	{
			// Allocate possibly aligned memory
			void* ret = faster_malloc(s, faster::FASTER_CACHE_LINE_SIZE);

			// Throw exception if allocation failed
			if(!ret)
				throw FastERException(FastERException::ALLOC_FAILED);

			// Allocation was successful
			return ret;
		}

		/**
		 * Deallocation
		 * @param pointer to memory block to be freed.
		 */
		void operator delete(void* p) 	{
			// Free possibly aligned memory
			faster_free(p);
		}

	};

	/**
	 * Allocator for STL containers with aligned memory. 
	 * Throws FastERException if allocation fails.
	 */
	template <typename T>
	class AlignedAllocator {
	public:

		// Typedefs
		typedef T value_type;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;

		// Rebind
		template <typename U> 
		struct rebind { typedef AlignedAllocator<U> other; };

		// Get address
		pointer address(reference r) const { return &r; }
		const_pointer address(const_reference r) const { return &r; }

		// Construction, destruction
		AlignedAllocator() {}
		template <typename U> AlignedAllocator(const AlignedAllocator<U>& ) {}
		~AlignedAllocator() {}

		// Allocation
		pointer allocate(size_type n, typename std::allocator<void>::const_pointer = 0) {
			// Aligned allocation
			pointer ret = (pointer)faster_malloc(n * sizeof(T), FASTER_CACHE_LINE_SIZE);
			if(!ret)
				throw FastERException(FastERException::ALLOC_FAILED);

			// Successful
			return ret;
		}

		void deallocate(pointer p, size_type n) {
			faster_free(p);
		}

		void construct(pointer p, const T& val) {
			new(p) T(val);
		}

		void destroy(pointer p) {
			p->~T();
		}

		// Max array size
		size_type max_size() const {
			size_type ret = ((size_type)-1) / sizeof(T);
			return (ret < 1 ? 1 : ret);
		}

		// Comparison
		bool operator==(AlignedAllocator const&) { return true; }
		bool operator!=(AlignedAllocator const& a) { return !operator==(a); }
	};

	/**
	 *	A bounding box.
	 */
	struct BoundingBox {
		int minX, maxX, minY, maxY;

		/**
			*	Check for overlap.
			*/
		bool intersects(const BoundingBox& other) const 
		{
			return std::max(minX, other.minX) <= std::min(maxX, other.maxX)
				&& std::max(minY, other.minY) <= std::min(maxY, other.maxY);
		}

		/**
			*	Manhattan distance to point.
			*/
		int manhattanDistance(int x, int y) const 
		{
			return std::max(std::max(minX - x, 0), std::max(x - maxX, 0)) +
				std::max(std::max(minY - y, 0), std::max(y - maxY, 0));
		}

		/**
			*	Get delta vector that has to be subtracted from provided point to reach closest pixel of bounding box (returns 0,0 if point is within).
			*/
		void getDeltaVectorToPoint(int x, int y, int& dX, int& dY) const 
		{
			if(x > maxX)
				dX = x - maxX;
			else if(x < minX)
				dX = x - minX;
			else 
				dX = 0;

			if(y > maxY)
				dY = y - maxY;
			else if(y < minY)
				dY = y - minY;
			else 
				dY = 0;
		}

		// Default constructor: leaves entries uninitialized
		BoundingBox() 
		{}

		// Constructor 
		BoundingBox(int _minX, int _maxX, int _minY, int _maxY)
			: minX(_minX), maxX(_maxX), minY(_minY), maxY(_maxY)
		{}

		// Comparison
		bool operator==(const BoundingBox& other) const
		{
			return minX == other.minX && minY == other.minY && maxX == other.maxX && maxY == other.maxY;
		}
	};

	/**
	 * A (sub-) region seed - consumes O(1) memory, but contains enough information to define the pixels belonging to 
	 * a (sub-) region. 
	 */
	struct SubRegionSeed {
			
		// Seed of extremal region containing this region
		int seedER;

		// Index of sub-region (-1 if complete ER)
		int subRegionIndex;

		// If region is not complete ER, but only sub-region: BoundingBoxes of this region and 
		// other sub-regions contained in the ER, and their priorities (true means that a pixel
		// that has same distance to this sub-region's bounding box and other sub-region's bounding
		// box belongs to other sub-region)
		BoundingBox boundingBoxes[MAX_CHILDREN];
		bool boundingBoxPriorities[MAX_CHILDREN];
		int numBoundingBoxes;

		// For debugging only: number of pixels, gradient length and magnitude
#ifdef _DEBUG
		int numPixels;
		double sumGradientMagnitudes;
		int numGradients;
#endif

		// Constructor
		SubRegionSeed(int _seedEr = -1, int _numBoundingBoxes = 0, int _subRegionIndex = -1)
			: seedER(_seedEr), numBoundingBoxes(_numBoundingBoxes), subRegionIndex(_subRegionIndex)
		{
#ifdef _DEBUG
			sumGradientMagnitudes = 0.0;
			numGradients = 0;
			numPixels = 0;
#endif
		}

		// Constructor: ER with two sub-regions
		SubRegionSeed(int _seedEr, const BoundingBox& subRegionBoundingBox, const BoundingBox& otherSubRegionBoundingBox, bool otherSubRegionComesFirst)
			: seedER(_seedEr), numBoundingBoxes(2), subRegionIndex(otherSubRegionComesFirst ? 1 : 0)
		{
			boundingBoxes[0] = subRegionBoundingBox;
			boundingBoxes[1] = otherSubRegionBoundingBox;
			boundingBoxPriorities[1] = otherSubRegionComesFirst;

#ifdef _DEBUG
			sumGradientMagnitudes = 0.0;
			numGradients = 0;
			numPixels = 0;
#endif
		}
	};

	/**
	 * An identifier for a (sub-) region: contains enough information to identify a (sub-) region internally, 
	 * but not sufficient to enumerate the pixels in it.
	 */
	struct RegionID {
		// Seed of extremal region containing this region
		int seedER;

		// Sub region index (-1 if complete ER)
		int subRegionIndex;

		// Constructor
		RegionID(const SubRegionSeed& seed) 
			: seedER(seed.seedER), subRegionIndex(seed.subRegionIndex)
		{}
		RegionID(int _seedEr, int _subRegionIndex)
			: seedER(_seedEr), subRegionIndex(_subRegionIndex)
		{}

		// Comparison
		bool operator==(const RegionID& other) const
		{
			return seedER == other.seedER && subRegionIndex == other.subRegionIndex;
		}

	};

	// Hash function for RegionID
	struct RegionIDHasher {
		std::size_t operator()(const RegionID& key) const 
		{
			return (std::hash<int>()(key.seedER) ^ 
				(std::hash<int>()(key.subRegionIndex) << 1));
		}
	};

	// Typedefs
	typedef std::vector<int, AlignedAllocator<int>> pixel_list_type;
	typedef std::vector<double, AlignedAllocator<double>> sample_feature_list_type;

	/**
	 * FastERDetector
	 * Main class containing fastER implementation. All relevant memory blocks can be aligned according
	 * to the template parameter ALIGN.
	 */
	class FastERDetector : public FastERBase {
	public:

		/**
		 * Default constructor.
		 */
		FastERDetector() ;

		/**
		 * Constructor
		 */
		FastERDetector(int imageSizeX, int imageSizeY);

		/**
		 * Destructor
		 */
		~FastERDetector();

		/**
		 * Extract positive and negative sample extremal regions to train the SVM classifier.
		 * Tries to find for each foreground label on best fitting extremal region and the same
		 * number of samples for background labels. Can throw exception, if not enough background samples were found.
		 */
		void getSamplesFromLabels(const unsigned char* imageData, 
									int imageSizeX, 
									int imageSizeY, 
									bool darkOnBright,
									const std::vector<std::vector<int>>& foregroundLabels, 
									const std::vector<int>& backgroundPixels,
									bool randomSubsetFromNegSamples,
									int minSize,
									int maxSize,
									bool disableErSplitting);

		/**
		 * Get features extracted from found samples druing last call to getSamplesFromLabels().
		 * @param posSampleFeatures receives observations for each positive label (only if sample has been found)
		 * @param negSampleFeatures receives all negative observations
		 */
		void getSampleFeatures(sample_feature_list_type& posSampleFeatures, sample_feature_list_type& negSampleFeatures)
		{
			posSampleFeatures = m_positiveSampleObservations;
			negSampleFeatures = m_negativeSampleObservations;
		}

		/**
		 * Segment image, returns number of found regions.
		 * If svmModel is null, a set of extremal regions within the minSize/maxSize limits will be returned.
		 */
		int segmentImage (
			const svm_model* svmModel,
			const double statsMean[],
			const double statsStandardDeviation[],
			const unsigned char* imageData, 
			int imageSizeX, 
			int imageSizeY, 
			bool darkOnBright, 
			int minSize, 
			int maxSize, 
			double minProbability,
			const std::unordered_set<RegionID, RegionIDHasher>& constrainedPositiveRegions,
			bool disableErSplitting);

		/**
		 * Get number of found foreground or background regions.
		 */
		int getNumRegions(bool foreground) const 
		{
			return foreground ? m_foundRegionsPositive.size() : m_foundRegionsNegative.size();
		}

		/**
		 * Fill region to get linear pixel indexes belonging to region
		 */
		std::vector<int, AlignedAllocator<int>> fillRegion(int regionIndex, bool darkOnBright, bool* outRegionTouchesImageBorder, bool posSample = true);

		/**
		 * Get identifiers of positive sample regions found by getSamplesFromLabels() to use them as parameter for segmentImage().
		 */
		void getConstrainedRegions(std::unordered_set<RegionID, RegionIDHasher>& positiveRegions) const
		{
			// Create identifier from each seed
			positiveRegions.clear();
			for(auto it = m_foundRegionsPositive.cbegin(); it != m_foundRegionsPositive.cend(); ++it) {
				positiveRegions.emplace(*it);
			}
		}

		/**
		 * @return number of investigated regions during last call to segmentImage() or getSamples().
		 */
		int getNumCheckedRegions() const 
		{
			return m_numCheckedRegions;
		}

		/**
		 * @return number of investigated regions during last call to segmentImage() or getSamples().
		 */
		int getNumCheckedRegionsTooSmall() const 
		{
			return m_numCheckedRegionsTooSmall;
		}

		/**
		 * @return number of investigated regions during last call to segmentImage() or getSamples().
		 */
		int getNumCheckedRegionsTooBig() const 
		{
			return m_numCheckedRegionsTooBig;
		}

		/**
		 * @return number of regions accepted by SVM.
		 */
		int getNumAcceptedRegionsBySvm() const
		{
			return m_numAcceptedBySvm;
		}

		/**
		 * @return number of regions accepted by SVM.
		 */
		int getNumRejectedRegionsBySvm() const
		{
			return m_numRejectedBySvm;
		}

		/**
		 * @return number of support vectors used by SVM.
		 */
		int getNumSupportVectors() const 
		{
			if(m_svmModel)
				return m_svmModel->l;
			else
				return 0;
		}

		/**
		 * @return sum of scores.
		 */
		double getSumOfScores() const 
		{
			return m_sumOfScores;
		}

	private:

		// Mode of algorithm (extract samples or segment image)
		static const int MODE_GET_SAMPLES = 0;
		static const int MODE_SEGMENTATION = 1;

		// Maximal size of SubComponent to merge into other SubComponents
		static const int MAX_SUBCOMPONENT_MERGE_SIZE = 1;

		// SVM
		const svm_model* m_svmModel;

		// Hard constrained positive regions
		const std::unordered_set<RegionID, RegionIDHasher>* m_constrainedPositiveRegions;

		// Normalization params
		double m_statsMean[NUM_FEATURES];					
		double m_statsStandardDeviation[NUM_FEATURES];	

		// Parameters
		int m_minSize;
		int m_maxSize;
		double m_minProbability;
		bool m_disableErSplitting;

		// Size of current image 
		int m_imageSizeX;
		int m_imageSizeY;

		// Buffer with (possibly inverted) image data
		unsigned char* m_imageData;

		// Some statistics: number of checked regions, too small, too big, accepted/rejected by svm, sum of scores
		int m_numCheckedRegions;
		int m_numCheckedRegionsTooSmall;
		int m_numCheckedRegionsTooBig;
		int m_numRejectedBySvm;
		int m_numAcceptedBySvm;
		double m_sumOfScores;

		// Final list of found regions: seeds and bounding boxes (after calling segmentImage(), segmentation will be stored in m_foundRegionsPositive)
		std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> m_foundRegionsPositive;
		std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> m_foundRegionsNegative;

		// If current image is processed with bright-on-dark search 
		bool m_brightOnDark;

		// Image size used for memory allocations (can be greater than size of current image)
		int m_allocImageSizeX;
		int m_allocImageSizeY;

		// Visited pixels bitmask
		unsigned char* m_visitedPixels;

		// Background labels bitmask for extracting samples
		unsigned char* m_backgroundLabelsBitmask;
		int m_backgroundLabelsBitmaskSize;

		// Foreground labels and map for extracting samples
		std::vector<std::vector<int>> m_foregroundLabels;
		std::vector<int, AlignedAllocator<int>> m_foregroundLabelsMap;

		// For each foreground label, seed of found sample region and its score (seed = -1 if nothing found yet), and 
		// features of positive samples (feaures of sample i are at indexes [i*NUM_FEATURES; i*NUM_FEATURES+(NUM_FEATURES-1)])
		std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> m_positiveSamples;
		std::vector<double> m_positiveSampleScores;
		sample_feature_list_type m_positiveSampleObservations;
		int m_numPosSamples;

		// Negative samples, and features of negative samples (feaures of sample i are at indexes [i*NUM_FEATURES; i*NUM_FEATURES+(NUM_FEATURES-1)])
		std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> m_negativeSamples;
		sample_feature_list_type m_negativeSampleObservations;

		/**
		 * Get random subset of size n with numbers in [0; max] (no repeats).
		 */
		static std::vector<int> getSample(int n, int maxVal)
		{
			assert(n <= maxVal);
			std::vector<int> ret;
			ret.reserve(n);
			for(int i = 0; i < n; ++i) {
				int r = rand() % (maxVal-i);
				while(std::find(ret.begin(), ret.end(), r) != ret.end())
					++r;
				ret.push_back(r);
			}
			return ret;
		}

		/**
		 * Process image function.
		 */
		template<int MODUS>	void processImageInternal(const unsigned char* imageData, int imageSizeX, int imageSizeY, bool darkOnBright, int minSize, int maxSize);

		/**
		 * Linear time processing of extremal regions.
		 */
		template<int MODUS>	void processExtremalRegions(int imageSizeX, int imageSizeY);

		/**
		 * Update data structures according to m_imageSizeX and m_imageSizeY, throws FastERException in case of error
		 */
		void resetInternalStructures();

		/**
		 * Prepare data structures for next image
		 */
		void prepareForNextImage();

		/**
		 * Process stack
		 * @param newGrayLevel graylevel of next pixel
		 */
		template<int MODE> void processStack( int newGrayLevel);

		/**
		 * Compute sample covariance (or variance) between two variables using computational formula by Steiner.
		 */
		static double sample_cov_steiner(double sumXY, double sumX, double sumY, int n) {
			if(n > 1)
				return (sumXY - sumX*sumY / static_cast<double>(n)) / static_cast<double>(n-1);
			else
				return (sumXY - sumX*sumY / static_cast<double>(n)) / static_cast<double>(n);
		}

		//////////////////////////////////////////////////////////////////////////
		// Heap data
		//////////////////////////////////////////////////////////////////////////

		// Stacks (one for each graylevel with size of pixels for that gray level),
		// store linear indexes of boundary pixels where darker pixels have higher priorities
		// along with one byte indicating the next edge to process (2 most significant bits).
		unsigned int* m_heapStacks;

		// Array of starting indexes of stacks for each graylevel in heapStacks
		int m_heapStacksIndexes[NUM_GRAY_LEVELS];

		// Array of number of elements currently in each stack
		int m_heapStacksLengths[NUM_GRAY_LEVELS];

		// Bitmask set to 1 for stacks with pixels
		fasterUInt64 m_heapBitMask[NUM_GRAY_LEVELS / 64];

		// Lower bound of m_heapBitMask (no bit is set below this graylevel) 
		unsigned int m_heapBitMaskLowerBound;

		// Number of bytes per element in the boundary pixels heap
		static const int BYTES_PER_HEAP_ELEMENT = 4;

		// Edge constants
		static const unsigned int EDGE_RIGHT = 0;
		static const unsigned int EDGE_DOWN = 1<<30;
		static const unsigned int EDGE_LEFT = 2<<30;
		static const unsigned int EDGE_UP = 3<<30;
		static const unsigned int EDGE_MASK = 3<<30;

		// DEBUG
#ifdef _DEBUG
		mutable std::vector<int> processedPixels;
		mutable int maxPixelsQueueSize;
		mutable int splitStopsDueToNumberOfChildren;
		mutable int splitStopsDueToOverlapConflicts;
#endif

		//////////////////////////////////////////////////////////////////////////
		// Heap functions
		//////////////////////////////////////////////////////////////////////////

		/**
		 * Get top element in heap and remove it from heap
		 * @param index receives linear index of top pixel in heap or -1 if heap is empty
		 * @param nextEdge next edge to process for pixel
		 * @return true if heap was not empty
		 */
		bool heapPopTopPixel(int& index, unsigned int& nextEdge) 	{
			// Get darkest pixel gray level
			int grayLevel = faster_bitScanReverse256(m_heapBitMask, m_heapBitMaskLowerBound);
			if(grayLevel == -1)
				return false;

			// Get heap index
			int heapIndex = m_heapStacksIndexes[grayLevel] + m_heapStacksLengths[grayLevel] - 1;

			// Get pixel 
			index = m_heapStacks[heapIndex] & ~EDGE_MASK;
			nextEdge = m_heapStacks[heapIndex] & EDGE_MASK;

			// Decrease counter for gray level
			if(--m_heapStacksLengths[grayLevel] == 0) {
				// Unset bit
				m_heapBitMask[grayLevel / 64] &= ~((fasterUInt64)1 << (grayLevel % 64));
			}

			return true;
		}

		/**
		 * Push pixel on heap
		 * @param index linear index of pixel in image
		 * @param grayLevel gray level of pixel
		 * @param nextEdge next edge to process
		 */
		void heapPushPixel(unsigned int index, unsigned int grayLevel, unsigned int nextEdge) {
			// Get heap index
			int heapIndex = m_heapStacksIndexes[grayLevel] + m_heapStacksLengths[grayLevel];

			// Store value
			m_heapStacks[heapIndex] = index | nextEdge;

			// Increase counter for pixels of gray level
			m_heapStacksLengths[grayLevel]++;

			// Ensure that corresponding bit is set to 1
			m_heapBitMask[grayLevel / 64] |= (fasterUInt64)1 << (grayLevel % 64);

			// Update lower bound
			m_heapBitMaskLowerBound = std::min(m_heapBitMaskLowerBound, grayLevel);
		}

		/**
		 * Get next edge. Order is always: RIGHT -> DOWN -> LEFT -> UP -> RIGHT -> ...
		 * @param edge the current edge constant
		 * @return next edge constant
		 */
		unsigned int getNextEdge(unsigned int edge)
		{
			unsigned int tmp = (edge >> 30) + 1;
			return tmp << 30;
		}

		/**
		 * Get pixel coordinate of specified neighbor
		 * @param pixel current pixel
		 * @param edge specifies which neighbor
		 * @return pixel coordinate or -1 if neighbor does not exist (for boundary pixels)
		 */
		int getNeighbor(int pixel, unsigned int edge)
		{
			switch(edge) {
			case EDGE_RIGHT:
				// Check if pixel is on right edge of image
				if(pixel % m_imageSizeX == m_imageSizeX - 1)
					return -1;
				return pixel + 1;
			case EDGE_DOWN:
				// Check if pixel is on bottom edge of image
				if(pixel / m_imageSizeX >= m_imageSizeY - 1)
					return -1;
				return pixel + m_imageSizeX;
			case EDGE_LEFT:
				// Check if pixel is on left edge of image
				if(pixel % m_imageSizeX == 0)
					return -1;
				return pixel - 1;
			case EDGE_UP:
				// Check if pixel is on top edge of image
				if(pixel < m_imageSizeX)
					return -1;
				return pixel - m_imageSizeX;
			default:
				throw FastERException(FastERException::INTERNAL_ERROR);
			}
		}

		//////////////////////////////////////////////////////////////////////////
		// Regions and components
		//////////////////////////////////////////////////////////////////////////
		struct SubComponent;

		/**
		 * Initialize components.
		 */
		void initializeComponents();

		/**
		 * Statistics about a region (i.e. any connected component).
		 */
		struct RegionStats {

			// Number of pixels
			int numPixels;

			// Min/max graylevel
			int minGrayLevel;
			int maxGrayLevel;
			
			// Bounding box
			BoundingBox boundingBox;

			// Sufficient statistics for minor and major axis length
			double sumX, sumY, sumX2, sumXY, sumY2;	
			
			// Sufficient statistics for mean intensity and heterogeneity (i.e. pixel intensity std. deviation)
			double sumInt, sumInt2;	

			// Sufficient statistics for average gradient magnitude
			double sumGradientMagnitudes;
			double sumGradientMagnitudes2;
			int numGradients;	

			// Sum of intensities of surrounding pixels 
			double sumSurroundingPixelIntensities;

			// Statistics only used for sample extraction.
			int numPixelsInBackground;				// Number of pixels overlapping with background labels (if > 0, then all foreground information is invalid)	
			int numPixelsInForeground;				// Number of pixels overlapping with a foreground label (multiple overlap not allowed)
			int indexOfOverlappingForegroundLabel;	// Index of overlapping foreground label (only valid if numPixelsInForeground > 0; -1 if region overlaps with more than one label)	

			/**
			 * Reset all values to default for empty region.
			 */
			template<int MODE> void reset()
			{
				// Number of pixels
				numPixels = 0;

				// Min/max graylevel
				minGrayLevel = NUM_GRAY_LEVELS + 1;
				maxGrayLevel = -1;

				// Bounding box
				boundingBox.minX = std::numeric_limits<int>::max();
				boundingBox.minY = std::numeric_limits<int>::max();
				boundingBox.maxX = 0;
				boundingBox.maxY = 0;

				// Sufficient statistics (note that numGradients and related variables can be temporarily invalid
				// while flow direction is downward, but they are guaranteed to be valid in processStack())
				sumX = 0.0;
				sumY = 0.0;
				sumX2 = 0.0;
				sumXY = 0.0;
				sumY2 = 0.0;
				sumInt = 0.0;
				sumInt2 = 0.0;
				sumGradientMagnitudes = 0.0;
				sumGradientMagnitudes2 = 0.0;
				numGradients = 0;
				sumSurroundingPixelIntensities = 0.0;

				// Sample extraction statistics
				if(MODE == MODE_GET_SAMPLES) {
					numPixelsInBackground = 0;
					numPixelsInForeground = 0;
				}
			}

			/**
			 * Update statistics by adding a new pixel.
			 * @param d detector
			 * @param pixel linear pixel index
			 * @param x x coordinate of pixel
			 * @param y y coordinate of pixel
			 * @param grayLevel gray level I(pixel) of pixel
			 */
			template<int MODE> void addPixel(const FastERDetector* d, int pixel, int x, int y, int grayLevel, bool doNotRemoveGradients, SubComponent* subComponent, SubComponent* otherSubComponent = 0, bool otherSubComesFirst = false);

			/**
			 * Merge with statistics of other component. Assumes that statistics
			 * can simply be added together.
			 */
			template<int MODE> void merge(const RegionStats& other)
			{
				// Number of pixels
				numPixels += other.numPixels;

				// Min/max graylevel
				minGrayLevel = std::min(minGrayLevel, other.minGrayLevel);
				maxGrayLevel = std::max(maxGrayLevel, other.maxGrayLevel);

				// Bounding box
				boundingBox.minX = std::min(boundingBox.minX, other.boundingBox.minX);
				boundingBox.maxX = std::max(boundingBox.maxX, other.boundingBox.maxX);
				boundingBox.minY = std::min(boundingBox.minY, other.boundingBox.minY);
				boundingBox.maxY = std::max(boundingBox.maxY, other.boundingBox.maxY);

				// Sufficient statistics
				sumX += other.sumX;
				sumY += other.sumY;
				sumX2 += other.sumX2;
				sumY2 += other.sumY2;
				sumXY += other.sumXY;
				sumInt += other.sumInt;
				sumInt2 += other.sumInt2;
				sumGradientMagnitudes += other.sumGradientMagnitudes;
				sumGradientMagnitudes2 += other.sumGradientMagnitudes2;
				numGradients += other.numGradients;
				sumSurroundingPixelIntensities += other.sumSurroundingPixelIntensities;

				// Merge information for sample extraction, if required
				if(MODE == MODE_GET_SAMPLES) {
					numPixelsInBackground += other.numPixelsInBackground;
					if(!numPixelsInBackground) {
						// No pixels in background, handle pixels in foreground (ignored otherwise)
						if(numPixelsInForeground && indexOfOverlappingForegroundLabel >= 0) {		
							// We have foreground pixels -> check if other as well
							if(other.numPixelsInForeground) {
								// Other has foreground pixels as well -> need to merge
								if(other.indexOfOverlappingForegroundLabel == indexOfOverlappingForegroundLabel) {
									// Both have pixels with same foreground label -> merge possible
									numPixelsInForeground += other.numPixelsInForeground;
								}
								else {
									// We already overlap with several labels or components overlap with different fg-labels -> merge not possible
									indexOfOverlappingForegroundLabel = -1;
								}
							}					
						}
						else {
							// We have NO foreground pixels
							if(other.numPixelsInForeground) {
								// But other has foreground pixels -> just overwrite ours
								numPixelsInForeground = other.numPixelsInForeground;
								indexOfOverlappingForegroundLabel = other.indexOfOverlappingForegroundLabel;
							}
						}
					}
				}
			}
		};

		/**
		 * Structure describing one child component (corresponds to a region within an 
		 * extremal region). 
		 */
		struct SubComponent {

			// Statistics
			RegionStats stats;

			// Number of subComponents 
			int subComponentCount;

			// Bounding box for watershedding (set when this SubComponents becomes a nested child)
			BoundingBox watersheddingBoundingBox;

			// Current gray level - maximum gray level for which SubComponent may contain pixels (but not necessarily does)
			int currentGrayLevel;

			// Seed pixel
			int seed;

			// List of pixels (only updated until number of pixels exceeds MAX_SUBCOMPONENT_MERGE_SIZE), only
			// valid if stats.numPixels <= MAX_SUBCOMPONENT_MERGE_SIZE (may contain invalid values otherwise)
			int pixels[MAX_SUBCOMPONENT_MERGE_SIZE];
			
			/**
			 * Reset all values to default for empty component.
			 */
			template<int MODE> void reset()
			{
				// Reset statistics
				stats.reset<MODE>();

				// Indicate that watersheddingBoundingBox and seed are not set
				watersheddingBoundingBox.minX = -1;	
				seed = -1;
				subComponentCount = 1;
			}

			/**
			 * Add pixel to component and update statistics etc.
			 * @param d detector, required for image size.
			 * @param pixel linear pixel index.
			 * @param grayLevel gray level I(pixel) of pixel
			 * @param otherSubComponentStats if there are two sub components, this pointer holds the other sub component's statistics
			 */
			template<int MODE> void addPixel(const FastERDetector* d, int pixel, int grayLevel, bool doNotRemoveGradients, SubComponent* otherSubComponent, bool otherSubComesFirst)
			{
				// Discarded regions may result in lower pixels being added now (then doNotRemoveGradients must be true)
				assert(grayLevel == currentGrayLevel || (grayLevel < currentGrayLevel && doNotRemoveGradients));	

				// Set seed
				if(grayLevel > stats.maxGrayLevel)
					seed = pixel;

				// Remember first MAX_SUBCOMPONENT_MERGE_SIZE pixels
				if(stats.numPixels < MAX_SUBCOMPONENT_MERGE_SIZE)
					pixels[stats.numPixels] = pixel;

				// Update statistics
				int x = pixel % d->m_imageSizeX;
				int y = pixel / d->m_imageSizeX;
				stats.addPixel<MODE>(d, pixel, x, y, grayLevel, doNotRemoveGradients, this, otherSubComponent, otherSubComesFirst);
			}

			/**
			 * Merge with other SubComponent. Does not check if the SubComponents are actually connected. Assumes
			 * that all statistics can simply be added (does not work for sub regions corresponding to split ERs).
			 */
			template<int MODE> void merge(const SubComponent& other)
			{
				// Merge pixels list
				assert(other.stats.numPixels > 0);
				int availableSpace = MAX_SUBCOMPONENT_MERGE_SIZE - stats.numPixels;
				if(availableSpace > other.stats.numPixels) {
					std::copy(other.pixels, other.pixels + other.stats.numPixels, pixels + stats.numPixels);
				}

				// Merge statistics
				stats.merge<MODE>(other.stats);

				// Update sub components count 
				if(stats.minGrayLevel < currentGrayLevel)
					// SubComponent already contains nested regions
					subComponentCount += other.subComponentCount;
				else
					// SubComponent does not contain any nested regions
					subComponentCount = other.subComponentCount;
			}

			/**
			 * Manhattan distance to watershedding bounding box.
			 */
			int distanceToWatersheddingBoundingBox(int x, int y) const
			{
				assert(watersheddingBoundingBox.minX >= 0);
				return watersheddingBoundingBox.manhattanDistance(x, y);
			}

			/**
			 * Manhattan distance to watershedding bounding box for each dimension.
			 */
			void deltaVectorFromWatersheddingBoundingBox(int x, int y, int& dX, int& dY) const
			{
				watersheddingBoundingBox.getDeltaVectorToPoint(x, y, dX, dY);
			}

			/**
			 *	Check if bounding box intersects with bounding box of other SubComponent.
			 */
			bool boundingBoxesIntersect(const SubComponent& other) const 
			{
				return stats.boundingBox.intersects(other.stats.boundingBox);
			}
		};

		/**
		 * Structure describing one component (always corresponds to one extremal region). 
		 */
		struct Component {
			// Pixels queue (pixels queued to be added; found while flowing down to minimum)
			std::vector<int, AlignedAllocator<int>> pixelsQueue;

			//// Pixels from discarded SubComponents (currently not used)
			//std::vector<int, AlignedAllocator<int>> pixelsFromDiscardedSubComponents;

			// Max score
			double maxSumOfScores;

			// Seeds of maximally interesting extremal regions (if faster is sub-region)
			std::vector<SubRegionSeed, AlignedAllocator<SubRegionSeed>> positiveRegionSeeds;

			// SubComponent corresponding to the complete extremal region
			SubComponent* completeEr;

			// Nested regions to keep 
			SubComponent* children[MAX_CHILDREN];
			int numChildren;

			// If all children come from the same ER
			bool childrenBelongToOneNestedEr;

			// If splitting of the region is allowed (in some cases nested regions cannot be used for splitting and
			// can also not be merged into other sub regions - then the component cannot have sub components ever again)
			bool allowSplitting;

			// Current gray level of the component (is also the maximum graylevel for which this component may contain
			// pixels)
			int currentGrayLevel;

			// Minimal gray level of the component (only smaller than grayLevel if the component contains smaller nested 
			// extremal regions)
			int minGrayLevel;

			// Seed pixel of component
			int seedPixel;

			/**
			 *	Set current gray level.
			 */
			void setGrayLevel(int grayLevel) 
			{
				currentGrayLevel = grayLevel;
				completeEr->currentGrayLevel = grayLevel;
				for(auto itChild = children; itChild < children + numChildren; ++itChild)
					(*itChild)->currentGrayLevel = grayLevel;
			}

			/**
			 * Add a pixel: add pixel to completeEr.
			 */
			template<int MODE> void addPixel(const FastERDetector* d, int grayLevel, int pixel)
			{
				// Debug
#ifdef _DEBUG
				assert(d->processedPixels[pixel] == 0);
				d->processedPixels[pixel] = 1;
#endif

				// Set seed
				if(grayLevel > completeEr->stats.maxGrayLevel)
					seedPixel = pixel;

				// Add to SubComponent representing complete ER
				completeEr->addPixel<MODE>(d, pixel, grayLevel, false, nullptr, false);
			}

			/**
			 * Add a pixel: find correct SubComponent and add pixel to it.
			 */
			template<int MODE> void addPixelToSubComponent(const FastERDetector* d, int grayLevel, int pixel, bool doNotRemoveGradients)
			{
				assert(allowSplitting && numChildren > 1);

				// Find closest sub region
				int x = pixel % d->m_imageSizeX;
				int y = pixel / d->m_imageSizeX;
				int iClosest = 0;
				int dClosest = children[0]->distanceToWatersheddingBoundingBox(x, y);
				SubComponent* closestChild = children[0];
				for(int i = 1; dClosest > 0 && i < numChildren; ++i) {
					int curDist = children[i]->distanceToWatersheddingBoundingBox(x, y);
					if(curDist < dClosest) {
						iClosest = i;
						dClosest = curDist;
						closestChild = children[i];
					}
				}

				// Make sure there is at least one pixel closer to the closest sub-region whose graylevel is not > grayLevel
				int dX, dY;
				if(dClosest > 0) {
					// Use distance to WatersheddingBoundingBox
					closestChild->deltaVectorFromWatersheddingBoundingBox(x, y, dX, dY);
					assert(dX != 0 || dY != 0);
				}
				else {
					// Use distance to some pixel from SubComponent
					int somePixelOfClosestChild = closestChild->pixels[0];
					int somePixelX = somePixelOfClosestChild % d->m_imageSizeX;
					int somePixelY = somePixelOfClosestChild / d->m_imageSizeX;
					dX = x - somePixelX;
					dY = y - somePixelY;
				}
				bool foundCloserNeighborPixel = false;
				if(dX != 0) {
					int xN = x + (dX > 0 ? -1 : 1);
					foundCloserNeighborPixel = xN >= 0 && xN < d->m_imageSizeX && d->m_imageData[y * d->m_imageSizeX + xN] <= grayLevel;
				}
				if(dY != 0 && !foundCloserNeighborPixel) {
					int yN = y + (dY > 0 ? -1 : 1);
					foundCloserNeighborPixel = yN >= 0 && yN < d->m_imageSizeY && d->m_imageData[yN * d->m_imageSizeX + x] <= grayLevel;
				}

				// If neighbor criterion is violated, splitting is not possible
				if(!foundCloserNeighborPixel) {
					allowSplitting = false;
					return;
				}

				// Get pointer to other SubComponent
				assert(MAX_CHILDREN <= 2 && numChildren == 2);
				SubComponent* otherSub = (iClosest == 0) ? children[1] : children[0];
				bool otherFirst = iClosest == 1;

				// Can add pixel to SubComponent
				children[iClosest]->addPixel<MODE>(d, pixel, grayLevel, doNotRemoveGradients, otherSub, otherFirst);
			}

			/**
			 * Add pixel to queue for later processing.
			 */
			void enqueuePixel(const FastERDetector* d, int pixel)
			{
				assert(allowSplitting);

				// Simplest approach: std::vector used as stack (does not affect runtime significantly)
				if(pixelsQueue.size() <= MAX_CHILDREN * d->m_maxSize) {
					pixelsQueue.push_back(pixel);
				}
				else {
					allowSplitting = false;
				}
			}

			/**
			 * If there is only one child, discard it, otherwise make sure every child has watershed bounding box.
			 */
			void initializeChildren()
			{
				if(allowSplitting && numChildren > 1) {
					for(int i = 0; i < numChildren; ++i) {
						if(children[i]->watersheddingBoundingBox.minX < 0) 
							children[i]->watersheddingBoundingBox = children[i]->stats.boundingBox;
					}
				}
				else {
					// Discard children if splitting not allowed or if there was only one
					numChildren = 0;
					pixelsQueue.clear(); 
					//pixelsFromDiscardedSubComponents.clear();
				}
			}

			/**
			 * Add all queued pixels.
			 */
			template<int MODE> void addQueuedPixels(const FastERDetector* d)
			{
				// Process all queued pixels
				for(auto it = pixelsQueue.cbegin(); allowSplitting && it != pixelsQueue.cend(); ++it) {
					addPixelToSubComponent<MODE>(d, d->m_imageData[*it], *it, false);
				}
				pixelsQueue.clear(); 
			}

			/**
			 *	Add pixels from discarded regions.
			 */
//			template<int MODE> void addDiscardedRegionPixels(const FastERDetector* d, const unsigned char* imageData)
//			{
//#ifdef _DEBUG
//				d->maxPixelsQueueSize = std::max(d->maxPixelsQueueSize, (int)pixelsQueue.size());
//#endif
//				// Process pixelsFromDiscardedSubComponents
//				for(auto it = pixelsFromDiscardedSubComponents.cbegin(); allowSplitting && it != pixelsFromDiscardedSubComponents.cend(); ++it) {
//					addPixelToSubComponent<MODE>(d, imageData, imageData[*it], *it, true);
//				}
//				pixelsFromDiscardedSubComponents.clear();
//			}

			/**
			 * Post process children, i.e. clean up if splitting was stopped.
			 */
			void postProcessChildren()
			{
				// Clean up if splitting was stopped
				if(!allowSplitting) {
					numChildren = 0;
					pixelsQueue.clear();
					//pixelsFromDiscardedSubComponents.clear();
				}
			}
		};

		/**
		 * Evaluate region against foreground/background labels.
		 */
		void scoreRegionAgainstLabels(Component* component, const SubComponent* subComponent, const SubComponent* otherSubComponent = nullptr, bool otherSubRegionFirst = false) 
		{	
			// statistics
			++m_numCheckedRegions;

			// Size threshold checks
			if(subComponent->stats.numPixels < m_minSize) {
				++m_numCheckedRegionsTooSmall;
				return;
			}
			if(subComponent->stats.numPixels > m_maxSize) {
				++m_numCheckedRegionsTooBig;
				return;
			}

			// Extract features
			double f[NUM_FEATURES];
			getFeatures(subComponent, f);

			// Evaluate region
			double score = -1.0;
			if(subComponent->stats.numPixelsInForeground > 0 && subComponent->stats.indexOfOverlappingForegroundLabel == -1) {
				// Region overlaps with more than one foreground label
				score = 0.0;
			} 
			else if(subComponent->stats.numPixelsInBackground > 0) {
				if(subComponent->stats.numPixelsInBackground > 0.2f * static_cast<float>(subComponent->stats.numPixels)) {
					// Region overlaps with background - check if at least 20% are in background
					score = 0.0;
				}
			}
			else {
				if(subComponent->stats.numPixelsInForeground > 0) {
					assert(subComponent->stats.indexOfOverlappingForegroundLabel >= 0);
					int tp = subComponent->stats.numPixelsInForeground;
					int labelSize = m_foregroundLabels[subComponent->stats.indexOfOverlappingForegroundLabel].size();
					int fn = labelSize - tp;
					int fp = subComponent->stats.numPixels - subComponent->stats.numPixelsInForeground;
					assert(tp > 0 && fp >= 0 && fn >= 0);
					double sensitivity = static_cast<double>(tp) / (tp + fn);
					double specificityAdjusted = static_cast<double>(tp) / (tp + fp);
					assert(sensitivity >= 0 && sensitivity <= 1 && specificityAdjusted >= 0 && specificityAdjusted <= 1);

					// If more than 2/3 of region overlap with fg-label and less than 1/3 of fg-label overlap with region, treat it as negative sample
					//if(tp > 2*fp && 2*tp < fn)
					//	score = 0.0;
					//else
						// Region overlaps with one foreground label, calculate score
						score = (2*sensitivity*specificityAdjusted) / (sensitivity+specificityAdjusted);
				}
			}

			if(score >= 0.0) {
				if(score == 0.0) {
					// Negative sample
					if(otherSubComponent)
						m_negativeSamples.push_back(SubRegionSeed(component->seedPixel, subComponent->watersheddingBoundingBox, otherSubComponent->watersheddingBoundingBox, otherSubRegionFirst));
					else
						m_negativeSamples.push_back(SubRegionSeed(component->seedPixel));

#ifdef _DEBUG
					m_negativeSamples.back().numPixels = subComponent->stats.numPixels;
					m_negativeSamples.back().numGradients = subComponent->stats.numGradients;
					m_negativeSamples.back().sumGradientMagnitudes = subComponent->stats.sumGradientMagnitudes;
#endif

					m_negativeSampleObservations.insert(m_negativeSampleObservations.end(), f, f + NUM_FEATURES);
				}
				else {
					// Positive sample -> check if overlapping positve label has no associated region or this region has higher score					
					if(m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel].seedER == -1 || score > m_positiveSampleScores[subComponent->stats.indexOfOverlappingForegroundLabel]) {
						// For label nothing has been found yet or found region has lower score
						if(m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel].seedER == -1)
							// Label did not have region before
							++m_numPosSamples;

						// Found first or better region for sample: overwrite seed pixel, score and observation vector
						if(otherSubComponent)
							m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel] = SubRegionSeed(component->seedPixel, subComponent->watersheddingBoundingBox, otherSubComponent->watersheddingBoundingBox, otherSubRegionFirst);
						else
							m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel] = SubRegionSeed(component->seedPixel);

#ifdef _DEBUG
						m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel].numPixels = subComponent->stats.numPixels;
						m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel].numGradients = subComponent->stats.numGradients;
						m_positiveSamples[subComponent->stats.indexOfOverlappingForegroundLabel].sumGradientMagnitudes = subComponent->stats.sumGradientMagnitudes;
#endif

						m_positiveSampleScores[subComponent->stats.indexOfOverlappingForegroundLabel] = score;
						std::copy(f, f + NUM_FEATURES, m_positiveSampleObservations.begin() + subComponent->stats.indexOfOverlappingForegroundLabel*NUM_FEATURES);
					}
				}
			}
		}

		/**
		 * Extract feature vector for the region corresponding to the provided SubComponent. 
		 */
		static void getFeatures(const SubComponent* subComponent, double f[])
		{		
			
			// Calculate minor axis and major axis length
			double varX = sample_cov_steiner(subComponent->stats.sumX2, subComponent->stats.sumX, subComponent->stats.sumX, subComponent->stats.numPixels);
			double varY = sample_cov_steiner(subComponent->stats.sumY2, subComponent->stats.sumY, subComponent->stats.sumY, subComponent->stats.numPixels);
			double covXY = sample_cov_steiner(subComponent->stats.sumXY, subComponent->stats.sumX, subComponent->stats.sumY, subComponent->stats.numPixels);
			double T = varX + varY;
			double D = varX*varY - covXY*covXY;
			double S = sqrt(T*T/4.0 - D);
			double e1 = T*0.5 - S;
			double e2 = T*0.5 + S;

			// Other features
			double fittedRadius = sqrt(subComponent->stats.numPixels / M_PI);	
			double avgPixelInt = subComponent->stats.sumInt / subComponent->stats.numPixels;
			double avgBackgroundVal = subComponent->stats.sumSurroundingPixelIntensities / subComponent->stats.numGradients;
			double avgGradientMagnitude = subComponent->stats.sumGradientMagnitudes / subComponent->stats.numGradients;
			assert(avgBackgroundVal >= 0 && avgBackgroundVal <= NUM_GRAY_LEVELS - 1);
			assert(e1 >= 0 && e2 >= 0);
			assert(e2 >= e1);

			// Shape: number of pixels, major axis length and minor axis length
			f[0] = subComponent->stats.numPixels;	
			f[1] = e1; 
			f[2] = e2; 

			// Texture: average pixel intensity and heterogeneity, i.e. std. dev. of pixel intensities
			f[3] = avgPixelInt;
			f[4] = sqrt(sample_cov_steiner(subComponent->stats.sumInt2, subComponent->stats.sumInt, subComponent->stats.sumInt, subComponent->stats.numPixels));

			// Gradient: average gradient magnitude and std. dev. of gradient magnitudes
			f[5] = avgGradientMagnitude;
			f[6] = sqrt(sample_cov_steiner(subComponent->stats.sumGradientMagnitudes2, subComponent->stats.sumGradientMagnitudes, subComponent->stats.sumGradientMagnitudes, subComponent->stats.numGradients));

			// Eccentrictiy: number_of_boundary_edges / radius_of_perfect_circle_with_same_area
			f[7] = static_cast<double>(subComponent->stats.numGradients) / fittedRadius; 

			// Background: average pixel intensity of surrounding pixels
			f[8] = avgBackgroundVal;

			/*
			discarded features:
		
			f[1] = subComponent->stats.sumGradientMagnitudes2 / numGradients; 
			f[3] = fittedRadius;
			f[9] = subComponent->subComponentCount;
			f[11] = avgBackgroundVal - avgPixelInt;
			*/
		}

		/**
		 * Calculate score for the region corresponding to the provided SubComponent.
		 */
		double score(int seedEr, const SubComponent* subComponent, int subRegionIndex)
		{
			// statistics
			++m_numCheckedRegions;

			// Check if region is constrained to be positive
			RegionID regionId(seedEr, subRegionIndex);
			if(m_constrainedPositiveRegions->find(regionId) != m_constrainedPositiveRegions->end()) {
				assert(subComponent->stats.numPixels >= m_minSize && subComponent->stats.numPixels <= m_maxSize);
				return std::numeric_limits<double>::infinity();
			}

			// Size threshold checks
			if(subComponent->stats.numPixels < m_minSize) {
				++m_numCheckedRegionsTooSmall;
				return 0.0;
			}
			if(subComponent->stats.numPixels > m_maxSize) {
				++m_numCheckedRegionsTooBig;
				return 0.0;
			}

			// Extract features
			double f[NUM_FEATURES];
			getFeatures(subComponent, f);

			// Generate normalized sample
			svm_node x[NUM_FEATURES+1];
			for(int c = 0; c < NUM_FEATURES; ++c) {
				x[c].index = c + 1;
				x[c].value = (f[c] - m_statsMean[c]) / m_statsStandardDeviation[c];
			}
			x[NUM_FEATURES].index = -1;
			x[NUM_FEATURES].value = 0;

			// Apply SVM
			double probability[2];
			if(m_svmModel)
				svm_predict_probability(m_svmModel, x, probability);
			else {
				// No SVM because of no negative samples - accept all regions within size limits, favoring bigger ones to provoke merge errors - if
				// the user then labels the two sub-regions, the merged region can serve as negative sample for the SVM
				probability[0] = 0.5 + subComponent->stats.numPixels / (2.0 * m_maxSize);
			}

			// Statistics
			if(probability[0] > m_minProbability)
				++m_numAcceptedBySvm;
			else 
				++m_numRejectedBySvm;

			return probability[0];		 
		 }


		// Stack of components (+1 for dummy component)
		Component m_C[NUM_GRAY_LEVELS + 1];

		// Number of pixels currently in C
		int m_componentsCount;

		// Sub components (MAX_CHILDREN+1 for each component)
		SubComponent m_subComponents[(NUM_GRAY_LEVELS + 1) * (MAX_CHILDREN + 1)];

		//////////////////////////////////////////////////////////////////////////
		// Component functions
		//////////////////////////////////////////////////////////////////////////

		/**
		 * Clear components stack
		 */
		void clearComponents() 	{

			// Set count to 0
			m_componentsCount = 0;
		}

		/**
		 * Debug helper: check if all values in [b, e[ have the same value val.                                                                  
		 */
		template <class It, class T>
		static bool checkArrayValues(It b, It e, const T& val) {
			while(b != e) {
				if(*b != val)
					return false;
				++b;
			}
			return true;
		}

		/**
		 * Add a component to the components stack
		 * @param grayLevel graylevel of component
		 */
		template<int MODE> void addComponent(int grayLevel) 	{
			// Set data for top component
			Component& topC = m_C[m_componentsCount];
			topC.currentGrayLevel = grayLevel;
			topC.minGrayLevel = grayLevel;
			topC.maxSumOfScores = 0.0;
			topC.numChildren = 0;
			topC.childrenBelongToOneNestedEr = true;
			topC.allowSplitting = true;
			topC.completeEr->reset<MODE>();
			topC.completeEr->currentGrayLevel = grayLevel;
			
			// Clear should not be needed here
			assert(topC.positiveRegionSeeds.size() == 0);
			assert(topC.pixelsQueue.size() == 0);
			//assert(topC.pixelsFromDiscardedSubComponents.size() == 0);

			// Increase counter
			++m_componentsCount;
		}

		/**
		 * Merge the top component on the stack into the second one
		 */
		template<int MODE> void mergeTopComponents();
	};
	

	template<int MODE>
	void faster::FastERDetector::RegionStats::addPixel(const FastERDetector* d, int pixel, int x, int y, int grayLevel, bool , SubComponent* subComponent, SubComponent* otherSubComponent, bool otherSubComesFirst)
	{
		// Only one other SubComponent supported (otherwise, otherSubComponent would need to be changed to an array)
		assert(MAX_CHILDREN <= 2);

		// Increase pixel count
		++numPixels;

		// Update GrayLevel
		minGrayLevel = std::min(minGrayLevel, grayLevel);
		maxGrayLevel = std::max(maxGrayLevel, grayLevel);

		// Update bounding box
		boundingBox.minX = std::min(boundingBox.minX, x);
		boundingBox.maxX = std::max(boundingBox.maxX, x);
		boundingBox.minY = std::min(boundingBox.minY, y);
		boundingBox.maxY = std::max(boundingBox.maxY, y);

		// Update sufficient statistics
		sumX += x;
		sumY += y;
		sumX2 += x*x;
		sumY2 += y*y;
		sumXY += x*y;
		sumInt += grayLevel;
		sumInt2 += grayLevel*grayLevel;

		// Update gradient: enumerate neighbor pixels
		const int xSteps[] = {-1, 0, 1, 0};
		const int ySteps[] = {0, -1, 0, 1};
		for(int i = 0; i < 4; ++i) {
			int xNeighbor = x + xSteps[i];
			int yNeighbor = y + ySteps[i];
			if(xNeighbor >= 0 && yNeighbor >= 0 && xNeighbor < d->m_imageSizeX  && yNeighbor < d->m_imageSizeY) {
				int indexLinear = yNeighbor * d->m_imageSizeX + xNeighbor;

				// Neighbor pixel is within image boundaries - check if it does not belong to this (Sub-)Region
				bool neighborIsCloserToOtherChild = false;
				if(otherSubComponent) {
					int distToSubComponent = subComponent->distanceToWatersheddingBoundingBox(xNeighbor, yNeighbor);
					int distToOtherSubComponent = otherSubComponent->distanceToWatersheddingBoundingBox(xNeighbor, yNeighbor);
					neighborIsCloserToOtherChild = distToOtherSubComponent < distToSubComponent || (distToOtherSubComponent == distToSubComponent && otherSubComesFirst);
				}
				int neighborPixelVal = d->m_imageData[indexLinear];
				double gradientMagn = std::abs(grayLevel - neighborPixelVal);
				if(neighborPixelVal > grayLevel || neighborIsCloserToOtherChild) {
					// Neighbor does not belong to this ER or belongs to other SubComponent -> add gradient
					++numGradients;
					sumGradientMagnitudes += gradientMagn;
					sumGradientMagnitudes2 += gradientMagn * gradientMagn;
					sumSurroundingPixelIntensities += neighborPixelVal;
				}
				else if(neighborPixelVal < grayLevel) {	
					// Neighbor was outside this ER, but is not anymore -> remove gradient

					// This code is only correct as long as sub-regions are never discarded: thus, an edge to a neighbor pixel
					// from a different sub-region never has to be removed, because the border to the other sub-region
					// will never change until all sub-regions are discarded
					
					//if(!neighborIsCloserToOtherChild) {
						--numGradients;
						sumGradientMagnitudes -= gradientMagn;
						sumGradientMagnitudes2 -= gradientMagn * gradientMagn;
						sumSurroundingPixelIntensities -= grayLevel;
					//}
					//else {
					//	// Must remove gradient from other subComponent
					//	--otherSubComponent->stats.numGradients;
					//	otherSubComponent->stats.sumGradientMagnitudes -= gradientMagn;
					//	otherSubComponent->stats.sumGradientMagnitudes2 -= gradientMagn * gradientMagn;

					//	otherSubComponent->stats.sumSurroundingPixelIntensities -= grayLevel;
					//}
				}
			}
		}

		/**
		 * Processing steps only required for MODE_GET_SAMPLES.
		 */
		if(MODE == MODE_GET_SAMPLES) {
			// Update statistics required to get samples
			if(testBit(d->m_backgroundLabelsBitmask, pixel))
				++numPixelsInBackground;
			else {
				int fgLabel = d->m_foregroundLabelsMap[pixel];
				if(fgLabel >= 0) {
					if(numPixelsInForeground > 0) {
						if(indexOfOverlappingForegroundLabel != fgLabel)
							// Overlap with more than one fg-label detected
							indexOfOverlappingForegroundLabel = -1;
						else
							++numPixelsInForeground;
					}
					else {
						indexOfOverlappingForegroundLabel = fgLabel;
						numPixelsInForeground = 1;
					}
				}
			}
		}
	}
}
#endif // faster_h__