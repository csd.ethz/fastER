/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationcountcells_h__
#define segmentationoperationcountcells_h__

// Project
#include "segmentationoperation.h"
#include "logging.h"

// Qt
#include <QFile>
#include <QTextStream>
#include <QHash>
#include <QString>
#include <QVector>
#include <QSet>
#include <QMutex>
#include <QMutexLocker>


class SegmentationOperationCountCells : public SegmentationOperation {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	SegmentationOperationCountCells() {}

	/**
	 * Reimplemented.
	 */
	void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner);

	/**
	 * Reimplemented.
	 */
	QString getName() const
	{
		return "Count cells";
	}

	/**
	 * Reimplemented.
	 */
	QHash<QString, QVariant> getParameters() const
	{
		return QHash<QString, QVariant>();
	}

	/**
	 * Reimplemented.
	 */
	void setParameters(const QHash<QString, QVariant>& parameters){}

	/**
	 * Reset cell counts.
	 */
	static void resetCellCount()
	{
		// Synchronize
		QMutexLocker lock1(&m_timePointsWithCellCountsMutex);
		QMutexLocker lock2(&m_cellCountsMutex);

		// Reset..
		m_timePointsWithCellCounts.clear();
		m_cellCounts.clear();
	}

	/**
	 * Export cell counts.
	 */
	static bool exportCellCounts(const QString& fileName);

private:

	// Time points for which cell counts exist
	static QSet<int> m_timePointsWithCellCounts;
	static QMutex m_timePointsWithCellCountsMutex;

	// Cell counts (first index: position, second index: time point) 
	static QHash<int, QHash<int, int>> m_cellCounts; 
	static QMutex m_cellCountsMutex;

};


#endif // segmentationoperationcountcells_h__
