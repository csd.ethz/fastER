/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef exception_h__
#define exception_h__

// Qt
#include <QString>

class ExceptionBase {

public:

	/**
	 * Constructor.
	 */
	ExceptionBase(const QString& where, const QString& what) 
		: m_what(what), m_where(where)
	{}

	/**
	 * What happened.
	 */
	QString what() const {
		return m_what;
	}

	/**
	 * Where did it happen.
	 */
	QString where() const {
		return m_where;
	}

private:

	QString m_what;

	QString m_where;

};


#endif // exception_h__
