/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONDISPLAY_H
#define POSITIONDISPLAY_H

// Project includes
#include "positionthumbnail.h"

// Qt includes
#include <QGraphicsView>

// Forward declarations
class SettingsManager;


/**
	All positions have the same size.
	The visible range depends on the microscope scaling factors (TV adapter and ocular).
	The factor micrometer -> pixel is calculated from the size in micrometer and the size in pixel.
*/
class PositionDisplay : public QGraphicsView
{

	Q_OBJECT
	
public:
	
	/**
	* Constructor, just creates the widget without contents
	*/
	PositionDisplay (QWidget *_parent);

	/**
	* Load position thumbnails (does not check if this has already been done!)
	* @param _experiment experiment descriptor, can be null (then all existing thumbnails will be removed)
	* @param _drawThumbNailFrame if frame around thumbnails should be drawn
	* @param _exclusiveSelection if at most one position can be selected at the same time
	* @param _showProgressBar if progressbar should be shown.
	*/
	void initialize(Experiment* _experiment, bool _drawThumbNailFrame = true, bool _exclusiveSelection = true, bool _showProgressBar = false);

	/**
	 * @return if initialization has already been done successfully
	 */
	bool isInitialized() const {
		return initialized;
	}

	/**
	* Returns the thumbnail corresponding to the provided position index, 0 if it does not exist
	* @param _index the position index
	* @return the thumbnail for the provided index, or 0 if it does not exist
	*/
	PositionThumbnail* getThumbnail (int _index) const;

	/**
	 * Update display according to current tv-factor, ocular factor and other display relevant settings
	 */
	void updateDisplay();

	/**
	 * Disable or enable frames of thumbnails, automatically updates display
	 */
	void setDrawThumbNailFrames(bool _on);
	
	/**
	 * @return drawThumbNailFrames
	 */
	bool getDrawThumbNailFrames() const {
		return drawThumbNailFrames;
	}

	/**
	 * Disable or enable text on thumbnails, automatically updates display
	 */
	void setDrawText(bool _on);

	/**
	 * @return drawText
	 */
	bool getDrawText() const {
		return drawText;
	}

	/**
	 * Change the timepoint used for the thumbnail preview images.
	 * For the changes to take effect, initialize() must be called after this function!
	 * @param _timePoint new timepoint
	 */
	void setThumbnailsTimePoint(int _timePoint);

	/**
	 * Change the wavelength used for the thumbnail preview images.
	 * For the changes to take effect, initialize() must be called after this function!
	 * @param _timePoint new timepoint
	 */
	void setThumbnailsWavelength(int _wl);

	/**
	 * Get the timepoint used for the thumbnail preview images.
	 */
	int getThumbnailsTimePoint() const {
		return thumbnailsTimePoint;
	}
	
	/**
	 * Get the wavelength used for the thumbnail preview images.
	 */
	int getThumbnailsWavelength() const {
		return thumbnailsWaveLength;
	}

	///**
	// * Get list of active (i.e. selected) positions
	// * @return list of position managers of active (selected) positions. Can be empty
	// */
	//QList<PositionDescriptor*> getActivePositions() const;

	/**
	 * @return list of number of selected positions
	 */
	QList<int> getActivePositionNumbers() const;

	/**
	 * Select position thumbnail
	 * @param _index index of position to select
	 * @return true if successful (i.e. if no error occurred)
	 */
	bool selectPosition(int _index);

	/**
	 * Deselect position thumbnail
	 * @param _index index of position to select. if _index is -1, all position will be unselected
	 * @return true if successful (i.e. if no error occurred)
	 */
	bool deSelectPosition(int _index);

	/**
	 * check if position is selected
	 * @param _index index of position to check
	 * @return true position exists and is selected
	 */
	bool isPositionSelected(int _index);

	/**
	 * Get current scaling factor.
	 */
	float getCurrentScaling() const {
		return scalingFactor;
	}
	
	/**
	 * Zoom function.
	 * @param _scaleFactor scaling factor, 1.0f for 100% zoom.
	 */
	void scaleView(qreal _scaleFactor);

	/////a conversion factor for displaying the thumbnails in the correct sizes
	/////is completely independent of the real microscope values which are defined in WavelengthInformation and is just used for this display - nothing else
	//static const float PIXEL_PER_MICROMETER_FOR_DISPLAY;

	// Minimum and maximum scaling values
	static const float MAX_SCALE;
	static const float MIN_SCALE;

public slots:

	/**
	 * Enable/disable region selection mode to select tracking region (rectangle). If true, positions cannot be selected/deselected and
	 * the mouse can be used to draw a rectangle as tracking region or remove a tracking region by clicking on it with left button
	 * @param _on true to enable
	 */
	void setRegionSelectionMode(bool _on);

signals:

	/**
	* emitted when a position was left clicked
	* @param _index the index of the selected position
	*/
	void positionLeftClicked (int _index);

	/**
	 * emitted when a position was double clicked
	 * @param _index the index of the selected position
	 */
	void positionDoubleClicked (int _index);
	
	/**
	 * emitted when the user clicked on a position with the right mouse button
	 * @param _index the index of the selected position
	 */
	void positionRightClicked (int _index);

	/**
	 * emitted when a position has been selected or deselected
	 * @param _index index of position
	 * @param _selected true if selected
	 */
	void positionSelectionChanged(int _index, bool _selection);

	/**
	 * emitted when zoom has been changed
	 * @param _zoom the new scaling factor
	 */
	void zoomChanged(float _zoom);

protected:

	// Mouse wheel event, used for zoom
	void wheelEvent(QWheelEvent *_event);

	// Mouse button and move events, used for navigation with mouse
	void mousePressEvent( QMouseEvent *_event );
	void mouseReleaseEvent( QMouseEvent *_event );
	void mouseMoveEvent ( QMouseEvent *_event );

private:

	/**
	 * Helper function to convert scene coordinates to experiment coordinates
	 * @param _point scene coordinates
	 * @return experiment coordinates corresponding to _point
	 */
	QPointF mapSceneToExperiment(const QPointF& _pos);

	/**
	* called when the sizes of the position thumbnails have to change due to a change in the microscope factor settings
	*/
	void updateSizes();

	/**
	 * Emits double click signal, called by thumbnails
	 * @param _index index of left clicked position
	 */
	void reportPositionLeftClicked(int _index);

	/**
	 * Emits double click signal, called by thumbnails
	 * @param _index index of double clicked position
	 */
	void reportPositionDoubleClicked(int _index);

	/**
	 * Emits right click signal called by thumbnails
	 * @param _index index of right clicked position
	 */
	void reportPositionRightClicked(int _index);

	// Region selection mode, to select tracking region (rectangle). If true, positions cannot be selected/deselected and
	// the mouse can be used to draw a rectangle as tracking region or remove a tracking region by clicking on it with left button
	bool regionSelectionMode;

	// If region is currently being selected
	bool regionIsBeingSelected;

	// Rectangle when region is being selected (in scene coordinates)
	QGraphicsRectItem* currentlySelectedRegion;

	// Thumbnails by position index
	QHash<int, PositionThumbnail*> thumbnails;

	// Tracking area
	QList<QGraphicsRectItem*> trackingArea;

	// If at most one position can be selected at the same time
	bool exclusiveSelection;

	// Current scaling factor
	float scalingFactor;

	// Position that was selected at last (if exclusive this is the only selected position)
	int indexOfLastSelectedPosition;

	// If thumbnails should have frames
	bool drawThumbNailFrames;

	// If text on thmbunails should be dranw
	bool drawText;

	// Already initialized
	bool initialized;

	// Timepoint and wavelength of pictures used for thumbnails
	int thumbnailsTimePoint;
	int thumbnailsWaveLength;

	// Mouse navigation
	int mouseMoveStartX, mouseMoveStartY;
	bool scrolling;

	// Experiment descriptor
	Experiment* experiment;
	
	// Z-Value for tracking region rectangles
	static const float Z_TRACKINGREGION_RECT;

	// Friend classes
	friend PositionThumbnail;
};

#endif
