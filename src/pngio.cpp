/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pngio.h"

// LibPNG
#include "png.h"

// STL
#include <stdlib.h>
#include <string.h>
#include <cassert>

#ifdef __unix
#define fopen_s(pFile,filename,mode) ((*(pFile))=fopen((filename),(mode)))==NULL
#endif

// Actual internal saving function
int saveImageAsPng( unsigned char* imageData, int width, int height, const char* fileName, int bitDepth, bool littleEndian, int acquisitionYear, int acquisitionMonth, int acquisitionDay, int acquisitionHour, int acquisitionMinute, int acquisitionSecond );


int save16BitImageAsPng( unsigned char* imageData, int width, int height, const char* fileName, bool littleEndian, int acquisitionYear, int acquisitionMonth, int acquisitionDay, int acquisitionHour, int acquisitionMinute, int acquisitionSecond )
{
	// Delegate
	return saveImageAsPng(imageData, width, height, fileName, 16, littleEndian, acquisitionYear, acquisitionMonth, acquisitionDay, acquisitionHour, acquisitionMinute, acquisitionSecond);
}

int save8BitImageAsPng( unsigned char* imageData, int width, int height, const char* fileName, int acquisitionYear, int acquisitionMonth, int acquisitionDay, int acquisitionHour, int acquisitionMinute, int acquisitionSecond )
{
	// Delegate
	return saveImageAsPng(imageData, width, height, fileName, 8, false, acquisitionYear, acquisitionMonth, acquisitionDay, acquisitionHour, acquisitionMinute, acquisitionSecond);
}

int saveImageAsPng( unsigned char* imageData, int width, int height, const char* fileName, int bitDepth, bool littleEndian, int acquisitionYear, int acquisitionMonth, int acquisitionDay, int acquisitionHour, int acquisitionMinute, int acquisitionSecond )
{
	// Check parameters
	if(!imageData || width < 0 || height < 0 || !fileName || (bitDepth != 8 && bitDepth != 16))
		return 8;

	// PNG settings
	png_byte bit_depth = static_cast<png_byte>(bitDepth);
	png_byte color_type = PNG_COLOR_TYPE_GRAY;

	// Init variables
	int errorCode = 0;
	FILE* fp = 0;
	png_structp png_ptr = 0;
	png_infop info_ptr = 0;
	png_bytep* row_pointers = 0;

	// Init row_pointers
	row_pointers = static_cast<png_bytep*>(malloc(sizeof(png_bytep) * height));
	for(int y = 0; y < height; ++y)
		row_pointers[y] = static_cast<png_byte*>(imageData + width*y*(bitDepth/8));

	// Open file
	int err;
	if( (err = fopen_s(&fp, fileName, "wb")) != 0 ) {
		errorCode = 1;
		goto Cleanup;
	}

	// Initialize libPNG
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png_ptr) {
		errorCode = 2;
		goto Cleanup;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		errorCode = 3;
		goto Cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 4;
		goto Cleanup;
	}
	png_init_io(png_ptr, fp);

	// Create header
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 5;
		goto Cleanup;
	}
	png_set_IHDR(png_ptr, info_ptr, width, height,
		bit_depth, color_type, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	// Add tIME stampe
	png_time_struct timestamp;
	if(acquisitionYear > 0) {
		timestamp.year = acquisitionYear;
		timestamp.month = acquisitionMonth;
		timestamp.day = acquisitionDay;
		timestamp.hour = acquisitionHour;
		timestamp.minute = acquisitionMinute;
		timestamp.second = acquisitionSecond;
		png_set_tIME(png_ptr, info_ptr, &timestamp);
	}

	// Add text chunk with creation time
	png_text text;
	if(acquisitionYear > 0) {
		text.compression = PNG_TEXT_COMPRESSION_NONE;
		text.key = "Creation Time";
		char buf[100];
#ifdef _MSC_VER
		sprintf_s(buf, 100, "%02d/%02d/%d %02d:%02d:%02d UTC", acquisitionMonth, acquisitionDay, acquisitionYear, acquisitionHour, acquisitionMinute, acquisitionSecond);
#else
		sprintf(buf, "%02d/%02d/%d %02d:%02d:%02d UTC", acquisitionMonth, acquisitionDay, acquisitionYear, acquisitionHour, acquisitionMinute, acquisitionSecond);
#endif
		text.text = buf;
		png_set_text(png_ptr, info_ptr, &text, 1);
	}

	// Write header
	png_write_info(png_ptr, info_ptr);

	// Write bytes
	if(littleEndian && bit_depth == 16)
		png_set_swap(png_ptr);
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 6;
		goto Cleanup;
	}
	png_write_image(png_ptr, row_pointers);

	// End write
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 7;
		goto Cleanup;
	}
	png_write_end(png_ptr, NULL);

Cleanup:

	// Free png memory, row pointers memory and close file
	png_destroy_write_struct(&png_ptr, &info_ptr);
	if(row_pointers) 
		free(row_pointers);
	if(fp != 0)
		fclose(fp);

	return errorCode;
}

// Helper function: check if character is a number, i.e. '0' to '9' in US-ASCII
// code or compatible (e.g. Latin-1)
bool isNumber(char ch)
{
	return ch >= 48 && ch <= 57;
}

// Helper function: convert character to number, e.g. '5' to 5.
int charToNum(char ch)
{
	return static_cast<int>(ch) - 48;
}

// Helper function to convert a date string (e.g. "05/30/1992 15:10:52 UTC" tag into a date
bool parseAcqDateString(char* dateString, int len, int& acquisitionYear, int& acquisitionMonth, int& acquisitionDay, int& acquisitionHour, int& acquisitionMinute, int& acquisitionSecond)
{
	// Length
	if(len != 23)
		return false;

	// Check format
	int numberIndexes[] = {0, 1, 3, 4, 6, 7, 8, 9, 11, 12, 14, 15, 17, 18};
	int numberIndexesLength = sizeof(numberIndexes) / sizeof(int);
	for(int i = 0; i < numberIndexesLength; ++i) {
		if(!isNumber(dateString[numberIndexes[i]]))
			return false;
	}
	if(dateString[2] != '/' || 
		dateString[5] != '/' || 
		dateString[10] != ' ' || 
		dateString[13] != ':' || 
		dateString[16] != ':' || 
		dateString[19] != ' ' || 
		dateString[20] != 'U' || 
		dateString[21] != 'T' || 
		dateString[22] != 'C')
		return false;

	// Convert
	acquisitionMonth = charToNum(dateString[0]) * 10 + charToNum(dateString[1]);
	acquisitionDay = charToNum(dateString[3]) * 10 + charToNum(dateString[4]);
	acquisitionYear = charToNum(dateString[6]) * 1000 + charToNum(dateString[7]) * 100 + charToNum(dateString[8]) * 10 + charToNum(dateString[9]);
	acquisitionHour = charToNum(dateString[11]) * 10 + charToNum(dateString[12]);
	acquisitionMinute = charToNum(dateString[14]) * 10 + charToNum(dateString[15]);
	acquisitionSecond = charToNum(dateString[17]) * 10 + charToNum(dateString[18]);

	// Final check if ok
	return acquisitionYear > 0 &&
		acquisitionYear <= 9999 &&	// do not decide what makes sense here, just check integrity
		acquisitionMonth >= 1 &&
		acquisitionMonth <= 12 &&
		acquisitionDay >= 1 && 
		acquisitionDay <= 31 &&
		acquisitionMinute >= 1 && 
		acquisitionMinute <= 59 &&
		acquisitionHour >= 0 && 
		acquisitionHour <= 23 &&
		acquisitionSecond >= 0 &&	// allow leap seconds 
		acquisitionSecond <= 59;

}


int openPngGrayScaleImage(const char* fileName, unsigned char** imageData, int& outBitDepth, int& outWidth, int& outHeight, bool littleEndian, int* acquisitionYear, int* acquisitionMonth, int* acquisitionDay, int* acquisitionHour, int* acquisitionMinute, int* acquisitionSecond)
{
	// Check parameters
	if(!imageData || !fileName)
		return 8;

	// Init variables
	int errorCode = 0;
	FILE* fp = 0;
	png_structp png_ptr = 0;
	png_infop info_ptr = 0;
	png_bytep* row_pointers = 0;
	int bitDepth;
	int numChannels = 1;
	int bytesPerChannel;

	// Open file
	int err;
	if( (err = fopen_s(&fp, fileName, "rb")) != 0 ) {
		errorCode = 1;
		goto Cleanup;
	}

	// Check signature
	unsigned char sig[8];
	fread(sig, 1, 8, fp);
	if (!png_check_sig(sig, 8)) {
		errorCode = 2;
		goto Cleanup;
	}

	// Initialize libPNG
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png_ptr) {
		errorCode = 3;
		goto Cleanup;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		errorCode = 4;
		goto Cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 5;
		goto Cleanup;
	}
	png_init_io(png_ptr, fp);
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 6;
		goto Cleanup;
	}

	// Start reading
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);

	// Read info
	int color_type;
#ifdef __unix
	long unsigned int tmpWidth, tmpHeight;
#else
	unsigned int tmpWidth, tmpHeight;
#endif	
	png_get_IHDR(png_ptr, info_ptr, &tmpWidth, &tmpHeight, &bitDepth, &color_type, NULL, NULL, NULL);
	outBitDepth = bitDepth;
	outWidth = tmpWidth;
	outHeight = tmpHeight;

	// Read date
	png_textp textChunks;
	int numTextChunks;
	png_get_text(png_ptr, info_ptr, &textChunks, &numTextChunks);
	if(acquisitionYear && acquisitionMonth && acquisitionDay && acquisitionHour && acquisitionMinute && acquisitionSecond) {
		*acquisitionYear = -1;
		for(int i = 0; i < numTextChunks; ++i) {
			if(strcmp(textChunks[i].key, "Creation Time") == 0) {
				// Found it, try to parse it
				if(!parseAcqDateString(textChunks[i].text, textChunks[i].text_length, *acquisitionYear, *acquisitionMonth, *acquisitionDay, *acquisitionHour, *acquisitionMinute, *acquisitionSecond)) {
					// Parsing failed
					*acquisitionYear = -2;
				}
			}
		}
	}

	// Check if image is grayscale
	if(color_type != PNG_COLOR_TYPE_GRAY && color_type != PNG_COLOR_TYPE_PALETTE && color_type != PNG_COLOR_TYPE_RGB && color_type != PNG_COLOR_TYPE_GRAY_ALPHA && color_type != PNG_COLOR_TYPE_RGBA){
		errorCode = 7;
		goto Cleanup;
	}

	// Specify that RGB should be silently converted to grayscale (also required for 1-bit palette type which is expanded to RGB by png_set_expand)
	if(color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_rgb_to_gray_fixed(png_ptr, 1, -1, -1);	// Warning: this feature has not been tested yet

	// Determine number of channels
	if(color_type == PNG_COLOR_TYPE_GRAY_ALPHA || color_type == PNG_COLOR_TYPE_RGBA)
		numChannels = 2;	// Additional channel for alpha (RGB will be converted automatically, see above)

	// Check if bitdepth is supported and calc bytes per pixel
	if(bitDepth == 1 || bitDepth == 8)
		bytesPerChannel = 1;
	else if(bitDepth == 16)
		bytesPerChannel = 2;
	else {
		errorCode = 8;
		goto Cleanup;
	}

	// Create image buffer and init row pointers
	*imageData = new unsigned char[outWidth * outHeight * bytesPerChannel * numChannels];
	row_pointers = static_cast<png_bytep*>(malloc(sizeof(png_bytep) * outHeight));
	for(int y = 0; y < outHeight; ++y)
		row_pointers[y] = static_cast<png_byte*>(*imageData + outWidth*y*bytesPerChannel*numChannels);

	// Get background color and fill image
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_bKGD)) {
		png_color_16p pBackground;
		png_get_bKGD(png_ptr, info_ptr, &pBackground);
		if(bitDepth == 1) {
			// Fill image with 0 or 255
			for(unsigned char* itImageData = *imageData; itImageData != *imageData + outWidth*outHeight*numChannels; ++itImageData)
				*itImageData = pBackground->gray? 255 : 0;
		}
		else if(bitDepth == 8) {
			// Convert to 8 bit
			for(unsigned char* itImageData = *imageData; itImageData != *imageData + outWidth*outHeight*numChannels; ++itImageData)
				*itImageData = pBackground->gray >> 8;
		}
		else if(bitDepth == 16) {
			// Use as is
			for(unsigned short* itImageData = reinterpret_cast<unsigned short*>(*imageData); itImageData != reinterpret_cast<unsigned short*>(*imageData) + outWidth*outHeight*numChannels; ++itImageData)
				*itImageData = pBackground->gray;
		}
	}

	// Inform libPNG to expand 1bit grayscale values to 8 bit
	if(bitDepth == 1) {
		png_set_expand(png_ptr);
		outBitDepth = 8;
	}

	// Expand any tRNS chunk data into a full alpha channel
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png_ptr);	

	// Gamma correction
	//double  gamma;
	//if (png_get_gAMA(png_ptr, info_ptr, &gamma))
	//	png_set_gamma(png_ptr, display_exponent, gamma);

	// Actual read
	if(littleEndian)
		png_set_swap(png_ptr);
	png_read_image(png_ptr, row_pointers);

	// Remove ignored additional channels if necessary 
	if(numChannels > 1) {
		unsigned char* tmpImageData = new unsigned char[outWidth * outHeight * bytesPerChannel];
		if(bytesPerChannel == 1) {
			// Copy, skipping every other byte
			unsigned char* itDst = tmpImageData;
			for(unsigned char* itImageData = *imageData; itImageData != *imageData + outWidth*outHeight*numChannels; itImageData += 2)
				*(itDst++) = *itImageData;
		}
		else {
			// Copy, skipping every other word (i.e. 2 bytes)
			unsigned short* itDst = reinterpret_cast<unsigned short*>(tmpImageData);
			assert(bytesPerChannel == 2);
			for(unsigned short* itImageData = reinterpret_cast<unsigned short*>(*imageData); itImageData != reinterpret_cast<unsigned short*>(*imageData) + outWidth*outHeight*numChannels; itImageData += 2)
				*(itDst++) = *itImageData;
		}
		delete[] *imageData;
		*imageData = tmpImageData;
	}

Cleanup:

	// Free png memory, row pointers memory and close file
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	if(row_pointers) 
		free(row_pointers);
	if(fp != 0)
		fclose(fp);

	return errorCode;
}
