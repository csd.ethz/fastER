/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmprogress_h__
#define frmprogress_h__


#include "ui_frmProgress.h"

// Qt
#include <QVector>
#include <QCloseEvent>
#include <QTime>
#include <QTimer>
#include <QElapsedTimer>

// STL
#include <vector>
#include <cassert>

class JobRunnerMaster;
class JobSet;


/**
 * Form for generic progress bars with display of remaining time.
 */
class FrmProgress : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	FrmProgress(QWidget* parent = 0) 
		: QWidget(parent)
	{
		m_ui.setupUi(this);
		m_cancel = false;
		m_currentJobSetIndex = -1;
		m_ui.lblRemainingTime->setText("");
		m_ui.lblElapsedTime->setText("");
		connect(m_ui.pbtCancel, SIGNAL(clicked()), this, SLOT(canceled()));
		connect(&m_timer, SIGNAL(timeout()), this, SLOT(updateTimes()));
	}

	/**
	 * Destructor.
	 */
	~FrmProgress();

	/**
	 * @return if user wants to cancel operation.
	 */
	bool getIfCanceled() const
	{
		return m_cancel;
	}

	static QString elapsedSecondsToString(int elapsedSecs)
	{
		if(elapsedSecs > 0) {
			int h = elapsedSecs / 3600;
			elapsedSecs = elapsedSecs % 3600;
			int m = elapsedSecs / 60;
			elapsedSecs = elapsedSecs % 60;
			int s = elapsedSecs;
			return QString("%1:%2:%3").arg(h, 2, 10, QChar('0')).arg(m, 2, 10, QChar('0')).arg(s, 2, 10, QChar('0'));
		}
		return "00:00:00";
	}

public slots:

	/**
	 * Set total number of tasks. Transfers ownership.
	 */
	void runJobs(const std::vector<JobRunnerMaster*>& masterJobRunners, const std::vector<JobSet*>& jobs);

	///**
	// * Set number of operations for current task. -1 stands for NA.
	// */
	//void setNumOperationsCurTask(int total)
	//{
	//	m_ui.progressBar->setMaximum(total);
	//}

	///**
	// * Set number of finished operations for current task.
	// */
	//void setNumOperationsFinishedCurTask(int finished)
	//{
	//	m_ui.progressBar->setValue(finished);
	//	m_numJobsFinished = finished;
	//}

signals:

	/**
	 * Cancel button clicked.
	 */
	void cancelClicked();

protected:

	void closeEvent(QCloseEvent* event);

private slots:

	/**
	 * User clicked cancel.
	 */
	void canceled()
	{
		close();
	}

	/**
	 * Job finished.
	 */
	void jobFinished(int numFinished);

	/**
	 * Current task finished or canceled.
	 */
	void taskStarted();
	void taskFinished();
	void taskCanceled();

	/**
	 * Update times.
	 */
	void updateTimes();

private:

	// Cleanup
	void cleanUp();

	// Jobs to complete, corresponding JobRunners and index of currently running jobset
	std::vector<JobSet*> m_jobs;
	std::vector<JobRunnerMaster*> m_masterJobRunners;
	int m_currentJobSetIndex;

	// If canceled
	bool m_cancel;

	// Total number of jobs (and number of finished) for current task and number of digits to use for progress label
	int m_numJobsTotal;
	int m_progressDigits;
	int m_numJobsFinished;

	// Timer
	QTime m_elapsedTime;
	QTimer m_timer;

	// Elapsed time for all tasks
	QElapsedTimer m_elapsedTimeAllTasks;

	// GUI
	Ui::Progress m_ui;

};


#endif // frmprogress_h__
