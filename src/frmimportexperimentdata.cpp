/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmimportexperimentdata.h"

// Project
#include "changecursorobject.h"
#include "fastdirectorylisting.h"
#include "experimentexternalindividualimages.h"
#include "tools.h"
#include "logging.h"

// Qt
#include <QDir>
#include <QMessageBox>



FrmImportExperimentData::FrmImportExperimentData(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	// Init variables
	m_foundImages = new FrmImportExperimentDataModel(this, m_ui.treeView);
	m_ui.treeView->setModel(m_foundImages);
	m_currentFilterAppliedToFolderNames = false;
	m_currentFilterAppliedToImageNames = false;

	// Signals/slots
	connect(m_ui.clbLoad, SIGNAL(clicked()), this, SLOT(importSelectedImages()));
	connect(m_ui.lieFilter, SIGNAL(textChanged(const QString&)), this, SLOT(applyFilter()));
	connect(m_ui.chkApplyFilterToImages, SIGNAL(toggled(bool)), this, SLOT(applyFilter()));
	connect(m_ui.chkApplyFilterToFolderNames, SIGNAL(toggled(bool)), this, SLOT(applyFilter()));
	connect(m_ui.grbAdvancedImport, SIGNAL(toggled(bool)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.liePositionMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieTpMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieChannelMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieZIndexMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));

	// Adjust column widths
	QHeaderView* header = m_ui.treeView->header();
	if(header)
		header->resizeSection(0, 500);
}

void FrmImportExperimentData::setCurrentFolder(QString folder)
{
	ChangeCursorObject cc;
	
	// Make sure path ends with '/'
	folder = QDir::fromNativeSeparators(folder);
	if(folder.right(1) != "/")
		folder += '/';
	m_currentFolder = folder;

	// Look for images in all sub folders and in root folder
	QHash<QString, QStringList> imagesInSubfolders; 
	QList<QString> imagesInRoot;
	QStringList directories = QDir(folder).entryList(QDir::AllDirs | QDir::NoDotDot);
	qSort(directories);
	bool foundSubfoldersWithImages = false;
	QStringList imageExtensions;
	imageExtensions << ".jpg" << ".tif" << ".png";
	int totalImageCount = 0;
	for(int iFolder = 0; iFolder < directories.size(); ++iFolder) {
		QString curSubFolder = directories[iFolder];
		if(curSubFolder.toLower() == "segmentation" || curSubFolder.toLower() == "background" || curSubFolder[0] == '_') {
			gLog << "Import experiment: skipping folder '" << curSubFolder << "'.\n";
			continue;
		}
		QStringList imagesInCurrentFolder = FastDirectoryListing::listFiles(QDir(folder + curSubFolder), imageExtensions);
		qSort(imagesInCurrentFolder);
		auto it = imagesInCurrentFolder.begin();
		while(it != imagesInCurrentFolder.end()) {
			if(it->toLower().contains("segmentation"))
				it = imagesInCurrentFolder.erase(it);
			else
				++it;
		}
		if(imagesInCurrentFolder.size()) {
			totalImageCount += imagesInCurrentFolder.size();
			if(curSubFolder == ".")
				imagesInRoot = std::move(imagesInCurrentFolder);
			else
				imagesInSubfolders[curSubFolder] = std::move(imagesInCurrentFolder);
		}
	}

	// Update GUI
	m_foundImages->setData(imagesInSubfolders, imagesInRoot);
	m_ui.clbLoad->setEnabled(totalImageCount > 0);
	m_ui.clbLoad->setText(QString("Import %1 images").arg(totalImageCount));
	m_ui.lblFoundImageData->setText(QString("Found image data in '%1':").arg(folder));

	// Apply filter
	m_currentFilter = "";
	applyFilter();
}

void FrmImportExperimentData::applyFilter()
{
	ChangeCursorObject cc;

	// If new filter contains old filter, hidden items do not need to be checked again (if filter settings are equal, nothing needs to be done)
	QString newFilter = m_ui.lieFilter->text();
	if(newFilter == m_currentFilter && m_currentFilterAppliedToFolderNames == m_ui.chkApplyFilterToFolderNames->isChecked() && m_currentFilterAppliedToImageNames == m_ui.chkApplyFilterToImages->isChecked())
		return;
	bool hiddenRowsRemainHidden = newFilter.contains(m_currentFilter);
	//bool excludeFilesContainingFilter = newFilter.size() && newFilter[0] == '!';

	// Apply filter
	int totalImageCount = 0;
	for(int topRow = 0; topRow < m_foundImages->rowCount(); ++topRow) {
		QModelIndex topRowIndex = m_foundImages->index(topRow, 0);
		bool hideTopRow = false;
		if(topRow < m_foundImages->getSubFoldersCount()) {
			// TopRow is folder, check if it should be visible
			if(m_ui.chkApplyFilterToFolderNames->isChecked()) {
				if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(topRow, QModelIndex()))
					hideTopRow = true;
				else {
					QString rowName = m_foundImages->data(topRowIndex).toString();
					hideTopRow = !rowName.contains(newFilter, Qt::CaseSensitive);
				}
			}
			
			// If topRow is visible, process children
			if(!hideTopRow) {
				int childrenCount = m_foundImages->rowCount(topRowIndex);
				for(int childRow = 0; childRow < childrenCount; ++childRow) {
					// Check if child row should be visible
					QModelIndex childRowIndex = m_foundImages->index(childRow, 0, topRowIndex);
					bool hideChildRow = false;
					if(m_ui.chkApplyFilterToImages->isChecked()) {
						if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(childRow, topRowIndex))
							hideChildRow = true;
						else {
							QString rowName = m_foundImages->data(childRowIndex).toString();
							hideChildRow = !rowName.contains(newFilter, Qt::CaseSensitive);
						}
					}

					// Hide/show child row
					m_ui.treeView->setRowHidden(childRow, topRowIndex, hideChildRow);
					if(!hideChildRow)
						++totalImageCount;
				}
			}
		}
		else {
			// TopRow is image, check if it should be hidden
			if(m_ui.chkApplyFilterToImages->isChecked()) {
				if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(topRow, QModelIndex()))
					hideTopRow = true;
				else {
					QString rowName = m_foundImages->data(topRowIndex).toString();
					hideTopRow = !rowName.contains(newFilter, Qt::CaseSensitive);
				}
			}
			if(!hideTopRow)
				++totalImageCount;
		}

		// Hide/show top row
		m_ui.treeView->setRowHidden(topRow, QModelIndex(), hideTopRow);
	}

	// Filter updated
	m_currentFilter = newFilter;
	m_currentFilterAppliedToFolderNames = m_ui.chkApplyFilterToFolderNames->isChecked();
	m_currentFilterAppliedToImageNames = m_ui.chkApplyFilterToImages->isChecked();

	// Update GUI
	m_ui.clbLoad->setEnabled(totalImageCount > 0);
	m_ui.clbLoad->setText(QString("Import %1 images").arg(totalImageCount));
}

void FrmImportExperimentData::importSelectedImages()
{
	ChangeCursorObject cc;

	// Determine conversion settings
	int bp = -1;
	int wp = -1;
	if(m_ui.grbConversion->isChecked()) {
		bp = m_ui.spbBlackPoint->value();
		wp = m_ui.spbWhitePoint->value();
	}

	ExperimentExternalIndividualImages* experiment = new ExperimentExternalIndividualImages(m_currentFolder, bp, wp);
	QSharedPointer<ExperimentExternalIndividualImages> experimentSharedPtr(experiment);

	try {
		// Create mapper and add all selected files
		for(int topRow = 0; topRow < m_foundImages->rowCount(); ++topRow) {
			QModelIndex topRowIndex = m_foundImages->index(topRow, 0);
			if(topRow < m_foundImages->getSubFoldersCount()) {

				// TopRow is folder, process children (i.e. images in folder) if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					QString folderName = m_foundImages->data(topRowIndex).toString();
					int imageCount = m_foundImages->rowCount(topRowIndex);
					for(int imageRow = 0; imageRow < imageCount; ++imageRow) {
						// Add image of child row if child row is visible
						QModelIndex childRowNameIndex = m_foundImages->index(imageRow, 0, topRowIndex);
						if(!m_ui.treeView->isRowHidden(imageRow, topRowIndex)) {
							QString imageName = m_foundImages->data(childRowNameIndex).toString();
							QString imageRelName = folderName + '/' + imageName;
							int pos = m_foundImages->data(m_foundImages->index(imageRow, 1, topRowIndex)).toInt();
							int tp = m_foundImages->data(m_foundImages->index(imageRow, 2, topRowIndex)).toInt();
							int ch = m_foundImages->data(m_foundImages->index(imageRow, 3, topRowIndex)).toInt();
							int z = m_foundImages->data(m_foundImages->index(imageRow, 4, topRowIndex)).toInt();
							if(pos >= 0 && tp >= 0 && ch >= 0 && z >= 0) {
								PictureIndex pi(pos, tp, ch, z);
								experiment->addImageMapping(pi, imageRelName);
							}
						}
					}
				}
			}
			else {
				// TopRow is image, add it to mapper if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					QString imageName = m_foundImages->data(topRowIndex).toString();
					int pos = m_foundImages->data(m_foundImages->index(topRow, 1)).toInt();
					int tp = m_foundImages->data(m_foundImages->index(topRow, 2)).toInt();
					int ch = m_foundImages->data(m_foundImages->index(topRow, 3)).toInt();
					int z = m_foundImages->data(m_foundImages->index(topRow, 4)).toInt();
					if(pos >= 0 && tp >= 0 && ch >= 0 && z >= 0) {
						PictureIndex pi(pos, tp, ch, z);
						experiment->addImageMapping(pi, imageName);
					}
				}
			}
		}
	}
	catch(ExceptionBase& e) {
		// Log error and show error message to user
		gErr << "Cannot open experiment: " << e.what() << " - file: " << e.where();
		QMessageBox::critical(this, "Error", QString("Cannot open experiment: ") + e.what());
	}

	// Done
	emit experimentImported(experimentSharedPtr, false);
}

int FrmImportExperimentData::mapImageNameToPositionNumber(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.liePositionMarker->text();
		if(separator.length())
			return Tools::getNumberFromString(imgName, separator);
	}

	return -1;
}

int FrmImportExperimentData::mapImageNameToTimePoint(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieTpMarker->text();
		if(separator.length())
			return Tools::getNumberFromString(imgName, separator);
	}

	return -1;
}

int FrmImportExperimentData::mapImageNameToChannel(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieChannelMarker->text();
		if(separator.length())
			return Tools::getNumberFromString(imgName, separator);
	}

	return -1;
}

int FrmImportExperimentData::mapImageNameToZIndex(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieZIndexMarker->text();
		if(separator.length())
			return Tools::getNumberFromString(imgName, separator);
	}

	return -1;
}
