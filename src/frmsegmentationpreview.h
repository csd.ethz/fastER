/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef frmsegmentationpreviewwindow_h__
#define frmsegmentationpreviewwindow_h__

#include "ui_frmSegmentationPreview.h"

// Project
#include "imageprocessingtools.h"
#include "pictureindex.h"

// fastER
#include "faster.h"

// OpenCV
#include "opencv2/core/core.hpp"

class FrmMainWindow;

/**
 * Preview window for segmentation results.
 */
class FrmSegmentationPreview : public QWidget {

	Q_OBJECT

public:

	FrmSegmentationPreview(QWidget* parent = 0);

	~FrmSegmentationPreview();

	/**
	 * Get ImageView.
	 */
	ImageView* getImageView() const {
		return m_ui.imageView;
	}

	/**
	 * Set image (to use for segmentation).
	 */
	void setImage(const QImage& image, const PictureIndex& pi);

	/**
	 * Set image to project segmentation results on (only has effect if "perimeter" or "blobs" display mode is selected).
	 * previewChannel channel to use for display of results. Set to -1 to show segmentation results using the same image as used for segmentation.
	 */
	void setPreviewImageChannel(int previewChannel)
	{
		// Change
		m_channelForPreview = previewChannel;
	}


	/**
	 * @Reimplemented.
	 */
	QSize sizeHint() const
	{
		return QSize(2000, 400);
	}

public slots:

	/**
	 * Update display of segmentation results.
	 */
	void updateSegmentationResults(const QString& saveResultToPath = "");

	// Visualize current segmentation results
	void visualizeSegmentation(const QString& saveResultToPath = "");

	//// Show short instructions
	//void showShortInstructions();

	// Show pixel x and y and value
	void showPixelValue(int x, int y, int grayLevel);

private slots:

	/**
	 * Refresh segmentation preview
	 */
	void refreshSegmentationButtonClicked();

private:

	//// Create transparent overlay image with segmentation results
	//QImage createOverlayImage(bool drawOnlyPerimeters) const;

	// Clear segmentation results (clears m_segmentationResultsList etc., but does not update display or anything)
	void clearSegmentationResults() 
	{
		m_segmentationResultsList.clear();
		delete[] m_segmentationResultsLabelMap;
		m_segmentationResultsLabelMap = 0;
		m_segmentationResultsImageSizeX = 0;
		m_segmentationResultsImageSizeY = 0;
	}

	// GUI
	Ui::SegmentationPreview m_ui;

	// Original image and its PictureIndex
	QImage m_originalImage;
	PictureIndex m_originalImagePictureIndex;

	// Channel of image to project segmentation results on (if -1, original image will be used)
	int m_channelForPreview;

	// Buffer for segmentation algorithm
	cv::Mat m_imageBuffer;

	// Current segmentation results (as list and label map; both contain the same information)
	std::vector<Blob> m_segmentationResultsList;
	const unsigned int* m_segmentationResultsLabelMap;
	int m_segmentationResultsImageSizeX;
	int m_segmentationResultsImageSizeY;

	// Image after segmentation - always null, unless image data type is still IDT_UInt8Gray after segmentation
	// Required to display not really segmented images (but just denoised for example)
	QImage m_processedImage;

	// fastER
	faster::FastERDetector m_faster;

	// Image processing tools
	ImageProcessingTools m_imageProcessingTools;

	friend FrmMainWindow;
};


#endif // frmsegmentationpreviewwindow_h__
