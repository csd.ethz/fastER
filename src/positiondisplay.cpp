/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positiondisplay.h"

// Qt
#include <QScrollBar>
#include <QMouseEvent>
#include <QProgressDialog>

// Project includes
#include "experiment.h"
#include "settings.h"
#include "frmmainwindow.h"


// Min/max scaling
const float PositionDisplay::MIN_SCALE = 0.01f;		// 1%
const float PositionDisplay::MAX_SCALE = 20.0f;		// 2000%
const float PositionDisplay::Z_TRACKINGREGION_RECT = 10.0f;


PositionDisplay::PositionDisplay (QWidget *_parent)
	: QGraphicsView (_parent)
{
	// Init variables
	scrolling = false;
	drawThumbNailFrames = true;
	drawText = true;
	initialized = false;
	experiment = 0;
	regionSelectionMode = false;
	regionIsBeingSelected = false;
	currentlySelectedRegion = 0;
	thumbnailsTimePoint = 1;
	thumbnailsWaveLength = 0;
	scalingFactor = 1.0f;

	// Enable anti aliasing
	setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

	// Every PositionDisplay has its own scene
	setScene (new QGraphicsScene (this));

	// Set background color to gray
	scene()->setBackgroundBrush(QBrush(QColor(200, 200, 200)));

	// Make zoom like in google maps
	setTransformationAnchor(AnchorUnderMouse);

	// Align top, left
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
}

void PositionDisplay::initialize(Experiment* _experiment, bool _drawThumbNailFrame, bool _exclusiveSelection, bool _showProgressBar)
{
	// Init (or reset) variables
	exclusiveSelection = _exclusiveSelection;
	indexOfLastSelectedPosition = -1;
	experiment = _experiment;

	// If we already have thumbnails, delete them first
	for(QHash<int, PositionThumbnail*>::iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
		scene()->removeItem(*it);
		delete *it;
	}
	thumbnails.clear();

	// Check if this was all to do
	if(!_experiment)
		return;

	// Get positions
	const QList<int>& positions = _experiment->getPositionNumbers();

	// Show progress bar
	std::unique_ptr<QProgressDialog> progessBar = 0;
	if(_showProgressBar) {		
		progessBar = std::unique_ptr<QProgressDialog>(new QProgressDialog("Loading thumbnails..", QString(), 0, positions.size(), this));
		progessBar->setWindowTitle("CellAutoTracking");
		progessBar->setWindowModality(Qt::ApplicationModal);
		progessBar->setMinimumDuration(0);
		progessBar->setValue(0);

		// Disable context-help button
		progessBar->setWindowFlags( (progessBar->windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint) & (~Qt::WindowCloseButtonHint));
	}

	// Reset view transformation
	setTransform(QTransform());
	scalingFactor = 1.0f;

	// Default zoom: 10%
	float deltaScale = 0.1f / scalingFactor;
	scaleView(deltaScale);

	// Create position thumbnails
	float currentZValue = 0;
	int counter = 0;
	for(auto itPos = positions.constBegin(); itPos != positions.constEnd(); ++itPos) {
		//// Check if pos is available
		//if (! iter.current()->isAvailable())
		//	continue;
		int curPos = *itPos;

		// Create thumbnail and add it to scene
		PositionThumbnail *pt = new PositionThumbnail(_experiment, curPos, _drawThumbNailFrame, this, currentZValue);
		scene()->addItem(pt);

		// Save in hashmap
		thumbnails.insert(curPos, pt);

		// Get next z-value
		currentZValue += PositionThumbnail::Z_IMAGE_DELTA;

		// Update progress bar
		if(progessBar)
			progessBar->setValue(++counter);
	}

	// Update display
	updateDisplay();

	initialized = true;
}

PositionThumbnail* PositionDisplay::getThumbnail( int _index ) const
{
	// Check if thumbnail exists or return 0
	if(thumbnails.contains(_index))
		return thumbnails[_index];

	return 0;
}

void PositionDisplay::updateDisplay()
{
	// Get width and height
	int widthPixels = g_FrmMainWindow->experiment()->getImageSizeX();
	int heightPixels = g_FrmMainWindow->experiment()->getImageSizeY();

	// Update all thumbnails (set position etc) and grab scene coordinate bounds
	for(QHash<int, PositionThumbnail*>::iterator it = thumbnails.begin(); it != thumbnails.end(); ++it)
		(*it)->updateDisplay(widthPixels, heightPixels);

	// Remove tracking area rectangles
	for(int i = 0; i < trackingArea.size(); ++i) {
		scene()->removeItem(trackingArea[i]);
		delete trackingArea[i];
	}
	trackingArea.clear();

	// Update tracking area
	if(experiment) {
		// Get boundaries of coordinate system (in micrometers)
		const QRectF& coordinateBounds = experiment->getCoordinateBoundaries();

		// Get micrometer per pixel
		const QList<int>& positions = experiment->getPositionNumbers();
		assert(positions.size());
		float mmpp = experiment->getMicroMetersPerPixel(PictureIndex(positions[0], thumbnailsTimePoint, thumbnailsWaveLength, 1));

		// Create tracking area objects
		QList<QVariant> settingsTrackingArea = Settings::get<QList<QVariant> >(Settings::KEY_TRACKINGAREA);
		for(int i = 0; i < settingsTrackingArea.size(); ++i) {
			QRectF curRect = settingsTrackingArea[i].toRectF();

			// Transform rectangle if inverted
			float left = curRect.left(),
				top = curRect.top(),
				width = qAbs(curRect.width()),
				height = qAbs(curRect.height());
			//if(Settings::get<bool>(Settings::KEY_INVERTEDCOORDINATES)) {
			if(experiment->getCoordinatesInverted()) {
				//subtract coordinates from the bottom right corner (which in fact is the top left)
				left = coordinateBounds.right() - (left+width);
				top = coordinateBounds.bottom() - (top+height);
			}
			else {
				//subtract top left corner to account for a shifted coordinate origin
				left = left - coordinateBounds.left();
				top = top - coordinateBounds.top();
			}

			// Transform to pixels
			left /= mmpp;
			top /= mmpp;
			width /= mmpp;
			height /= mmpp;

			// Create item
			QGraphicsRectItem* newItem = new QGraphicsRectItem(left, top, width, height);
			newItem->setZValue(Z_TRACKINGREGION_RECT);
			trackingArea.append(newItem);

			// Add item
			scene()->addItem(newItem);
		}
	}
}

QList<int> PositionDisplay::getActivePositionNumbers() const
{
	// Find active positions
	QList<int> activePositions;
	for(QHash<int, PositionThumbnail*>::const_iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
		if((*it)->isSelected())
			activePositions.append((*it)->getPositionNumber());
	}

	return activePositions;
}

void PositionDisplay::setDrawThumbNailFrames( bool _on )
{
	if(drawThumbNailFrames != _on) {
		// Change and redraw thumbnails
		drawThumbNailFrames = _on;
		updateDisplay();
	}
}

void PositionDisplay::setDrawText( bool _on )
{
	if(drawText != _on) {
		// Change and redraw thumbnails
		drawText = _on;
		updateDisplay();
	}
}

void PositionDisplay::wheelEvent( QWheelEvent *_event )
{
	// Zoom
	scaleView(pow((double)2, _event->delta() / 400.0));
}

void PositionDisplay::scaleView( qreal _scaleFactor )
{
	// Check boundaries
	if(scalingFactor * _scaleFactor < MIN_SCALE) 
		_scaleFactor =  MIN_SCALE / scalingFactor;
	else if(scalingFactor * _scaleFactor > MAX_SCALE)
		_scaleFactor = MAX_SCALE / scalingFactor;

	// Set zoom
	scale(_scaleFactor, _scaleFactor);

	// Emit zoomChanged
	scalingFactor *= _scaleFactor;
	emit zoomChanged(scalingFactor);
}

bool PositionDisplay::selectPosition( int _index )
{
	// Get thumbnail for position
	PositionThumbnail *pos = 0;
	if(thumbnails.contains(_index))
		pos = thumbnails[_index];

	// Position not found
	if(!pos)
		return false;

	// Check if position is already selected, nothing to do
	if(pos->isSelected())
		return true;

	// If exclusive, unmark old position
	if(exclusiveSelection && indexOfLastSelectedPosition > 0) {
		if(thumbnails.contains(indexOfLastSelectedPosition))
			thumbnails[indexOfLastSelectedPosition]->setSelected(false);
	}

	// Mark position active
	pos->setSelected(true);

	// _index is now last selected
	indexOfLastSelectedPosition = _index;

	// Update display
	updateDisplay();

	// Emit signal
	emit positionSelectionChanged(_index, true);

	return true;
}

bool PositionDisplay::deSelectPosition( int _index )
{
	// Deselect all position if _index is -1
	if(_index == -1) {
		for(QHash<int, PositionThumbnail*>::const_iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
			if((*it)->isActive())
				(*it)->setSelected(false);
		}

		// Nothing selected
		indexOfLastSelectedPosition = -1;

		return true;
	}

	// Get thumbnail for position
	PositionThumbnail *pos = 0;
	if(thumbnails.contains(_index))
		pos = thumbnails[_index];

	// Position not found
	if(!pos)
		return false;

	// Check if position is already deselected, nothing to do
	if(!pos->isSelected())
		return true;

	// Deselect position
	pos->setSelected(false);

	// Update display
	updateDisplay();

	// Emit signal
	emit positionSelectionChanged(_index, false);

	return true;
}

void PositionDisplay::reportPositionDoubleClicked(int _index )
{
	// Ignore in region selection mode
	if(regionSelectionMode)
		return;

	// Emit signal
	emit positionDoubleClicked(_index);
}

void PositionDisplay::reportPositionRightClicked( int _index )
{
	// Ignore in region selection mode
	if(regionSelectionMode) {
		return;
	}

	// Emit signal
	emit positionRightClicked(_index);
}

void PositionDisplay::reportPositionLeftClicked( int _index )
{
	// Ignore in region selection mode
	if(regionSelectionMode)
		return;

	// Emit signal
	emit positionLeftClicked(_index);
}

void PositionDisplay::mousePressEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mousePressEvent(_event);

	if(regionSelectionMode && !regionIsBeingSelected && _event->button() == Qt::LeftButton) {
		// Get pos
		QPoint pos = mapFromGlobal(_event->globalPos());

		// Map to scene
		QPointF scenePos = mapToScene(pos);

		// Create item
		if(!currentlySelectedRegion) {
			currentlySelectedRegion = new QGraphicsRectItem();
			currentlySelectedRegion->setZValue(Z_TRACKINGREGION_RECT);
		}

		// Set rect
		currentlySelectedRegion->setRect(QRectF(scenePos, scenePos));

		// Add to scene
		scene()->addItem(currentlySelectedRegion);

		// Enable region selection
		regionIsBeingSelected = true;
	}
	else {
		// Start mouse navigation
		mouseMoveStartX = _event->globalX();
		mouseMoveStartY = _event->globalY();
		scrolling = true;
	}
}

void PositionDisplay::mouseReleaseEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseReleaseEvent(_event);

	// Stop mouse navigation
	scrolling = false;

	if(regionIsBeingSelected) {
		// Stop region selection
		regionIsBeingSelected = false;

		if(currentlySelectedRegion) {
			QRectF rectScene = currentlySelectedRegion->rect().normalized();

			if(rectScene.width() && rectScene.height()) {
				// Map region to experiment
				QPointF topLeft = mapSceneToExperiment(rectScene.topLeft());
				QPointF bottomRight = mapSceneToExperiment(rectScene.bottomRight());

				// Add region
				QRectF rectExp(topLeft, bottomRight);
				rectExp = rectExp.normalized();
				QList<QVariant> settingsTrackingArea = Settings::get<QList<QVariant> >(Settings::KEY_TRACKINGAREA);
				settingsTrackingArea.append(QVariant(rectExp));
				Settings::set(Settings::KEY_TRACKINGAREA, settingsTrackingArea);

				// Select all overlapping regions 
				for(QHash<int, PositionThumbnail*>::const_iterator it = thumbnails.constBegin(); it != thumbnails.constEnd(); ++it) {
					// Get experiment bounding rect of position
					//QRectF tmp = (*it)->boundingRect();
					int posNumber = (*it)->getPositionNumber();
					QRectF tmp = experiment->getGlobalImageRect(PictureIndex(posNumber, thumbnailsTimePoint, thumbnailsWaveLength, 1));

					// Check for overlap
					if(tmp.intersects(rectExp)) {
						// Select position
						selectPosition((*it)->getPositionNumber());
					}
				}

				// Update display
				updateDisplay();
			}

			// Remove currentlySelectedRegion from scene
			scene()->removeItem(currentlySelectedRegion);
			delete currentlySelectedRegion;
			currentlySelectedRegion = 0;
		}
	}

	// Check if region selection mode
	if(regionSelectionMode) {
		// Remove region if right mouse button
		if(_event->button() == Qt::RightButton) {
			// Get position of mouse click in widget
			QPoint pos = mapFromGlobal(_event->globalPos());

			// Map to scene
			QPointF posScene = mapToScene(pos);

			// Map to experiment
			QPointF posExp = mapSceneToExperiment(posScene);

			// Remove tracking region containing posExp
			QList<QVariant> settingsTrackingArea = Settings::get<QList<QVariant> >(Settings::KEY_TRACKINGAREA);
			for(int i = settingsTrackingArea.size() - 1; i >= 0; --i) {
				QRectF curRect = settingsTrackingArea[i].toRectF();
				if(curRect.contains(posExp)) {
					settingsTrackingArea.removeAt(i);
					break;
				}
			}
			Settings::set(Settings::KEY_TRACKINGAREA, settingsTrackingArea);

			// Update display
			updateDisplay();
		}
	}
}

void PositionDisplay::mouseMoveEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseMoveEvent(_event);

	// Scrolling
	if(scrolling) {
		// Get mouse pos
		int x = _event->globalX(),
			y = _event->globalY();

		// Get delta
		int deltaX = mouseMoveStartX - x,
			deltaY = mouseMoveStartY - y;

		// Get new scrollbar positions
		int scrollX = horizontalScrollBar()->value() + deltaX,
			scrollY = verticalScrollBar()->value() + deltaY;

		// Set scrollbar values
		horizontalScrollBar()->setValue(scrollX);
		verticalScrollBar()->setValue(scrollY);

		// Set start positions
		mouseMoveStartX = x;
		mouseMoveStartY = y;
	}
	else if(regionIsBeingSelected && currentlySelectedRegion) {
		// Region selection
		QPoint pos = mapFromGlobal(_event->globalPos());

		// Map to scene
		QPointF posScene = mapToScene(pos);

		// Update rect
		QRectF sceneRect = currentlySelectedRegion->rect();
		sceneRect.setBottomRight(posScene);
		currentlySelectedRegion->setRect(sceneRect);
	}
}

bool PositionDisplay::isPositionSelected( int _index )
{
	if(thumbnails.contains(_index)) 
		return thumbnails[_index]->isSelected();

	return false;
}

void PositionDisplay::setRegionSelectionMode( bool _on )
{
	regionSelectionMode = _on;
}

QPointF PositionDisplay::mapSceneToExperiment( const QPointF& _pos )
{
	// Make sure we have experiment descriptor
	if(!experiment)
		return QPointF();

	// Convert to micrometers
	const QList<int>& positions = experiment->getPositionNumbers();
	assert(positions.size());
	float mmpp = experiment->getMicroMetersPerPixel(PictureIndex(positions[0], thumbnailsTimePoint, thumbnailsWaveLength, 1));
	QPointF ret(_pos);
	ret *= mmpp;

	// Get experiment boundaries
	QRectF bounds = experiment->getCoordinateBoundaries();

	// Regard coordinate system bounds and whether the coordinate system is inverted
	//if (Settings::get<bool>(Settings::KEY_INVERTEDCOORDINATES)) {
	if(experiment->getCoordinatesInverted()) {
		// BottomRight is bigger value, so actually top left
		ret = bounds.bottomRight() - ret;
	}
	else {
		ret += bounds.topLeft();
	}

	// Done
	return ret;
}

void PositionDisplay::setThumbnailsTimePoint( int _timePoint )
{
	thumbnailsTimePoint = _timePoint;
}

void PositionDisplay::setThumbnailsWavelength( int _wl )
{
	thumbnailsWaveLength = _wl;
}




