/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationconverter_h__
#define segmentationconverter_h__

// STL
#include <vector>

// QT
#include <QImage>
#include <QHash>

// OpenCV
#include "opencv2/core/core.hpp"

// Project
#include "blob.h"



/**
 * Types of image data supported by segmentation pipelines.
 */
enum ImageDataType {
	IDT_UInt8Gray,			// Anything between 0 and 255
	IDT_UInt8Binary,		// Only 0 and 255
	IDT_UInt32LabelMap		// Each pixel has 0 if in background and id of corresponding blob, otherwise
};

/**
 * Get bytes per pixel for provided ImageDataType.
 */
inline int bytesPerPixelForImageDataType(ImageDataType type)
{
	switch(type) {
	case IDT_UInt8Binary:
	case IDT_UInt8Gray:
		return 1;
	case  IDT_UInt32LabelMap:
		return 4;
	default:
		// Should not happen
		assert(false);
	}
	return 1;
}


/**
 * 
 */
class ImageProcessingTools {

public:



	/////////////////////////////////////////////////////////////////////////
	// Static stuff
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Draw cells or extract perimeters from uint32 label map (randomly colors each object).
	 * Labels must be stored in row-by-row order, with sizeX*sizeof(unsigned int) bytes per row and 0 as background value.
	 */
	static QImage visualizeLabelMap(const unsigned int* lblMap, int sizeX, int sizeY, bool drawPerimetersOnly)	 
	{
		// Create empty return color image
		QImage img = QImage(sizeX, sizeY, QImage::Format_ARGB32);
		int bpl = img.bytesPerLine();
		img.fill(qRgba(0, 0, 0, 0));
		unsigned char* retData = img.bits();
		if(!lblMap)
			return img;

		// Neighborhood of a foreground pixel (use 8-neighborhood to get perimeters that are closed at corners)
		const int deltaX[] = {1, -1, -1, 0, 1, -1, 0, 1};
		const int deltaY[] = {0, 0, -1, -1, -1, 1, 1, 1};

		// Hash table with random color for each label
		QHash<int, QRgb> colorTable;

		// Iterate over pixels and identify perimeter pixels
		for(int y = 0; y < sizeY; ++y) {
			for(int x = 0; x < sizeX; ++x) {
				// Get current pixel value and skip background pixels
				int curval = lblMap[y*sizeX + x];
				if(curval == 0)
					continue;

				// Check if neighbor with different pixel value exists
				bool drawPixel = false;
				if(drawPerimetersOnly) {
					for(int iNeighbor = 0; iNeighbor < (sizeof(deltaX)/sizeof(*deltaX)); ++iNeighbor) {
						int xN = x + deltaX[iNeighbor];
						int yN = y + deltaY[iNeighbor];
						if(xN < 0 || yN < 0 || xN >= sizeX || yN >= sizeY) {
							// Pixel is at image border -> perimeter
							drawPixel = true;
							break;
						}

						int neighborVal = lblMap[yN*sizeX + xN];
						if(neighborVal != curval) {
							// Pixel has neighbor that is background or has other region label -> perimeter
							drawPixel = true;
							break;
						}
					}
				}
				else
					// Always draw pixel
					drawPixel = true;

				// Draw pixel in return image if it belongs to perimeter
				if(drawPixel) {
					// Determine color
					QRgb color;
					auto itColor = colorTable.find(curval);
					if(itColor != colorTable.end()) {
						color = *itColor;
					} 
					else {
						// Create random color with minimum value of 100 in each channel
						const int offset = 100;
						int r1 = qrand() % (255 - offset);
						int r2 = qrand() % (255 - offset);
						int r3 = qrand() % (255 - offset);
						color = qRgba(offset+r1, offset+r2, offset+r3, 255);
						colorTable.insert(curval, color);
					}

					// Paint pixel
					*(((unsigned int*)(retData+y*bpl))+x) = color;
				}
			}
		}

		return img;
	}

	/////////////////////////////////////////////////////////////////////////
	// Non static stuff
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Transform binary image with blobs so that only perimeters are shown anymore.
	 */
	template <class T>
	QImage drawPerimeters(QImage& img, const T& foregroundColor, const T& backgroundColor, int pixelConnectivityForeground)
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		if(img.isNull() || img.width() == 0 || img.height() == 0)
			return QImage();

		// Reset visited mask
		int sizeX = img.width(),
			sizeY = img.height();
		if(m_visitedMask.cols < sizeX+2 || m_visitedMask.rows < sizeY+2)
			m_visitedMask = cv::Mat(sizeY+2, sizeX+2, CV_8UC1);
        m_visitedMask = cv::Scalar(0);

		// Initialize
		QImage retImage(img.size(), img.format());
		retImage.fill(backgroundColor);
		unsigned char* imdataOut = retImage.bits();
		int bytesPerLineOut = retImage.bytesPerLine();
		unsigned char* imdata = img.bits();
		int bytesPerLine = img.bytesPerLine();

		// Find background pixel
		for(int y = 0; y < sizeY; ++y) {
			for(int x = 0; x < sizeX; ++x) {
				if(*(((T*)(imdata + y*bytesPerLine))+x) == backgroundColor) {
					m_stackBackground.push_back(cv::Point2i(x, y));
					y = sizeY;
					break;
				}
			}
		}

		while(m_stackBackground.size()) {
			// Pop background pixel
			cv::Point2i curPoint = m_stackBackground.back();
			m_stackBackground.pop_back();

			// Investigate neighbors
			checkPixelPerimeter(sizeX, sizeY, curPoint.y, curPoint.x - 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
			checkPixelPerimeter(sizeX, sizeY, curPoint.y, curPoint.x + 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
			checkPixelPerimeter(sizeX, sizeY, curPoint.y + 1, curPoint.x, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
			checkPixelPerimeter(sizeX, sizeY, curPoint.y - 1, curPoint.x, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
			if(pixelConnectivityForeground == 4) {
				checkPixelPerimeter(sizeX, sizeY, curPoint.y + 1, curPoint.x + 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
				checkPixelPerimeter(sizeX, sizeY, curPoint.y + 1, curPoint.x - 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
				checkPixelPerimeter(sizeX, sizeY, curPoint.y - 1, curPoint.x - 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
				checkPixelPerimeter(sizeX, sizeY, curPoint.y - 1, curPoint.x + 1, imdata, foregroundColor, bytesPerLine, imdataOut, bytesPerLineOut, false);
			}
		}

		return retImage;
	}

	/**
	 * Remove blobs in a binary QImage that are smaller than minSize or bigger than maxSize (size: number of pixels).
	 * T must be the type used for pixels, e.g. QRGBA if image has type QImage::Format_ARGB32 or
	 * unsigned char if QImage::Format_Indexed8.
	 */
	template <class T>
	void sizeFilter(QImage& img, const T& foregroundColor, const T& backgroundColor, int minSize, int maxSize, int pixelConnectivityForeground) 
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		// Check input
		Blob blob;
		if(img.isNull() || img.width() == 0 || img.height() == 0)
			return;

		// Reset visited mask
		int sizeX = img.width(),
			sizeY = img.height();
		if(m_visitedMask.cols < sizeX+2 || m_visitedMask.rows < sizeY+2)
			m_visitedMask = cv::Mat(sizeY+2, sizeX+2, CV_8UC1);
        m_visitedMask = cv::Scalar(0);

		// Initialize
		unsigned char* imdata = img.bits();
		int bytesPerLine = img.bytesPerLine();

		// Check if first pixel is background
		T val = *((T*)imdata);
		if(val != foregroundColor)
			m_stackBackground.push_back(cv::Point2i(0, 0));
		else
			m_stackForeground.push_back(cv::Point2i(0, 0));

		// Investigate all pixels
		Blob* curBlob = 0;
		while(m_stackBackground.size() || m_stackForeground.size()) {
			cv::Point2i curPoint;
			bool curPointIsBackground;
			if(m_stackForeground.size()) {
				if(!curBlob) {
					// New blob found
					curBlob = &blob;
				}

				// Pop pixel and add it to current blob
				curPoint = m_stackForeground.back();
				curPointIsBackground = false;
				m_stackForeground.pop_back();
				curBlob->addPixel(curPoint);
			}
			else {
				if(curBlob) {
					// Current blob finished, so check size
					if(curBlob->getArea() < minSize || curBlob->getArea() > maxSize) {
						// Fill blob with background pixels
						fillBlob(imdata, bytesPerLine, backgroundColor, blob.getPixels());
					}

					// Clear current blob
					curBlob->setPixels();
					curBlob = 0;
				}
				

				// Pop background pixel
				curPoint = m_stackBackground.back();
				curPointIsBackground = true;
				m_stackBackground.pop_back();
			}

			// Investigate neighbors
			checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x - 1, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x + 1, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			if(pixelConnectivityForeground == 8) {
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
			}
		}
		if(curBlob) {
			if(curBlob->getArea() < minSize || curBlob->getArea() > maxSize) {
				// Fill blob with background pixels
				fillBlob(imdata, bytesPerLine, backgroundColor, blob.getPixels());
			}
		}
	}

	/**
	 * Fill holes (4-connectivity of foreground and 8-connectivity of background) in a binary QImage. 
	 * Holes are pixels that cannot be reached by flooding the image starting with the first found background pixel.
	 * T must be the type used for pixels, e.g. QRGBA if image has type QImage::Format_ARGB32 or
	 * unsigned char if QImage::Format_Indexed8.
	 */
	template <class T>
	QImage fillHoles(const QImage& img, const T& foregroundColor, const T& backgroundColor, int pixelConnectivityForeground) 
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		// Check input
		if(img.isNull() || img.width() == 0 || img.height() == 0)
			return img;

		// Reset visited mask
		int sizeX = img.width(),
			sizeY = img.height();
		if(m_visitedMask.cols < sizeX+2 || m_visitedMask.rows < sizeY+2)
			m_visitedMask = cv::Mat(sizeY+2, sizeX+2, CV_8UC1);
        m_visitedMask = cv::Scalar(0);

		// Initialize
		const unsigned char* imdata = img.bits();
		int bytesPerLine = img.bytesPerLine();
		QImage retImage(img.size(), img.format());
		retImage.fill(foregroundColor);
		unsigned char* retImdata = retImage.bits();
		int retImageBytesPerLine = retImage.bytesPerLine();

		// Find background pixel
		for(int y = 0; y < sizeY; ++y) {
			for(int x = 0; x < sizeX; ++x) {
				if(*(((T*)(imdata + y*bytesPerLine))+x) != foregroundColor) {
					m_stackBackground.push_back(cv::Point2i(x, y));
					y = sizeY;
					break;
				}
			}
		}

		// Investigate all pixels
		while(m_stackBackground.size()) {
			// Pop background pixel
			cv::Point2i curPoint = m_stackBackground.back();
			m_stackBackground.pop_back();

			// Set pixel in returned image
			if(curPoint.x >= 0 && curPoint.x < sizeX && curPoint.y >= 0 && curPoint.y < sizeY)
				*(((T*)(retImdata + curPoint.y*retImageBytesPerLine))+curPoint.x) = backgroundColor;

			// Investigate neighbors
			checkPixelBackground(sizeX, sizeY, curPoint.y-1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			checkPixelBackground(sizeX, sizeY, curPoint.y, curPoint.x-1, imdata, foregroundColor, bytesPerLine);
			checkPixelBackground(sizeX, sizeY, curPoint.y, curPoint.x+1, imdata, foregroundColor, bytesPerLine);
			checkPixelBackground(sizeX, sizeY, curPoint.y+1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			if(pixelConnectivityForeground == 4) {
				checkPixelBackground(sizeX, sizeY, curPoint.y-1, curPoint.x-1, imdata, foregroundColor, bytesPerLine);
				checkPixelBackground(sizeX, sizeY, curPoint.y-1, curPoint.x+1, imdata, foregroundColor, bytesPerLine);
				checkPixelBackground(sizeX, sizeY, curPoint.y+1, curPoint.x-1, imdata, foregroundColor, bytesPerLine);
				checkPixelBackground(sizeX, sizeY, curPoint.y+1, curPoint.x+1, imdata, foregroundColor, bytesPerLine);
			}
		}

		return retImage;
	}

	/**
	 * Extract connected components (4-connectivity) of a QImage that have the given foreground color.
	 * T must be the type used for pixels, e.g. QRGBA if image has type QImage::Format_ARGB32 or
	 * unsigned char if QImage::Format_Indexed8.
	 */
	template <class T>
	std::vector<Blob> convertBinaryQImageToBlobs(const QImage& img, const T& foregroundColor, int pixelConnectivityForeground)
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		// Check input
		std::vector<Blob> blobs;
		if(img.isNull() || img.width() == 0 || img.height() == 0)
			return blobs;

		// Reset visited mask
		int sizeX = img.width(),
			sizeY = img.height();
		if(m_visitedMask.cols < sizeX+2 || m_visitedMask.rows < sizeY+2)
			m_visitedMask = cv::Mat(sizeY+2, sizeX+2, CV_8UC1);
        m_visitedMask = cv::Scalar(0);

		// Initialize
		const unsigned char* imdata = img.bits();
		int bytesPerLine = img.bytesPerLine();

		// Check if first pixel is background
		T val = *((T*)imdata);
		if(val != foregroundColor)
			m_stackBackground.push_back(cv::Point2i(0, 0));
		else
			m_stackForeground.push_back(cv::Point2i(0, 0));

		// Investigate all pixels
		Blob* curBlob = 0;
		while(m_stackBackground.size() || m_stackForeground.size()) {
			cv::Point2i curPoint;
			bool curPointIsBackground;
			if(m_stackForeground.size()) {
				if(!curBlob) {
					// Add new blob
					blobs.push_back(Blob());
					curBlob = &blobs.back();
				}

				// Pop pixel and add it to current blob
				curPoint = m_stackForeground.back();
				curPointIsBackground = false;
				m_stackForeground.pop_back();
				curBlob->addPixel(curPoint);
			}
			else {
				curBlob = 0;

				// Pop background pixel
				curPoint = m_stackBackground.back();
				curPointIsBackground = true;
				m_stackBackground.pop_back();
			}

			// Investigate neighbors
			checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x - 1, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x + 1, imdata, foregroundColor, bytesPerLine);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x, imdata, foregroundColor, bytesPerLine);
			if(pixelConnectivityForeground == 8) {
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
			}
		}

		return blobs;
	}

	/**
	 * Extract connected components (4-connectivity) of a cv::Mat image that have the given foreground color.
	 * Only 2-D images with type CV_8UC1 are supported!
	 */
	std::vector<Blob> convertBinaryMatToBlobs(const cv::Mat& img, unsigned char foregroundColor, int pixelConnectivityForeground)
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		// Check input
		std::vector<Blob> blobs;
		if(img.type() != CV_8UC1 || img.rows <= 0 || img.cols <= 0)
			return blobs;

		// Reset visited mask
		int sizeX = img.cols,
			sizeY = img.rows;
		if(m_visitedMask.cols < sizeX+2 || m_visitedMask.rows < sizeY+2)
			m_visitedMask = cv::Mat(sizeY+2, sizeX+2, CV_8UC1);
        m_visitedMask = cv::Scalar(0);

		// Initialize
		const unsigned char* imdata = img.data;

		// Check if first pixel is background
		if(*imdata != foregroundColor)
			m_stackBackground.push_back(cv::Point2i(0, 0));
		else
			m_stackForeground.push_back(cv::Point2i(0, 0));

		// Investigate all pixels
		Blob* curBlob = 0;
		while(m_stackBackground.size() || m_stackForeground.size()) {
			cv::Point2i curPoint;
			bool curPointIsBackground;
			if(m_stackForeground.size()) {
				if(!curBlob) {
					// Add new blob
					blobs.push_back(Blob());
					curBlob = &blobs.back();
				}

				// Pop pixel and add it to current blob
				curPoint = m_stackForeground.back();
				curPointIsBackground = false;
				m_stackForeground.pop_back();
				curBlob->addPixel(curPoint);
			}
			else {
				curBlob = 0;

				// Pop background pixel
				curPoint = m_stackBackground.back();
				curPointIsBackground = true;
				m_stackBackground.pop_back();
			}

			// Investigate neighbors
			checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x, imdata, foregroundColor, sizeX);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x - 1, imdata, foregroundColor, sizeX);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y, curPoint.x + 1, imdata, foregroundColor, sizeX);
			if(curPointIsBackground && m_stackForeground.size()) {
				m_stackBackground.push_back(curPoint);
				continue;
			}
			checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x, imdata, foregroundColor, sizeX);
			if(pixelConnectivityForeground == 8) {
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x - 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y - 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
				if(curPointIsBackground && m_stackForeground.size()) {
					m_stackBackground.push_back(curPoint);
					continue;
				}
				checkPixel(sizeX, sizeY, curPoint.y + 1, curPoint.x + 1, imdata, foregroundColor, sizeX);
			}
		}

		return blobs;
	}

	/**
	 * Convert UInt32 label map top list of blobs. It is assumed that background pixels have value 0.
	 */
	std::vector<Blob> convertUInt32LabelMapToBlobs(const unsigned int* labelMap, int width, int height)
	{
		// Extract blobs
		QHash<int, std::vector<Blob::pixel_type>> pixelsByLabel;
		for(int y = 0; y < height; ++y) {
			for(int x = 0; x < width; ++x) {
				int curPixel = *(labelMap++);
				if(curPixel > 0) {
					pixelsByLabel[curPixel].push_back(Blob::pixel_type(x, y));
				}
			}
		}

		// Convert to list of blobs
		std::vector<Blob> blobs;
		blobs.resize(pixelsByLabel.size());
		int count = 0;
		for(auto it = pixelsByLabel.begin(); it != pixelsByLabel.end(); ++it) {
			blobs[count++].setPixels(std::move(*it));
		}

		return blobs;
	}

	/**
	 * Extract marked pixels that have the given foreground color.
	 * T must be the type used for pixels, e.g. QRGBA if image has type QImage::Format_ARGB32 or
	 * unsigned char if QImage::Format_Indexed8.
	 */
	template <class T>
	std::vector<cv::Point2i> convertBinaryQImageToPixelList(const QImage& img, const T& foregroundColor)
	{
		int bytesPerLine = img.bytesPerLine();
		const unsigned char* imdata = img.bits();
		std::vector<cv::Point2i> pixels;
		for(int y = 0; y < img.height(); ++y) {
			for(int x = 0; x < img.width(); ++x) {
				if(*(((T*)(imdata + y*bytesPerLine))+x) == foregroundColor)
					pixels.push_back(cv::Point2i(x, y));
			}
		}
		return pixels;
	}

	

	/**
	 * Convert one buffer with provided ImageDataType into a different ImageDataType.
	 * @param imDataSrc pointer to original image data, will not be altered.
	 * @param imageSizeX width of original image.
	 * @param imageSizeY height of original image.
	 * @newType the pixel type the original image should be converted to. Can also be
	 * @return pointer to newly allocated image of same size as input image but with type newType. Caller must delete[] the returned data.
	 */
	unsigned char* convertImageData(const unsigned char* imDataSrc, int imageSizeX, int imageSizeY, ImageDataType srcType, ImageDataType newType, int pixelConnectivityForeground)
	{
		assert(pixelConnectivityForeground == 4 || pixelConnectivityForeground == 8);

		// Create buffer
		int bufSize = imageSizeX * imageSizeY * bytesPerPixelForImageDataType(newType);
		unsigned char* buf = new unsigned char[bufSize];
		if(newType == srcType || (newType == IDT_UInt8Gray && srcType == IDT_UInt8Binary) ) {
			// Can just copy
			memcpy(buf, imDataSrc, imageSizeX * imageSizeY * bytesPerPixelForImageDataType(newType));
		}
		else {
			// Need to convert
			assert((newType == IDT_UInt32LabelMap && (srcType == IDT_UInt8Gray || srcType == IDT_UInt8Binary)) || 
				((newType == IDT_UInt8Gray || newType == IDT_UInt8Binary) && srcType == IDT_UInt32LabelMap) ||
				(newType == IDT_UInt8Binary && srcType == IDT_UInt8Gray));
			if(newType == IDT_UInt32LabelMap && (srcType == IDT_UInt8Gray || srcType == IDT_UInt8Binary)) {
				// Treat source image as binary image and convert to label map
				memset(buf, 0, bufSize);
				const cv::Mat cvImage(imageSizeY, imageSizeX, CV_8UC1, const_cast<unsigned char*>(imDataSrc));	// Data will still not be changed!
				auto blobs = convertBinaryMatToBlobs(cvImage, 255, pixelConnectivityForeground);
				int blobId = 1;
				for(auto itBlob = blobs.begin(); itBlob != blobs.end(); ++itBlob) {
					for(auto itPixel = itBlob->getPixels().begin(); itPixel != itBlob->getPixels().end(); ++itPixel) {
						reinterpret_cast<unsigned int*>(buf)[itPixel->y * imageSizeX + itPixel->x] = blobId;
					}
					++blobId;
				}
			}
			else if((newType == IDT_UInt8Gray || newType == IDT_UInt8Binary) && srcType == IDT_UInt32LabelMap) {
				// Convert label map into a binary image
				for(int y = 0; y < imageSizeY; ++y) {
					for(int x = 0; x < imageSizeX; ++x) {
						buf[y * imageSizeX + x] = reinterpret_cast<const unsigned int*>(imDataSrc)[y * imageSizeX + x] != 0 ? 255 : 0;
					}
				}
			}
			else if(newType == IDT_UInt8Binary && srcType == IDT_UInt8Gray) {
				// Convert grayscale image to a binary image -> everything > 0 becomes 255
				for(int y = 0; y < imageSizeY; ++y) {
					for(int x = 0; x < imageSizeX; ++x) {
						buf[y * imageSizeX + x] = imDataSrc[y * imageSizeX + x] != 0 ? 255 : 0;
					}
				}
			}
			else
				assert(false);
		}

		return buf;
	}

private:

	// Check if pixel is within image boundaries, not visited yet and if so, add it to foreground or background stack and mark as visited.
	template <class T>
	void checkPixel(int sizeX, int sizeY, int y, int x, const unsigned char* imdata, const T& foregroundColor, int bytesPerLine)
	{
		if(x >= 0 && x < sizeX && y >= 0 && y < sizeY && m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
			// Mark as visited
			m_visitedMask.at<unsigned char>(y+1, x+1) = 1;

			// Add to stack
			T val = *(((T*)(imdata + y*bytesPerLine))+x);
			if(val != foregroundColor)
				m_stackBackground.push_back(cv::Point2i(x, y));
			else
				m_stackForeground.push_back(cv::Point2i(x, y));
		}
		else {
			// Check if pixel is on 1-pixel background line around image
			if(x >= -1 && x <= sizeX && y >= -1 && y <= sizeY) {
				if(m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
					m_visitedMask.at<unsigned char>(y+1, x+1) = 1;
					m_stackBackground.push_back(cv::Point2i(x, y));
				}
			}
		}
	}

	// Check if pixel is within image boundaries, not visited yet, has background color and if so, add it to background stack and mark as visited.
	template <class T>
	void checkPixelBackground(int sizeX, int sizeY, int y, int x, const unsigned char* imdata, const T& foregroundColor, int bytesPerLine)
	{
		if(x >= 0 && x < sizeX && y >= 0 && y < sizeY && m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
			// Mark as visited
			m_visitedMask.at<unsigned char>(y+1, x+1) = 1;

			// Add to stack
			T val = *(((T*)(imdata + y*bytesPerLine))+x);
			if(val != foregroundColor)
				m_stackBackground.push_back(cv::Point2i(x, y));
		}
		else {
			// Check if pixel is on 1-pixel background line around image
			if(x >= -1 && x <= sizeX && y >= -1 && y <= sizeY) {
				if(m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
					m_visitedMask.at<unsigned char>(y+1, x+1) = 1;
					m_stackBackground.push_back(cv::Point2i(x, y));
				}
			}
		}
	}

	// Fill blob
	template <class T>
	void fillBlob(unsigned char* imdata, int bytesPerLine, const T& color, const std::vector<Blob::pixel_type>& pixels)
	{
		for(auto it = pixels.begin(); it != pixels.end(); ++it) 
			*(((T*)(imdata + it->y*bytesPerLine))+it->x) = color;
	}

	// Add pixel to background stack if it belongs to background, otherwise mark pixel in imageOut if it has been reached from a background pixel
	template <class T>
	void checkPixelPerimeter(int sizeX, int sizeY, int y, int x, const unsigned char* imdata, const T& foregroundColor, int bytesPerLine, unsigned char* imdataOut, int bytesPerLineOut, bool pixelWasForeground)
	{
		if(x >= 0 && x < sizeX && y >= 0 && y < sizeY && m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
			// Mark as visited
			m_visitedMask.at<unsigned char>(y+1, x+1) = 1;

			// Add to stack
			T val = *(((T*)(imdata + y*bytesPerLine))+x);
			if(val != foregroundColor)
				m_stackBackground.push_back(cv::Point2i(x, y));
			else if(!pixelWasForeground)
				*(((T*)(imdataOut + y*bytesPerLineOut))+x) = foregroundColor;
		}
		else {
			// Check if pixel is on 1-pixel background line around image
			if(x >= -1 && x <= sizeX && y >= -1 && y <= sizeY) {
				if(m_visitedMask.at<unsigned char>(y+1, x+1) == 0) {
					m_visitedMask.at<unsigned char>(y+1, x+1) = 1;
					m_stackBackground.push_back(cv::Point2i(x, y));
				}
			}
		}
	}

	// Stacks (Note: background stack usually also contains +1 pixel at each image boarder)
	std::vector<cv::Point2i> m_stackBackground;
	std::vector<cv::Point2i> m_stackForeground;

	// Visited mask
	cv::Mat m_visitedMask;
};


#endif // segmentationconverter_h__
