/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef settings_h__
#define settings_h__

// Qt
#include <QString>
#include <QHash>
#include <QVariant>

// STL
#include <cassert>
#include <vector>
#include <deque>
#include <memory>

//// Eigen library
//#include <Eigen/Dense>

// LibSVM
#include "svm.h"

// Project
#include "segmentationoperationfaster.h"
#include "faster.h"
#include "segmentationoperationbilateralfilter.h"
#include "pictureindex.h"



class SegmentationOperationList;
class SegmentationOperationSizeFilter;


class Settings {

	/**
	 * Private constructor. Use static methods to obtain or change settings.
	 */
	Settings();

	/**
	 * Destructor.
	 */
	~Settings();

public:

	/**
	 * Get setting. Setting key must be valid, i.e. there must be a corresponding entry in m_settings.
	 */
	template <class T>
	static T get(const QString& key);

	/**
	 * Get setting as QVariant. Setting key must be valid, i.e. there must be a corresponding entry in m_settings.
	 */
	static QVariant getVariant(const QString& key) {
		if(!m_inst)
			m_inst = new Settings();
		assert(m_inst->m_settings.contains(key));
		return m_inst->m_settings.value(key, QVariant());
	}

	/**
	 * Check if setting is set, i.e. not null. Setting key must be valid, i.e. there must be a corresponding entry in m_settings.
	 */
	static bool isSet(const QString& key) {
		if(!m_inst)
			m_inst = new Settings();
		assert(m_inst->m_settings.contains(key));
		return !m_inst->m_settings.value(key, QVariant()).isNull();
	}

	/**
	 * Change setting. Setting key must be valid, i.e. there must be a corresponding entry in m_settings.
	 */
	template <class T>
	static void set(const QString& key, const T& val);

	/**
	 * Get description of setting.
	 */
	static QString getDescription(const QString& key);

	/**
	 * Cleanup.
	 */
	static void cleanUp();

	/**
	 * Special functions for specific settings.
	 */

	// Get segmentation pipeline
	static std::shared_ptr<SegmentationOperationList> getSegmentationOperations() {
		if(!m_inst)
			m_inst = new Settings();
		return m_inst->m_segmentationOperations;
	}

	// Set segmentation pipeline
	static void setSegmentationOperations(std::shared_ptr<SegmentationOperationList>& segOperations)
	{
		if(!m_inst)
			m_inst = new Settings();
		m_inst->m_segmentationOperations = segOperations;
	}

	/**
	 * Train SVM based on observations. SVM will not be trained if posObs or negObs is empty.  
	 */
	static void trainSVM(std::vector<double>& posObs, 
		std::vector<double>& negObs, 
		const double statsMean[], 
		const double statsStandardDeviation[],
		bool autoScale);

	 /**
	  * @return fastER SVM model or 0 if model has not been created.
	  */
	static const svm_model* getSvmModel()
	{
		 if(!m_inst)
			 m_inst = new Settings();
		 return m_inst->m_svmModel;
	}

	/**
	 * Returns if a properly trained SVM exists (i.e. if the SVM returned by getSvmModel() can be used).
	 */
	static bool svmReady() 
	{
		return m_inst && m_inst->m_svmModel && m_inst->m_svmModelReady;
	}

	/**
	 * Get reference to constrained positive samples for given PictureIndex - creates element for PictureIndex
	 * if required.
	 */
	static std::unordered_set<faster::RegionID, faster::RegionIDHasher>& getConstrainedRegionsPositive(const PictureIndex& pi);

	/**
	 *	Clear constrained regions.
	 */
	static void clearConstrainedRegions()
	{
		if(m_inst)
			m_inst->m_constrainedPositiveSamples.clear();
	}

	/**
	 * Apply denoising parameters.
	 */
	static void applyLearnedDenoisingParameters(SegmentationOperationBilateralFilter* op = nullptr);

	 /**
	  * Set fastER SVM parameters. Note: will only become effective after calling applyFastERParameters().
	  */
	static void setFastERParameters(bool darkOnBright, int minSize, int maxSize, double minProbability, bool disableErSplitting)
	{
		 if(!m_inst)
			 m_inst = new Settings();
		 m_inst->m_fasterDob = darkOnBright;
		 m_inst->m_fasterMinSize = minSize;
		 m_inst->m_fasterMaxSize = maxSize;
		 m_inst->m_fasterMinProb = minProbability;
		 m_inst->m_disableErSplitting = disableErSplitting;
	}

	 /**
	  * Apply fastER SVM parameters.
	  * @param op segmentation operation to update. If NULL, segmentation operations in segmentation pipeline will be updated.
	  */
	static void applyFastERParameters(SegmentationOperationFastER* op = nullptr);

	/**
	 * Get fastER svm parameters.
	 */
	static void getFastERParameters(bool& darkOnBright, int& minSize, int& maxSize, double& minProbability, bool& disableErSplitting)
	{
		if(!m_inst)
			m_inst = new Settings();
		darkOnBright = m_inst->m_fasterDob;
		minSize = m_inst->m_fasterMinSize;
		maxSize = m_inst->m_fasterMaxSize;
		minProbability = m_inst->m_fasterMinProb;
		disableErSplitting = m_inst->m_disableErSplitting;
	}

	/**
	 * Get fastER normalization parameters.
	 */
	static const double* getSvmStatsMean()
	{
		if(!m_inst)
			m_inst = new Settings();
		return m_inst->m_statsMean;
	}
	static const double* getSvmStatsStandardDeviation()
	{
		if(!m_inst)
			m_inst = new Settings();
		return m_inst->m_statsStandardDeviation;
	}

	/**
	 * Keys for settings accessible with set() and get().
	 */
	static const char* KEY_VERBOSE;
	static const char* KEY_TVFACTOR;
	static const char* KEY_OCULARFACTOR;
	static const char* KEY_SELECTEDPOSITIONS;
	static const char* KEY_STARTFRAME;
	static const char* KEY_STOPFRAME;
	static const char* KEY_SCANBINFACTOR;
	static const char* KEY_TRACKINGWL;
	static const char* KEY_TRACKINGZINDEX;
	static const char* KEY_TRACKINGAREA;
	static const char* KEY_MEDIANPREPROCESSKERNELSIZE;
	static const char* KEY_ACTIVECONTOURCHANNEL;
	static const char* KEY_DENOISING_KERNELSIZE;
	static const char* KEY_DENOISING_SIGMACOLOR;
	static const char* KEY_DENOISING_SIGMASPACE;
	static const char* KEY_DENOISING_SIGMACOLORFACTOR;
	static const char* KEY_CONSTRAIN_SEGM_POS_SAMPLES;

	/**
	 * Keys for use with QSettings (list is incomplete).
	 */
	static const char* KEY_CHANGELOGVERSION;
	static const char* KEY_SHOWHELPATSTART;

private:

	// Set default settings
	void addDefaultSettings();

	// Helper function to add default settings
	void addDefaultSetting(const QString& key, const QVariant& val, const QString& descr) {
		m_settings.insert(key, val);
		m_descriptions.insert(key, descr);
	}

	// Singleton instance
	static Settings* m_inst;

	// Settings values and descriptions
	QHash<QString, QVariant> m_settings;
	QHash<QString, QString> m_descriptions;

	// Segmentation pipeline
	std::shared_ptr<SegmentationOperationList> m_segmentationOperations;

	//// fastER statistics
	//std::vector<double> m_mu;
	//std::vector<double> m_minimum;
	//std::vector<double> m_maximum;
	//std::vector<double> m_sigma;

	// fastER SVM model
	svm_problem m_svmTrainingProblem;
	svm_model* m_svmModel;

	// If the fastER SVM model can be used for segmentation
	bool m_svmModelReady;

	// Constrained regions
	QHash<PictureIndex, std::unordered_set<faster::RegionID, faster::RegionIDHasher>> m_constrainedPositiveSamples;

	// Data used by SVM model
	svm_parameter m_svmParameters;
	std::vector<double> m_svmTargetValues;
	std::vector<svm_node> m_svmTrainingData;
	std::vector<svm_node*> m_svmNodesArray;

	// Normalization params
	double m_statsMean[faster::NUM_FEATURES];					
	double m_statsStandardDeviation[faster::NUM_FEATURES];	

	// Other fastER parameters
	int m_fasterMinSize;
	int m_fasterMaxSize;
	bool m_fasterDob;
	double m_fasterMinProb;
	bool m_disableErSplitting;
};

template <class T>
T Settings::get( const QString& key )
{
	if(!m_inst)
		m_inst = new Settings();
	assert(m_inst->m_settings.contains(key));
	return m_inst->m_settings.value(key, QVariant()).value<T>();
}

template <class T>
void Settings::set( const QString& key, const T& val )
{
	if(!m_inst)
		m_inst = new Settings();
	assert(m_inst->m_settings.contains(key));
	m_inst->m_settings.insert(key, val);
}


#endif // settings_h__
