/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "jobsetsegmentation.h"

// Project
#include "settings.h"
#include "experiment.h"
#include "logging.h"
#include "jobrunnersegmentation.h"
#include "jobsegmentation.h"
#include "exceptionbase.h"
#include "frmmainwindow.h"

// QT
#include <QMutexLocker>
#include <QFile>
#include <QImage>
#include <QDir>

// OpenCV
#include "opencv2/core/core.hpp"


JobSetSegmentation::JobSetSegmentation()
{
	// DEBUG
	gDbg << reinterpret_cast<unsigned long long>(this) << "->JobSetSegmentation()\n";

	// Init variables
	m_numJobsCompleted = 0;
	m_canceled = false;
	m_currentPositionIndex = 0;
	m_wl = Settings::get<int>(Settings::KEY_TRACKINGWL); 
	m_zIndex = Settings::get<int>(Settings::KEY_TRACKINGZINDEX); 
	m_descr = "Image segmentation";

	// Get selected positions
	QList<QVariant> selectedPositions = Settings::get<QList<QVariant> >(Settings::KEY_SELECTEDPOSITIONS);
	const auto& allPositions = g_FrmMainWindow->experiment()->getPositionNumbers();
	for(auto it = allPositions.begin(); it != allPositions.end(); ++it) {
		if(selectedPositions.isEmpty() || selectedPositions.contains(QVariant(*it)))
			m_positions.push_back(*it);
	}

	// Get selected time points
	m_startTp = std::max(1, Settings::get<int>(Settings::KEY_STARTFRAME));
	m_stopTp = Settings::get<int>(Settings::KEY_STOPFRAME);
	if(m_stopTp < 1)
		m_stopTp = g_FrmMainWindow->experiment()->getMaxPictureIndex().timePoint;
	m_currentTimePoint = m_startTp;
}

Job* JobSetSegmentation::popJob( JobRunner* jobRunner )
{
	// Check if image is left
	int jobIndex;
	PictureIndex pi;
	QImage img;
	int errCode = openCurrentImageAndGoToNextImage(pi, img, jobIndex);
	//// DEBUG
	//gDbg << reinterpret_cast<unsigned long long>(this) << "->openCurrentImageAndGoToNextImage(" << pi.toString() << ", " << "[img], " << jobIndex << ") returned " << errCode << "\n";
	if(errCode == 1)
		return 0;
	if(errCode != 0 || img.isNull()) {
		gLog << "Error: cannot open image file for segmentation: " << PictureIndex(m_currentPositionIndex, m_currentTimePoint, m_wl, m_zIndex).toString() << "\n";
		return 0;
	}

	// Get image data as cv::Mat (for historical reasons, and to make it release the memory afterwards)
	cv::Mat imageData(img.height(), img.width(), CV_8UC1);
	unsigned char* imageDataSource = img.bits();
	int bpl = img.bytesPerLine();
	for(int y = 0; y < img.height(); ++y)
		memcpy(imageData.data + y*img.width(), imageDataSource + y*bpl, img.width());

	// Create job
	JobRunnerSegmentation* jobRunnerSeg = qobject_cast<JobRunnerSegmentation*>(jobRunner);
	return new JobSegmentation(jobIndex, Settings::getSegmentationOperations(), imageData.data, imageData.cols, imageData.rows, jobRunnerSeg->getFastERDetector(), jobRunnerSeg->getImageProcessingTools(), pi);
}

int JobSetSegmentation::openCurrentImageAndGoToNextImage(PictureIndex& outPI, QImage& outImage, int& outNextJobId)
{
	// Lock mutex
	QMutexLocker lock(&m_currentImageMutex);

	// Check if canceled
	if(m_canceled)
		return 1;

	// Check if image is left that exists
	Experiment* exp = g_FrmMainWindow->experiment();
	while(m_currentPositionIndex < m_positions.size()) {
		while(m_currentTimePoint <= m_stopTp) {
			//QString curImagePath = m_positions[m_currentPositionIndex]->getFolder();
			//QString imageFileName = m_positions[m_currentPositionIndex]->getImageFileName(m_currentTimePoint, m_zIndex, m_wl);
			//curImagePath += imageFileName;
			//if(QFile::exists(curImagePath)) {
			PictureIndex pi(m_positions[m_currentPositionIndex], m_currentTimePoint, m_wl, m_zIndex);
			int errCode = g_FrmMainWindow->experiment()->getImage(pi, outImage);
			if(errCode == 2)
				return 2;	// Image found, but could not be opened
			if(!errCode && !outImage.isNull()) {
				//outPosName = m_positions[m_currentPositionIndex]->getBaseName();
				outNextJobId = m_numJobsCompleted;
				outPI = pi;
				++m_currentTimePoint;
				++m_numJobsCompleted;
				return 0;
			}

			++m_numJobsCompleted;
			++m_currentTimePoint;
		}

		m_currentTimePoint = m_startTp;
		++m_currentPositionIndex;
	}

	return 1;
}

void JobSetSegmentation::jobFinished( JobRunner* jobRunner, Job* job )
{
	// Get segmentation data
	JobSegmentation* jobSeg = static_cast<JobSegmentation*>(job);

	/*
	const cv::Mat& imageSegmented = jobSeg->getImageData();

	// Convert to QImage
	QImage img(imageSegmented.cols, imageSegmented.rows, QImage::Format_MonoLSB);
	img.fill(0);
	unsigned char* imData = img.bits();
	int bpl = img.bytesPerLine();
	for(int y = 0; y < img.height(); ++y) {
		for(int x = 0; x < img.width(); ++x) {
			if(imageSegmented.data[y * img.width() + x] == 255)
				imData[y*bpl + (x>>3)] |= 1 << (x % 8);  
		}
	}

	// Create output folder
	QString imgName = jobSeg->getImageName();
	QString posName = imgName.left(imgName.indexOf("_t"));
	QString outputFolder = g_FrmMainWindow->experiment()->getPath() + posName + "/segmentation/";
	if(!QDir(outputFolder).exists()) {
		QMutexLocker lock(&m_outFolderMutex);
		if(!QDir(outputFolder).exists()) {
			if(!QDir(outputFolder).mkpath(outputFolder))
				throw ExceptionBase(__FUNCTION__, QString("Cannot create output folder: %1").arg(outputFolder));
		}
	}

	// Save image data
	QString fileName = PositionDescriptor::convertImageNameToSegmentationName(jobSeg->getImageName());
	img.save(outputFolder + '/' + fileName);
	*/ 


	// Delete job
	delete job;

	// Call base implementation
	JobSet::jobFinished(jobRunner, 0);
}

JobSetSegmentation::~JobSetSegmentation()
{
	// DEBUG
	gDbg << reinterpret_cast<unsigned long long>(this) << "->~JobSetSegmentation()\n";
}
