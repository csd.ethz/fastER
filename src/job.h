/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef job_h__
#define job_h__

// Qt
#include <QObject>

class JobRunner;


/**
 * Base class for jobs that are part of tasks that can be parallelized.
 */
class Job : public QObject {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	Job(int id)
		: m_id(id)
	{}

	/**
	 * Run job. Blocking call until job is finished.
	 * @param callingJobRunner the thread in which this function is called - can be 0 if it is called in the main thread.
	 */
	virtual void run(JobRunner* callingJobRunner) = 0;

	/**
	 * Destructor.
	 */
	virtual ~Job() {}

	/**
	 * Get id.
	 */
	int getId() const 
	{
		return m_id;
	}

private:

	// Job id
	int m_id;

};


#endif // job_h__
