/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmprogress.h"

// Project
#include "jobrunnermaster.h"
#include "jobset.h"
#include "changecursorobject.h"

// Qt
#include <QMessageBox>


FrmProgress::~FrmProgress()
{
	cleanUp();
}

void FrmProgress::runJobs(const std::vector<JobRunnerMaster*>& masterJobRunners, const std::vector<JobSet*>& jobs)
{
	assert(masterJobRunners.size() == jobs.size());

	// Delete old stuff
	cleanUp();

	// Set new stuff
	m_masterJobRunners = masterJobRunners;
	m_jobs = jobs;

	// Signals/slots
	for(int i = 0; i < m_masterJobRunners.size(); ++i) {
		connect(m_masterJobRunners[i], SIGNAL(finished()), this, SLOT(taskFinished()));
		connect(m_masterJobRunners[i], SIGNAL(started()), this, SLOT(taskStarted()));
		connect(jobs[i], SIGNAL(jobFinishedSignal(int)), this, SLOT(jobFinished(int)));
	}

	// Start first task
	m_elapsedTimeAllTasks.start();
	m_cancel = false;
	m_ui.pbtCancel->setEnabled(true);
	if(m_masterJobRunners.size()) {
		// Start first job
		m_currentJobSetIndex = 0;
		m_masterJobRunners[m_currentJobSetIndex]->start();
	}
}

void FrmProgress::taskStarted()
{
	// Update label
	assert(m_currentJobSetIndex < m_jobs.size());
	if(m_jobs.size() > 1)
		m_ui.lblTask->setText(QString("Completing task %1 of %2: ").arg(m_currentJobSetIndex+1).arg(m_jobs.size()) + m_jobs[m_currentJobSetIndex]->getDescription());
	else
		m_ui.lblTask->setText(QString("Completing task: ") + m_jobs[m_currentJobSetIndex]->getDescription());

	// Reset progress bar and label
	m_numJobsTotal = m_jobs[m_currentJobSetIndex]->getNumJobsTotal();
	m_progressDigits = QString::number(m_numJobsTotal).length();
	m_ui.lblProgress->setText(QString("%1/%2").arg(0, m_progressDigits, 10, QChar('0')).arg(m_numJobsTotal));
	m_ui.progressBar->setMaximum(m_numJobsTotal);
	m_ui.progressBar->setValue(0);
	m_numJobsFinished = 0;

	// Start timer
	m_elapsedTime = QTime::currentTime();
	m_timer.start(1000);
}

void FrmProgress::taskFinished()
{
	// Wait for thread
	m_masterJobRunners[m_currentJobSetIndex]->wait();

	// Indicate that we are done
	m_ui.progressBar->setValue(m_ui.progressBar->maximum());
	m_ui.lblProgress->setText(QString("%1/%2").arg(m_numJobsTotal, m_progressDigits, 10, QChar('0')).arg(m_numJobsTotal));

	// Stop timer
	m_timer.stop();

	// Get elapsed time
	qint64 elapsedSec = m_elapsedTimeAllTasks.elapsed() / static_cast<qint64>(1000);
	QString elapsedString = elapsedSecondsToString(elapsedSec);
	gLog << "All tasks finished - elapsed time: " << elapsedString << '\n';


	// Check if no task left or canceled
	if(m_cancel || m_currentJobSetIndex >= m_masterJobRunners.size() - 1) {
		m_currentJobSetIndex = -1;
		m_ui.pbtCancel->setEnabled(false);

		if(!m_cancel) {
			m_ui.progressBar->setValue(m_ui.progressBar->maximum());
			QMessageBox::information(this, "Finished", "Segmentation completed successfully.\n\nElapsed time: " + elapsedString);
		}

		close();
		return;
	}

	// Start next task
	m_masterJobRunners[++m_currentJobSetIndex]->start();
}

void FrmProgress::taskCanceled()
{
	if(m_currentJobSetIndex == -1)
		return;

	// Request cancel and wait for thread
	ChangeCursorObject cc;
	m_cancel = true;
	m_jobs[m_currentJobSetIndex]->requestCancel();
	m_masterJobRunners[m_currentJobSetIndex]->wait();
}

void FrmProgress::cleanUp()
{
	// Delete jobs and job runners
	for(auto it = m_masterJobRunners.begin(); it != m_masterJobRunners.end(); ++it)
		delete *it;
	m_masterJobRunners.clear();
	for(auto it = m_jobs.begin(); it != m_jobs.end(); ++it)
		delete *it;
	m_jobs.clear();
}

void FrmProgress::jobFinished( int numFinished )
{
	m_ui.progressBar->setValue(numFinished);
	m_ui.lblProgress->setText(QString("%1/%2").arg(numFinished, m_progressDigits, 10, QChar('0')).arg(m_numJobsTotal));
	m_numJobsFinished = numFinished;
}

void FrmProgress::closeEvent( QCloseEvent* event )
{
	//if(!m_cancel) {
	//	// Cancel not requested yet
	//	if(QMessageBox::question(this, "Quit", "Do you want to abort all running operations?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Cancel) != QMessageBox::Yes)
	//		return;
	//	m_cancel = true;
	//	m_ui.pbtCancel->setEnabled(false);
	//	event->ignore();
	//}
	//else {
	//	// Cancel already requested
	//}

	// If nothing is going on, just close
	if(m_currentJobSetIndex == -1) {
		event->accept();
		m_timer.stop();
		return;
	}

	// Ask user for verification
	if(QMessageBox::question(this, "Quit", "Do you want to abort all running operations?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No) != QMessageBox::Yes) {
		event->ignore();
		return;
	}

	// Request cancel and wait for thread
	ChangeCursorObject cc;
	m_cancel = true;
	m_jobs[m_currentJobSetIndex]->requestCancel();
	m_masterJobRunners[m_currentJobSetIndex]->wait();
	m_timer.stop();
}

void FrmProgress::updateTimes()
{
	// Get elapsed time
	int elapsedSecs = m_elapsedTime.secsTo(QTime::currentTime());
	m_ui.lblElapsedTime->setText(elapsedSecondsToString(elapsedSecs));

	// Estimate remaining time
	if(elapsedSecs > 0 && m_numJobsFinished > 0) {
		float secsPerJob = static_cast<float>(elapsedSecs) / m_numJobsFinished;
		float secsRemaining = (m_numJobsTotal - m_numJobsFinished) * secsPerJob;
		m_ui.lblRemainingTime->setText(elapsedSecondsToString(secsRemaining + 0.5f));
	}
	else
		m_ui.lblRemainingTime->setText("");
}
