/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmselectexperiment.h"

// Project
#include "settings.h"
#include "frmimportexperimentdata.h"
#include "changecursorobject.h"
#include "experimenttat.h"

// Qt
#include <QSettings>
#include <QDir>
#include <QFileDialog>
#include <QCloseEvent>
#include <QDebug>
#include <QMessageBox>



FrmSelectExperiment::FrmSelectExperiment(QWidget* parent)
	: QWidget(parent)
{
	// Init variables
	 m_frmImportExperimentData = 0;

	// Init UI
	m_ui.setupUi(this);
	m_ui.lblError->setVisible(false);
	m_ui.clbCompleteExperiment->setEnabled(false);
	m_ui.clbSelectPos->setEnabled(false);
	m_ui.clbCompleteExperiment->setEnabled(false);
	connect(m_ui.pbtSelectExperiment, SIGNAL(clicked()), this, SLOT(changeExperimentClicked()));
	connect(m_ui.lieExperimentPath, SIGNAL(textEdited(QString)), this, SLOT(experimentSelectionEdited()));
	connect(&m_experimentSelectionEditTimer, SIGNAL(timeout()), this, SLOT(experimentSelectionEditingFinished()));
	connect(m_ui.clbCompleteExperiment, SIGNAL(clicked()), this, SLOT(openExperimentComplete()));
	connect(m_ui.clbSelectPos, SIGNAL(clicked()), this, SLOT(openExperimentAndSelectPositions()));
	connect(m_ui.clbImportExperiment, SIGNAL(clicked()), this, SLOT(openExperimentImport()));

	// Init experiment selection with last experiment
	QSettings settings;
	QString lastExperiment = settings.value("experiment", QString()).toString();
	if(!lastExperiment.isEmpty()) {
		m_ui.lieExperimentPath->setText(lastExperiment);
		checkExperiment();
	}
}

void FrmSelectExperiment::changeExperimentClicked()
{
	// Propose parent folder of currently selected experiment
	QString dir = QDir::fromNativeSeparators(m_ui.lieExperimentPath->text());
	while(!dir.isEmpty() && dir.at(dir.length()-1) == '/')
		dir = dir.left(dir.length()-1);
	dir = dir.left(dir.lastIndexOf('/'));

	// Check if dir is invalid
	if(!dir.isEmpty() && !QDir(dir).exists())
		dir = "";

	// Get folder
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select experiment folder", dir);
	if(newFolder.isEmpty())
		return;

	// Change lieInputFolder and save selection
	m_ui.lieExperimentPath->setText(newFolder);
	QSettings settings;
	settings.setValue("experiment", newFolder);

	// Check experiment
	checkExperiment();
}

bool FrmSelectExperiment::checkExperiment()
{
	QString errorMsg;
	bool tatExpXmlFound = false;

	// Check experiment folder
	QString path = m_ui.lieExperimentPath->text();
	path = QDir::fromNativeSeparators(path);
	if(!path.isEmpty()) {
		if(!QDir(path).exists()) {
			errorMsg = "Experiment folder is invalid.";
		}
		else {
			QString tatExpXmlName = QDir(path).dirName() + "_TATexp.xml";
			tatExpXmlFound = QDir(path).exists(tatExpXmlName);
		}
	}

	// Show error or enable buttons if there is no error
	m_ui.lblError->setText(errorMsg);
	m_ui.lblError->setVisible(!errorMsg.isEmpty());
	m_ui.clbCompleteExperiment->setEnabled(!path.isEmpty() && tatExpXmlFound && errorMsg.isEmpty());
	m_ui.clbSelectPos->setEnabled(!path.isEmpty() && tatExpXmlFound && errorMsg.isEmpty());
	m_ui.clbImportExperiment->setEnabled(!path.isEmpty() && !tatExpXmlFound && errorMsg.isEmpty());

	// Experiment ok if errorMsg is empty
	return !path.isEmpty() && errorMsg.isEmpty();
}

void FrmSelectExperiment::experimentSelectionEdited()
{
	// Check experiment after timeout
	m_experimentSelectionEditTimer.start(100);
}

void FrmSelectExperiment::experimentSelectionEditingFinished()
{
	// Stop timer
	m_experimentSelectionEditTimer.stop();

	// Check experiment
	checkExperiment();
}

void FrmSelectExperiment::closeEvent( QCloseEvent* event )
{
	event->accept();

	if(m_frmImportExperimentData)
		m_frmImportExperimentData->close();

	emit experimentSelectionClosed();
}

void FrmSelectExperiment::openExperimentAndSelectPositions()
{
	// Delegate
	openExperimentInternal(true);
}

void FrmSelectExperiment::openExperimentComplete()
{
	// Delegate
	openExperimentInternal(false);
}

void FrmSelectExperiment::openExperimentInternal(bool selectPositions)
{
	// Just to make sure, check experiment again
	if(!checkExperiment())
		return;

	// Save selection
	QSettings qtSettings;
	qtSettings.setValue("experiment", m_ui.lieExperimentPath->text());

	// Try to open experiment
	try {
		ChangeCursorObject cc;

		// Open experiment
		QSharedPointer<Experiment> experiment = QSharedPointer<Experiment>(new ExperimentTAT(m_ui.lieExperimentPath->text()));
		emit experimentSelected(experiment, selectPositions);
	}
	catch(ExceptionBase& e) {
		// Log error and show error message to user
		gErr << "Cannot open experiment: " << e.what() << " - file: " << e.where();
		QMessageBox::critical(this, "Error", QString("Cannot open experiment: ") + e.what());
	}
}

void FrmSelectExperiment::openExperimentImport()
{
	// Show import tool
	if(!m_frmImportExperimentData) {
		m_frmImportExperimentData = new FrmImportExperimentData();
		m_frmImportExperimentData->setWindowModality(Qt::ApplicationModal);
		m_frmImportExperimentData->setWindowFlags( (m_frmImportExperimentData->windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint) & (~Qt::WindowMinimizeButtonHint) & (~Qt::WindowMaximizeButtonHint));
		connect(m_frmImportExperimentData, SIGNAL(experimentImported(QSharedPointer<Experiment>, bool)), this, SIGNAL(experimentSelected(QSharedPointer<Experiment>, bool)));
	}
	m_frmImportExperimentData->show();
	m_frmImportExperimentData->setCurrentFolder(m_ui.lieExperimentPath->text());
}