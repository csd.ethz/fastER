/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationsavesegmentation.h"

// Project
#include "jobsegmentation.h"
#include "experiment.h"
#include "jobrunnermaster.h"
#include "exceptionbase.h"
#include "jobrunnersegmentation.h"
#include "frmmainwindow.h"

// QT
#include <QMutexLocker>
#include <QFile>
#include <QImage>
#include <QDir>

// OpenCV
#include "opencv2/core/core.hpp"



void SegmentationOperationSaveSegmentation::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	// Check type 
	if(*imageDataType != IDT_UInt8Binary && *imageDataType != IDT_UInt32LabelMap)
		throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));


	if(*imageDataType == IDT_UInt8Binary) {
		const unsigned char* imageSegmented = *imageData;
		int imageSegmentedBytesPerPixel = jobSegmentation->getImageDataBytesPerPixel();
		int imageSegmentedSizeX = sizeX;
		int imageSegmentedSizeY = sizeY;

		// Save as binary image using QImage
		QImage img(imageSegmentedSizeX, imageSegmentedSizeY, QImage::Format_MonoLSB);
		img.fill(0);
		unsigned char* imData = img.bits();
		int bpl = img.bytesPerLine();
		for(int y = 0; y < img.height(); ++y) {
			for(int x = 0; x < img.width(); ++x) {
				if(imageSegmentedBytesPerPixel == 4) {
					if(reinterpret_cast<const unsigned int*>(imageSegmented)[y * imageSegmentedSizeX + x] != 0)
						imData[y*bpl + (x>>3)] |= 1 << (x % 8); 
				}
				else {
					assert(imageSegmentedBytesPerPixel == 1);
					if(imageSegmented[y * imageSegmentedSizeX + x] != 0)
						imData[y*bpl + (x>>3)] |= 1 << (x % 8); 
				} 
			}
		}

		// Save 
		g_FrmMainWindow->experiment()->writeSegmentationData(jobSegmentation->getPictureIndex(), img, jobSegmentation->getSegmentationMaskIndex());
	}
	else {
		assert(*imageDataType == IDT_UInt32LabelMap);

		// Convert 32 bit to 16 bit label map
		const unsigned int* imageDataSrc = reinterpret_cast<unsigned int*>(*imageData);
		unsigned short* imageData16Bit = new unsigned short[sizeX * sizeY];
		std::unique_ptr<unsigned short[]> cleanUp(imageData16Bit);	// To delete memory
		for(int y = 0; y < sizeY; ++y) {
			for(int x = 0; x < sizeX; ++x) {
				imageData16Bit[y*sizeX + x] = imageDataSrc[y*sizeX + x];
			}
		}

		// Save as 16 bit png image
		g_FrmMainWindow->experiment()->writeSegmentationData(jobSegmentation->getPictureIndex(), imageData16Bit, sizeX, sizeY, jobSegmentation->getSegmentationMaskIndex());
	}

	/*
	// Create output folder
	QString imgName = jobSegmentation->getImageName();
	QString posName = imgName.left(imgName.indexOf("_t"));
	QString outputFolder = g_FrmMainWindow->experiment()->getPath() + posName + "/segmentation/";
	if(!QDir(outputFolder).exists()) {
		// If there are more threads, this must be synchronized
		QMutex* mutex = 0;
		if(callingJobRunner && callingJobRunner->getMasterJobRunner())
			mutex = callingJobRunner->getMasterJobRunner()->getMutexPtrByName("SegmentationOperationSaveSegmentation");
		if(mutex)
			mutex->lock();

		// Check again, to make sure no other thread created the directory in the mean time
		if(!QDir(outputFolder).exists()) {
			if(!QDir(outputFolder).mkpath(outputFolder)) {
				if(mutex)
					mutex->unlock();
				throw ExceptionBase(__FUNCTION__, QString("Cannot create output folder: %1").arg(outputFolder));
			}
		}

		if(mutex)
			mutex->unlock();
	}

	// Save image data
	QString fileName = PositionDescriptor::convertImageNameToSegmentationName(jobSegmentation->getImageName());
	img.save(outputFolder + '/' + fileName);
	*/
}
