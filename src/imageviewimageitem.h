/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef imageviewimageitem_h__
#define imageviewimageitem_h__

// STL
#include <vector>

// Qt
#include <QGraphicsItem>
#include <QCursor>
#include <QGraphicsPixmapItem>

// Project
#include "imagedisplaysettings.h"
#include "blob.h"
#include "imageprocessingtools.h"

// OpenCV
#include "opencv2/core/core.hpp"


class ImageView;


class ImageViewImageItem : public QGraphicsItem {

public:

	/**
	 * Constructor.
	 */
	ImageViewImageItem(ImageView* parentView, QGraphicsItem* parent = 0) 
		: QGraphicsItem(parent)
	{
		m_parentImageView = parentView;
		m_painting = false;
		m_brushDiameter = 10;
		m_brushColor =  ImageViewImageItem::COLOR_FOREGROUND;
		m_brushCursor = 0;
		m_hideMarks = false;

		// Hover events to move cursor
		setAcceptHoverEvents(true);

		// Focusable to receive key events
		setFlag(QGraphicsItem::ItemIsFocusable);
	}

	~ImageViewImageItem();

	/**
	 * Set image.
	 */
	void setImage(const QImage& image, bool keepMarkings = false);

	/**
	 * Add an additional overlay image.
	 */
	void setOverlayImage(const QImage& image)
	{
		// Change overlay image and redraw
		m_overlayImage = image;
		update();
	}

	/**
	 * Get image.
	 */
	QImage& getImage()
	{
		return m_imageOriginal;
	}

	/**
	 * Apply image display settings. Call after changing settings.
	 */
	void applyImageDisplaySettings()
	{
		if(m_imageOriginal.isNull())
			return;
		m_imageDisplaySettings.applyDisplaySettings(m_imageOriginal);
		update();
	}

	/**
	 * Access image display settings. Call applyImageDisplaySettings() for changes to take effect.
	 */
	ImageDisplaySettings& getImageDisplaySettings() {
		return m_imageDisplaySettings;
	}

	/**
	 * Reimplemented.
	 */
	QRectF boundingRect() const 
	{
		if(m_imageOriginal.isNull())
			return QRectF();
		else
			return QRectF(QPoint(0, 0), m_imageOriginal.size());
	}

	/**
	 * Access labels created by user.
	 */
	const std::vector<Blob>& getLabels() const {
		return m_paintedBlobs;
	}

	/**
	 * Access background labels created by user.
	 */
	const std::vector<cv::Point2i> getBackgroundLabels() const {
		return m_paintedBackground;
	}

	/**
	 * Add cell labels.
	 */
	void addLabels(const std::vector<Blob>& labels);

	/**
	 *	Remove all labels.
	 */
	void clearLabels();

	/**
	 * Add background labels.
	 */
	void addBackgroundLabels(const std::vector<cv::Point2i>& labels);

	/**
	 * Hide markings.
	 */
	void toggleHideMarkings() {
		m_hideMarks = !m_hideMarks;
		update();
	}

protected:

	/**
	 * Reimplemented.
	 */
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	/**
	 * Reimplemented.
	 */
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent* _event);
	void wheelEvent(QGraphicsSceneWheelEvent* event);

	/**
	 * Reimplemented.
	 */
	void hoverEnterEvent( QGraphicsSceneHoverEvent* event );
	void hoverLeaveEvent( QGraphicsSceneHoverEvent* event );
	void hoverMoveEvent( QGraphicsSceneHoverEvent* event );

	/**
	 * Reimplemented to adjust color of painting brush cursor.
	 */
	void keyPressEvent (QKeyEvent* event);
	void keyReleaseEvent (QKeyEvent* event);

	/**
	 * Display brush cursor from other ImageView that is paintable.
	 */
	void displayBrushCursor(int posX, int posY, QGraphicsPixmapItem* brush);

	// Colors
	static const QColor COLOR_FOREGROUND;
	static const QColor COLOR_BACKGROUND;

	// Transparency of markings (0 = transparent, 255 = opaque)
	static const int MARKINGS_TRANSPARENCY = 127/*80*/;

private:

	/**
	 * Process markings painted on image (fill holes, extract blobs).
	 */
	void processMarkings();

	/**
	 * Update cursor (size, color and position).
	 */
	void updateCursor();

	/**
	 * Update cursor color after key event. 
	 */
	void updateCursorColorAfterKeyEvent(QKeyEvent* event);

	/**
	 * Update cursor position.
	 */
	void updateCursorPosition(QPoint scenePos);

	/**
	 * Drawing helper functions.
	 */
	static void drawCircle(QImage& img, QPoint pos, int diameter, QColor color);
	static void drawLine(QImage& img, QPoint posStart, QPoint posEnd, int width, QColor color);
	static void eraseCircle(QImage& img, QPoint pos, int diameter);
	static void eraseLine(QImage& img, QPoint posStart, QPoint posEnd, int width);

	/**
	 * Create painting cursor depending on brush size, zoom...
	 */
	static QPixmap createBrushCursor (int diameter, QColor color);

	// Parent ImageView
	ImageView* m_parentImageView;

	// Original image
	QImage m_imageOriginal;

	// Display settings for original image
	ImageDisplaySettings m_imageDisplaySettings;

	// Image used for painting foreground and background labels
	QImage m_imagePaintingForeground;
	QImage m_imagePaintingBackground;

	// Arbitrary additional overlay image
	QImage m_overlayImage;

	// Brush cursor item and cursor scene position
	QGraphicsPixmapItem* m_brushCursor;
	QPoint m_cursorScenePosition;

	// If user is currently painting on this image
	bool m_painting;

	// Blobs painted on the image
	std::vector<Blob> m_paintedBlobs;

	// Background pixels marked
	std::vector<cv::Point2i> m_paintedBackground;

	// Brush diameter in scene units (=image pixels), brush color
	int m_brushDiameter;	// Brush diameter, always an even number or 0 indicating brush size 1
	QColor m_brushColor;

	// Converter for painted blobs
	ImageProcessingTools m_segConverter;

	// If marks should be hidden
	bool m_hideMarks;

	friend class ImageView;
};


#endif // imageviewimageitem_h__
