/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmmainwindow.h"

// Qt
#include <QMessageBox>
#include <QCloseEvent>
#include <QDockWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QDataStream>
#include <QVector>
#include <QSignalMapper>
#include <QDebug>
#include <QDesktopServices>
#include <QUrl>
#include <QSettings>

// Project
#include "frmcolortransformation.h"
#include "imageviewimageitem.h"
#include "frmmainwindow.h"
#include "frmsegmentationpreview.h"
#include "fastdirectorylisting.h"
#include "settings.h"
#include "tools.h"
#include "jobsegmentation.h"
#include "jobsetsegmentation.h"
#include "jobrunnersegmentation.h"
#include "jobrunnermaster.h"
#include "blob.h"
#include "changecursorobject.h"
#include "frmselectexperiment.h"
#include "frmselectpositions.h"
#include "exceptionbase.h"
#include "experiment.h"
#include "settings.h"
#include "changecursorobject.h"
#include "logging.h"
#include "frmsegmentationpreview.h"
#include "frmoriginalimageview.h"
#include "frmconsole.h"
#include "frmimageviewsettings.h"
#include "frmstarttasks.h"
#include "frmbasicsegmentationsettings.h"
#include "frmprogress.h"
#include "positioncluster.h"
#include "segmentationoperationlist.h"
#include "experimenttat.h"
#include "segmentationoperationcountcells.h"
#include "segmentationoperationbilateralfilter.h"
#include "segmentationoperationsizefilter.h"
#include "segmentationoperationfillholes.h"
#include "segmentationoperationsizefilter.h"
#include "segmentationoperationfaster.h"
#include "segmentationoperationcountcells.h"
#include "segmentationoperationsavesegmentation.h"
#include "frmabout.h"
#include "frmhelp.h"

FrmMainWindow* g_FrmMainWindow = 0;

FrmMainWindow::FrmMainWindow( QWidget* parent /*= 0*/ )
	: QMainWindow(parent)
{
	// Set global pointer (must be done first)
	g_FrmMainWindow = this;

	// Init ui
	m_ui.setupUi(this);

	// Currently disabled for normal users: 
#ifndef _DEBUG
	//m_ui.actionEdit_Segmentation_Pipeline->setEnabled(false);
	//m_ui.actionExport_Foreground_Labels_Mask->setEnabled(false);
	//m_ui.actionTest->setEnabled(false);
#endif

	// Init variables
	m_frmSelectPositions = 0;
	m_frmProgress = 0;
	m_frmVersionInfo = 0;
	m_ignoreImageTimepointChanged = false;
	m_ignorePositionChanged = false;
	m_segmentationPreviewChannel = -1;
	m_frmTreeVisualization3D = 0;
	m_automaticallyDeterminedDob = 0;
	m_enforceUpdateOfSamplesInAllImages = false;
	m_frmQuickHelp = 0;

	// Init menu
	connect(m_ui.actionSelect_Experiment, SIGNAL(triggered()), this, SLOT(showExperimentSelectionForm()));
	connect(m_ui.actionSelect_Positions, SIGNAL(triggered()), this, SLOT(showPositionSelectionForm()));
	connect(m_ui.actionExit, SIGNAL(triggered()), this, SLOT(close()));
	connect(m_ui.actionExport_Current_Labels, SIGNAL(triggered()), this, SLOT(exportLabels()));
	connect(m_ui.actionImport_Labels, SIGNAL(triggered()), this, SLOT(importLabels()));
	connect(m_ui.actionExport_Foreground_Labels_Mask, SIGNAL(triggered()), this, SLOT(exportFGLabelsOfCurrentImage()));
	//connect(m_ui.actionChangelog, SIGNAL(triggered()), this, SLOT(showVersionInfo()));
	//connect(m_ui.actionOpen_Help, SIGNAL(triggered()), this, SLOT(showHelp()));
	connect(m_ui.actionExport_cell_counts_to_csv_during_segmentation, SIGNAL(triggered()), this, SLOT(countCells()));
	connect(m_ui.actionImport_Training_Labels_From_Image, SIGNAL(triggered()), this, SLOT(addLabelsFromBinaryImage()));
	connect(m_ui.actionAbout_CellAutoTracker, SIGNAL(triggered()), this, SLOT(showAboutWindow()));
	connect(m_ui.action_QuickHelp, SIGNAL(triggered()), this, SLOT(showQuickInstructions()));

	// Create and show modal experiment selection window
	m_frmSelectExperiment = new FrmSelectExperiment(0);
	connect(m_frmSelectExperiment, SIGNAL(experimentSelected(QSharedPointer<Experiment>, bool)), this, SLOT(experimentSelected(QSharedPointer<Experiment>, bool)));
	connect(m_frmSelectExperiment, SIGNAL(experimentSelectionClosed()), this, SLOT(experimentSelectionClosed()));
	m_frmSelectExperiment->setWindowModality(Qt::ApplicationModal);
	m_frmSelectExperiment->setWindowFlags( (m_frmSelectExperiment->windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint) & (~Qt::WindowMinimizeButtonHint) & (~Qt::WindowMaximizeButtonHint));
	m_frmSelectExperiment->show();

	// Create dummy central widget
	QWidget* centralWidget = new QWidget();
	setCentralWidget(centralWidget);
	centralWidget->hide();

	// Add color transformation dock widget
	m_frmColorTransformation = new FrmColorTransformation();
	connect(m_frmColorTransformation, SIGNAL(settingsChanged()), this, SLOT(displaySettingsChanged()));
	QDockWidget* dockColorTransformations = new QDockWidget("Enhance Image Contrast", this);
	dockColorTransformations->setWidget(m_frmColorTransformation);
	addDockWidget(Qt::LeftDockWidgetArea, dockColorTransformations);
	m_ui.menuView->addAction(dockColorTransformations->toggleViewAction());

	// Add basic segmentation settings dock widget
	m_frmBasicSegmentationSettings = new FrmBasicSegmentationSettings();
	m_frmBasicSegmentationSettings->setAutoFillBackground(true);
	QDockWidget* dockBasicSegSettings = new QDockWidget("Basic Segmentation Settings", this);
	dockBasicSegSettings->setWidget(m_frmBasicSegmentationSettings);
	addDockWidget(Qt::LeftDockWidgetArea, dockBasicSegSettings);
	m_ui.menuView->addAction(dockBasicSegSettings->toggleViewAction());

	// Add image view settings dock widget
	m_frmImageViewSettings = new FrmImageViewSettings();
	m_frmImageViewSettings->setAutoFillBackground(true);
	QDockWidget* dockImageViewSettings = new QDockWidget("Show Training Samples", this);
	dockImageViewSettings->setWidget(m_frmImageViewSettings);
	addDockWidget(Qt::LeftDockWidgetArea, dockImageViewSettings);
	connect(m_frmImageViewSettings->m_ui.bgrDisplayOfFoundRegions, SIGNAL(buttonReleased(int)), this, SLOT(updateDisplayOfFoundExtremalRegions()));
	m_ui.menuView->addAction(dockImageViewSettings->toggleViewAction());

	// Add start tasks dock widget
	m_frmStartTasks = new FrmStartTasks();
	m_frmStartTasks->setAutoFillBackground(true);
	QDockWidget* dockStartTasks = new QDockWidget("Batch Processing", this);
	dockStartTasks->setWidget(m_frmStartTasks);
	addDockWidget(Qt::LeftDockWidgetArea, dockStartTasks);
	connect(m_frmStartTasks->m_ui.pbtSelectPositions, SIGNAL(clicked()), this, SLOT(showPositionSelectionForm()));
	connect(m_frmStartTasks->m_ui.pbtSetStartTpToFirst, SIGNAL(clicked()), this, SLOT(setStartTpToFirst()));
	connect(m_frmStartTasks->m_ui.pbtSetStopTpToLast, SIGNAL(clicked()), this, SLOT(setStopTpToLast()));
	connect(m_frmStartTasks->m_ui.clbStartTracking, SIGNAL(clicked()), this, SLOT(startTasks()));
	m_ui.menuView->addAction(dockStartTasks->toggleViewAction());

	// Add console dock widget
	m_frmConsole = FrmConsole::createInst(0);
	m_frmConsole->setAutoFillBackground(true);
	QDockWidget* dockConsole = new QDockWidget("Status Window", this);
	dockConsole->setWidget(m_frmConsole);
	addDockWidget(Qt::LeftDockWidgetArea, dockConsole);
	m_ui.menuView->addAction(dockConsole->toggleViewAction());
	dockConsole->hide();

	// Add original image view dock widget
	m_frmOriginalImageView = new FrmOriginalImageView();
	m_frmOriginalImageView->setAutoFillBackground(true);
	m_dockOriginalImage = new QDockWidget("Train Segmentation Algorithm - 0 cell and 0 background candidate regions marked", this);
	m_dockOriginalImage->setWidget(m_frmOriginalImageView);
	addDockWidget(Qt::RightDockWidgetArea, m_dockOriginalImage);
	m_dockOriginalImage->setFeatures(QDockWidget::NoDockWidgetFeatures);
	connect(m_frmOriginalImageView->m_ui.imageView, SIGNAL(zoomChanged(float, QPointF)), this, SLOT(zoomChanged(float)));
	connect(m_frmOriginalImageView->m_ui.imageView, SIGNAL(markingFinished()), this, SLOT(markingFinished()));
	connect(m_frmOriginalImageView->m_ui.pbtProposePreviewPic, SIGNAL(clicked()), this, SLOT(randomPreviewPic()));
	connect(m_frmOriginalImageView->m_ui.pbtPreviewNext, SIGNAL(clicked()), this, SLOT(previewNextTimePoint()));
	connect(m_frmOriginalImageView->m_ui.pbtPreviewPrevious, SIGNAL(clicked()), this, SLOT(previewPrevTimePoint()));
	connect(m_frmOriginalImageView->m_ui.spbSegmentationChannel, SIGNAL(valueChanged(int)), this, SLOT(channelSegmentationChanged(int)));
	connect(m_frmOriginalImageView->m_ui.spbPreviewChannel, SIGNAL(valueChanged(int)), this, SLOT(channelPreviewChanged(int)));
	QSignalMapper* mapper = new QSignalMapper(this);
	connect(m_frmOriginalImageView->m_ui.pbtMinus100, SIGNAL(clicked()), mapper, SLOT(map()));
	mapper->setMapping(m_frmOriginalImageView->m_ui.pbtMinus100, -100);
	connect(m_frmOriginalImageView->m_ui.pbtMinus10, SIGNAL(clicked()), mapper, SLOT(map()));
	mapper->setMapping(m_frmOriginalImageView->m_ui.pbtMinus10, -10);
	connect(m_frmOriginalImageView->m_ui.pbtPlus10, SIGNAL(clicked()), mapper, SLOT(map()));
	mapper->setMapping(m_frmOriginalImageView->m_ui.pbtPlus10, 10);
	connect(m_frmOriginalImageView->m_ui.pbtPlus100, SIGNAL(clicked()), mapper, SLOT(map()));
	mapper->setMapping(m_frmOriginalImageView->m_ui.pbtPlus100, 100);
	connect(m_frmOriginalImageView->m_ui.pbtPlus250, SIGNAL(clicked()), mapper, SLOT(map()));
	mapper->setMapping(m_frmOriginalImageView->m_ui.pbtPlus250, 250);
	connect(mapper, SIGNAL(mapped(int)), this, SLOT(previewChangeTimePoint(int)));
	m_frmOriginalImageView->m_ui.imageView->setPaintable(true);

	// Add segmentation preview dock widget
	m_frmSegPreview = new FrmSegmentationPreview();
	m_frmSegPreview->setAutoFillBackground(true);
	m_dockSegPreview = new QDockWidget("Segmentation Results Preview", this);
	m_dockSegPreview->setWidget(m_frmSegPreview);
	addDockWidget(Qt::RightDockWidgetArea, m_dockSegPreview);
	m_ui.menuView->addAction(m_dockSegPreview->toggleViewAction());
	connect(m_frmOriginalImageView->m_ui.imageView, SIGNAL(scrollingChanged(QPointF)), m_frmSegPreview->getImageView(), SLOT(setScrollPosition(QPointF)));
	connect(m_frmOriginalImageView->m_ui.imageView, SIGNAL(zoomChanged(float, QPointF)), m_frmSegPreview->getImageView(), SLOT(setZoom(float, QPointF)));
	connect(m_frmSegPreview->getImageView(), SIGNAL(scrollingChanged(QPointF)), m_frmOriginalImageView->m_ui.imageView, SLOT(setScrollPosition(QPointF)));
	connect(m_frmSegPreview->getImageView(), SIGNAL(zoomChanged(float, QPointF)), m_frmOriginalImageView->m_ui.imageView, SLOT(setZoom(float, QPointF)));
	connect(m_frmOriginalImageView->m_ui.imageView, SIGNAL(brushPositionChanged(int, int, QGraphicsPixmapItem*)), m_frmSegPreview->getImageView(), SLOT(displayBrushCursor(int, int, QGraphicsPixmapItem*)));
	connect(m_frmSegPreview->getImageView(), SIGNAL(mousePositionChanged(int, int, int)), m_frmSegPreview, SLOT(showPixelValue(int, int, int)));
	connect(m_frmOriginalImageView->m_ui.spbPreviewTimePoint, SIGNAL(valueChanged(int)), this, SLOT(imageTimepointChanged(int)));
	connect(m_frmOriginalImageView->m_ui.spbPosition, SIGNAL(valueChanged(int)), this, SLOT(imagePositionChanged(int)));
	connect(m_dockSegPreview, SIGNAL(topLevelChanged(bool)), this, SLOT(dockSegPreviewTopLevelChanged(bool)));

	// Title
	updateWindowTitle();

	// Create default segmentation pipeline
	//m_frmSegmentationPipeline->loadFastERPipeline();
	loadFastERPipeline();
}

FrmMainWindow::~FrmMainWindow()
{
	// Delete forms
	delete m_frmSelectExperiment;
	delete m_frmSelectPositions;
	delete m_frmProgress;
	delete m_frmQuickHelp;
}

void FrmMainWindow::zoomChanged( float scaling )
{
	// Update zoom label
	m_frmOriginalImageView->m_ui.lblZoom->setText(QString("%1%").arg(static_cast<int>(scaling*100+0.5f)));
}

void FrmMainWindow::experimentSelected(QSharedPointer<Experiment> experiment, bool selectPositions)
{
	try {
		// Change experiment
		m_experiment = experiment;

		// Experiment changed successfully
		emit currentExperimentChanged();

		// Clear labels and corresponding samples
		clearLabels(true);

		// Clear other experiment specific stuff
		m_automaticallyDeterminedDob = 0;
		Settings::set(Settings::KEY_TRACKINGAREA, QList<QVariant>());
		Settings::clearConstrainedRegions();
		setImage(PictureIndex());

		// Clear segmentation
		applySegmentationSettingsAndUpdatePreview();

		// Update title
		updateWindowTitle();

		// Close experiment selection
		m_frmSelectExperiment->close();

		// Set temporal range
		setStartTpToFirst();
		setStopTpToLast();

		// Open position selection form if required
		if(selectPositions)
			showPositionSelectionForm();
		else {
			// Select all positions by default
			const QList<int>& positions = m_experiment->getPositionNumbers();
			QList<QVariant> selectedPositions;
			for(auto it = positions.begin(); it != positions.end(); ++it)
				selectedPositions.append(*it);
			Settings::set(Settings::KEY_SELECTEDPOSITIONS, QVariant(selectedPositions));
			updateDisplayOfSelectedPositions();

			// Display random image
			randomPreviewPic();
		}

		//// Debug
		//QDateTime tmp;
		//if(m_experiment)
		//	m_experiment->getImageAcquisitionTime(PictureIndex(1, 1, 0, 1), tmp);
	}
	catch(ExceptionBase& e) {
		// Update title
		updateWindowTitle();

		// Log error and show error message to user
		gErr << "Cannot open experiment: " << e.what() << " - file: " << e.where();
		QMessageBox::critical(m_frmSelectExperiment, "Error", QString("Cannot open experiment: ") + e.what());
	}
}

void FrmMainWindow::randomPreviewPic()
{

	// Randomly select an image with the desired wavelength from a random position
	int currentChannel = m_frmOriginalImageView->m_ui.spbPreviewChannel->value();
	PictureIndex pi = m_experiment->getRandomImage(true);
	if(!pi.isValid())
		return;

	// Load image
	setImage(pi);

	// Update selected channel if required
	if(pi.channel != currentChannel) {
		m_frmOriginalImageView->m_ui.spbSegmentationChannel->setValue(pi.channel);
	}
}

//bool FrmMainWindow::openExperiment(const QString& experimentPath)
//{
//	try {
//		ChangeCursorObject cc;
//
//		// Open experiment
//		m_experiment = new ExperimentTAT(experimentPath);
//
//		// Experiment changed successfully
//		emit currentExperimentChanged();
//
//		// Clear markings and other experiment specific stuff
//		m_labelsForeground.clear();
//		m_labelsBackground.clear();
//		findExtremalRegionsForCurrentLabels();
//		Settings::set(Settings::KEY_TRACKINGAREA, QList<QVariant>());
//		setImage(PictureIndex());
//
//		// Update title
//		updateWindowTitle();
//	}
//	catch(ExceptionBase& e) {
//		// Update title
//		updateWindowTitle();
//
//		// Log error and show error message to user
//		gErr << "Cannot open experiment: " << e.what() << " - file: " << e.where();
//		QMessageBox::critical(m_frmSelectExperiment, "Error", QString("Cannot open experiment: ") + e.what());
//
//		return false;
//	}
//
//	return true;
//}

void FrmMainWindow::closeEvent( QCloseEvent* event )
{
	if(m_frmQuickHelp)
		m_frmQuickHelp->close();

	event->accept();
}

void FrmMainWindow::startTracking()
{
	//// Show tracking subview and start tracking
	//showTrackingSubview(m_frmLearnSegmentationSettings);
	//m_frmTracking->startTracking();
}

void FrmMainWindow::showExperimentSelectionForm()
{
	if(!m_frmSelectExperiment) {
		return;
	}

	m_frmSelectExperiment->show();
	m_frmSelectExperiment->raise();
}

void FrmMainWindow::experimentSelectionClosed()
{
	// Show quick instructions, if form has not been shown yet
	if(!m_frmQuickHelp) {
		// Show version info if necessary
		QSettings settings;
		if(settings.value(Settings::KEY_SHOWHELPATSTART, true).toBool())
			showQuickInstructions();
	}

	// If no experiment is set, user has canceled initial selection of experiment and probably wants to close the program
	if(!g_FrmMainWindow->experiment())
		close();
}

void FrmMainWindow::showPositionSelectionForm()
{
	// Create window
	if(!m_frmSelectPositions) {
		m_frmSelectPositions = new FrmSelectPositions();
		connect(this, SIGNAL(currentExperimentChanged()), m_frmSelectPositions, SLOT(experimentChanged()));
		m_frmSelectPositions->setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);
		m_frmSelectPositions->setWindowModality(Qt::ApplicationModal);
	}

	// Show window
	m_frmSelectPositions->show();
	m_frmSelectPositions->raise();
}

void FrmMainWindow::updateWindowTitle()
{
	QString title = "fastER";

	// Add version
	QString version = QString("v%1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR_1).arg(VERSION_MINOR_2);
	bool is32bit = sizeof(int*) == 4;
	if(is32bit)
		version += " - 32bit";
	else
		version += " - 64bit";
	title += " - " + version;

	// Add experiment
	if(g_FrmMainWindow->experiment()) {
		QString expName = g_FrmMainWindow->experiment()->getPath();
		if(!expName.isEmpty())
			title += " - " + expName;
	}

	setWindowTitle(title);
}

void FrmMainWindow::displaySettingsChanged()
{
	// Apply settings
	m_frmColorTransformation->applySettings(m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->getImageDisplaySettings());
	m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->applyImageDisplaySettings();
	m_frmColorTransformation->applySettings(m_frmSegPreview->m_ui.imageView->getImageViewItem()->getImageDisplaySettings());
	m_frmSegPreview->m_ui.imageView->getImageViewItem()->applyImageDisplaySettings();
}

void FrmMainWindow::markingFinished()
{

	// Wait cursor
	ChangeCursorObject cc;

	// Get markings
	const std::vector<Blob>& blobs = m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->getLabels();
	const std::vector<cv::Point2i>& markinsBackground = m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->getBackgroundLabels();
	//if(blobs.size() == 0 && markinsBackground.size() == 0) {
		// Remove old labels
		clearLabels(false);
	//}
	//else {
		// Add new labels
		if(blobs.size())
			m_labelsForeground.insert(m_currentPictureIndex, blobs);
		if(markinsBackground.size())
			m_labelsBackground.insert(m_currentPictureIndex, markinsBackground);
	//}

	// Update label statistics
	updateLabelStatistics();

	// Do not find ERs or anything, if desired
	if(!m_ui.actionAuto_update_ERs_after_labeling->isChecked())
		return;

	// Get samples from labels
	extractSamplesForLabels(false);

	// Train SVM and update segmentation
	if(m_frmOriginalImageView->autoRefreshSegmentation()) {
		learnParametersSvm();
		applySegmentationSettingsAndUpdatePreview();
	}

	// Update original image view window title
	QString caption = QString("Train Segmentation Algorithm - %1 cell and %2 background candidate regions marked").arg(getNumLabeledCellExtremalRegions()).arg(getNumLabeledBackgroundExtremalRegions());
	m_dockOriginalImage->setWindowTitle(caption);
}

void FrmMainWindow::setImage( const PictureIndex& pi )
{
	ChangeCursorObject cc;

	// Change current PictureIndex
	m_currentPictureIndex = pi;

	// Get image
	QImage img;
	int errCode = 0;
	if(m_currentPictureIndex.isValid())
		errCode = m_experiment->getImage(pi, img);

	// Clear image if image cannot be opened
	if(errCode || !m_currentPictureIndex.isValid()) {
		m_curImage = QImage();
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->setImage(QImage());
		//m_frmOriginalImageView->m_ui.liePreviewPic->setText("");
		if(m_frmSegPreview) 
			m_frmSegPreview->setImage(QImage(), PictureIndex());

		return;
	}

	// Image opened successfully, show it
	int newTp = pi.timePoint;
	if(newTp > 0 && m_frmOriginalImageView->m_ui.spbPreviewTimePoint->value() != newTp) {
		m_ignoreImageTimepointChanged = true;
		m_frmOriginalImageView->m_ui.spbPreviewTimePoint->setValue(newTp);
		m_ignoreImageTimepointChanged = false;
	}
	if(pi.positionNumber > 0 && m_frmOriginalImageView->m_ui.spbPosition->value() != newTp) {
		m_ignorePositionChanged = true;
		m_frmOriginalImageView->m_ui.spbPosition->setValue(pi.positionNumber);
		m_ignorePositionChanged = false;
	}
	m_curImage = img.convertToFormat(QImage::Format_Indexed8);
	//m_currentImagePath = QDir::fromNativeSeparators(QDir(path).canonicalPath());
	m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->setImage(m_curImage);
	//m_frmOriginalImageView->m_ui.liePreviewPic->setText(QDir::toNativeSeparators(path));
	//m_frmOriginalImageView->m_ui.liePreviewPic->setText(m_currentPictureIndex.toString());

	// Update preview
	if(m_frmSegPreview) {
		m_frmSegPreview->setImage(m_curImage, m_currentPictureIndex);
		applySegmentationSettingsAndUpdatePreview();
	}

	// Display markings if available
	if(m_labelsForeground.contains(m_currentPictureIndex))
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->addLabels(m_labelsForeground.value(m_currentPictureIndex));
	if(m_labelsBackground.contains(m_currentPictureIndex))
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->addBackgroundLabels(m_labelsBackground.value(m_currentPictureIndex));

	// Display identified extremal regions
	updateDisplayOfFoundExtremalRegions();
	//if(m_foundExtremalRegions.contains(m_currentImagePath))
	//	m_ui.imageView->getImageViewItem()->setOverlayImage(m_foundExtremalRegions[m_currentImagePath]);
}

void FrmMainWindow::imageTimepointChanged( int timepoint )
{
	if(m_ignoreImageTimepointChanged)
		return;

	// Get current
	if(!m_currentPictureIndex.isValid())
		return;

	// Select file
	if(timepoint != m_currentPictureIndex.timePoint) {
		PictureIndex newPi = m_currentPictureIndex;
		newPi.timePoint = timepoint;
		setImage(newPi);
	}
}


void FrmMainWindow::imagePositionChanged(int position)
{
	if(m_ignorePositionChanged)
		return;

	// Get current
	if(!m_currentPictureIndex.isValid())
		return;

	// Select file
	if(position != m_currentPictureIndex.positionNumber) {
		PictureIndex newPi = m_currentPictureIndex;
		newPi.positionNumber = position;
		setImage(newPi);
	}
}

void FrmMainWindow::previewNextTimePoint()
{
	// Get current
	if(!m_currentPictureIndex.isValid())
		return;

	PictureIndex pi = m_currentPictureIndex;
	int counter = 0;
	while(counter++ < 100) {
		// Go to next tp
		++pi.timePoint;

		// Check if file exists
		if(m_experiment->hasImage(pi)) {
			setImage(pi);
			return;
		}
	}
}

void FrmMainWindow::previewPrevTimePoint()
{
	// Get current
	if(!m_currentPictureIndex.isValid())
		return;

	PictureIndex pi = m_currentPictureIndex;
	int counter = 0;
	while(counter++ < 100 && pi.timePoint > 0) {
		// Go to previous tp
		--pi.timePoint;

		// Check if file exists
		if(m_experiment->hasImage(pi)) {
			setImage(pi);
			return;
		}
	}
}

//void FrmMainWindow::selectPreviewPic()
//{
//	// Look in experiment dir
//	QString dir = g_FrmMainWindow->experiment()->getPath();
//
//	// Get folder
//	QString imFile = QFileDialog::getOpenFileName(this, "Select image file", dir);
//	if(imFile.isEmpty())
//		return;
//
//	// Set pic
//	setImage(imFile);
//}



void FrmMainWindow::exportLabels()
{
	try {
		// Get filename
		QString proposedFile = g_FrmMainWindow->experiment()->getPath();
		QString fileName = QFileDialog::getSaveFileName(this, "Select file to save labels in", proposedFile, "labels files (*.lbl)");
		if(fileName.isEmpty())
			return;

		// Open file
		QFile outFile(fileName);
		if(!outFile.open(QIODevice::WriteOnly)) {
			QMessageBox::critical(this, "Error", QString("Could not open file: %1\n").arg(fileName));
			return;
		}

		// Write magic number and version
		QDataStream out(&outFile);
		out << (quint32)0xD4552433;
		out << (quint32)3;

		// Convert labels to QVector
		QHash<PictureIndex, QVector<Blob> > labelsForeGround;
		for(auto it = m_labelsForeground.begin(); it != m_labelsForeground.end(); ++it) 
			labelsForeGround.insert(it.key(), QVector<Blob>::fromStdVector(it.value()));

		QHash<PictureIndex, QVector<cv::Point2i> > labelsBackGround;
		for(auto it = m_labelsBackground.begin(); it != m_labelsBackground.end(); ++it) 
			labelsBackGround.insert(it.key(), QVector<cv::Point2i>::fromStdVector(it.value()));

		// Export labels
		out << labelsForeGround;
		out << labelsBackGround;
	}
	catch(ExceptionBase& e) {
		QMessageBox::critical(this, "Error", QString("Operation failed: %1\n\nat: %2").arg(e.what()).arg(e.where()));
	}
}

void FrmMainWindow::importLabels()
{
	try {

		// Get filename
		QString proposedFile = g_FrmMainWindow->experiment()->getPath();
		QString fileName = QFileDialog::getOpenFileName(this, "Select file to import labels from", proposedFile, "labels files (*.lbl)");
		if(fileName.isEmpty())
			return;

		gDbg << "Loading labels from file " << fileName << "\n";
		ChangeCursorObject cc;
		//QApplication::processEvents();	// Workaround for ChangeCursor problems (see constructor of ChangeCursor) - should be safe here

		// Open file
		QFile outFile(fileName);
		if(!outFile.open(QIODevice::ReadOnly)) {
			cc.restoreCursor();
			QMessageBox::critical(this, "Error", QString("Could not open file: %1\n").arg(fileName));
			return;
		}

		// Check magic number and version
		quint32 magic, version;
		QDataStream in(&outFile);
		in >> magic;
		in >> version;
		if(magic != (quint32)0xD4552433 || version > 3) {
			cc.restoreCursor();
			QMessageBox::critical(this, "Error", QString("File is invalid or version too new (magic: %1, version: %2).").arg(magic).arg(version));
			return;
		}

		// Clean current labels, samples, overlay images, learned DOB, and constrained regions 
		if(m_labelsForeground.size()) {
			if(QMessageBox::question(this, "Overwrite labels?", "Overwrite all current labels?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
				return;
		}
		clearLabels(true);
		m_automaticallyDeterminedDob = 0;
		Settings::clearConstrainedRegions();

		// Make sure no markings are shown for current image
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->clearLabels();

		// Read labels
		if(version <= 2)
			importLabelsOld(in, version);
		else {
			// Version >= 3
			QHash<PictureIndex, QVector<Blob> > labelsForeGround;
			in >> labelsForeGround;
			for(auto it = labelsForeGround.begin(); it != labelsForeGround.end(); ++it) {
				if(m_labelsForeground.contains(it.key()))
					m_labelsForeground.remove(it.key());
				gLog << "loaded " << it.value().size() << " cell labels for image " << it.key().toString() << "\n";
				m_labelsForeground.insert(it.key(), std::vector<Blob>(it.value().begin(), it.value().end()));
			}
			QHash<PictureIndex, QVector<cv::Point2i> > labelsBackGround;
			in >> labelsBackGround;
			for(auto it = labelsBackGround.begin(); it != labelsBackGround.end(); ++it) {
				if(m_labelsBackground.contains(it.key()))
					m_labelsBackground.remove(it.key());
				m_labelsBackground.insert(it.key(), std::vector<cv::Point2i>(it.value().begin(), it.value().end()));
			}
		}

		// Display markings if available
		if(m_labelsForeground.contains(m_currentPictureIndex))
			m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->addLabels(m_labelsForeground.value(m_currentPictureIndex));
		if(m_labelsBackground.contains(m_currentPictureIndex))
			m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->addBackgroundLabels(m_labelsBackground.value(m_currentPictureIndex));

		// Update label statistics
		updateLabelStatistics();

		// Do not find ERs or anything, if desired
		if(!m_ui.actionAuto_update_ERs_after_labeling->isChecked())
			return;

		// Get samples from labels
		extractSamplesForLabels(true);

		// Train SVM and update segmentation
		if(m_frmOriginalImageView->autoRefreshSegmentation()) {
			learnParametersSvm();
			applySegmentationSettingsAndUpdatePreview();
		}

		// Update original image view window title
		QString caption = QString("Train Segmentation Algorithm - %1 cell and %2 background candidate regions marked").arg(getNumLabeledCellExtremalRegions()).arg(getNumLabeledBackgroundExtremalRegions());
		m_dockOriginalImage->setWindowTitle(caption);
	}
	catch(ExceptionBase& e) {
		QMessageBox::critical(this, "Error", QString("Operation failed: %1\n\nat: %2").arg(e.what()).arg(e.where()));
	}
}

void FrmMainWindow::importLabelsOld(QDataStream& in, int version)
{
	// Read labels
	QHash<QString, QVector<Blob> > labelsForeGround;
	in >> labelsForeGround;
	for(auto it = labelsForeGround.begin(); it != labelsForeGround.end(); ++it) {
		QString pathRel = fullPathToExpRelPath(it.key());
		PictureIndex pi = m_experiment->converFileNameToPictureIndex(pathRel);

		if(m_labelsForeground.contains(pi))
			m_labelsForeground.remove(pi);
		gLog << "loaded " << it.value().size() << " cell labels for image " << pi.toString() << "\n";
		m_labelsForeground.insert(pi, std::vector<Blob>(it.value().begin(), it.value().end()));
	}
	if(version > 1) {
		QHash<QString, QVector<cv::Point2i> > labelsBackGround;
		in >> labelsBackGround;
		for(auto it = labelsBackGround.begin(); it != labelsBackGround.end(); ++it) {
			QString pathRel = fullPathToExpRelPath(it.key());
			PictureIndex pi = m_experiment->converFileNameToPictureIndex(pathRel);

			if(m_labelsBackground.contains(pi))
				m_labelsBackground.remove(pi);
			m_labelsBackground.insert(pi, std::vector<cv::Point2i>(it.value().begin(), it.value().end()));
		}
	}
}

void FrmMainWindow::startTrackingClicked()
{
	/*
#ifndef _DEBUG
	// Make sure parameters are up to date
	if(!m_parametersUpToDate) {
		m_startTrackingAfterLearning = true;
		learnParameters();
		return;
	}
#endif
	*/

	// Emit signal
	emit startTracking();
}

//void FrmMainWindow::matlabTestFastER()
//{
//	// Must have image
//	if(m_curImage.isNull() || m_currentImagePath.isEmpty()) {
//		QMessageBox::critical(this, "Error", "No image opened.");
//		return;
//	}
//
//	try {
//		ChangeCursorObject cc;
//
//		// Create test instance
//		if(!m_fasterTest)
//			m_fasterTest = new MatlabFastERTest();
//
//		// Get image with foreground and image with background labels
//		QImage foregroundImage(m_curImage.width(), m_curImage.height(), QImage::Format_Indexed8);
//		foregroundImage.setColorTable(Tools::getColorTableGrayIndexed8());
//		foregroundImage.fill(0);
//		unsigned char* imData = foregroundImage.bits();
//		int bpl = foregroundImage.bytesPerLine();
//		auto labels = m_labelsForeground.value(m_currentPictureIndex);
//		for(auto itBlob = labels.begin(); itBlob != labels.end(); ++itBlob) {
//			const auto& pixels = itBlob->getPixels();
//			for(auto itPx = pixels.begin(); itPx != pixels.end(); ++itPx) 
//				imData[itPx->y * bpl + itPx->x] = 255;
//		}
//		QImage backgroundImage(m_curImage.width(), m_curImage.height(), QImage::Format_Indexed8);
//		backgroundImage.setColorTable(Tools::getColorTableGrayIndexed8());
//		backgroundImage.fill(0);
//		imData = backgroundImage.bits();
//		bpl = backgroundImage.bytesPerLine();
//		auto labelsBackGround = m_labelsBackground.value(m_currentPictureIndex);
//		for(auto itPx = labelsBackGround.begin(); itPx != labelsBackGround.end(); ++itPx) 
//			imData[itPx->y * bpl + itPx->x] = 255;
//
//		// Execute test
//		QStringList output = m_fasterTest->testFastER(m_currentImagePath, foregroundImage, backgroundImage);
//		if(output.length()) {
//			cc.restoreCursor();
//			QMessageBox::information(this, "Done", QString("Matlab output:\n\n%1").arg(output.join("\n")));
//		}
//	}
//	catch(ExceptionBase& e) {
//		QMessageBox::information(this, "ExceptionBase", e.what());
//	}
//}

//void FrmMainWindow::findExtremalRegionsForCurrentLabelsAndUpdateObservationsInAllImages()
//{
//	//m_frmOriginalImageView->m_ui.lblSamples->setText(" - 0 samples)");
//	m_obsPos.clear();
//	m_obsNeg.clear();
//
//	// If there are no labels for the current image, make sure none are displayed
//	if(!m_labelsForeground.contains(m_currentPictureIndex) && !m_labelsBackground.contains(m_currentPictureIndex))
//		updateDisplayOfFoundExtremalRegions();
//
//	// Check if there are labels
//	if(m_labelsForeground.size() == 0 || m_labelsForeground.size() == 0) {
//		QString caption = QString("Train Segmentation Algorithm - 0 cells and 0 background extremal regions marked");
//		m_dockOriginalImage->setWindowTitle(caption);
//
//		return;
//	}
//
//	// Extract observations
//	QString errorMsg;
//	try {
//		ChangeCursorObject cc;
//
//		// Determine connectivity
//		SegmentationOperationList* segOperations = Settings::getSegmentationOperations().get();
//		int connectivity = segOperations ? segOperations->getPixelConnectivity() : 4;
//
//		// Basic settings
//		int minSize = m_frmBasicSegmentationSettings->m_ui.spbMinSize->value();
//		int maxSize = m_frmBasicSegmentationSettings->m_ui.spbMaxSize->value();
//		//int medianFilterSize = Settings::get<int>(Settings::KEY_MEDIANPREPROCESSKERNELSIZE);
//		
//		// Determine bilateral filter settings from segmentation pipeline
//		int pixelNeighborhoodDiameter = -1;
//		double sigmaColor = -1.0;
//		double sigmaSpace = -1.0;
//		if(segOperations) {
//			SegmentationOperation* bilFilter = segOperations->getOperation(SegmentationOperationBilateralFilter::SEGOP_NAME);
//			if(bilFilter) {
//				const auto bilFilterParams = bilFilter->getParameters();
//				pixelNeighborhoodDiameter = bilFilterParams.value("pixelNeighborhoodDiameter", 5).toInt();
//				sigmaColor = bilFilterParams.value("sigmaColor", 10.0).toDouble();
//				sigmaSpace = bilFilterParams.value("sigmaSpace", 10.0).toDouble();
//			}
//		}
//
//		// Check if DOB setting needs to be learned
//		faster::FastERDetector dDob, dBod;
//		int sumNegSamplesTotal = 0;
//		int DOB = 0;	// 0: learn if DOB/BOD is better (using the image with most markings), 1: DOB, 2: BOD
//		if(!m_frmBasicSegmentationSettings->m_ui.chkLearnDOB->isChecked()) {
//			if(m_frmBasicSegmentationSettings->m_ui.optDOB->isChecked())
//				DOB = 1;
//			else 
//				DOB = 2;
//		}
//
//		// Extract list (without duplicates) of image names that have foreground and/or background labels
//		QSet<PictureIndex> imageSet = m_labelsForeground.keys().toSet();
//		imageSet.unite(m_labelsBackground.keys().toSet());
//
//		// Convert to list and place element with most foreground labels first, as it will be used to determine DOB setting
//		QList<PictureIndex> imageList = imageSet.toList();
//		int maxFGLabelsIndex = -1;
//		int bestNumFGLabels = 0;
//		for(int i = 0; i < imageList.size(); ++i) {
//			int curNumFGLabels = m_labelsForeground.value(imageList[i]).size();
//			if(curNumFGLabels > bestNumFGLabels) {
//				bestNumFGLabels = curNumFGLabels;
//				maxFGLabelsIndex = i;
//			}
//		}
//		if(maxFGLabelsIndex > 0) {
//			imageList.swap(0, maxFGLabelsIndex);
//		}
//
//		// Get observations from all marked images
//		for(auto itPi = imageList.constBegin(); itPi != imageList.constEnd(); ++itPi) {
//			yo(*itPi);
//		}
//
//		// Update display
//		updateDisplayOfFoundExtremalRegions();
//
//		// Show how much ERs found
//		int numCells = m_obsPos.size()/faster::NUM_FEATURES;
//		QString caption = QString("Train Segmentation Algorithm - %1 cell and %2 background extremal regions marked").arg(numCells).arg(sumNegSamplesTotal);
//		m_dockOriginalImage->setWindowTitle(caption);
//
//
//		//// DEBUG: write features to file
//		//QFile f("Observations.txt");
//		//if(f.open(QIODevice::WriteOnly)) {
//		//	QTextStream s(&f);
//		//	int iObs = 0;
//		//	for(int iLine = 0; iLine < m_obsPos.size()/4; ++iLine) {
//		//		s << m_obsPos[iObs++] << "\t";
//		//		s << m_obsPos[iObs++] << "\t";
//		//		s << m_obsPos[iObs++] << "\t";
//		//		s << m_obsPos[iObs++] << "\t";
//		//		s << "1" << "\n";
//		//	}
//		//	iObs = 0;
//		//	for(int iLine = 0; iLine < m_obsNeg.size()/4; ++iLine) {
//		//		s << m_obsNeg[iObs++] << "\t";
//		//		s << m_obsNeg[iObs++] << "\t";
//		//		s << m_obsNeg[iObs++] << "\t";
//		//		s << m_obsNeg[iObs++] << "\t";
//		//		s << "-1" << "\n";
//		//	}
//		//}
//
//	}
//	catch(std::exception& e) {
//		errorMsg = QString("STL Exception: ") + e.what();
//	}
//	catch(ExceptionBase& e) {
//		errorMsg = QString("Exception at %2: %1").arg(e.what()).arg(e.where());
//	}
//	catch(faster::FastERException& e){
//		errorMsg = QString("fastER exception: %1").arg(e.getType());
//	}
//
//	// Check if error
//	if(!errorMsg.isEmpty()) {
//		QMessageBox::critical(this, "Error", QString("Parameter learning failed:\n\n") + errorMsg);
//		return;
//	}
//}

void FrmMainWindow::updateDisplayOfFoundExtremalRegions()
{
	// Show sample regions
	if(m_frmImageViewSettings->m_ui.optShowPosSamples->isChecked()) {
		QImage ov = m_foundExtremalRegionsPos.value(m_currentPictureIndex, QImage());
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->setOverlayImage(ov);
	}
	else if(m_frmImageViewSettings->m_ui.optShowNoSamples->isChecked()) {
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->setOverlayImage(QImage());
	}
	else {
		QImage ov = m_foundExtremalRegionsNeg.value(m_currentPictureIndex, QImage());
		if(!ov.isNull())
			m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->setOverlayImage(ov);
	}
}

void FrmMainWindow::learnParametersSvm()
{
	// Check if image
	if(m_curImage.isNull())
		return;

	QString errorMsg;
	try {
		ChangeCursorObject cc;

		// Copy data into single vectors
		std::vector<double> obsPos, obsNeg;
		convertSampleFeaturesToVectors(obsPos, obsNeg);

		// Normalize data and train SVM (if enough observations exist, otherwise use trainSVM() to set flag that SVM is not ready)
		Settings::trainSVM(obsPos, obsNeg, m_sampleFeatureStatsMean, m_sampleFeatureStatsStdDev, true);

		// Done
		cc.restoreCursor();
	}
	catch(std::exception& e) {
		errorMsg = QString("STL Exception: ") + e.what();
	}
	catch(ExceptionBase& e) {
		errorMsg = QString("Exception at %2: %1").arg(e.what()).arg(e.where());
	}
	catch(faster::FastERException& e){
		errorMsg = QString("fastER exception: %1").arg(e.getType());
	}

	// Check if error
	if(!errorMsg.isEmpty()) {
		QMessageBox::critical(this, "Error", QString("Parameter learning failed:\n\n") + errorMsg);
		return;
	}
}

void FrmMainWindow::mouseMoved( int posX, int posY )
{
	//m_frmOriginalImageView->m_ui.lblMousePos->setText(QString("%1/%2").arg(posX).arg(posY));
}

void FrmMainWindow::hideMarkings()
{
	m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->toggleHideMarkings();
}

void FrmMainWindow::channelSegmentationChanged( int newChannel )
{
	// Change in settings.	
	Settings::set(Settings::KEY_TRACKINGWL, newChannel);

	// Change channel to use in preview as well
	m_segmentationPreviewChannel = newChannel;
	m_frmSegPreview->setPreviewImageChannel(m_segmentationPreviewChannel);

	// Update image for labeling and segmentation preview
	if(m_currentPictureIndex.isValid() && newChannel != m_currentPictureIndex.channel) {
		PictureIndex newPi = m_currentPictureIndex;
		newPi.channel = newChannel;
		setImage(newPi);
	}

	// Change value of preview spinbox
	m_frmOriginalImageView->m_ui.spbPreviewChannel->setValue(newChannel);
}

void FrmMainWindow::channelPreviewChanged(int newChannel)
{
	// Update segmentation preview
	if(m_frmSegPreview && m_segmentationPreviewChannel != newChannel) {
		m_segmentationPreviewChannel = newChannel;
		m_frmSegPreview->setPreviewImageChannel(m_segmentationPreviewChannel);
		m_frmSegPreview->visualizeSegmentation();
	}
}

void FrmMainWindow::setStartTpToFirst()
{
	// Set spinbox to first time point of experiment
	int minTp = g_FrmMainWindow->experiment()->getMinPictureIndex().timePoint;
	if(minTp > 0)
		m_frmStartTasks->m_ui.spbFirstTimePoint->setValue(minTp);
}

void FrmMainWindow::setStopTpToLast()
{
	// Set spinbox to last time point of experiment
	int maxTp = g_FrmMainWindow->experiment()->getMaxPictureIndex().timePoint;
	if(maxTp > 1)
		m_frmStartTasks->m_ui.spbLastTimePoint->setValue(maxTp);
}

void FrmMainWindow::updateDisplayOfSelectedPositions()
{
	// Display number of selected positions
	QList<QVariant> positions = Settings::get<QList<QVariant> >(Settings::KEY_SELECTEDPOSITIONS);
	m_frmStartTasks->m_ui.lblSelectedPositions->setText(QString("Number of positions: %1").arg(positions.size()));
}

void FrmMainWindow::startTasks()
{
	// Check if segmentation parameters are set
	//if(!m_frmStartTasks->m_ui.optTrackingOnly->isChecked()) {
		int numLabeledCells = getNumLabeledCellExtremalRegions();
		int numLabeledBackground = getNumLabeledBackgroundExtremalRegions();
		if(numLabeledCells < 1 || numLabeledBackground < 1) {
			QMessageBox::information(this, "Note", "Segmentation not possible: mark at least one cell and one background extremal region.");
			return;
		} 
	//}

	// Update fastER settings and apply the to the segmentation pipeline
	Settings::setFastERParameters(m_frmBasicSegmentationSettings->m_ui.optDOB->isChecked(), m_frmBasicSegmentationSettings->m_ui.spbMinSize->value(), m_frmBasicSegmentationSettings->m_ui.spbMaxSize->value(), 0.5, m_frmBasicSegmentationSettings->m_ui.chkEnforceBinaryMask->isChecked());
	Settings::applyFastERParameters(NULL);

	// Apply denoising setting to segmentation pipeline
	if(!m_ui.actionDisable_Learning_of_Denoising_parameters->isChecked()) 
		Settings::applyLearnedDenoisingParameters();

	// Set temporal range
	Settings::set(Settings::KEY_STARTFRAME, m_frmStartTasks->m_ui.spbFirstTimePoint->value());
	Settings::set(Settings::KEY_STOPFRAME, m_frmStartTasks->m_ui.spbLastTimePoint->value());

	// Create JobSets and JobRunners
	try {
		int idealThreadCount = JobRunnerMaster::optimalThreadCount();
		std::vector<JobRunnerMaster*> masterJobRunners; 
		std::vector<JobSet*> jobs;

		// Segmentation
		//if(!m_frmStartTasks->m_ui.optTrackingOnly->isChecked()) {
			// Reset cell counts
			SegmentationOperationCountCells::resetCellCount();

			// Create set with all images to segment
			JobSetSegmentation* jobsSegmentation = new JobSetSegmentation();
			jobs.push_back(jobsSegmentation);
			
			// Create one JobRunnerMaster and add a JobRunnerSegmentation for each thread
			JobRunnerMaster* masterJobRunnerSegmentation = new JobRunnerMaster();
			for(int i = 0; i < idealThreadCount; ++i) 
				masterJobRunnerSegmentation->addJobRunner(new JobRunnerSegmentation(jobsSegmentation, masterJobRunnerSegmentation));
			masterJobRunners.push_back(masterJobRunnerSegmentation);
		//}

		// m_jobRunners is only empty in debug scenarios
		if(masterJobRunners.size()) {
			gLog << "Using " << idealThreadCount << " threads\n";

			// Show progress form
			showProgressForm();
			m_frmProgress->runJobs(masterJobRunners, jobs);
		}
	}
	catch(ExceptionBase& e)
	{
		gLog << "Error: " << QString("Running tasks failed: %1 at: %2\n").arg(e.what()).arg(e.where());
		QMessageBox::critical(this, "Error", QString("Running tasks failed: %1\n\nat: %2").arg(e.what()).arg(e.where()));
	}
}

void FrmMainWindow::countCells()
{
	/*
	// Make sure, count cells pipeline is set
	Settings::setSegmentationOperations(m_countCellsPipeline);

	// Set temporal range
	Settings::set(Settings::KEY_STARTFRAME, m_frmStartTasks->m_ui.spbFirstTimePoint->value());
	Settings::set(Settings::KEY_STOPFRAME, m_frmStartTasks->m_ui.spbLastTimePoint->value());

	// Create JobSets and JobRunners
	try {
		int idealThreadCount = JobRunnerMaster::optimalThreadCount();
		std::vector<JobRunnerMaster*> masterJobRunners; 
		std::vector<JobSet*> jobs;

		// Create set with all images to count cells in
		JobSetSegmentation* jobsSegmentation = new JobSetSegmentation();
		jobs.push_back(jobsSegmentation);
		jobsSegmentation->setDescription("Count cells");

		// Create one JobRunnerMaster and add a JobRunnerSegmentation for each thread
		JobRunnerMaster* masterJobRunnerSegmentation = new JobRunnerMaster();
		for(int i = 0; i < idealThreadCount; ++i) 
			masterJobRunnerSegmentation->addJobRunner(new JobRunnerSegmentation(jobsSegmentation, masterJobRunnerSegmentation));
		masterJobRunners.push_back(masterJobRunnerSegmentation);

		// Show progress form
		gLog << "Using " << idealThreadCount << " threads\n";
		showProgressForm();
		m_frmProgress->runJobs(masterJobRunners, jobs);
	}
	catch(ExceptionBase& e)
	{
		gLog << "Error: " << QString("Running tasks failed: %1 at: %2\n").arg(e.what()).arg(e.where());
		QMessageBox::critical(this, "Error", QString("Running tasks failed: %1\n\nat: %2").arg(e.what()).arg(e.where()));
	}
	*/

	// Just export cell counts
	if(!m_experiment)
		return;
	QString expDir = m_experiment->getPath();
	QString fileName = expDir + "cellCounts.txt";
	if(SegmentationOperationCountCells::exportCellCounts(fileName))
		Tools::displayMessageBoxWithOpenFolder("Export completed.", "Done", fileName, true, this);
	else
		QMessageBox::critical(this, "Error", "Export failed.");
}

void FrmMainWindow::showProgressForm()
{
	// Create window
	if(!m_frmProgress) {
		m_frmProgress = new FrmProgress();
		m_frmProgress->setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);
		m_frmProgress->setWindowModality(Qt::ApplicationModal);
	}

	// Show window
	m_frmProgress->show();
	m_frmProgress->raise();
}

void FrmMainWindow::applySegmentationSettingsAndUpdatePreview()
{
	QString errorMsg;

	try {
		QString resultMsg;
		QTextStream s(&resultMsg);

		if(m_frmSegPreview && m_frmSegPreview->isVisible()) {

			//// Make sure, preview segmentation pipeline is set
			//Settings::setSegmentationOperations(m_segmentationPipelinePreview);

			// Update fastER settings and apply to segmentation pipeline
			Settings::setFastERParameters(m_frmBasicSegmentationSettings->m_ui.optDOB->isChecked(), m_frmBasicSegmentationSettings->m_ui.spbMinSize->value(), m_frmBasicSegmentationSettings->m_ui.spbMaxSize->value(), 0.5, m_frmBasicSegmentationSettings->m_ui.chkEnforceBinaryMask->isChecked());
			Settings::applyFastERParameters(NULL);

			// Apply denoising setting to segmentation pipeline
			if(!m_ui.actionDisable_Learning_of_Denoising_parameters->isChecked()) 
				Settings::applyLearnedDenoisingParameters();

			// Update segmentation (optionally saving segmentation results
			QString previewSavePath = "";
			if(m_ui.actionExport_Segmentation->isChecked()) {
				previewSavePath = g_FrmMainWindow->experiment()->getPath();
				previewSavePath += m_currentPictureIndex.toString() + "segmentation_preview_export.png";

			}
			m_frmSegPreview->updateSegmentationResults(previewSavePath);
		}

		if(!resultMsg.isEmpty() && m_ui.actionLog_FastER_Training_Results->isChecked())
			gLog << resultMsg;
	}
	catch(std::exception& e) {
		errorMsg = QString("STL Exception: ") + e.what();
	}
	catch(ExceptionBase& e) {
		errorMsg = QString("Exception at %2: %1").arg(e.what()).arg(e.where());
	}
	catch(faster::FastERException& e){
		errorMsg = QString("fastER exception: %1").arg(e.getType());
	}

	// Check if error
	if(!errorMsg.isEmpty()) {
		QMessageBox::critical(this, "Error", QString("Parameter learning failed:\n\n") + errorMsg);
		return;
	}
}


void FrmMainWindow::previewChangeTimePoint( int delta )
{
	// Update time point, but not beyond limits
	int minTp = std::max(1, g_FrmMainWindow->experiment()->getMinPictureIndex().timePoint);
	int maxTp = std::max(1, g_FrmMainWindow->experiment()->getMaxPictureIndex().timePoint);
	int newTp = m_frmOriginalImageView->m_ui.spbPreviewTimePoint->value() + delta;
	newTp = std::max(newTp, minTp);
	newTp = std::min(newTp, maxTp);
	if(newTp != m_frmOriginalImageView->m_ui.spbPreviewTimePoint->value())
		m_frmOriginalImageView->m_ui.spbPreviewTimePoint->setValue(newTp);
}

void FrmMainWindow::exportFGLabelsOfCurrentImage()
{
	// Get labels
	if(m_curImage.isNull() || !m_labelsForeground.contains(m_currentPictureIndex)) {
		QMessageBox::information(this, "Error", "There are no labels in the current image!");
		return;
	}
	ChangeCursorObject cc;
	const std::vector<Blob>& labels = m_labelsForeground[m_currentPictureIndex];

	// Create binary image
	QImage img(m_curImage.width(), m_curImage.height(), QImage::Format_MonoLSB);
	img.fill(0);
	unsigned char* imData = img.bits();
	int bpl = img.bytesPerLine();
	for(auto itBlob = labels.begin(); itBlob != labels.end(); ++itBlob) {
		for(auto itPixel = itBlob->getPixels().begin(); itPixel != itBlob->getPixels().end(); ++itPixel) {
			int x = itPixel->x,
				y = itPixel->y;
			imData[y*bpl + (x>>3)] |= 1 << (x % 8);  
		} 
	}

	// Get export file name
	QString defaultPath = m_experiment->getPath() + "_segmentationReference/";
	if(!QDir(defaultPath).exists())
		QDir().mkpath(defaultPath);
	defaultPath += m_currentPictureIndex.toStringWithMarkers() + ".png";
	cc.restoreCursor();
	QString finalPath = QFileDialog::getSaveFileName(this, "Select destination file", defaultPath);
	if(finalPath.isEmpty())
		return;

	// Save
	img.save(finalPath);
}

void FrmMainWindow::showEvent(QShowEvent *event)
{
	event->accept();
}

//void FrmMainWindow::showHelp()
//{
//	// Open help pdf file in systems default pdf viewer
//	const QString path = "file:///" + QDir::currentPath() + "/Instructions.pdf";
//	if(!QDesktopServices::openUrl(QUrl(path)))
//		QMessageBox::critical(this, "Error", "Opening help file failed.");
//}

QString FrmMainWindow::fullPathToExpRelPath(const QString& imgPath)
{
	if(!m_experiment)
		return imgPath;

	// Cut off everything until the last occurrence of "/<expName>/"
	// (needed for backward compatibility: conversion of image paths
	// that were created when experiment was in a different location)
	QString imgPathConv = QDir::fromNativeSeparators(imgPath);
	QString expNamePart = "/" + m_experiment->getName() + "/";
	int idx = imgPathConv.lastIndexOf(expNamePart);
	if(idx >= 0)
		return imgPathConv.mid(idx + expNamePart.length());
	else
		return imgPath;
}

QString FrmMainWindow::expRelPathToFullPath(const QString& imgPath)
{
	if(!m_experiment)
		return imgPath;

	return m_experiment->getPath() + imgPath;
}

void FrmMainWindow::dockSegPreviewTopLevelChanged(bool floating)
{
	// This is required to give the dock widget a maximize button when undocked
	if(floating) {
		m_dockSegPreview->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
		m_dockSegPreview->show();
	}
}

int FrmMainWindow::getNumForegroundLabels() const
{
	return m_labelsForeground.size();
}

int FrmMainWindow::getNumBackgroundLabels() const
{
	return m_labelsBackground.size();

}

void FrmMainWindow::updateLabelStatistics()
{
	// Create basic statistics from all blobs
	m_minForegroundLabelSize = -1;
	m_maxForegroundLabelSize = -1;
	m_numForegroundLabels = 0;
	for(auto it = m_labelsForeground.begin(); it != m_labelsForeground.end(); ++it) {
		for(auto blobIt = it->begin(); blobIt != it->end(); ++blobIt) {
			++m_numForegroundLabels;
			if(m_minForegroundLabelSize == -1) {
				m_minForegroundLabelSize = blobIt->getArea();
				m_maxForegroundLabelSize = blobIt->getArea();
			}
			else {
				m_minForegroundLabelSize = std::min(m_minForegroundLabelSize ,blobIt->getArea());
				m_maxForegroundLabelSize = std::max(m_maxForegroundLabelSize, blobIt->getArea());
			}
		}
	}

	// Set minSize/maxSize and denoising parameters based on markings
	int minSize = m_minForegroundLabelSize * 0.7;
	int maxSize = m_maxForegroundLabelSize * 1.3;
	if(m_frmBasicSegmentationSettings->m_ui.chkLearnSize->isChecked()) {
		if(m_minForegroundLabelSize > 0)
			m_frmBasicSegmentationSettings->m_ui.spbMinSize->setValue(minSize);
		if(m_maxForegroundLabelSize > 0)
			m_frmBasicSegmentationSettings->m_ui.spbMaxSize->setValue(maxSize);
	}
	if(!m_ui.actionDisable_Learning_of_Denoising_parameters->isChecked()) {
		double diameter = 2*sqrt(minSize / M_PI);
		qDebug() << "min diameter: " << diameter;
		int sigma = std::max(10.0, (4.0 * diameter) - 10.0);	// Set sigma to 4 times minimal cell diameter - 10 (but not below 10)
		int kernelSize = 2*(3 + sigma / 50) + 1;	// Set kernel size to 7 and increase by 2*(sigma/50)
		if(sigma != Settings::get<double>(Settings::KEY_DENOISING_SIGMACOLOR) 
			|| sigma != Settings::get<double>(Settings::KEY_DENOISING_SIGMASPACE) 
			|| kernelSize != Settings::get<int>(Settings::KEY_DENOISING_KERNELSIZE)) {

				// Update denoising parameters
				Settings::set(Settings::KEY_DENOISING_SIGMACOLOR, sigma);
				Settings::set(Settings::KEY_DENOISING_SIGMASPACE, sigma);
				Settings::set(Settings::KEY_DENOISING_KERNELSIZE, kernelSize);
				Settings::applyLearnedDenoisingParameters();

				// This requires complete update of samples etc.
				m_enforceUpdateOfSamplesInAllImages = true;
				gLog << "Updating samples from all images after change of denoising parameters (sigma: " << sigma << ", k: " << kernelSize << ").\n";
		}
	}
}

void FrmMainWindow::extractSamplesForLabels(bool useAllLabeledImages)
{
	// Determine if dob/bod should be determined, i.e. if automatic determination is on and onlyInCurrentImage is false
	// or current image contains most labels or settings has not been determined yet
	int DOB = 0;
	if(m_frmBasicSegmentationSettings->m_ui.chkLearnDOB->isChecked()) {
		// Automatic determination enabled
		if(m_automaticallyDeterminedDob != 0) {
			// Setting was determined already, so only update if current image has most fg labels
			int numFgLabelsInCurImage = m_labelsForeground.value(m_currentPictureIndex).size();
			for(auto itImgWithFgLabels = m_labelsForeground.cbegin(); itImgWithFgLabels != m_labelsForeground.cend(); ++itImgWithFgLabels) {
				if(itImgWithFgLabels.key() != m_currentPictureIndex && itImgWithFgLabels->size() > numFgLabelsInCurImage) {
					// Found another image with more foreground labels, so do not update DOB/BOD
					DOB = m_automaticallyDeterminedDob;
					break;
				}
			}
		}
	}
	else {
		// Automatic determination disabled
		DOB = m_frmBasicSegmentationSettings->m_ui.optDOB->isChecked() ? 1 : 2;
	}

	// List of images to use to extract samples: if DOB has to be detected and is thereby changed
	// or if useAllLabeledImages is true, all images with labels are used, otherwise only
	// current image is used
	QSet<PictureIndex> imageSet;
	if(useAllLabeledImages || m_enforceUpdateOfSamplesInAllImages) {
		// Use all images
		imageSet = m_labelsForeground.keys().toSet();
		imageSet.unite(m_labelsBackground.keys().toSet());

		m_enforceUpdateOfSamplesInAllImages = false;
	}
	else {
		// Use only current (unless DOB changes, see below)
		if(m_currentPictureIndex.isValid())
			imageSet.insert(m_currentPictureIndex);
	}

	// Try to detect DOB setting
	if(DOB == 0) {
		// Find image with most foreground labels
		PictureIndex piWithMostFgLabels = findIndexOfImageWithMostLabels();
		if(piWithMostFgLabels.isValid()) {
			// Use it to determine DOB setting
			extractSamplesForLabelsSingleImage(piWithMostFgLabels, DOB);
			if(DOB == 0) {
				// DOB determination failed
				gErr << "Warning: cannot detect dark-on-bright setting (used image: " << piWithMostFgLabels.toString() << ").\n"; 
				return;
			}

			// Check if DOB setting changed,
			if(DOB != m_automaticallyDeterminedDob) {
				// Remember new setting
				m_automaticallyDeterminedDob = DOB;
				if(m_automaticallyDeterminedDob == 1)
					m_frmBasicSegmentationSettings->m_ui.optDOB->setChecked(true);
				else
					m_frmBasicSegmentationSettings->m_ui.optBOD->setChecked(true);

				// Re-extract samples also from all remaining images
				imageSet = m_labelsForeground.keys().toSet();
				imageSet.unite(m_labelsBackground.keys().toSet());
			}

			// Image with most foreground labels has been processed
			imageSet.remove(piWithMostFgLabels);
		}
	}

	// Extract samples from images in imageSet
	if(DOB > 0) {
		for(auto itPi = imageSet.cbegin(); itPi !=imageSet.cend(); ++itPi) {
			PictureIndex curPi = *itPi;	
			assert(itPi->isValid());
			assert(DOB == 1 || DOB == 2);
			extractSamplesForLabelsSingleImage(curPi, DOB);
		}
	}

	// Update statistics about samples
	updateSampleFeatureStatistics();

	// Display found sample regions
	updateDisplayOfFoundExtremalRegions();
}


void FrmMainWindow::updateSampleFeatureStatistics()
{
	// Check image
	if(m_curImage.isNull())
		return;

	// Calculate mean and std deviation of all features
	int count = 0;
	double sum[faster::NUM_FEATURES];
	double sum2[faster::NUM_FEATURES];
	std::fill(sum, sum + faster::NUM_FEATURES, 0);
	std::fill(sum2, sum2 + faster::NUM_FEATURES, 0);
	for(auto itImage = m_posSampleFeatures.cbegin(); itImage != m_posSampleFeatures.cend(); ++itImage) {	// Positive samples
		const faster::sample_feature_list_type& curImageObservations = *itImage;
		int numObservations = curImageObservations.size() / faster::NUM_FEATURES;
		for(int r = 0; r < numObservations; ++r) {
			for(int c = 0; c < faster::NUM_FEATURES; ++c) {
				double curVal = curImageObservations[r*faster::NUM_FEATURES + c];
				sum[c] += curVal;
				sum2[c] += curVal * curVal;
			}
			++count;
		}
	}
	for(auto itImage = m_negSampleFeatures.cbegin(); itImage != m_negSampleFeatures.cend(); ++itImage) {	// Negative samples
		const faster::sample_feature_list_type& curImageObservations = *itImage;
		int numObservations = curImageObservations.size() / faster::NUM_FEATURES;
		for(int r = 0; r < numObservations; ++r) {
			for(int c = 0; c < faster::NUM_FEATURES; ++c) {
				double curVal = curImageObservations[r*faster::NUM_FEATURES + c];
				sum[c] += curVal;
				sum2[c] += curVal * curVal;
			}
			++count;
		}
	}

	// Calculate mean and std deviation
	if(count > 1) {
		for(int c = 0; c < faster::NUM_FEATURES; ++c) {
			m_sampleFeatureStatsMean[c] = sum[c] / count;
			m_sampleFeatureStatsStdDev[c] = sqrt((sum2[c] - sum[c]*sum[c]/count) / (count-1));
		}
	}
	else {
		// Use some default values
		std::fill(m_sampleFeatureStatsMean, m_sampleFeatureStatsMean + faster::NUM_FEATURES, 0.0);
		std::fill(m_sampleFeatureStatsStdDev, m_sampleFeatureStatsStdDev + faster::NUM_FEATURES, 1.0);
	}


	//std::vector<double> allObservations;
	//allObservations.reserve(m_obsPos.size() + m_obsNeg.size());
	//allObservations.insert(allObservations.end(), m_obsPos.begin(), m_obsPos.end());
	//allObservations.insert(allObservations.end(), m_obsNeg.begin(), m_obsNeg.end());
	//m_sampleStatistics.calculateStatistics(allObservations);
}

void FrmMainWindow::extractSamplesForLabelsSingleImage(const PictureIndex& pi, int& DOB) 
{
	try {
		// Remove old sample features for picture
		m_posSampleFeatures.remove(pi);
		m_negSampleFeatures.remove(pi);

		// Remove old overlay images for samples
		m_foundExtremalRegionsPos.remove(pi);
		m_foundExtremalRegionsNeg.remove(pi);

		// Get basic segmentation settings
		SegmentationOperationList* segOperations = Settings::getSegmentationOperations().get();
		int connectivity = segOperations ? segOperations->getPixelConnectivity() : 4;
		int minSize = m_frmBasicSegmentationSettings->m_ui.spbMinSize->value();
		int maxSize = m_frmBasicSegmentationSettings->m_ui.spbMaxSize->value();
		bool disableErSplitting = m_frmBasicSegmentationSettings->m_ui.chkEnforceBinaryMask->isChecked();

		// Determine bilateral filter settings from segmentation pipeline
		int pixelNeighborhoodDiameter = -1;
		double sigmaColor = -1.0;
		double sigmaSpace = -1.0;
		if(segOperations) {
			SegmentationOperation* bilFilter = segOperations->getOperation(SegmentationOperationBilateralFilter::SEGOP_NAME);
			if(bilFilter) {
				const auto bilFilterParams = bilFilter->getParameters();
				pixelNeighborhoodDiameter = bilFilterParams.value("pixelNeighborhoodDiameter", 5).toInt();
				sigmaColor = bilFilterParams.value("sigmaColor", 10.0).toDouble();
				sigmaSpace = bilFilterParams.value("sigmaSpace", 10.0).toDouble();
			}
		}

		// Open image
		QImage image;
		int err = m_experiment->getImage(pi, image);
		if(image.isNull()) {
			gLog << "Warning in extractSamplesForLabelsSingleImage(): cannot open image " << pi.toString() << " - error = " << err << '\n';
			return;
		}
		int imSizeX = image.width(),
			imSizeY = image.height();

		// Convert QImage to unsigned char*
		unsigned char* dataDest = new unsigned char[imSizeX*imSizeY];
		std::unique_ptr<unsigned char[]> dataDestCleanup(dataDest);
		const unsigned char* dataSrc = image.bits();
		int bytesPerLineSrc = image.bytesPerLine();
		for(int y = 0; y < imSizeY; ++y) {
			for(int x = 0; x < imSizeX; ++x) 
				dataDest[y * imSizeX + x] = dataSrc[y*bytesPerLineSrc + x];
		}

		// Bilateral filter
		if(sigmaColor > 0.0 && sigmaSpace > 0.0) {
			qDebug() << "extractSamplesForLabelsSingleImage(): Filter bil. - sigmaColor: " << sigmaColor << " sigmaSpace: " << sigmaSpace << " k: " << pixelNeighborhoodDiameter;

			cv::Mat cvImageDst(imSizeY, imSizeX, CV_8UC1, dataDest);	// no deep copy
			cv::Mat cvImageSrc = cvImageDst.clone();	// deep copy
			bilateralFilter(cvImageSrc, cvImageDst, pixelNeighborhoodDiameter, sigmaColor, sigmaSpace);
		}

		// Convert labels into vectors of linear pixel indexes
		std::vector<std::vector<int>> foregroundLabels;
		std::vector<int> backgroundPixels;
		convertLabelsToLinearPixelIndexes(pi, imSizeX, foregroundLabels, backgroundPixels);

		// Finally, get samples
		assert(DOB == 0 || DOB == 1 || DOB == 2);
		if(DOB == 1) {
			m_fasterDetectorDob.getSamplesFromLabels(dataDest, imSizeX, imSizeY, true, foregroundLabels, backgroundPixels, false, minSize, maxSize, disableErSplitting);
			gLog << "Sample regions score: " << m_fasterDetectorDob.getSumOfScores() << ".\n";
		} 
		else if(DOB == 2) {
			m_fasterDetectorBod.getSamplesFromLabels(dataDest, imSizeX, imSizeY, false, foregroundLabels, backgroundPixels, false, minSize, maxSize, disableErSplitting);
			gLog << "Sample regions score: " << m_fasterDetectorBod.getSumOfScores() << ".\n";
		}
		else {
			// Try dob and bod
			m_fasterDetectorDob.getSamplesFromLabels(dataDest, imSizeX, imSizeY, true, foregroundLabels, backgroundPixels, false, minSize, maxSize, disableErSplitting);
			m_fasterDetectorBod.getSamplesFromLabels(dataDest, imSizeX, imSizeY, false, foregroundLabels, backgroundPixels, false, minSize, maxSize, disableErSplitting);

			// Choose best
			if(m_fasterDetectorDob.getSumOfScores() >= m_fasterDetectorBod.getSumOfScores()) {
				// DOB wins
				DOB = 1;
			}
			else { 
				// BOD wins
				DOB = 2;
			}
			gLog << "Using " << (DOB == 1 ? "dark-on-bright" : "bright-on-dark") << " mode (scores: " << m_fasterDetectorDob.getSumOfScores() << " vs. " << m_fasterDetectorBod.getSumOfScores() << ", used image: " << pi.toString() << " ).\n";
		}

		// Get features of extracted samples
		assert(DOB == 1 || DOB == 2);
		faster::FastERDetector& d = DOB == 1 ? m_fasterDetectorDob : m_fasterDetectorBod;
		d.getSampleFeatures(m_posSampleFeatures[pi], m_negSampleFeatures[pi]);

		// Get constrained regions
		std::unordered_set<faster::RegionID, faster::RegionIDHasher>& constraints = Settings::getConstrainedRegionsPositive(pi);
		d.getConstrainedRegions(constraints);

		// Create overlay image for foreground and background
		m_foundExtremalRegionsPos[pi] = createOverlayImage(d, DOB == 1, true, imSizeX, imSizeY);
		m_foundExtremalRegionsNeg[pi] = createOverlayImage(d, DOB == 1, false, imSizeX, imSizeY);
	}
	catch(std::exception& e) {
		gErr << QString("STL Exception: ") + e.what();
	}
	catch(ExceptionBase& e) {
		gErr << QString("Exception at %2: %1").arg(e.what()).arg(e.where());
	}
	catch(faster::FastERException& e){
		gErr << QString("fastER exception: %1").arg(e.getType());
	}
}

void FrmMainWindow::convertLabelsToLinearPixelIndexes(const PictureIndex& pi, int imageSizeX, std::vector<std::vector<int>>& foregroundLabels, std::vector<int>& backgroundPixels) const
{
	// Foreground
	if(m_labelsForeground.contains(pi)) {
		const std::vector<Blob>& posLabels = m_labelsForeground[pi];
		foregroundLabels.resize(posLabels.size());
		int iBlob = 0;
		for(auto itBlob = posLabels.begin(); itBlob != posLabels.end(); ++itBlob) {
			const auto& pixels = itBlob->getPixels();
			foregroundLabels[iBlob].resize(pixels.size());
			int iPixel = 0;
			for(auto itPx = pixels.begin(); itPx != pixels.end(); ++itPx) 
				foregroundLabels[iBlob][iPixel++] = itPx->y * imageSizeX + itPx->x;
			++iBlob;
		}
	}

	// Background
	if(m_labelsBackground.contains(pi) ) {
		const std::vector<cv::Point2i>& negLabels = m_labelsBackground[pi];
		backgroundPixels.resize(negLabels.size());
		int iPixel = 0;
		for(auto itPx = negLabels.begin(); itPx != negLabels.end(); ++itPx) 
			backgroundPixels[iPixel++] = itPx->y * imageSizeX + itPx->x;
	}
}

PictureIndex FrmMainWindow::findIndexOfImageWithMostLabels() const
{
	// Find image with most foreground labels
	PictureIndex maxFGLabelsIndex;
	int bestNumFGLabels = 0;
	auto imageList =  m_labelsForeground.keys();
	for(auto itImage = m_labelsForeground.cbegin(); itImage != m_labelsForeground.cend(); ++itImage) {
		int curNumFGLabels = itImage->size();
		if(curNumFGLabels > bestNumFGLabels) {
			bestNumFGLabels = curNumFGLabels;
			maxFGLabelsIndex = itImage.key();
		}
	}
	return maxFGLabelsIndex;
}

QImage FrmMainWindow::createOverlayImage(faster::FastERDetector& d, bool dob, bool positive, int imageSizeX, int imageSizeY) const
{
	//// Time
	//QElapsedTimer td;
	//td.start();

	// Convert segmentation to label map
	unsigned int* lblMap = new unsigned int[imageSizeX*imageSizeY];
	memset(lblMap, 0, imageSizeX*imageSizeY*sizeof(unsigned int));
	for(int iRegion = 0; iRegion < d.getNumRegions(positive); ++iRegion) {
		auto pixels = d.fillRegion(iRegion, dob, nullptr, positive);
		for(auto itPixel = pixels.cbegin(); itPixel != pixels.cend(); itPixel++) {
			lblMap[*itPixel] = iRegion + 1;
		}
	}

	// Extract perimeters
	QImage img = ImageProcessingTools::visualizeLabelMap(lblMap, imageSizeX, imageSizeY, true);

	// Clean up
	delete[] lblMap;

	// Done
	//gLog << "FrmMainWindow::createOverlayImage() - elapsed time: " << td.elapsed() << "ms \n";
	return img;


	/*
	// Create empty image
	QImage img = QImage(imageSizeX, imageSizeY, QImage::Format_ARGB32);
	int bpl = img.bytesPerLine();
	img.fill(qRgba(0, 0, 0, 0));

	unsigned char* data = img.bits();
	for(int iRegion = 0; iRegion < d.getNumRegions(positive); ++iRegion) {
		// Determine color: blue for background, random for foreground (but not too dark)
		unsigned int color;
		if(positive) {
			const int offset = 100;
			int r1 = qrand() % (255 - offset);
			int r2 = qrand() % (255 - offset);
			int r3 = qrand() % (255 - offset);
			color = qRgba(offset+r1, offset+r2, offset+r3, 255);
		}
		else {
			color = qRgba(0, 0, 255, 255);
		}
		//unsigned int color = positive ?  qRgba(qrand() % 255, qrand() % 255, 0, 255) :  qRgba(0, 0, 255, 255);

		auto pixels = d.fillRegion(iRegion, dob, nullptr, positive);
		for(auto itPixel = pixels.cbegin(); itPixel != pixels.cend(); itPixel++) {
			// Only draw perimeter pixels: check if there is at least one neighbor which does not belong to current region
			int y = *itPixel / imageSizeX;
			int x = *itPixel % imageSizeX;
			int deltaX[] = {1, -1, 0, 0, 1, 1, -1, -1};
			int deltaY[] = {0, 0, 1, -1, 1, -1, 1, -1};
			bool pixelHasNeighborOutsideRegion = false;
			for(int iNeighbor = 0; iNeighbor < 8; ++iNeighbor) {
				int xN = x + deltaX[iNeighbor];
				int yN = y + deltaY[iNeighbor];
				if(xN >= 0 && yN >= 0 && xN < imageSizeX && yN < imageSizeY) {
					int linearN = yN * imageSizeX + xN;
					if(std::find(pixels.cbegin(), pixels.cend(), linearN) == pixels.cend()) {
						// Neighbor is valid pixel and does not belong to region
						pixelHasNeighborOutsideRegion = true;
						break;
					}
				}
			}
			if(!pixelHasNeighborOutsideRegion)
				continue;

			// Set pixel
			*(((unsigned int*)(data+y*bpl))+x) = color;
		}
	}

	return img;
	*/
}

void FrmMainWindow::convertSampleFeaturesToVectors(std::vector<double>& obsPos, std::vector<double>& obsNeg) const
{
	// Count observations first
	int numPos = 0, numNeg = 0;
	for(auto it = m_posSampleFeatures.cbegin(); it != m_posSampleFeatures.cend(); ++it)
		numPos += it->size();
	for(auto it = m_negSampleFeatures.cbegin(); it != m_negSampleFeatures.cend(); ++it)
		numNeg += it->size();

	// Copy observations into single vectors
	obsPos.clear();
	obsPos.resize(numPos);
	auto itDstPos = obsPos.begin();
	for(auto itImage = m_posSampleFeatures.cbegin(); itImage != m_posSampleFeatures.cend(); ++itImage) {
		for(auto itSrc = itImage->cbegin(); itSrc != itImage->cend(); ++itSrc)
			*(itDstPos++) = *itSrc;
	}
	obsNeg.clear();
	obsNeg.resize(numNeg);
	auto itDstNeg = obsNeg.begin();
	for(auto itImage = m_negSampleFeatures.cbegin(); itImage != m_negSampleFeatures.cend(); ++itImage) {
		for(auto itSrc = itImage->cbegin(); itSrc != itImage->cend(); ++itSrc)
			*(itDstNeg++) = *itSrc;
	}
}

void FrmMainWindow::clearLabels(bool fromAllLabeledImages)
{
	if(fromAllLabeledImages) {
		// Clear everything
		m_labelsForeground.clear();
		m_labelsBackground.clear();
		m_foundExtremalRegionsPos.clear();
		m_foundExtremalRegionsNeg.clear();
		m_posSampleFeatures.clear();
		m_negSampleFeatures.clear();
	}
	else {
		// Clear only current image
		m_labelsForeground.remove(m_currentPictureIndex);
		m_labelsBackground.remove(m_currentPictureIndex);
		m_foundExtremalRegionsPos.remove(m_currentPictureIndex);
		m_foundExtremalRegionsNeg.remove(m_currentPictureIndex);
		m_posSampleFeatures.remove(m_currentPictureIndex);
		m_negSampleFeatures.remove(m_currentPictureIndex);
	}
}

void FrmMainWindow::addLabelsFromBinaryImage()
{
	// Get filename
	QString proposedFile = g_FrmMainWindow->experiment()->getPath();
	QString fileName = QFileDialog::getOpenFileName(this, "Select file to import labels from", proposedFile, "labels image (*.png;*.tif;*.tiff)");
	if(fileName.isEmpty())
		return;

	// Open image
	std::vector<Blob> blobs;
	if(Tools::loadSegmentationFromPNG(fileName, blobs, -1, -1) != 0)
		return;

	// Add labels
	if(blobs.size()) {
		m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->addLabels(blobs);
		blobs = m_frmOriginalImageView->m_ui.imageView->getImageViewItem()->getLabels();
		m_labelsForeground[m_currentPictureIndex] = blobs;
		updateLabelStatistics();
	}
}

void FrmMainWindow::loadFastERPipeline()
{
	// Get pipeline
	std::shared_ptr<SegmentationOperationList> segmentationPipeline = Settings::getSegmentationOperations();
	if(!segmentationPipeline)
		segmentationPipeline = std::make_shared<SegmentationOperationList>();

	// Change pipeline
	segmentationPipeline->clear();
	segmentationPipeline->operations.push_back(new SegmentationOperationBilateralFilter());
	SegmentationOperationFastER* faster = new SegmentationOperationFastER();
	segmentationPipeline->operations.push_back(faster);
	segmentationPipeline->operations.push_back(new SegmentationOperationFillHoles());
	segmentationPipeline->operations.push_back(new SegmentationOperationCountCells());
	segmentationPipeline->operations.push_back(new SegmentationOperationSaveSegmentation());
	segmentationPipeline->maskIndex = 0;

	// Make sure new pipeline is set
	Settings::setSegmentationOperations(segmentationPipeline);
}

void FrmMainWindow::showAboutWindow()
{
	FrmAbout aboutWindow;
	aboutWindow.exec();
}

void FrmMainWindow::keyReleaseEvent(QKeyEvent* event)
{
	// Ctrl+L: resize window to 1600:900 pixels
	if(event->key() == Qt::Key_L && event->modifiers() & Qt::ControlModifier) {
		int frameWidthX = frameGeometry().width() - width(),
			frameWidthY = frameGeometry().height() - height();
		resize(1600 - frameWidthX, 900 - frameWidthY);
	}
	else {
		QMainWindow::keyReleaseEvent(event);
	}
}

void FrmMainWindow::showQuickInstructions()
{
	// Show help window
	if(!m_frmQuickHelp)
		m_frmQuickHelp = new FrmHelp();

	m_frmQuickHelp->show();
	m_frmQuickHelp->raise();
}
