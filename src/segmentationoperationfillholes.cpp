/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationfillholes.h"

// OpenCV
#include "opencv2/core/core.hpp"

// Project
#include "exceptionbase.h"
#include "imageprocessingtools.h"
#include "jobsegmentation.h"
#include "logging.h"



void SegmentationOperationFillHoles::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	try {
		// Check if type is supported
		if(*imageDataType != IDT_UInt8Binary && *imageDataType != IDT_UInt32LabelMap)
			throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));

		if(*imageDataType == IDT_UInt8Binary) {
			// Run holes filling
			unsigned char* imageDataPtr = *imageData;
			QImage tmp = jobSegmentation->getImageProcessingTools()->fillHoles<unsigned char>(QImage(*imageData, sizeX, sizeY, sizeX, QImage::Format_Indexed8), 255, 0, jobSegmentation->getPixelConnectivity());

			// Replace imageData
			unsigned char* imDataNew = tmp.bits();
			int bytesPerLine = tmp.bytesPerLine();
			for(int y = 0; y < sizeY; ++y) {
				for(int x = 0; x < sizeX; ++x) {
					imageDataPtr[y*sizeX + x] = imDataNew[y*bytesPerLine + x];
				}
			}
		}
		else {
			// Fill holes with openCV: create binary segmentation mask (0 background, 1 foreground), fill background with 2, iterate over filled image
			// and look for pixels that are still 0 (i.e. belong to holes) and set their value in the original segmentation label map to the last found
			// label

			// Create image to flood
			unsigned int* imageDataPtr = reinterpret_cast<unsigned int*>(*imageData);
			cv::Mat cvImageDst(sizeY+2, sizeX+2, CV_8U, cv::Scalar(0));
			for(int y = 0; y < sizeY; ++y) {
				for(int x = 0; x < sizeX; ++x) {
					cvImageDst.at<unsigned char>(y+1, x+1) = (imageDataPtr[y*sizeX + x] > 0) ? 1 : 0;
				}
			}

			// Flood
			cv::floodFill(cvImageDst, cv::Point(0, 0), 2, 0, cv::Scalar(), cv::Scalar(), 8);

			// Iterate over pixels again and fill holes in imageDataPtr
			unsigned int lastObjectId = 0;
			for(int y = 0; y < sizeY; ++y) {
				for(int x = 0; x < sizeX; ++x) {
					if(cvImageDst.at<unsigned char>(y+1, x+1) == 0)
						imageDataPtr[y*sizeX + x] = lastObjectId;

					lastObjectId = imageDataPtr[y*sizeX + x];
				}
			}
		}
	}
	catch(cv::Exception& e) {
		throw ExceptionBase(__FUNCTION__, QString("OpenCV Exception: %1").arg(e.what()));
	}
}
