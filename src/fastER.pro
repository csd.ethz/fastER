#-------------------------------------------------
#
# Project created by QtCreator 2015-10-31T09:45:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fastER
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x
QMAKE_CXXFLAGS += -Wno-reorder
QMAKE_CXXFLAGS += -Wno-unused-parameter
QMAKE_CXXFLAGS += -Wno-sign-compare
INCLUDEPATH += /usr/include/libsvm
LIBS += `pkg-config --libs opencv`
LIBS += /usr/lib/x86_64-linux-gnu/libpng.so
LIBS += /usr/lib/x86_64-linux-gnu/libtiff.so
LIBS += /usr/lib/libsvm.so

SOURCES += \
    blob.cpp \
    cattiffhandler.cpp \
    changecursorobject.cpp \
    exceptionbase.cpp \
    experiment.cpp \
    experimentexternalindividualimages.cpp \
    experimenttat.cpp \
    fastdirectorylisting.cpp \
    frmabout.cpp \
    frmbasicsegmentationsettings.cpp \
    frmcolortransformation.cpp \
    frmconsole.cpp \
    frmhelp.cpp \
    frmimageviewsettings.cpp \
    frmimportexperimentdata.cpp \
    frmmainwindow.cpp \
    frmoriginalimageview.cpp \
    frmprogress.cpp \
    frmsegmentationpreview.cpp \
    frmselectexperiment.cpp \
    frmselectpositions.cpp \
    frmselectsegmentationoperation.cpp \
    frmstarttasks.cpp \
    imagedisplaysettings.cpp \
    imageprocessingtools.cpp \
    imageview.cpp \
    imageviewimageitem.cpp \
    job.cpp \
    jobrunner.cpp \
    jobrunnermaster.cpp \
    jobrunnersegmentation.cpp \
    jobsegmentation.cpp \
    jobset.cpp \
    jobsetsegmentation.cpp \
    logging.cpp \
    main.cpp \
    pictureindex.cpp \
    pngio.cpp \
    positioncluster.cpp \
    positiondescriptor.cpp \
    positiondisplay.cpp \
    positionthumbnail.cpp \
    faster.cpp \
    segmentationoperation.cpp \
    segmentationoperationbilateralfilter.cpp \
    segmentationoperationcountcells.cpp \
    segmentationoperationfillholes.cpp \
    segmentationoperationlist.cpp \
    segmentationoperationfaster.cpp \
    segmentationoperationsavesegmentation.cpp \
    segmentationoperationsizefilter.cpp \
    settings.cpp \
    tools.cpp \
    colortransformwidget.cpp

HEADERS  += \
    blob.h \
    cattiffhandler.h \
    changecursorobject.h \
    colortransformwidget.h \
    exceptionbase.h \
    experiment.h \
    experimentexternalindividualimages.h \
    experimenttat.h \
    fastdirectorylisting.h \
    frmabout.h \
    frmbasicsegmentationsettings.h \
    frmcolortransformation.h \
    frmconsole.h \
    frmhelp.h \
    frmimageviewsettings.h \
    frmimportexperimentdata.h \
    frmmainwindow.h \
    frmoriginalimageview.h \
    frmprogress.h \
    frmsegmentationpreview.h \
    frmselectexperiment.h \
    frmselectpositions.h \
    frmselectsegmentationoperation.h \
    frmstarttasks.h \
    imagedisplaysettings.h \
    imageprocessingtools.h \
    imageview.h \
    imageviewimageitem.h \
    job.h \
    jobrunner.h \
    jobrunnermaster.h \
    jobrunnersegmentation.h \
    jobsegmentation.h \
    jobset.h \
    jobsetsegmentation.h \
    logging.h \
    pictureindex.h \
    pngio.h \
    positioncluster.h \
    positiondescriptor.h \
    positiondisplay.h \
    positionthumbnail.h \
    faster.h \
    faster_defs.h \
    segmentationoperation.h \
    segmentationoperationbilateralfilter.h \
    segmentationoperationcountcells.h \
    segmentationoperationfillholes.h \
    segmentationoperationlist.h \
    segmentationoperationfaster.h \
    segmentationoperationsavesegmentation.h \
    segmentationoperationsizefilter.h \
    settings.h \
    tools.h

FORMS    += \
    frmAbout.ui \
    frmBasicSegmentationSettings.ui \
    frmConsole.ui \
    frmDisplaySettings.ui \
    frmHelp.ui \
    frmImageViewSettings.ui \
    frmImportExperimentData.ui \
    frmMainWindow.ui \
    frmOriginalImageView.ui \
    frmProgress.ui \
    frmSegmentationPreview.ui \
    frmSelectExperiment.ui \
    frmSelectPositions.ui \
    frmSelectSegmentationOperation.ui \
    frmStartTasks.ui

RESOURCES += \
    resources.qrc
