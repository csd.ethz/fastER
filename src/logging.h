/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef logging_h__
#define logging_h__

// Qt
#include <QString>
#include <QElapsedTimer>

// For debug use
extern QElapsedTimer gDbgTimer;

/**
 * A logging device.
 */
class Logging {

public:

	/**
	 * Type of logging device.
	 */
	enum Type {
		LOG_ERROR,
		LOG_DBG,
		LOG_STANDARD
	};

	/**
	 * Constructor.
	 */
	Logging(Type type);

	/**
	 * Logging functions.
	 */
	Logging& operator<<(const QString& s);
	Logging& operator<<(const char* s);
	Logging& operator<<(char ch);
	Logging& operator<<(short s);
	Logging& operator<<(signed int i);
	Logging& operator<<(unsigned int i);
	Logging& operator<<(long long i);
	Logging& operator<<(unsigned long long i);
	Logging& operator<<(float f);
	Logging& operator<<(double d);
	Logging& operator<<(const QStringList& s);

private:

	Type m_type;

};

extern Logging gDbg;
extern Logging gLog;
extern Logging gErr;


#endif // logging_h__
