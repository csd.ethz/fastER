/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef experiment_h__
#define experiment_h__

// Project
#include "pictureindex.h"
#include "tools.h"
#include "fastdirectorylisting.h"

// STL
#include <vector>
#include <cassert>
#include <memory>

// Qt
#include <QHash>
#include <QPointF>
#include <QRectF>
#include <QString>
#include <QVector>
#include <QImage>
#include <QDir>
#include <QSharedPointer>
#include <QDateTime>
#include <QProgressDialog>
#include <QSet>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

class QXmlStreamReader;
class QXmlStreamAttributes;
class PositionCluster;


/**
 * Abstract base class for classes providing experiment data, i.e. image data, 
 * background image data, segmentation image data, etc. by image index, and meta
 * information (available channels, micrometer per pixel, etc.).
 */
class Experiment : public QObject {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	Experiment(const QString& path)
		: m_path(QDir::fromNativeSeparators(path))
	{
		// Make sure path is valid and ends with "/"
		if(m_path.size() == 0)
			throw ExceptionBase(__FUNCTION__, "No experiment path specified.");
		if(m_path.right(1) != "/")
			m_path += '/';

		// Extract name, i.e. folder name
		int baseStart = m_path.lastIndexOf("/", -2) + 1;
		m_name = m_path.mid(baseStart);
		m_name = m_name.left(m_name.length() - 1);
		if(m_name.isEmpty())
			throw ExceptionBase(__FUNCTION__, "Specified experiment is invalid.");
	}

	/**
	 * Virtual destructor (required to enable deletion of instances of derived classes using pointer to base class).
	 */
	virtual ~Experiment()
	{}

	/**
	 * @return if experiment is null.
	 */
	bool isNull() const 
	{
		return !m_name.isEmpty();
	}

	/**
	 * @return experiment name.
	 */
	QString getName() const 
	{
		return m_name;
	}

	/**
	 * @return experiment path.
	 */
	QString getPath() const 
	{
		return m_path;
	}

	/**
	 * @return list with all position numbers, sorted ascending.
	 */
	const QList<int>& getPositionNumbers() const
	{
		return m_positionNumbers;
	}

	/**
	 * Load image data (8-bit grayscale)/check if image exists.
	 * @return 0 if successful, 1 if file was not found, 2 if file was found but could not be read.
	 */
	virtual int getImage(const PictureIndex& pi, QImage& outImage) = 0;
	virtual int getImage(const PictureIndex& pi, cv::Mat& outImage) = 0;
	virtual bool hasImage(const PictureIndex& pi) const = 0;

	/**
	 * Get experiment relative path of image (i.e. file name of image file, image stack, movie, etc.) 
	 * if supported (returns empty string otherwise). Returned file name may be equal
	 * for different values of pi (e.g. if images are stored in tiff stack). 
	 * Append to experiment path to get full path of image file.
	 */
	virtual QString getImageFileNameRelative(const PictureIndex& pi, bool includeFileExtension = true) const = 0;

	/**
	 * Get PictureIndex a randomly selected image (considering only selected positions).
	 * @return valid PictureIndex if successful.
	 */
	virtual PictureIndex getRandomImage(bool allowDifferentChannel) = 0;

	/**
	 * Load background and gain image data (currently always 16-bit grayscale, little Endian).
	 * @param pi PictureIndex of image for which gain should be obtained, currently only position and wavelength are considered.
	 * @return 0 if successful, 1 if file was not found, 2 if file was found but could not be read.
	 */
	virtual int getBackgroundImage(const PictureIndex& pi, std::shared_ptr<unsigned char>& outImage, int& outBitDepth, int& outWidth, int& outHeight) = 0;
	virtual int getGainImage(const PictureIndex& pi, bool loadIfNecessary, std::shared_ptr<unsigned char>& outImag, int& outBitDepth, int& outWidth, int& outHeight) = 0;
	
	/**
	 * Load segmentation image data (binary image).
	 * @return 0 if successful, 1 if file was not found, 2 if file was found but could not be read, 3  if segmentation image has unexpected size (i.e. different from expectedSizeX and expectedSizeY).
	 */
	virtual int getSegmentationData(const PictureIndex& pi, std::vector<Blob>& outSegmentationData, int segmentationMethod, int expectedSizeX, int expectedSizeY) = 0;

	/**
	 * Write segmentation data using QImage.
	 * @return 0 if successful, 1 if image could not be saved, 2 if output folder could not be created.
	 */
	virtual int writeSegmentationData(const PictureIndex& pi, const QImage& maskImage, int segmentationMethod = 0) = 0;

	/**
	 * Write segmentation data using 16 bit label map.
	 * @return 0 if successful, 1 if image could not be saved, 2 if output folder could not be created.
	 */
	virtual int writeSegmentationData(const PictureIndex& pi, unsigned short* labelMap, int sizeX, int sizeY, int segmentationMethod = 0) = 0;

	/**
	 * Save quantification data corresponding to segmentation with specified method of specified PictureIndex.
	 * @return 0 if successful, 1 if image could not be saved, 2 if output folder could not be created.
	 */
	virtual int writeSegmentationQuantification(const PictureIndex& pi, const QString& quantificationData, int segmentationMethod = 0) = 0;

	/**
	 * Read quantification data corresponding to segmentation with specified method of specified PictureIndex.
	 * @return 0 if successful, error code otherwise (1: no quantification file found; other: internal error code).
	 */
	virtual int readSegmentationQuantification(const PictureIndex& pi, QString& outData, int segmentationMethod = 0) = 0;

	/**
	 * Read segmentation quantification data for multiple frames.
	 * @return number of successfully parsed files.
	 */
	int readSegmentationQuantificationForMultipleFrames(const QVector<PictureIndex>& pis, int segmentationMethod, QHash<PictureIndex, QVector<QString>>& outColumns, QHash<PictureIndex, QVector<QVector<QVariant>>>& outMatrices, int& outMaxNumRows, QSet<QString>& outAllRows, QStringList& outErrors, int& outNumberOfImagesWithoutQuantification, QProgressDialog* progressBar = 0);


	/**
	 * @return if micro meter per pixel are set.
	 */
	virtual bool supportsMicroMeterCoordinates() const
	{
		return false;
	}

	/**
	 * @param PictureIndex index of picture for which value should be obtained.
	 * @return micro meter per pixel, or 0.0f if not supported.
	 */
	virtual float getMicroMetersPerPixel(const PictureIndex& pi) const
	{
		return 0.0f;
	}

	/**
	 * @return if global (rather than image local) coordinates are supported.
	 */
	virtual bool supportsGlobalCoordinates() const
	{
		return false;
	}

	/**
	 * Get if inverted coordinates: x increases to the left and y increases to the top, but position offsets are still top/left, so image local coordinates
	 * need to be subtracted from position offset to get global coordinates, compare transformLocalToGlobalCoords().
	 * @return if coordinates are inverted.
	 */
	virtual bool getCoordinatesInverted() const
	{
		return false;
	}

	/**
	 * Set if inverted coordinates.
	 */
	virtual void setCoordinatesInverted(bool inverted)
	{}

	/**
	 * @return global coordinates bounding rectangle or a null-rectangle if not supported.
	 */
	virtual QRectF getCoordinateBoundaries() const 
	{
		return QRectF();
	}

	/**
	 * @return global offset of image. Always returns (0,0) if not supported. Usually only depends on position number.
	 */
	virtual QPointF getImageOffset(const PictureIndex& pi) const
	{
		return QPointF();
	}

	/**
	 * @return global image rect of image (usually identical for all images of a position). Returns invalid rectangle if not supported.
	 */
	virtual QRectF getGlobalImageRect(const PictureIndex& pi) const
	{
		// Get image size
		float w = getImageSizeX(pi);
		float h = getImageSizeY(pi);

		// Convert to micrometer
		float mmpp = getMicroMetersPerPixel(pi);
		if(mmpp > 0.0f) {
			w *= mmpp;
			h *= mmpp;
		}

		// Convert to global
		QPointF offset = getImageOffset(pi);
		if(!offset.isNull()) {	
			if(getCoordinatesInverted()) 
				return QRectF(offset.x() - w, offset.y() - h, w, h);
			else
				return QRectF(offset.x(), offset.y(), w, h);
		}
		
		// No global coordinates supported
		return QRectF(0, 0, w, h);
	}

	/**
	 * Transform picture local pixel coordinates to experiment global micrometer coordinates and vice versa (usually identical for all images of a position); must be thread-safe.
	 */
	QPointF transformLocalToGlobalCoords(const QPointF& _coords, const PictureIndex& pi) const
	{
		QPointF pointGlobal(_coords);

		// Convert to micrometer
		float mmpp = getMicroMetersPerPixel(pi);
		if(mmpp > 0.0f)
			pointGlobal *= mmpp;

		// Convert to global
		QPointF offset = getImageOffset(pi);
		if(!offset.isNull()) {
			if(getCoordinatesInverted()) 
				pointGlobal = offset - pointGlobal;
			else 
				pointGlobal += offset;
		}

		return pointGlobal;
	}
	QPoint transformGlobalToLocalCoords(const QPointF& _coords, const PictureIndex& pi) const
	{
		QPointF pointLocal(_coords);

		// Transform to image local
		QPointF offset = getImageOffset(pi);
		if(!offset.isNull()) {
			if(getCoordinatesInverted()) 
				pointLocal = offset - pointLocal;
			else 
				pointLocal -= offset;
		}

		// Convert to pixels
		float mmpp = getMicroMetersPerPixel(pi);
		if(mmpp > 0)
			pointLocal /= mmpp;

		// Round to integer
		return pointLocal.toPoint();
	}

	/**
	 * Get a list of all existing pictures for the given position. May take some time for large positions.
	 */
	virtual std::vector<PictureIndex> getAllPictureIndexesInPosition(int position) = 0;

	/**
	 * @return minimal/maximal PictureIndex of experiment.
	 * Returned PictureIndex is a combination of minimal/maximal time point, channel, etc., so no image necessarily exists there.
	 * Value only considers positions of which images have been listed so far.
	 */
	virtual PictureIndex getMinPictureIndex() const = 0;
	virtual PictureIndex getMaxPictureIndex() const = 0;

	/**
	 * @param PictureIndex index of picture for which size should be obtained (if invalid PictureIndex is provided, size of a w00 image is returned, assuming sizes are constant for all images).
	 * @return image size in pixels by channel.
	 */
	virtual int getImageSizeX(const PictureIndex& pi = PictureIndex()) const = 0;
	virtual int getImageSizeY(const PictureIndex& pi = PictureIndex()) const = 0;

	/**
	 * Get position clusters (sets of overlapping positions).
	 */
	virtual const std::vector<PositionCluster*> getPositionClusters() const
	{
		return std::vector<PositionCluster*>();
	}

	/**
	 * @return positions overlapping with image pi in the global coordinate system (at the time point specified in pi).
	 */
	virtual QVector<int> getOverlappingPositions(const PictureIndex& pi) const
	{
		return QVector<int>();
	}

	/**
	 * Convert image file name to PictureIndex (for experiments supporting this).
	 * Not all experiments store frames in individual files, therefore the result
	 * of this function may be unexpected.
	 * @return invalid PictureIndex if not supported.
	 */
	virtual PictureIndex converFileNameToPictureIndex(const QString& fileName) const
	{
		return PictureIndex();
	}

	/**
	 * Delete all existing tree fragments. Default implementation deletes csv files in target directory.
	 * Can throw.
	 */
	virtual void deleteExistingTreeFragments()
	{
		// List csv files and delete them
		QString dir = m_path + "TTTrack/TreeFragments/";
		if(QDir(dir).exists()) {
			QStringList fileList = FastDirectoryListing::listFiles(dir, QStringList() << ".csv");
			for(auto itFile = fileList.begin(); itFile != fileList.end(); ++itFile) {
				QFile f(dir + *itFile);
				if(!f.remove())
					throw ExceptionBase(__FUNCTION__, QString("Cannot remove existing file '%1': %2").arg(dir + *itFile).arg(f.errorString()));
			}
		}
		m_treeFragmentsCounter.clear();
	}

	/**
	 * Get if real time information is supported by experiment (does not mean that it is actually available).
	 */
	virtual bool supportsRealTime() const
	{
		return false;
	}

	/**
	 * Get real time at which an image was acquired using UTC time.
	 * Thread safe.
	 * @return 0 if successful, or error code otherwise: 
	 *		-1: real time is not supported by experiment type
	 *		 2: real time information not available (but would be supported by experiment)
	 *		 3: real time information available, but could not be read
	 *		 4: real time information available, but not for pi
	 *		>4: experiment type specific error codes
	 */
	virtual int getImageAcquisitionTime(const PictureIndex& pi, QDateTime& outAcquisitionTime) 
	{
		outAcquisitionTime = QDateTime();
		return -1;
	}

	/**
	 * Get acquisition time relative to image that was acquired first in milliseconds.
	 * Time information may be loaded on request, in which case the function will need some time.
	 * Thread safe.
	 * @return time (value >= 0) if successful, or error code otherwise: 
	 *		-1: real time is not supported by experiment type
	 *		-2: real time information not available
	 *		-3: real time information available, but could not be read
	 *		-4: real time information available, but not for pi
	 *		<-4: experiment type specific error codes
	 */
	virtual qint64 getImageAcquisitionTimeRelative(const PictureIndex& pi) 
	{
		return -1;
	}

	/**
	 * Update micrometer per pixel, e.g. after relevant setting was changed.
	 * @return true if updated successfully
	 */
	virtual bool updateMicroMeterPerPixel(double microMeterPerPixel = 0.0)
	{
		// Not supported
		return false;
	}

protected:

	// Experiment name, usually equals folder name
	QString m_name;

	// Path of experiment
	QString m_path;

	// Position numbers (must be sorted in ascending order)
	QList<int> m_positionNumbers;

	// Tree fragments count by position
	QVector<int> m_treeFragmentsCounter;
};


#endif // experiment_h__
