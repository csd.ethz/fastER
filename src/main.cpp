/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef _DEBUG
//#include "vld.h"
#endif

// Qt
#include <QApplication>
#include <QMessageBox>
#include <QFile>

// Project
#include "frmmainwindow.h"
#include "logging.h"
#include "settings.h"

//// ITK
//#include "itkMetaImageIOFactory.h"
//#include "itkPNGImageIOFactory.h"

// STL
#include <ctime>


int main(int argc, char *argv[])
{

	// Init random number generator
	qsrand(time(0));
	
	// Program start
	int ret = 0;
	try {
		QApplication a(argc, argv);

		// Set this for QSettings use
		QCoreApplication::setOrganizationName("Oliver Hilsenbeck (oliver.hilsenbeck@bsse.ethz.ch)");
		QCoreApplication::setApplicationName("fastER");

		// Show window
		FrmMainWindow mainWindow;
		mainWindow.showMaximized();
		ret = a.exec();

		// Cleanup
		Settings::cleanUp();
	}
	catch(...) {
		QMessageBox::critical(0, "Critical error", "Unhandled exception.");
	}
	return ret;
}
