/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef segmentationoperationsizefilter_h__
#define segmentationoperationsizefilter_h__


#include "segmentationoperation.h"
#include "logging.h"


/**
 * Filter out blobs that are too small or too big from binary image.
 */
class SegmentationOperationSizeFilter : public SegmentationOperation {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	SegmentationOperationSizeFilter(int _minSize = 10, int _maxSize = 1000)
		: minSize(_minSize), maxSize(_maxSize)
	{}

	/**
	 * Reimplemented.
	 */
	void run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner);

	/**
	 * Reimplemented.
	 */
	QString getName() const
	{
		return "Size Filter";
	}

	/**
	 * Reimplemented.
	 */
	QHash<QString, QVariant> getParameters() const
	{
		QHash<QString, QVariant> params;
		params.insert("minSize", minSize);
		params.insert("maxSize", maxSize);

		return params;
	}

	/**
	 * Reimplemented.
	 */
	void setParameters(const QHash<QString, QVariant>& parameters)
	{
		// Change parameters
		for(auto it = parameters.begin(); it != parameters.end(); ++it) {
			if(it.key() == "minSize" && it.value().type() == QVariant::Int) 
				minSize = it.value().toInt();
			else if(it.key() == "maxSize" && it.value().type() == QVariant::Int)
				maxSize = it.value().toInt();
			else
				gDbg << __FUNCTION__ << " - invalid call to setParameter() - key: " << it.key() << " - value: " << it.value().toString() << "\n";
		}
	}

	// Parameters
	int minSize, maxSize;

};


#endif // segmentationoperationsizefilter_h__
