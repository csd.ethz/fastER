/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "imageview.h"

// Qt
#include <QWheelEvent>
#include <QScrollBar>
#include <QApplication>
#include <QDebug>

// Project
#include "logging.h"
#include "imageviewimageitem.h"



const float ImageView::MIN_SCALING = 0.5f;
const float ImageView::MAX_SCALING = 20.0f;



ImageView::ImageView( QWidget* parent /*= 0*/ )
	: QGraphicsView(parent)
{
	m_scaling = 1.0;
	m_scrolling = false;
	m_allowPainting = false;
	m_ignoreScrollEvents = false;
	m_messageItem = 0;

	// Create scene and image item
	setScene(new QGraphicsScene());
	m_imageItem = new ImageViewImageItem(this);
	scene()->addItem(m_imageItem);
	scene()->setFocusItem(m_imageItem);

	// Make zoom like in Google maps
	setTransformationAnchor(AnchorUnderMouse);

	//setAlignment(Qt::AlignLeft | Qt::AlignTop);
}

ImageView::~ImageView() 
{
	//scene()->removeItem(m_imageItem);
	//delete m_imageItem;
	QGraphicsScene* scenePtr = scene();
	setScene(nullptr);
	delete scenePtr;
}

void ImageView::wheelEvent( QWheelEvent *event )
{
	// Zoom (only if no message is shown)
	if(!(event->modifiers() & Qt::ControlModifier) && (!m_messageItem || !m_messageItem->isVisible())) {
		// Calculate scaling factor
		float scaleFactor = pow((double)2, event->delta() / 400.0);
		if(m_scaling * scaleFactor < MIN_SCALING)
			scaleFactor = MIN_SCALING / m_scaling;
		if(m_scaling * scaleFactor > MAX_SCALING)
			scaleFactor = MAX_SCALING / m_scaling;

		// Change scaling
		m_ignoreScrollEvents = true;
		scale(scaleFactor, scaleFactor);
		m_ignoreScrollEvents = false;
		m_scaling *= scaleFactor;
		emit zoomChanged(m_scaling, getCurrentScrollPosition());
	}
	else {
		event->ignore();
		QGraphicsView::wheelEvent(event);	// Conflicts with zoom
	}
}

void ImageView::mousePressEvent( QMouseEvent *event )
{
	// Navigation (only if no message is shown)
	if((event->button() == Qt::RightButton || event->button() == Qt::MiddleButton) && event->modifiers() == Qt::NoModifier && (!m_messageItem || !m_messageItem->isVisible())) {
		m_mouseMoveStartX = event->globalX();
		m_mouseMoveStartY = event->globalY();

		m_scrolling = true;
	}
	else
		event->ignore();

	QGraphicsView::mousePressEvent(event);
}

void ImageView::mouseReleaseEvent( QMouseEvent *event )
{
	// Navigation
	if(m_scrolling) {
		m_scrolling = false;
	}
	else
		event->ignore();

	QGraphicsView::mouseReleaseEvent(event);
}

void ImageView::mouseMoveEvent( QMouseEvent *event )
{


	// Scrolling
	if(m_scrolling) {
		// Get mouse pos
		int x = event->globalX(),
			y = event->globalY();

		// Get delta
		int deltaX = m_mouseMoveStartX - x,
			deltaY = m_mouseMoveStartY - y;

		// Get new scrollbar positions
		int scrollX = horizontalScrollBar()->value() + deltaX,
			scrollY = verticalScrollBar()->value() + deltaY;

		// Make sure new values are valid
		scrollX = std::max(scrollX, horizontalScrollBar()->minimum());
		scrollX = std::min(scrollX, horizontalScrollBar()->maximum());
		scrollY = std::max(scrollY, verticalScrollBar()->minimum());
		scrollY = std::min(scrollY, verticalScrollBar()->maximum());

		// Set scrollbar values
		if(scrollX != horizontalScrollBar()->value())
			horizontalScrollBar()->setValue(scrollX);
		if(scrollY != verticalScrollBar()->value())
			verticalScrollBar()->setValue(scrollY);

		// Set start positions
		m_mouseMoveStartX = x;
		m_mouseMoveStartY = y;
	}
	else 
		event->ignore();

	QGraphicsView::mouseMoveEvent(event);
}

void ImageView::setZoom( float scaling, QPointF upperLeftPositionScene, bool emitSignal )
{
	//qDebug() << (void*)this << ": setZoom(scaling=" << scaling << ", upperLeftPositionScene=" << upperLeftPositionScene << ") - " << m_scalingOnMessageHide;

	// Make sure value is ok
	scaling = std::max(scaling, MIN_SCALING);
	scaling = std::min(scaling, MAX_SCALING);

	// Do not apply immediately, if message is shown
	if(m_messageItem && m_messageItem->isVisible()) {
		m_scalingOnMessageHide = scaling;
		m_scrollPositionOnMessageHide = upperLeftPositionScene;

		return;
	}

	// Change zoom
	float scaleFactor = scaling / m_scaling;
	m_ignoreScrollEvents = true;
	scale(scaleFactor, scaleFactor);
	m_ignoreScrollEvents = false;
	m_scaling *= scaleFactor;

	if(emitSignal)
		emit zoomChanged(m_scaling, getCurrentScrollPosition());

	// Change scroll position
	//QApplication::processEvents();
	setScrollPosition(upperLeftPositionScene);
}

//QCursor ImageView::createBrushCursor( int diameter, QColor color, float scaling )
//{
//	// Set some transparency
//	color.setAlpha(ImageViewImageItem::MARKINGS_TRANSPARENCY);
//
//	// Create pixmap with circle
//	QPixmap pixmap(diameter+2, diameter+2);
//	pixmap.fill(Qt::transparent);
//	QPainter p(&pixmap);
//	p.setPen(QPen(color, 1, Qt::SolidLine, Qt::RoundCap));
//	p.setBrush(QBrush(color));
//	p.setCompositionMode(QPainter::RasterOp_SourceOrDestination);
//	if(diameter > 0)
//		p.drawEllipse(0, 0, diameter, diameter);
//	else
//		p.drawPoint(0, 0);
//	p.end();
//
//	// Scale
//	pixmap = pixmap.transformed(QTransform::fromScale(scaling, scaling));
//
//	return QCursor(pixmap);
//}

void ImageView::setPaintable( bool paintable )
{
	m_allowPainting = paintable;
	m_imageItem->updateCursor();

	if(paintable)
		setCursor(Qt::CrossCursor);
	else
		unsetCursor();
}

void ImageView::setScrollPosition( QPointF upperLeftPositionScene, bool emitSignal )
{
	//qDebug() << (void*)this << ": setScrollPosition(upperLeftPositionScene=" << upperLeftPositionScene << ")";

	// Do not apply immediately, if message is shown
	if(m_messageItem && m_messageItem->isVisible()) {
		m_scrollPositionOnMessageHide = upperLeftPositionScene;

		return;
	}

	// Map to viewport position to calculate delta
	QPoint viewPortPosition = mapFromScene(upperLeftPositionScene);
	if(!viewPortPosition.isNull()) {
		viewPortPosition.rx() += width() / 2;
		viewPortPosition.ry() += height() / 2;
		m_ignoreScrollEvents = true;
		centerOn(mapToScene(viewPortPosition));
		m_ignoreScrollEvents = false;
	}

	if(emitSignal)
		emit scrollingChanged(upperLeftPositionScene);
}

void ImageView::scrollContentsBy( int dx, int dy )
{
	//qDebug() << (void*)this << ": scrollContentsBy(dx=" << dx << ", dy=" << dy << ")";

	// Ignore, if message is shown
	if(m_messageItem && m_messageItem->isVisible())
		return;

	// Scroll
	QGraphicsView::scrollContentsBy(dx, dy);

	// Inform connected views
	if(!m_ignoreScrollEvents)
		emit scrollingChanged(mapToScene(0, 0));
}

void ImageView::displayBrushCursor( int posX, int posY, QGraphicsPixmapItem* brush )
{
	// Delegate
	if(m_imageItem)
		m_imageItem->displayBrushCursor(posX, posY, brush);
}


void ImageView::showMessage(const QString& what, bool whatIsHtml)
{
	// Init variables for restoring scaling and scrolling, if message was not shown already
	if(!m_messageItem || !m_messageItem->isVisible()) {
		m_scrollPositionOnMessageHide = getCurrentScrollPosition();
		m_scalingOnMessageHide = m_scaling;
	}

	// Create message item, if needed
	if(!m_messageItem) {
		m_messageItem = new QGraphicsTextItem();
		m_messageItem->setFlag(QGraphicsItem::ItemIgnoresTransformations);
		m_messageItem->setFont(QFont("Arial", 13, 3));
		scene()->addItem(m_messageItem);
	}

	// Show text
	if(whatIsHtml)
		m_messageItem->setHtml(what);
	else
		m_messageItem->setPlainText(what);
	upatePositionOfMessageItem();
	m_messageItem->show();

	// Hide image
	if(m_imageItem)
		m_imageItem->hide();
}

void ImageView::hideMessage()
{
	// Do nothing if message is not shown
	if(!m_messageItem || !m_messageItem->isVisible())
		return;

	// Hide text
	m_messageItem->hide();

	// Show image
	if(m_imageItem)
		m_imageItem->show();

	// Restore zoom and scrolling
	setZoom(m_scalingOnMessageHide, m_scrollPositionOnMessageHide, false);
}

void ImageView::resizeEvent(QResizeEvent *event)
{
	// Update position of message item
	if(m_messageItem && m_messageItem->isVisible()) {
		upatePositionOfMessageItem();
	}
}

void ImageView::upatePositionOfMessageItem()
{
	assert(m_messageItem);

	// Put in middle of view
	QPointF scenePos = mapToScene(width()/2, height()/2);
	scenePos.setX(scenePos.x() - m_messageItem->boundingRect().width()/2);
	scenePos.setY(scenePos.y() - m_messageItem->boundingRect().height()/2);
	m_messageItem->setPos(scenePos);
}
