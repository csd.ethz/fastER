/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "fastdirectorylisting.h"


// This code will not work if windows api expects UNICODE strings (TCHAR becomes WCHAR)
#ifdef UNICODE
	#error this file must be compiled without the UNICODE symbol!
#endif

#ifdef Q_OS_WIN
#include <Windows.h>
#else
#pragma message ( "Warning: FastDirectoryListing not supported for compiler - using slower default Qt directory listing." )
#endif

// Qt Includes
#include <QDebug>



QStringList FastDirectoryListing::listFiles( QDir _path, QStringList _fileExtensions )
{
#ifdef Q_OS_WIN
	// Check if dir exists
	if(!_path.exists())
		return QStringList();

	// Convert extensions to lower case
	for(QStringList::iterator it = _fileExtensions.begin(); it != _fileExtensions.end(); ++it)
		*it = it->toLower();

	// Extract path as native ascii string
	QByteArray tmp = QDir::toNativeSeparators(_path.absolutePath()).toLatin1();

	// Append '\'
	if(tmp.right(1) != "\\")
		tmp += '\\';

	// Append '*'
	tmp += '*';

	// Note: path becomes invalid as soon as tmp gets destroyed
	const char* path = tmp.constData();

	// Make sure path is shorter than MAX_PATH
	if(tmp.length() >= MAX_PATH) {
		qWarning() << "listFiles(): Error 1";
		return listFilesWithQDir(_path, _fileExtensions);
	}

	// Api data
	WIN32_FIND_DATA findData;
	HANDLE hFindFile;

	// Return value
	QStringList ret;

	// Current file
	QByteArray curFile;

	// Find first file
	hFindFile = FindFirstFileEx(path, FindExInfoStandard, &findData, FindExSearchNameMatch, NULL, FIND_FIRST_EX_LARGE_FETCH);
	if(hFindFile == INVALID_HANDLE_VALUE) {
		qWarning() << "listFiles(): Error 2 - " << path;
		return listFilesWithQDir(_path, _fileExtensions);
	}

	// Add first file if it is no directory and has the right extension
	curFile = findData.cFileName;
	if(!(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && checkFileExtension(curFile, _fileExtensions))
		ret.append(findData.cFileName);

	// Find next files
	while(FindNextFile(hFindFile, &findData)) {
		// Add next file if it is no directory and has the right extension
		curFile = findData.cFileName;
		if(!(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && checkFileExtension(curFile, _fileExtensions))
			ret.append(findData.cFileName);
	}
	
	return ret;
#else
	// No Windows - use Qt
	return listFilesWithQDir(_path, _fileExtensions);
#endif
}

QStringList FastDirectoryListing::listFilesWithQDir( QDir _path, QStringList _fileExtensions )
{
	// Convert _fileExtensions to QDir::entryList filters
	for(int i = 0; i < _fileExtensions.size(); ++i) 
		_fileExtensions[i] = "*" + _fileExtensions[i];

	// Use QDir::entryList
	return _path.entryList(_fileExtensions, QDir::Files, QDir::Name);
}

bool FastDirectoryListing::checkFileExtension( const QByteArray& _fileName, const QStringList& _fileExtensions )
{
	// Return true if no extensions are given
	if(_fileExtensions.size() == 0)
		return true;

	// Iterate over all provided file extensions
	for(int i = 0; i < _fileExtensions.size(); ++i) {
		// Get provided file extension
		const QString curExt = _fileExtensions[i];

		// Get file extension of _fileName
		const QString curFileExt = _fileName.right(curExt.length()).toLower();
		if(curFileExt == curExt)
			return true;
	}

	return false;
}
