/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segmentationoperationfaster.h"

// Project
#include "faster.h"
#include "exceptionbase.h"
#include "jobsegmentation.h"
#include "settings.h"
#include "tools.h"

// Qt
#include <QDebug>

// OpenCV
//#include <opencv2/highgui/highgui.hpp>

void SegmentationOperationFastER::run(unsigned char** imageData, ImageDataType* imageDataType, const unsigned char* originalImageData, int sizeX, int sizeY, JobSegmentation* jobSegmentation, JobRunnerSegmentation* callingJobRunner)
{
	// Currently only one data type is supported
	if(*imageDataType != IDT_UInt8Gray)
		throw ExceptionBase(__FUNCTION__, QString("Unexpected image data type: %1").arg(*imageDataType));

	// Get SVM model (if ready, otherwise all regions within size limits will be returned)
	const svm_model* model = nullptr;
	if(Settings::svmReady()) {
		model = Settings::getSvmModel();
	}
	const double* mean = Settings::getSvmStatsMean();
	const double* stdDev = Settings::getSvmStatsStandardDeviation();


	try {
		//gDbg << "Executing fastER: " << minSize << " - " << maxSize << " - " << darkOnBright << " - " << imageData[0] << ", " << imageData[1] << ", " << imageData[2] << " - " << originalImageData[0] << ", " << originalImageData[1] << ", " << originalImageData[2] << "\n";

		// Get constrained positive regions, if desired
		std::unordered_set<faster::RegionID, faster::RegionIDHasher> empty;
		PictureIndex pi = jobSegmentation->getPictureIndex();
		std::unordered_set<faster::RegionID, faster::RegionIDHasher>& positiveConstraintsRef = constrainPosSamples ? Settings::getConstrainedRegionsPositive(pi) : empty;

		// Segment image
		faster::FastERDetector* detector = jobSegmentation->getFastERDetector();
		int numRegions = detector->segmentImage(model, mean, stdDev, *imageData, sizeX, sizeY, darkOnBright, minSize, maxSize, minProbability, positiveConstraintsRef, disableErSplitting);

		// Paint segmentation: use label map unless disableErSplitting is enabled
		if(disableErSplitting) {

			// Reset *imageData
			unsigned char* outputImage = *imageData;
			memset(outputImage, 0, sizeX*sizeY);

			// Add regions to outputImage
			for(int iSeed = 0; iSeed < numRegions; ++iSeed) {
				bool regionTouchesImageBorder;
				auto pixels = detector->fillRegion(iSeed, darkOnBright, &regionTouchesImageBorder);
			
				// Skip regions touching image border
				if(regionTouchesImageBorder)
					continue;

				// Set pixels in outputImage
				for(auto px = pixels.begin(); px != pixels.end(); ++px) {
#ifdef _DEBUG
					// Ensure that each pixel (except the first) has a neighbor, if regions contain more than one pixel
					if(px != pixels.begin()) {
						int y = *px / sizeX;
						int x = *px % sizeX;
						assert(outputImage[y*sizeX + x - 1] == 255 || outputImage[y*sizeX + x + 1] == 255 || outputImage[(y+1)*sizeX + x] == 255 || outputImage[(y-1)*sizeX + x] == 255);
					}
#endif
					outputImage[*px] = 255;
				}
			}

			//// Test
			//QImage imgTest(*imageData, sizeX, sizeY, QImage::Format_Indexed8);
			//imgTest.setColorTable(Tools::getColorTableGrayIndexed8());
			//QImage imgTest2 = imgTest.copy();
			//imgTest2.save("C:\\Tmp\\tmp_(delete)\\test.png");


			// Change image data type 
			*imageDataType = IDT_UInt8Binary;
		}
		else {

			// Create new label map image
			unsigned int* outputImage = new unsigned int[sizeX*sizeY];
			std::fill(outputImage, outputImage + sizeX*sizeY, 0);

			// Add regions
			for(int iSeed = 0; iSeed < numRegions; ++iSeed) {

				// Skip regions touching image border
				bool regionTouchesImageBorder;
				auto pixels = detector->fillRegion(iSeed, darkOnBright, &regionTouchesImageBorder);
				if(regionTouchesImageBorder)
					continue;
				for(auto px = pixels.begin(); px != pixels.end(); ++px) 
					outputImage[*px] = iSeed + 1;
			}

			// Change image data and data type (cvImage: no deep copy)
			*imageDataType = IDT_UInt32LabelMap;
			*imageData = reinterpret_cast<unsigned char*>(outputImage);
		}
	}
	catch(faster::FastERException& e) {
		throw ExceptionBase(__FUNCTION__, QString("fastER Exception: %1").arg(e.getType()));
	}
}
