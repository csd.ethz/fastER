/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef blob_h__
#define blob_h__

// STL
#include <vector>
#include <cmath>

// Qt
#include <QRect>
#include <QDataStream>
#include <QVector>

// OpenCV
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// Project
#include "exceptionbase.h"


/**
 * One blob (connected component).
 */
class Blob {

public:

	// Pixel type
	typedef cv::Point2i pixel_type;

	/**
	 * Construct empty blob.
	 */
	Blob() 
		: m_pixelsSorted(false),
		m_contoursCalculated(false),
		m_areaConvex(0.0),
		m_centroidXLocal(0.0),
		m_centroidYLocal(0.0),
		m_centroidXGlobal(0.0),
		m_centroidYGlobal(0.0),
		m_radius(0.0),
		m_radiusGlobal(0.0)
	{}

	/**
	 * Construct by copying pixels.
	 */
	Blob(const std::vector<pixel_type>& pixels) 
		: m_pixels(pixels),
		m_pixelsSorted(false),
		m_contoursCalculated(false),
		m_areaConvex(0.0),
		m_centroidXLocal(0.0),
		m_centroidYLocal(0.0),
		m_centroidXGlobal(0.0),
		m_centroidYGlobal(0.0),
		m_radius(0.0),
		m_radiusGlobal(0.0)
	{}

	/**
	 * Construct by moving pixels.
	 */
	Blob(std::vector<pixel_type>&& pixels) 
		: m_pixels(std::move(pixels)),
		m_pixelsSorted(false),
		m_areaConvex(0.0),
		m_contoursCalculated(false),
		m_centroidXLocal(0.0),
		m_centroidYLocal(0.0),
		m_centroidXGlobal(0.0),
		m_centroidYGlobal(0.0),
		m_radius(0.0),
		m_radiusGlobal(0.0)
	{}

	/**
	 * Get pixels.
	 */
	const std::vector<pixel_type>& getPixels() const {
		return m_pixels;
	}

	/**
	 * Get first pixel (convenience function).
	 */
	const pixel_type& getFirstPixel() const {
		assert(m_pixels.size());
		return m_pixels[0];
	}

	/**
	 * Set pixels by copying.
	 */
	void setPixels(const std::vector<pixel_type>& pixels = std::vector<pixel_type>())
	{
		m_pixels = pixels;
		m_pixelsSorted = false;
		m_areaConvex = 0.0;
		m_contoursCalculated = false;
		m_centroidXLocal = 0.0;
		m_centroidYLocal = 0.0;
		m_centroidXGlobal = 0.0;
		m_centroidYGlobal = 0.0;
		m_radius = 0.0;
		m_radiusGlobal = 0.0;
	}

	/**
	 * Set pixels by moving.
	 */
	void setPixels(std::vector<pixel_type>&& pixels)
	{
		m_pixels = std::move(pixels);
		m_pixelsSorted = false;
		m_areaConvex = 0.0;
		m_contoursCalculated = false;
		m_centroidXLocal = 0.0;
		m_centroidYLocal = 0.0;
		m_centroidXGlobal = 0.0;
		m_centroidYGlobal = 0.0;
		m_radius = 0.0;
		m_radiusGlobal = 0.0;
	}

	/**
	 * Sort pixels by y, x. Does nothing if they are already sorted.
	 */
	void sortPixels() 
	{
		if(m_pixelsSorted)
			return;
		std::sort(m_pixels.begin(), m_pixels.end(), [](const pixel_type& a, const pixel_type& b){ return a.y < b.y || (a.y == b.y && a.x < b.x); });
		m_pixelsSorted = true;
	}

	/**
	 * Calculate contours.
	 */
	void calcContours()
	{
		if(m_contoursCalculated)
			return;

		// Make sure pixels are sorted
		sortPixels();

		// Bounding box
		QRect boundingBox;
		for(auto it = m_pixels.begin(); it != m_pixels.end(); ++it) {
			if(boundingBox.left() == -1 || boundingBox.left() > it->x)
				boundingBox.setLeft(it->x);
			if(boundingBox.right() < it->x)
				boundingBox.setRight(it->x);

			if(boundingBox.top() == -1 || boundingBox.top() > it->y)
				boundingBox.setTop(it->y);
			if(boundingBox.bottom() < it->y)
				boundingBox.setBottom(it->y);
		}

		// Find contours
		cv::Point offset(boundingBox.left()-1, boundingBox.top()-1);
		cv::Mat tmp(boundingBox.height()+2, boundingBox.width()+2, CV_8UC1, cv::Scalar(0));
		for(auto it = m_pixels.cbegin(); it != m_pixels.cend(); ++it) {
			cv::Point p = *it;
			p -= offset;
			assert(p.x < boundingBox.width()+2 && p.y < boundingBox.height()+2);
			tmp.at<unsigned char>(p) = 1;
		}

		// There should be only one external contour
		std::vector<std::vector<cv::Point>> contours;
		cv::findContours(tmp, contours, CV_RETR_EXTERNAL /*CV_RETR_LIST*/, CV_CHAIN_APPROX_NONE, offset);
		assert(contours.size() == 1);
		m_contours = contours[0];
	}

	/**
	 * Add pixel.
	 */
	void addPixel(const pixel_type& px) 
	{
		m_pixels.push_back(px);
		m_pixelsSorted = false;
		m_areaConvex = 0.0;
		m_contoursCalculated = false;
		m_centroidXLocal = 0.0;
		m_centroidYLocal = 0.0;
		m_centroidXGlobal = 0.0;
		m_centroidYGlobal = 0.0;
		m_radius = 0.0;;
		m_radiusGlobal = 0.0;
	}

	/**
	 * Add pixels.
	 */
	void addPixels(const std::vector<pixel_type>& pixels) 
	{
		m_pixels.insert(m_pixels.end(), pixels.begin(), pixels.end());
		m_pixelsSorted = false;
		m_areaConvex = 0.0;
		m_contoursCalculated = false;
		m_centroidXLocal = 0.0;
		m_centroidYLocal = 0.0;
		m_centroidXGlobal = 0.0;
		m_centroidYGlobal = 0.0;
		m_radius = 0.0;;
		m_radiusGlobal = 0.0;
	}

	/**
	 * Add pixels of other blob. Does not check for duplicate pixels.
	 */
	void addBlob(const Blob& ohter) 
	{
		addPixels(ohter.m_pixels);
	}

	/**
	 * Get area, i.e. number of pixels.
	 */
	int getArea() const 
	{
		return m_pixels.size();
	}

	/**
	 * Get convex area. Returns 0.0 if not set.
	 */
	double getAreaConvex() const
	{
		return m_areaConvex;
	}

	/**
	 * Get if convex area has been calculated.
	 */
	bool areaConvexCalculated() const 
	{
		return m_areaConvex != 0.0;
	}

	/**
	 * Calculate convex area.
	 */
	void calcAreaConvex();

	/**
	 * Count intersecting pixels with other blob.
	 */
	int countIntersectingPixels(Blob& other)
	{
		// Make sure pixels are sorted
		sortPixels();
		other.sortPixels();

		// Count intersecting pixels
		auto itMe = m_pixels.cbegin();
		auto itO = other.m_pixels.cbegin();
		int intersect = 0;
		while(itMe != m_pixels.cend() && itO != other.m_pixels.cend()) {
			if(*itMe == *itO) {
				++itMe;
				++itO;
				++intersect;
			}
			else if(itMe->y < itO->y || (itMe->y == itO->y && itMe->x < itO->x))
				++itMe;
			else 
				++itO;
		}

		return intersect;
	}

	/**
	 * @return if pixels are sorted.
	 */
	bool pixelsSorted() const 
	{
		return m_pixelsSorted;
	}

	/**
	 * @return if contours have been calculated.
	 */
	bool contourCalculated() const
	{
		return m_contoursCalculated;
	}

	/**
	 * Calculate distance of point to contour of this blob.
	 */
	double calcDistanceToContour(const cv::Point& p)
	{
		// Make sure contours have been calculated
		calcContours();

		// Get minimum distance to all pixels of the contour of this blob
		double minDist = -1;
		for(auto it = m_contours.begin(); it != m_contours.end(); ++it) {
			double d1 = it->x - p.x;
			double d2 = it->y - p.y;
			double d = std::sqrt(d1*d1 + d2*d2);

			if(minDist < 0 || d < minDist)
				minDist = d;
		}

		return minDist;
	}

	/**
	 * Get global centroid. Can be 0.0 if not set or wrong if not set correctly.
	 */
	void getCentroidGlobal(double& centroidX, double& centroidY) const
	{
		centroidX = m_centroidXGlobal;
		centroidY = m_centroidYGlobal;
	}

	/**
	 * Set global centroid.
	 */
	void setCentroidGlobal(double centroidX, double centroidY)
	{
		m_centroidXGlobal = centroidX;
		m_centroidYGlobal = centroidY;
	}

	/**
	 * Get local centroid. Can be 0.0 if not set.
	 */
	void getCentroidLocal(double& centroidX, double& centroidY) const
	{
		centroidX = m_centroidXLocal;
		centroidY = m_centroidYLocal;
	}

	/**
	 * Calculate image-local centroid.
	 */
	void calcCentroidLocal()
	{
		int sumX = 0, sumY = 0;
		for(auto it = m_pixels.begin(); it != m_pixels.end(); ++it) {
			sumX += it->x;
			sumY += it->y;
		}
		m_centroidXLocal = sumX / m_pixels.size();
		m_centroidYLocal = sumY / m_pixels.size();
	}

	/**
	 * Calculate radius.
	 */
	void calcRadius()
	{
		m_radius = sqrt(getArea() / M_PI);
	}

	/**
	 * Get radius. Can be 0.0 if not calculated.
	 */
	double getRadius() const
	{
		return m_radius;
	}

	/**
	 * Get radius in micrometers. Can be 0.0 if not set or invalid.
	 */
	double getRadiusGlobal() const
	{
		return m_radiusGlobal;
	}

	/**
	 * Set global radius.
	 */
	void setRadiusGlobal(double radius)
	{
		m_radiusGlobal = radius;
	}

	/**
	 * Calculate global distance to other blob. Assumes that global centroids are set correctly.
	 */
	double calcGlobalDistanceTo(const Blob& other) const 
	{
		double d1 = m_centroidXGlobal - other.m_centroidXGlobal;
		double d2 = m_centroidYGlobal - other.m_centroidYGlobal;
		return sqrt(d1*d1 + d2*d2);
	}

	/**
	 * Calculate global distance to global point. Assumes that global centroid is set correctly.
	 */
	double calcGlobalDistanceTo(double x, double y) const 
	{
		double d1 = m_centroidXGlobal - x;
		double d2 = m_centroidYGlobal - y;
		return sqrt(d1*d1 + d2*d2);
	}

	/**
	 * Access additional data.
	 */
	QVector<double>& getAdditionalData() 
	{
		return m_additionalData;
	}
	const QVector<double>& getAdditionalData() const
	{
		return m_additionalData;
	}
	const QVector<double>& getAdditionalDataConst() const 
	{
		return m_additionalData;
	}
	void setAdditionalData(const QVector<double>& additionalData)
	{
		m_additionalData = additionalData;
	}
	void setAdditionalData(const QVector<double>&& additionalData)
	{
		m_additionalData = std::move(additionalData);
	}

private:

	// Pixels
	std::vector<pixel_type> m_pixels;

	// If pixels are sorted
	bool m_pixelsSorted;

	// Convex area. Not calculated automatically.
	double m_areaConvex;

	// Contours of blob
	std::vector<cv::Point> m_contours;
	bool m_contoursCalculated;

	// Centroid of blob (local coordinates)
	double m_centroidXLocal;
	double m_centroidYLocal;

	// Centroid of blob (global coordinates)
	double m_centroidXGlobal;
	double m_centroidYGlobal;

	// Radius in pixels of blob (i.e. radius of circle with same size)
	double m_radius;

	// Radius in micrometers of blob
	double m_radiusGlobal;

	// Additional data (e.g. quantification)
	QVector<double> m_additionalData;

	friend QDataStream &operator<<(QDataStream& stream, const Blob& b);
	friend QDataStream &operator>>(QDataStream& stream, Blob& b);
};



/**
* Write blob to QDataStream.
*/
inline QDataStream &operator<<(QDataStream& stream, const Blob& b)
{
	// Magic number + version
	stream << (quint32)0xD45CC4F3;
	stream << (qint32)1;

	// Data
	stream << (quint32)b.m_pixels.size();
	for(auto it = b.m_pixels.begin(); it != b.m_pixels.end(); ++it) {
		stream << (quint32)it->x;
		stream << (quint32)it->y;
	}
	return stream;
}

/**
* Read blob from QDataStream.
*/
inline QDataStream &operator>>(QDataStream& stream, Blob& b)
{
	// Read magic number + version
	quint32 magic, version;
	stream >> magic;
	stream >> version;
	if(magic != (quint32)0xD45CC4F3 || version > 1) {
		throw ExceptionBase(__FUNCTION__, QString("File is invalid or version too new (magic: %1, version: %2).").arg(magic).arg(version));
	}

	// Reset blob
	b.m_pixelsSorted = false;
	b.m_areaConvex = 0;
	b.m_contours.clear();
	b.m_contoursCalculated = false;

	// Read data
	quint32 numPixels;
	stream >> numPixels;
	b.m_pixels.resize(numPixels);
	for(int i = 0; i < numPixels; ++i) {
		quint32 x, y;
		stream >> x >> y;
		b.m_pixels[i].x = x;
		b.m_pixels[i].y = y;
	}
	return stream;
}

#endif // blob_h__