/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef imageview_h__
#define imageview_h__

#include <QGraphicsView>
#include <QFont>

class ImageViewImageItem;


class ImageView : public QGraphicsView {

	Q_OBJECT

public:

	/**
	 * Constructor. Creates scene and ImageViewImageItem.
	 */
	ImageView(QWidget* parent = 0);

	/**
	 * Destructor. Deletes scene.
	 */
	~ImageView();

	/**
	 * Get if painting mode is on.
	 */
	bool getPaintable() const 
	{
		return m_allowPainting;
	}

	/**
	 * Get ImageViewImageItem.
	 */
	ImageViewImageItem* getImageViewItem() const 
	{
		return m_imageItem;
	}

	/**
	 * Get scroll position. Position refers to scene position at upper left corner.
	 */
	QPointF getCurrentScrollPosition() const 
	{
		return mapToScene(0.0f, 0.0f);
	}

	/**
	 * Get current zoom factor.
	 */
	float getCurrentZoom() const {
		return m_scaling;
	}
	

public slots:

	/**
	 * Change zoom.
	 * @param scaling new zoom, e.g. 1.0 for 100%.
	 * @param upperLeftPositionScene refers to scene position at upper left corner after zoom has changed.
	 * @param emitSignal if zoomChanged() should be emited.
	 */
	void setZoom(float scaling, QPointF upperLeftPositionScene, bool emitSignal = false);

	/**
	 * Set if paintable.
	 */
	void setPaintable(bool paintable);

	/**
	 * Set scroll position. Position refers to scene position at upper left corner.
	 */
	void setScrollPosition(QPointF upperLeftPositionScene, bool emitSignal = false);
	

	/**
	 * Display brush cursor from other ImageView that is paintable.
	 */
	void displayBrushCursor(int posX, int posY, QGraphicsPixmapItem* brush);

	/**
	 * Show a message (on top of current image) or hide it.
	 */
	void showMessage(const QString& what, bool whatIsHtml);
	void hideMessage();

signals:
	
	/**
	 * User finished painting action.
	 */
	void markingFinished();

	/**
	 * Brush cursor position changed.
	 */
	void brushPositionChanged(int posX, int posY, QGraphicsPixmapItem* brush);

	/**
	 *	Mouse position changed.
	 */
	void mousePositionChanged(int posX, int posY, int grayLevel);

	/**
	 * Zoom changed.
	 * @param scaling new zoom, e.g. 1.0 for 100%.
	 * @param upperLeftPositionScene refers to scene position at upper left corner after zoom has changed.
	 */
	void zoomChanged(float scaling, QPointF upperLeftPositionScene);

	/**
	 * Scrolling changed.
	 */
	void scrollingChanged(QPointF upperLeftPositionScene);

protected:

	/**
	 * Reimplemented for zoom.
	 */
	void wheelEvent(QWheelEvent *event);

	/**
	 * Reimplemented for scrolling with right mouse button.
	 */
	void mousePressEvent( QMouseEvent *event );
	void mouseReleaseEvent( QMouseEvent *event );
	void mouseMoveEvent ( QMouseEvent *event );

	/**
	 * Reimplemented.
	 */
	void scrollContentsBy(int dx, int dy);

	/**
	 * Reimplemented.
	 */
	void resizeEvent(QResizeEvent *event);

private:

	/**
	 * Get painting cursor depending on brush size, zoom...
	 */
	//static QCursor createBrushCursor (int diameter, QColor color, float scaling);

	// Update message item position
	void upatePositionOfMessageItem();

	// Associated ImageViewImageItem
	ImageViewImageItem* m_imageItem; 

	// Zoom and scroll position (upper left point in scene coordinates)
	float m_scaling;

	// Mouse move start positions
	int m_mouseMoveStartX, m_mouseMoveStartY;
	bool m_scrolling;

	// If user can paint on this view
	bool m_allowPainting;

	// Ignore (i.e. emit no signal) scrolling events (true during zooming)
	bool m_ignoreScrollEvents;

	// Message item 
	QGraphicsTextItem* m_messageItem;

	// Scroll position and scaling to apply when message is hidden
	QPointF m_scrollPositionOnMessageHide;
	float m_scalingOnMessageHide;

	static const float MIN_SCALING;
	static const float MAX_SCALING;

	friend class ImageViewImageItem;
};

#endif // imageview_h__
