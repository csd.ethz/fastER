/**
 Copyright (c) 2017 ETH Zurich, 2013-2017 Oliver Hilsenbeck
  
 This file is part of fastER.
  
 fastER is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmhelp.h"

// Project
#include "settings.h"

// Qt
#include <QSettings>

FrmHelp::FrmHelp(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	// Adjust window style
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint /*| Qt::WindowStaysOnTopHint*/) & (~Qt::WindowContextHelpButtonHint));

	// Init ui
	QSettings settings;
	m_ui.chkShowHelpAtStartup->setChecked(settings.value(Settings::KEY_SHOWHELPATSTART, true).toBool());

	// Signals/Slots
	connect(m_ui.pbtOk, SIGNAL(clicked()), this, SLOT(okClicked()));
}

void FrmHelp::okClicked()
{
	// Save checkbox state
	QSettings settings;
	settings.setValue(Settings::KEY_SHOWHELPATSTART, m_ui.chkShowHelpAtStartup->isChecked());

	// Close window
	close();
}

